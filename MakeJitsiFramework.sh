#####-CONFIGURATION BUILD-####
JITSI_SDK_HOME=/Users/neolabx/Desktop/Projects/Calling/JitsiMeetSDK
CALLING_PROJECT_HOME=/Users/neolabx/Desktop/Projects/Calling/IOSCallingTalk 

echo "Please confirm your project settings:"
echo "-----------------------*****-----------------------------"
echo "----- iOS Project located: $CALLING_PROJECT_HOME"
echo "----- Jitsi SDK located: $JITSI_SDK_HOME"
echo "-----------------------*****-----------------------------"
read -p "Press any key to continue..."
RELEASE_FRAMEWORK_JITSI="$JITSI_SDK_HOME/ios/sdk/JitsiMeet.framework"
PROJECT_FRAMEWORK="$CALLING_PROJECT_HOME/JitsiMeet.framework"
#####-EXCUTE-BUILD-####
# Goto jitsi folder
cd $JITSI_SDK_HOME
# Make build
xcodebuild -workspace ios/jitsi-meet.xcworkspace -scheme JitsiMeet -destination='generic/platform=iOS' -configuration Release archive
# Copy frameWork to project folder
rm $PROJECT_FRAMEWORK && cp -r $RELEASE_FRAMEWORK_JITSI $PROJECT_FRAMEWORK
