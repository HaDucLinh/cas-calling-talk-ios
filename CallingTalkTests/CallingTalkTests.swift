//
//  CallingTalkTests.swift
//  CallingTalkTests
//
//  Created by Van Trung on 1/15/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import XCTest
import Quick
import KIF_Quick
@testable import CallingTalk

class CallingTalkTests: KIFSpec {
    let avatarButtonID = "avatarButton"
    override func spec() {
        describe("Login Sceen Test") {
            context("LoginScreen test avatar function") {
                context("Press avatar button and tap outside") {
                    beforeEach {
                        tester().waitForView(withAccessibilityLabel: self.avatarButtonID)
                        tester().tapView(withAccessibilityLabel: self.avatarButtonID)
                    }
                    it("should show avatar pick screen and close if touch outside") {
                        tester().waitForView(withAccessibilityLabel: "Please select an icon")
                    }
                    afterEach {
                        tester().tapScreen(at: CGPoint(x: 0, y: 0))
                    }
                }
                context("Select and avatar from avatar set") {
                    beforeEach {
                        tester().waitForView(withAccessibilityLabel: self.avatarButtonID)
                        tester().tapView(withAccessibilityLabel: self.avatarButtonID)
                        tester().waitForView(withAccessibilityLabel: "Please select an icon")
                        tester().tapView(withAccessibilityLabel: self.avatarButtonID)
                    }
                    it("should show avatar pick screen") {
                        
                    }
                    afterEach {
                        tester().tapScreen(at: CGPoint(x: 0, y: 0))
                    }
                }
            }
        }
    }
}
