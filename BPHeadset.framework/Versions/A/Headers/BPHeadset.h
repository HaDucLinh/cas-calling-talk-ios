//
//  BPHeadset.h
//
//  Copyright (c) 2017 Fenzar Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>

#define kBP_PARROTTBUTTON_SERVICE_UUID_STRING              @"95665a00-8704-11e5-960c-0002a5d5c51b"

/*!
 *  The headset button that was pressed. (More buttons may be added in the future)
 */
typedef NS_ENUM(NSInteger, BPButtonID) {
    /*!
     *  The Parrott Button
     */
    BPButtonIDParrot                       = 1
};

/*!
 *  Whether the headset is currently bondable.
 */
typedef NS_ENUM(NSInteger, VXIBondable) {
    /*!
     *  An unknown status
     */
    VXIBondableUnknown         = -1,
    /*!
     *  Bonding is currently disabled
     */
    VXIBondableDisabled        = 0,
    /*!
     *  Bonding is currently enabled
     */
    VXIBondableEnabled         = 1
};


/*!
 *  The resonCode provided to the onModeUpdateFailure callback on BPHeadsetListener
 */
typedef NS_ENUM(NSInteger, BPModeUpdateError) {
    /*!
     *  An unknown error
     */
    BPModeUpdateErrorUnknown              = 0,
    /*!
     *  Device was disconnected
     */
    BPModeUpdateErrorDisconnected         = 1,
};


/*!
 *  The resonCode provided to the onConnectFailure callback on BPHeadsetListener
 */
typedef NS_ENUM(NSInteger, BPConnectError) {
    /*!
     *  An unknown error
     */
    BPConnectErrorUnknown              = 0,
    /*!
     *  Bluetooth is disabled
     */
    BPConnectErrorBluetoothDisabled    = 1,
    /*!
     *  Incompatible firmware version
     */
    BPConnectErrorFirmwareTooOld       = 2,
    /*!
     *  Incompatible app version
     */
    BPConnectErrorSDKTooOld            = 3
};


/*!
 *  The status provided to the onConnectProgress callback on BPHeadsetListener
 */
typedef NS_ENUM(NSInteger, BPConnectProgress) {
    /*!
     *  Started connecting to the headset
     */
    BPConnectProgressStarted             = 0,
    /*!
     *  Scanning for device
     */
    BPConnectProgressScanning            = 1,
    /*!
     *  Device Found, but not yet connected
     */
    BPConnectProgressFound               = 2,
    /*!
     *  Reading current settings from headset
     */
    BPConnectProgressReading             = 3
};


/*!
 *  Notifies the application when there are headset events (connect/disconnect, button presses etc.) All events are optional so the application only needs to implement the methods for the events it wants to receive.
 */
@protocol BPHeadsetListener <NSObject>

@optional

/*!
 *  Status updates during the connection process.
 *
 *  @param status The current status of the headset.
 */
- (void) onConnectProgress:(BPConnectProgress) status;

/*!
 *  The headset has connected.
 */
- (void) onConnect;

/*!
 *  The headset failed to connect e.g. headset in wrong mode (mute, speed-dial, 'another app')
 *
 *  @param resonCode The error generated while trying to connect.
 */
- (void) onConnectFailure:(BPConnectError) resonCode;

/*!
 *  The headset has disconnected.
 */
- (void) onDisconnect;

/*!
 *  The attempt to set the mode of the headset succeeded.
 */
- (void) onModeUpdate;


/*!
 *  The attempt to set the mode of the headset failed.
 *
 *  @param resonCode The error generated while trying to set the mode.
 */
- (void) onModeUpdateFailure:(BPModeUpdateError) resonCode;

/*!
 *  The Parrott button was tapped once.
 *
 *  @param buttonID The ID of the button that was tapped.
 */
- (void) onTap:(BPButtonID) buttonID;

/*!
 *  The Parrott button was double-tapped.
 *
 *  @param buttonID The ID of the button that was double tapped.
 */
- (void) onDoubleTap:(BPButtonID) buttonID;

/*!
 *  The Parrott button was pressed down.
 *
 *  @param buttonID The ID of the button that was pressed.
 */
- (void) onButtonDown:(BPButtonID) buttonID;

/*!
 *  The Parrott button was released.
 *
 *  @param buttonID The ID of the button that was released.
 */
- (void) onButtonUp:(BPButtonID) buttonID;

/*!
 *  The Parrott button was long pressed.
 *
 *  @param buttonID The ID of the button that was long pressed.
 */
- (void) onLongPress:(BPButtonID) buttonID;


@end


/*!
 The main interface to the BlueParrott Headset.
 
 To instantiate, just use `sharedInstance`:
 
    BPHeadset *headset = [BPHeadset sharedInstance];
 
 To connect, set a listener that conforms to the [BPHeadsetListener](BPHeadsetListener) protocol, then call connect:

    // ViewController.h
    @interface ViewController : UIViewController <BPHeadsetListener>
    // ...
    @end
 
    // ViewController.m
    @implementation ViewController
    - (void)viewDidLoad {
        [super viewDidLoad];
        headset = [BPHeadset sharedInstance];
        [headset addListener:self];
    }
 
    - (IBAction)connectButtonTouched:(id)sender {
        [headset connect];
    }
    @end
 
 
 Then simply implement the methods from [BPHeadsetListener](BPHeadsetListener) that you are interested in:
 
    @implementation ViewController
    // ...
 
    #pragma mark BPHeadsetListener methods
    - (void) onConnect {
        // Update UI
     
        // Enable SDK Mode
        [headset enableSDKMode];
    }
 
    - (void) onConnectFailure:(BPConnectError) resonCode {
        // Inform the user
    }
 
    - (void) onDisconnect {
        // e.g. headset was powered off
    }
 
    - (void) onButtonDown:(BPButtonID) buttonID {
        // Button was pushed down, start recording...
    }
 
    - (void) onButtonUp:(BPButtonID) buttonID {
        // Button was released, stop recording...
    }
 
    @end
 
 */
@interface BPHeadset : NSObject

/*!
 *  Get an instance of the BPHeadset class
 *
 *  @return An instance of BPHeadset, or nil if running in the simulator
 */
+ (instancetype)sharedInstance;

/*!
 *  Indicates if a headset is currently connected
 */
@property (nonatomic, readonly) BOOL connected;

/*!
 *  Indicates if the headset is currently in SDK Mode
 */
@property (nonatomic, readonly) BOOL sdkModeEnabled;

/*!
 *  Indicates if the headset is currently bondable
 */
@property (nonatomic, readonly) VXIBondable bondableEnabled;

/*!
 *  Returns the version of the SDK
 */
@property (nonatomic, readonly) NSString *version;

/*!
 *  Connect to the headset
 *
 *  @return A BOOL indicating if the class can begin connecting. Will return NO if Bluetooth is powered off.
 */
- (void) connect;

/*!
 *  Disconnect from the headset
 */
- (void) disconnect;


/*!
 *  Add a listener which will respond to headset button events
 *
 *  @param listener The listener object to send disconnect and button events to.
 */
- (void) addListener:(id<BPHeadsetListener>) listener;

/*!
 *  Remove an already added listener. Will not generate an error if the listener wasn't added.
 *
 *  @param listener The listener object remove.
 */
- (void) removeListener:(id<BPHeadsetListener>) listener;

/*!
 *  Enables SDK mode on the headset, allowing the current app to receive button events.
 */
- (void) enableSDKMode;

/*!
 *  Disables SDK mode on the headset, returning it to default (mute-on-call) mode.
 */
- (void) disableSDKMode;

/*!
 *  Enables Bondable on the headset, allowing the the headset to bond to the
 *  phone so that it automatically reconnects when within range.
 *  After changing this value, the headset needs to be restarted.
 */
- (void) enableBondable;

/*!
 *  Disables Bondable on the headset.
 *  After changing this value, the headset needs to be restarted.
 */
- (void) disableBondable;


@end
