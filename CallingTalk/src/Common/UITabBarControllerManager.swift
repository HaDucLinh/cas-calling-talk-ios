//
//  UITabbarControllerManager.swift
//  CallingTalk
//
//  Created by Chinh Le on 5/29/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit

class UITabBarControllerManager: NSObject {
    static let shared = UITabBarControllerManager()
    fileprivate var mainTabBarController: UITabBarController!
    fileprivate var isAdmin: Bool!
    
    // Badge for Talk tab
    fileprivate var joinTalkInvitationsCount: Int = 0
    fileprivate var hasUnreadMessages: Bool = false
    fileprivate var unreadMentionedMessagesCount: Int = 0
    
    func mainTabBarController(isAdmin: Bool) -> UITabBarController {
        let mainNav = UINavigationController(rootViewController: MainVC())
        mainNav.navigationBar.setupNavigationBarWithColor(.appTheme)
        mainNav.tabBarItem = UITabBarItem(title: NSLocalizedString("main_item_tab_bar_title", comment: ""),
                                          image: UIImage(named: "ic_phone"),
                                          tag: 0)
        
        let memberNav = UINavigationController(rootViewController: MemberVC())
        memberNav.navigationBar.setupNavigationBarWithColor(.appTheme)
        memberNav.tabBarItem = UITabBarItem(title: NSLocalizedString("member_item_tab_bar_title", comment: ""),
                                            image: UIImage(named: "ic_member"),
                                            tag: 1)
        
        let settingsNav = UINavigationController(rootViewController: SettingsVC())
        settingsNav.navigationBar.setupNavigationBarWithColor()
        settingsNav.tabBarItem = UITabBarItem(title: NSLocalizedString("settings_item_tab_bar_title", comment: ""),
                                              image: UIImage(named: "ic_settings"),
                                              tag: 2)
        
        // Set font for TabBarItem
        UITabBarItem.appearance().setTitleTextAttributes([.font: UIFont.ct.font(.NotoSansCJKjp, type: .bold, size: 14.0)], for: .normal)
        UITabBarItem.appearance().badgeColor = UIColor.ct.badgeBackgroundColor
        
        UITabBarItem.appearance().setBadgeTextAttributes([
            .font: UIFont.ct.font(.NotoSansCJKjp, type: .bold, size: 9.0),
            .foregroundColor: UIColor.white
            ], for: .selected)
        UITabBarItem.appearance().setBadgeTextAttributes([
            .font: UIFont.ct.font(.NotoSansCJKjp, type: .bold, size: 9.0),
            .foregroundColor: UIColor.white
            ], for: .normal)
        
        let mainTabBarController = UITabBarController()
        // setup tab bar
        mainTabBarController.tabBar.tintColor = UIColor.ct.selectItemColor
        mainTabBarController.tabBar.unselectedItemTintColor = UIColor.ct.unselectedItemColor
        mainTabBarController.tabBar.barTintColor = UIColor.ct.bottomTabbarColor
        mainTabBarController.tabBar.isTranslucent = false
        mainTabBarController.viewControllers = isAdmin ? [mainNav, settingsNav] : [mainNav, settingsNav]
        // isAdmin ? [mainNav, memberNav, settingsNav] : [mainNav, settingsNav]
        
        self.isAdmin = isAdmin
        self.mainTabBarController = mainTabBarController
        
        getNotifications()
        
        return mainTabBarController
    }
    
    fileprivate func updateTalkBadge() {
        mainTabBarController.viewControllers?.first?.tabBarItem.badgeValue = badgeStringForTalkTab()
    }
    
    fileprivate func badgeStringForTalkTab() -> String? {
        if unreadMentionedMessagesCount > 0 {
            return (unreadMentionedMessagesCount <= kMaxBadgeNumberOfNotifications) ? String(unreadMentionedMessagesCount) : "\(kMaxBadgeNumberOfNotifications)+"
        }
        
        if hasUnreadMessages || joinTalkInvitationsCount > 0 {
            return "!"
        }
        
        return nil
    }
    
    func handleNewMessage(isMentionedMessage: Bool) {
        if isMentionedMessage {
            unreadMentionedMessagesCount += 1
        } else {
            hasUnreadMessages = true
        }
        updateTalkBadge()
    }
    
    func handleNewInvitation() {
        joinTalkInvitationsCount += 1
        updateTalkBadge()
    }
    
    func getNotifications() {
        API.getNotifications() { [weak self] (result) in
            guard let `self` = self else { return }
            switch result {
            case .success(let data):
                guard let dataDict = data as? Dictionary<String, Any> else { return }
                self.joinTalkInvitationsCount = dataDict["count_invitation"] as! Int
                self.unreadMentionedMessagesCount = dataDict["count_mention_message"] as! Int
                self.hasUnreadMessages = dataDict["has_unread_message"] as! Bool
                self.updateTalkBadge()
            case .failure(_): break
            }
        }
    }
}
