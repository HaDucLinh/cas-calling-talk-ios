//
//  SocketIOManager.swift
//  CallingTalk
//
//  Created by Chinh Le on 3/21/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit
import SocketIO
import ObjectMapper

enum SocketEvent: String {
    case NewMessage = "NEW_MESSAGE"
    case JoinedChannel = "JOINED_CHANNEL"
    case LeftChannel = "LEFT_CHANNEL"
    case LeftTalkGroup = "LEFT_TALK_GROUP"
    case MarkMessageAsRead = "READER_MESSAGE"
    case DeleteOrganization = "DELETE_ORGANIZATION"
    case KickedFromChannel = "KICK_FROM_CHANNEL"
    case UpdateOrganization = "UPDATED_ORGANIZATION"
    case DeletedChannel = "DELETED_CHANNEL"
}
extension Notification.Name {
    public static let ReconnectSocket = Notification.Name("ReconnectSocket")
    public static let DisconnectSocket = Notification.Name("DisconnectSocket")
}
class SocketIOManager: NSObject {
    static let sharedInstance = SocketIOManager()
    var e : Echo?
    var socketConnected: Bool = false
    
    func connectSocket() {
        if let token = UserDefaults.getToken() {
            // Connect to socket with an `Access-token` in header
            let options: [String : Any] = ["host": kCallingChatDomain,
                                           "auth": ["headers": ["Access-token": token]]]
            e = Echo(options: options)
    
            e?.connected() { [weak self ]data, ack in
                guard let self = self else { return }
                /*
                 After the socket connected and echo existed
                 We need to registering by dependency inject in order to use the function
                 */
                if let concreteEcho = self.e {
                    ServicesFacade.registerBroadcastServices(broadcast: SocketIOBroadcast(andEcho: concreteEcho))
                } else {
                    print("[Socket]can not create echo, terminate the broadcast function....")
                }
                self.socketConnected = true
                NotificationCenter.default.post(name: Notification.Name.ReconnectSocket, object: nil, userInfo: nil)
                if let user = User.getProfile() {
                    let channelName = String(format: "user.%i", user.id)
                    
                    // Leave in advance to ensure the old events are subscribed
                    // Then join to private channel with name format `user.userId`
                    // and listen to events
                    self.e?.leave(channel: channelName)
                    
                    let channel = self.e?.privateChannel(channel: channelName)
                    _ = channel!.listen(event: SocketEvent.NewMessage.rawValue, callback: { data, ack in
                        self.handleNewMessage(data: data)
                    })
                    _ = channel!.listen(event: SocketEvent.JoinedChannel.rawValue, callback: { data, ack in
                        self.handleJoinOrLeaveChannel(data: data, socketEvent: SocketEvent.JoinedChannel)
                    })
                    _ = channel!.listen(event: SocketEvent.LeftChannel.rawValue, callback: { data, ack in
                        self.handleJoinOrLeaveChannel(data: data, socketEvent: SocketEvent.LeftChannel)
                    })
                    _ = channel!.listen(event: SocketEvent.LeftTalkGroup.rawValue, callback: { data, ack in
                        self.handleJoinOrLeaveChannel(data: data, socketEvent: SocketEvent.LeftTalkGroup)
                    })
                    _ = channel!.listen(event: SocketEvent.MarkMessageAsRead.rawValue, callback: { data, ack in
                        self.handleMarkMessageAsRead(data: data)
                    })
                    _ = channel!.listen(event: SocketEvent.KickedFromChannel.rawValue, callback: { data, ack in
                        self.kickedFromChannel(data: data)
                    })
                    
                    _ = channel!.listen(event: SocketEvent.UpdateOrganization.rawValue, callback: { data, ack in
                        self.handleUpdateOrganization(data: data)
                    })
                    
                    _ = channel!.listen(event: SocketEvent.DeletedChannel.rawValue, callback: { data, ack in
                        self.handleForDeleteChannel(data: data)
                    })
                    
                    // Subscribe organizations
                    self.subscribeOrganizations(Array(user.organizations))
                }
            }
        }
    }
    
    private func handleJoinOrLeaveChannel(data: [Any], socketEvent: SocketEvent) {
        if data.count >= 2 {
            if let jsonDict = data[1] as? Dictionary<String, AnyObject> {
                if let dataDict = jsonDict["data"] as? Dictionary<String, AnyObject> {
                    if let channelId = dataDict["channel_id"] as? Int {
                        let userId = dataDict["user_id"] as? Int
                        let numberOfCalling = dataDict["in_calling"] as? Int
                        let myDict = ["channelId": channelId, "userId": userId ?? -1, "inCalling": numberOfCalling ?? 0, "socketEvent": socketEvent] as [String : Any]
                        
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: kMemberJoinOrLeaveTalkEvent), object: myDict)
                    }
                }
            }
        }
    }
    
    private func handleNewMessage(data: [Any]) {
        if data.count >= 2 {
            if let dataDict = data[1] as? Dictionary<String, AnyObject> {
                if let messageDict = dataDict["message"] as? Dictionary<String, AnyObject> {
                    if let message = Mapper<Message>().map(JSON: messageDict), let userId = User.getProfile()?.id {
                        // Update new unread message on tabBar
                        if message.owner?.id != userId {
                            let isMentionedMessage = (Array(message.mentionIds).contains(userId) || message.isMentionAll)
                            UITabBarControllerManager.shared.handleNewMessage(isMentionedMessage: isMentionedMessage)
                        }
                    }
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: kNewMessageEvent), object: messageDict)
                }
            }
        }
    }
    
    private func handleMarkMessageAsRead(data: [Any]) {
        // Get the audio url and play
        if data.count >= 2 {
            if let dataDict = data[1] as? Dictionary<String, AnyObject> {
                print("Data dict: ", dataDict)
                guard let channelId = dataDict["channelId"] as? Int else { return }
                guard let userDict = dataDict["user"] as? JSObject, let userId = userDict["id"] as? Int else { return
                }
                // Guard that the current user doesn't appear in seenUser list
                if let currentUser = User.getProfile() {
                    if userId == currentUser.id { return }
                }
                guard let messageId = dataDict["message"]?["id"] as? Int else { return }
                let previousReadMessageId = dataDict["prevReadMessageId"] as? Int
                let myDict = ["channelId": channelId, "user": userDict, "messageId": messageId, "previousReadMessageId": previousReadMessageId ?? -1] as [String : Any]
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: kMarkMessageAsReadEvent), object: myDict)
            }
        }
    }
    
    private func handleDeleteOrganization(data: [Any]) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: kDeleteOrganizationEvent), object: nil)
    }
    
    private func kickedFromChannel(data: [Any]) {
        if data.count >= 2 {
            if let dataDict = data[1] as? Dictionary<String, AnyObject> {
                print("Kick Message: ", dataDict)
                guard let channelDict = dataDict["data"] as? JSObject,
                      let channelId = channelDict["channel_id"] as? Int,
                    let userId = channelDict["user_id"] as? Int else { return }
                
                UITabBarControllerManager.shared.getNotifications()
                
                if let mainVC = UIApplication.mainViewController() {
                    mainVC.deleteMemberInTalkgroup(talkID: channelId, userId: userId)
                }
                
                let kickedDict = ["channelId": channelId, "userId": userId] as JSObject
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: kKickedFromChannelEvent), object: kickedDict)
            }
        }
    }
    
    private func handleUpdateOrganization(data: [Any]) {
        // Update my profile
        reloadProfile()
        
        if data.count >= 2 {
            if let dataDict = data[1] as? Dictionary<String, AnyObject> {
                guard let organizationsDict = dataDict["data"] as? JSObject,
                    let organizationIds = organizationsDict["organization_ids"] as? [Int] else { return }
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: kUpdateOrganizationsEvent), object: ["organizationIds": organizationIds])
            }
        }
    }
    
    private func reloadProfile() {
        API.getMyProfile { (result) in
            switch result {
            case .success(let profileData):
                guard let profile = profileData as? User else { return }
                
                // Save user's profile & token id
                Helpers.saveProfile(profile)
            case .failure(_): break
            }
        }
    }
    
    func disconnectSocket() {
        e?.disconnect()
        e = nil
        NotificationCenter.default.post(name: Notification.Name.DisconnectSocket, object: nil, userInfo: nil)
        self.socketConnected = false
    }
    
    func subscribeOrganizations(_ organizations: [Organization]) {
        if let e = self.e {
            let organizationNames = organizations.map({ String(format: "organization.%i", $0.id) })
            for organizationName in organizationNames {
                e.leave(channel: organizationName)
                let organiztion = e.privateChannel(channel: organizationName)
                _ = organiztion.listen(event: SocketEvent.DeleteOrganization.rawValue, callback: { data, ack in
                    self.handleDeleteOrganization(data: data)
                })
            }
        }
    }
    
    private func handleForDeleteChannel(data: [Any]) {
        
        if data.count >= 2 {
            if let dataDict = data[1] as? Dictionary<String, AnyObject> {
                
                guard let channelDict = dataDict["data"] as? JSObject,
                    let channelId = channelDict["channel_id"] as? Int,
                    let channelName = channelDict["channel_name"] as? String else {
                        return
                }
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: kDeleteChannelEvent), object: ["channelId": channelId, "channelName": channelName])
            }
        }
    }
}
