//
//  GlobalVariable.swift
//  CallingTalk
//
//  Created by Van Trung on 1/15/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit

struct GlobalVariable {
    static var access_token = ""
    static var ratioHeight = UIScreen.main.bounds.height / 667
    static var ratioWidth = UIScreen.main.bounds.width / 375
    static let screenWidth = UIScreen.main.bounds.width
    static let screenHeight = UIScreen.main.bounds.height
    static var didComeToIncomingVC = false
}

public enum DeviceType {
    case iPhone4
    case iPhone5
    case iPhone6
    case iPhone6p
    case iPhoneX
    case iPhoneXS
    case iPhoneXR
    case iPhoneXSMax
    case iPad
    case iPadPro105
    case iPadPro129
    
    public var size: CGSize {
        switch self {
        case .iPhone4: return CGSize(width: 320, height: 480)
        case .iPhone5: return CGSize(width: 320, height: 568)
        case .iPhone6: return CGSize(width: 375, height: 667)
        case .iPhone6p: return CGSize(width: 414, height: 736)
        case .iPhoneX: return CGSize(width: 375, height: 812)
        case .iPhoneXS: return CGSize(width: 375, height: 812)
        case .iPhoneXR: return CGSize(width: 414, height: 896)
        case .iPhoneXSMax: return CGSize(width: 414, height: 896)
        case .iPad: return CGSize(width: 768, height: 1024)
        case .iPadPro105: return CGSize(width: 834, height: 1112)
        case .iPadPro129: return CGSize(width: 1024, height: 1366)
        }
    }
}

public let kScreenSize = UIScreen.main.bounds.size

public let iPhone = (UIDevice.current.userInterfaceIdiom == .phone)
public let iPad = (UIDevice.current.userInterfaceIdiom == .pad)

public let iPhone4 = (iPhone && DeviceType.iPhone4.size == kScreenSize)
public let iPhone5 = (iPhone && DeviceType.iPhone5.size == kScreenSize)
public let iPhone6 = (iPhone && DeviceType.iPhone6.size == kScreenSize)
public let iPhone6p = (iPhone && DeviceType.iPhone6p.size == kScreenSize)
public let iPhoneX = (iPhone && DeviceType.iPhoneX.size == kScreenSize)
public let iPhoneXS = (iPhone && DeviceType.iPhoneXS.size == kScreenSize)
public let iPhoneXR = (iPhone && DeviceType.iPhoneXR.size == kScreenSize)
public let iPhoneXSMax = (iPhone && DeviceType.iPhoneXSMax.size == kScreenSize)


//public let kCallingChatDomain = "https://talk-chat.dev.calling.fun" // Dev
//public let jitsiDomain = "https://sfu-talk-dev.calling.fun/" // Dev

//public let kCallingChatDomain = "https://talk-chat.stg.calling.fun" // Stg
//public let jitsiDomain = "https://sfu-talk-stg.calling.fun/" // Stg

public let kCallingChatDomain = "https://talk-chat.calling.fun" // Prod
public let jitsiDomain = "https://sfu-talk.calling.fun/" // Prod


public let kCallingDomain = "calling.fun"

// NotificationCenter
public let kNewMessageEvent = "NewMessageEvent"
public let kMemberJoinOrLeaveTalkEvent = "MemberJoinOrLeaveTalkEvent"
public let kMarkMessageAsReadEvent = "MarkMessageAsReadEvent"
public let kLocalMarkMessageAsReadEvent = "LocalMarkMessageAsReadEvent"
public let kDeleteOrganizationEvent = "DeleteOrganizationEvent"
public let kKickedFromChannelEvent = "KickedFromChannelEvent"
public let kUpdateOrganizationsEvent = "UpdateOrganizationsEvent"
public let kDeleteChannelEvent = "kDeleteChannelEvent"

// Notification Badge Number
public let kMaxBadgeNumberOfNotifications = 99

