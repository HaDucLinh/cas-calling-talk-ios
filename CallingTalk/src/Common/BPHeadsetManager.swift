//
//  BPHeadsetManager.swift
//  CallingTalk
//
//  Created by Chinh Le on 5/15/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit

enum BPButtonCommand: String {
    case tap
    case doubleTap
    case longTap
    case finishLongTap
}

typealias BPHeadsetManagerButtonCommandHandler = (BPButtonCommand) -> Void
class BPHeadsetManager: NSObject {
    static let shared = BPHeadsetManager()
    var headSet: BPHeadset!
    var bpButtonCommandHandler: BPHeadsetManagerButtonCommandHandler?
    var isLongTapping: Bool = false
    
    override init() {
        super.init()
        
        headSet = BPHeadset.sharedInstance()
        headSet.add(self)
    }
    
    func connectHeadset() {
        if !isHeadsetConnected() {
            headSet.connect()
        }
    }
    
    func disconnectHeadset() {
        if isHeadsetConnected() {
            headSet.disconnect()
        }
    }
    
    func isHeadsetConnected() -> Bool {
        return headSet.connected
    }
}

extension BPHeadsetManager: BPHeadsetListener {
    // Connect/Disconnect
    func onConnect() {
        print("BPHeadset Log: onConnect")
        // Enable SDK mode to receive events from BlueParrott button
        if !headSet.sdkModeEnabled {
            headSet.enableSDKMode()
        }
    }
    
    func onConnectProgress(_ status: BPConnectProgress) {
        print("BPHeadset Log: onConnectProgress")
    }
    
    func onConnectFailure(_ resonCode: BPConnectError) {
        print("BPHeadset Log: onConnectFailure")
    }
    
    func onDisconnect() {
        print("BPHeadset Log: onDisconnect")
    }
    
    // Mode update
    func onModeUpdate() {
        print("BPHeadset Log: onModeUpdate")
    }
    
    func onModeUpdateFailure(_ resonCode: BPModeUpdateError) {
        print("BPHeadset Log: onModeUpdateFailure")
    }
    
    // Button Events
    func onTap(_ buttonID: BPButtonID) {
        print("BPHeadset Log: onTap - Should change the Mute mode")
        bpButtonCommandHandler?(.tap)
    }
    
    func onDoubleTap(_ buttonID: BPButtonID) {
        print("BPHeadset Log: onDoubleTap - Should change between Push/HandFree Mode")
        bpButtonCommandHandler?(.doubleTap)
    }
    
    func onButtonUp(_ buttonID: BPButtonID) {
        print("BPHeadset Log: onButtonUp - Should stop recording and check to save")
        // Just send .up event for longTap action
        if isLongTapping {
            bpButtonCommandHandler?(.finishLongTap)
        }
        isLongTapping = false
    }
    
    func onLongPress(_ buttonID: BPButtonID) {
        print("BPHeadset Log: onLongPress - Should start recording")
        bpButtonCommandHandler?(.longTap)
        isLongTapping = true
    }
}
