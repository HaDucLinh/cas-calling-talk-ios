//
//  JsonData.swift
//  CallingTalk
//
//  Created by Vu Tran on 2/15/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import Foundation

let JSONSAvatar = ["list":
    [
        [
            "id": 1,
            "name": "icon01",
            "url": ""
        ],
        [
            "id": 2,
            "name": "icon02",
            "url": ""
        ],
        [
            "id": 3,
            "name": "icon03",
            "url": ""
        ],
        [
            "id": 4,
            "name": "icon04",
            "url": ""
        ],
        [
            "id": 5,
            "name": "icon05",
            "url": ""
        ],
        [
            "id": 6,
            "name": "icon06",
            "url": ""
        ],
        [
            "id": 7,
            "name": "icon07",
            "url": ""
        ],
        [
            "id": 8,
            "name": "icon08",
            "url": ""
        ],
        [
            "id": 9,
            "name": "icon09",
            "url": ""
        ],
        [
            "id": 10,
            "name": "icon10",
            "url": ""
        ],
        [
            "id": 11,
            "name": "icon11",
            "url": ""
        ],
        [
            "id": 12,
            "name": "icon12",
            "url": ""
        ],
        [
            "id": 13,
            "name": "icon13",
            "url": ""
        ],
        [
            "id": 14,
            "name": "icon14",
            "url": ""
        ],
        [
            "id": 15,
            "name": "icon15",
            "url": ""
        ]
    ]
]

let JSONTalkImage = ["list":
    [
        [
            "id": 1,
            "name": "ic-newtalk01",
            "url": ""
        ],
        [
            "id": 2,
            "name": "ic-newtalk02",
            "url": ""
        ],
        [
            "id": 3,
            "name": "ic-newtalk03",
            "url": ""
        ],
        [
            "id": 4,
            "name": "ic-newtalk04",
            "url": ""
        ],
        [
            "id": 5,
            "name": "ic-newtalk05",
            "url": ""
        ],
        [
            "id": 6,
            "name": "ic-newtalk06",
            "url": ""
        ],
        [
            "id": 7,
            "name": "ic-newtalk07",
            "url": ""
        ],
        [
            "id": 8,
            "name": "ic-newtalk08",
            "url": ""
        ],
        [
            "id": 9,
            "name": "ic-newtalk09",
            "url": ""
        ],
        [
            "id": 10,
            "name": "ic-newtalk10",
            "url": ""
        ],
        [
            "id": 11,
            "name": "ic-newtalk11",
            "url": ""
        ],
        [
            "id": 12,
            "name": "ic-newtalk12",
            "url": ""
        ],
        [
            "id": 13,
            "name": "ic-newtalk13",
            "url": ""
        ],
        [
            "id": 14,
            "name": "ic-newtalk14",
            "url": ""
        ],
        [
            "id": 15,
            "name": "ic-newtalk15",
            "url": ""
        ]
    ]
]
