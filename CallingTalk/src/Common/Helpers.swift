//
//  Helpers.swift
//  CallingTalk
//
//  Created by Van Trung on 1/17/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit
import RealmS

enum ShareActivityType: Int {
    case QRCode = 0, URL = 1
}

enum CheckShowMentionAction: Int {
    case ShowMention = 0, Dismiss = 1
}

typealias CheckMentionCompletion = (_ type: CheckShowMentionAction,
    _ textTuple: (text: String, range: NSRange?, atEnd: Bool)) -> Void

class Helpers: NSObject {
    class func shareActivity(url: URL?, qrCode: UIImage?, type: ShareActivityType, fromVC: UIViewController) {
        var objectsToShare = [AnyObject]()
        if type == .QRCode {
            guard let qrCodeImage = qrCode else { return }
            objectsToShare.append(qrCodeImage)
        } else {
            guard let urlString = url else { return }
            objectsToShare.append(urlString as AnyObject)
        }
        let activityViewController = UIActivityViewController(activityItems: objectsToShare , applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = fromVC.view
        fromVC.present(activityViewController, animated: true, completion: nil)
    }
    
    class func showActionSheet(title: String?, actions: [UIAlertAction], sourceView: UIView? = nil) {
        var alertTitle = ""
        if let title = title {
            alertTitle = title
        }
        let titleAttributeText = NSMutableAttributedString(string: alertTitle, attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16)])
        
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        if title != nil {
            alert.setValue(titleAttributeText, forKey: "attributedMessage")
        }
        actions.forEach { (action) in
            alert.addAction(action)
        }
        if let sourceView = sourceView {
            alert.popoverPresentationController?.sourceView = sourceView
            alert.popoverPresentationController?.sourceRect = sourceView.bounds
        }
        alert.show()
    }
}

// Saving profile & token

extension Helpers {
    
    class func saveProfile(_ profile: User) {
        // Remove the old one first
        User.removeCurrentUser()
        
        // Save user's profile & Update Token
        RealmS().write {
            RealmS().add(profile)
        }
        UserDefaults.setToken(profile.talkAppToken)
    }
    
    class func clearProfile() {
        User.clearAll()
        UserDefaults.clearToken()
    }
    
    // Get Org name for user
    class func getOrganizationsName(user: User) -> String {
        var organizationText = ""
        user.organizations.forEach { (organization) in
            organizationText += organization.name + ", "
        }
        if !organizationText.isEmpty {
            organizationText = String(organizationText.dropLast(2))
        }
        return organizationText
    }
}

extension Helpers {
    class func replaceUserNameById(textMessage: String, taggedUsers: [TaggingModel]) -> String {
        var result = textMessage
        print("Text: ", textMessage)
        
        var deltaLocation: Int = 0
        
        taggedUsers.forEach { (model) in
            let nameText = "@" + model.user.name
            let idText = "@" + model.user.id.description + " "
            // Warning: need add Space for avoid error of checking Ids when user typing numeric after user mentioned.
            
            var currentRange = model.range
            currentRange.location += deltaLocation
            result.replaceSubrange(Range.init(currentRange, in: result)!, with: idText)
            
            deltaLocation += (idText as NSString).length - (nameText as NSString).length
        }
        print("Replace name by id: ", result)
        return result as String
    }
    
    class func findTaggingModelInMessageContent(textMessage: String, mentionUsers: [User]) -> [TaggingModel] {
        var taggingModel: [TaggingModel] = []
        let allText = textMessage + " "
        
        print("findTaggingModelInMessageContent: ", textMessage)
        
        let textRange = NSRange.init(location: 0, length: (allText as NSString).length)
        
        mentionUsers.forEach { (user) in
            let idText = "@" + user.id.description + " "
            do {
                let regex: NSRegularExpression = try NSRegularExpression.init(pattern: idText, options: .caseInsensitive)
                let matched = regex.matches(in: allText, options: .reportCompletion, range: textRange)
                matched.forEach({ (result) in
                    let tagModel = TaggingModel.init(user: user, range: result.range)
                    print("Range: \(result.range) with Name: \(user.name)")
                    taggingModel.append(tagModel)
                })
            } catch {}
        }
        
        return taggingModel
    }
    
    class func replaceUserIdByName(textMessage: String, mentionUsers: [User], isMentionAll: Bool) -> (String, [NSRange]) {
        var result = textMessage + " "
        var ranges: [NSRange] = []
        
        var mentionIds: [Int] = []
        mentionUsers.forEach { (user) in
            mentionIds.append(user.id)
        }
        
        var taggingModels = Helpers.findTaggingModelInMessageContent(textMessage: textMessage,
                                                                    mentionUsers: mentionUsers)
        
        taggingModels = taggingModels.sorted(by: { (modelA, modelB) -> Bool in
            return modelA.range.location < modelB.range.location
        })
        
        if taggingModels.isEmpty { return (textMessage, ranges) }
        
        var deltaLocation: Int = 0
        taggingModels.forEach { (model) in
            let nameText = "@" + model.user.name
            let idText = "@" + model.user.id.description + " "
            var rangeReplace = model.range
            rangeReplace.location += deltaLocation
            result.replaceSubrange(Range.init(rangeReplace, in: result)!, with: nameText)
            
            var currentRange = model.range
            currentRange.location += deltaLocation
            currentRange.length = (nameText as NSString).length
            ranges.append(currentRange)
            
            deltaLocation += (nameText as NSString).length - (idText as NSString).length
        }
        
//        mentionUsers.forEach { (user) in
//            let nameText = "@" + user.name
//            let idText = "@" + user.id.description + " "
//            // Warning: need add Space for avoid error of checking Ids when user typing numeric after user mentioned.
//
//            let ranges = result.ranges(of: idText)
//            if !ranges.isEmpty {
//                result = result.replacingOccurrences(of: idText, with: nameText)
//            }
//        }
        
        print("Replace id by name: ", result)
        return (result, ranges)
    }
    
    class func replaceCurrentUserIdByName(textMessage: String) -> String {
        guard let user = User.getProfile() else { return textMessage }
        var result = textMessage + " "
        
        let nameText = "@" + user.name
        let idText = "@" + user.id.description + " "
        // Warning: need add Space for avoid error of checking Ids when user typing numeric after user mentioned.
        
        let ranges = result.ranges(of: idText)
        if !ranges.isEmpty {
            result = result.replacingOccurrences(of: idText, with: nameText)
        }
        print("Replace current user id by name: ", result)
        return result
    }
}
