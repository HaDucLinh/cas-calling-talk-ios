//
//  IncomingNotificationManager.swift
//  CallingTalk
//
//  Created by Toof on 3/20/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit
import UserNotifications

struct LocalNotification {
    var id: String
    var title: String
    var message: String
    var userInfo: [String : Any]?
}

class NotificationManager: NSObject {
    static let shared: NotificationManager = NotificationManager()
    
    public func config(_ delegate: UNUserNotificationCenterDelegate) {
        UNUserNotificationCenter.current().delegate = delegate
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .badge, .sound]) {(accepted, error) in
            if let error = error {
                print("ERROR:... \(error)")
            }
        }
    }
    
    public func removeNotifications() {
        UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
        UNUserNotificationCenter.current().removeAllDeliveredNotifications()
    }
    
    public func addLocalNotification(_ notification: LocalNotification) {
        let content = UNMutableNotificationContent()
        content.title = notification.title
        content.body = notification.message
        content.sound = UNNotificationSound.default
        if let userInfo = notification.userInfo {
            content.userInfo = userInfo
        }
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1.0, repeats: false) // Push after 1s
        let request = UNNotificationRequest(identifier: UIApplication.appName + notification.id, content: content, trigger: trigger)
        
        UNUserNotificationCenter.current().add(request) { (error) in
            if let error = error {
                print("ERROR:... \(error)")
            }
        }
    }
}
