//
//  AVPlayerManager.swift
//  CallingTalk
//
//  Created by Chinh Le on 3/22/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit
import AVFoundation

typealias AVPlayerManagerCompletionHandler = () -> Void
class AVPlayerManager: NSObject {
    static let shared = AVPlayerManager()
    var player: AVPlayer?
    var completion: AVPlayerManagerCompletionHandler?
    // Saving the messageId of playing audio to use in cell
    var playingAudioMessageId: Int?
    
    override init() {
        super.init()
        
        NotificationCenter.default.addObserver(self, selector: #selector(finishAudio), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil)
    }
    
    func playRemoteAudio(urlString: NSString?) {
        guard let urlString = urlString else { return }
        
        playAudio(url: URL(string: urlString as String))
    }
    
    func playLocalAudio(name: String, type: String) {
        if let path = Bundle.main.path(forResource: name, ofType: type) {
            playAudio(url: URL(fileURLWithPath: path))
        }
    }
    
    func playAudio(url: URL?) {
        guard let url = url else { return }

        player = AVPlayer(url: url)
        player?.play()
    }
    
    func continuePlaying() {
        player?.play()
    }
    
    func pauseAudio() {
        player?.pause()
    }
    
    func stopAudio() {
        player = nil
    }
    
    func isPlayerReady() -> Bool {
        return (player?.currentItem?.status == .readyToPlay)
    }
    
    @objc private func finishAudio() {
        self.completion?()
    }
    
    func audioReverseCurrentTimeString() -> String {
        var reverseCurrentTimeInterval: TimeInterval = 0
        if let currentTime = player?.currentItem?.currentTime(), let duration = player?.currentItem?.duration {
            reverseCurrentTimeInterval = CMTimeGetSeconds(duration) - CMTimeGetSeconds(currentTime)
        }
        return stringFromTimeInterval(interval: reverseCurrentTimeInterval)
    }
    
    func audioCurrentTimeString() -> String {
        var currentTimeInterval: TimeInterval = 0
        if let currentTime = player?.currentItem?.currentTime() {
            currentTimeInterval = CMTimeGetSeconds(currentTime)
        }
        return stringFromTimeInterval(interval: currentTimeInterval)
    }
    
    func audioDurationString() -> String {
        var durationInterval: TimeInterval = 0
        if let duration = player?.currentItem?.duration {
            durationInterval = CMTimeGetSeconds(duration)
        }
        return stringFromTimeInterval(interval: durationInterval)
    }
    
    func stringFromTimeInterval(interval: TimeInterval?) -> String {
        guard let interval = interval, interval > 0 else {
            return "00:00"
        }
        
        var intervalInInteger = Int(interval)
        let remainder = interval - Double(intervalInInteger)
        if remainder > 0.5 {
            intervalInInteger += 1
        }
        
        let minutes = intervalInInteger / 60
        let seconds = intervalInInteger % 60
        return String(format: "%02d:%02d", minutes, seconds)
    }
}
