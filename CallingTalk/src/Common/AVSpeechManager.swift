//
//  AVSpeechManager.swift
//  CallingTalk
//
//  Created by Vu Tran on 4/17/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit
import AVFoundation

class AVSpeechManager: NSObject {
    static let shared = AVSpeechManager()
    var synthesizer: AVSpeechSynthesizer?
    
    override init() {
        super.init()
        synthesizer = AVSpeechSynthesizer()
    }
    
    func speechText(text: String?) {
        guard let text = text, !text.isEmpty  else { return }
        var language = "ja-JP"
        
        if let code = Locale.current.languageCode {
            language = (code == "ja") ? "ja-JP" : "en"
        }
        let utterance = AVSpeechUtterance(string: text)
        utterance.voice = AVSpeechSynthesisVoice(language: language)
        utterance.rate = 0.5
        synthesizer?.speak(utterance)
        
    }
    func cancelSpeech(){
        synthesizer?.stopSpeaking(at: .immediate)
    }
}
