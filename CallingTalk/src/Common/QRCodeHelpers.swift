//
//  QRCodeHelpers.swift
//  CallingTalk
//
//  Created by Van Trung on 4/5/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import Foundation

class QRCodeHelpers: NSObject {
    static let sharedInstance = QRCodeHelpers()
    func getQRCode(withTalkId talkGroupId: Int, talkgroupName: String?, fromVC: BaseViewController!){
        hud.show()
        API.showQRCode(talkGroupId) { (result) in
            DispatchQueue.main.async {
                switch result {
                case .success(let data):
                    guard let imageUrlString = data as? String else { return }
                    
                    self.showQRCodeAlertView(imageUrlString, talkGroupName: talkgroupName, fromVC: fromVC)
                case .failure(let error):
                    fromVC.showErrorAlertWithError(error, completion: nil)
                }
                hud.dismiss()
            }
        }
    }
    
    private func showQRCodeAlertView(_ qrCodeImageUrlString: String, talkGroupName: String?, fromVC: UIViewController!){
        let shareTalkView = ShareQRCodeView.loadViewFromNib()
        let talkGroupName = talkGroupName ?? ""
        let shareTitle = NSLocalizedString("trailing_talk_group_share_qr_code_text", comment: "")
        let titleText = talkGroupName + shareTitle
        shareTalkView.show(qrCodeImageUrlString, title: titleText) { (type) in
            switch type {
            case .Cancel: print("cancel")
            case .ShareCode(let image):
                Helpers.shareActivity(url: nil, qrCode: image, type: .QRCode, fromVC: fromVC)
            case .ShareUrl(let image):
                if let imageUrlString = image.parseQR().first {
                    Helpers.shareActivity(url: URL(string: imageUrlString), qrCode: nil, type: .URL, fromVC: fromVC)
                }
            }
        }
    }
}
