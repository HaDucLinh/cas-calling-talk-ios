//
//  AppDelegate.swift
//  CallingTalk
//
//  Created by Van Trung on 1/15/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import SVProgressHUD
import RealmSwift
import ObjectMapper
import PushKit
import Swinject
import Fabric
import Crashlytics
import UserNotifications

typealias hud = SVProgressHUD

public enum PushMessageType: String {
    case incoming = "INCOMING"
    case mention = "MENTION_MESSAGE"
    case join_organization = "JOIN_ORGANIZATION"
    case invite_message = "INVITE_MESSAGE"
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    let userDefaults: UserDefaults = UserDefaults.standard
    var systemTheme:SystemThemeable?
    /*
     servicesFacade: Services registering for all the application.
     Service can be access using Container.default.resolve(Protocol_defination)
     */
    var servicesFacade = ServicesFacade()
    class func share() -> AppDelegate? {
        return UIApplication.shared.delegate as? AppDelegate
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        Fabric.with([Crashlytics.self])
        // Migration
        self.migrate()
        self.setUpTheme()
        // Setup IQKeyboardManager
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        IQKeyboardManager.shared.enableAutoToolbar = false
        IQKeyboardManager.shared.shouldShowToolbarPlaceholder = false
        IQKeyboardManager.shared.disabledDistanceHandlingClasses = [ChatVC.self]
        IQKeyboardManager.shared.disabledTouchResignedClasses = [ChatVC.self]
        
        // Setup SVProgressHUD
        hud.setForegroundColor(UIColor.ct.mainBackgroundColor)
        
        // Setup BlueParrott headset SDK
        _ = BPHeadsetManager.shared
        
        // Voip Registration
        self.voipRegistration()
        
        // Local Notification
        NotificationManager.shared.config(self)
        NotificationManager.shared.removeNotifications()
        
        self.setupWindow()
        return true
    }
    func setUpTheme() {
        systemTheme = SystemTheme()// auto detecting the mode from setting
        Container.default.register(SystemThemeable.self) {(systemThemeable) -> SystemThemeable in
            return self.systemTheme!
        }
        
    }
    func applicationWillEnterForeground(_ application: UIApplication) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ReloadTalkGroups"), object: nil, userInfo: nil)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "CallMarkAsRead"), object: nil, userInfo: nil)
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        NotificationManager.shared.removeNotifications()
        
        // Send notification to get state for local track when active app
        NotificationCenter.default.post(name: Notification.Name("GET_LOCAL_STATE"), object: nil, userInfo: nil)
    }
}

extension AppDelegate {
    
    fileprivate
    func setupWindow() {
        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window?.backgroundColor = UIColor.ct.mainBackgroundColor
        self.window?.makeKeyAndVisible()
        window?.backgroundColor = UIColor.ct.mainBackgroundColor
        
        if let user = User.getProfile() {
            // Connect socket
            SocketIOManager.sharedInstance.connectSocket()
            
            self.showHomeTabbar(isAdmin: user.isAdmin)
        }
        else {
            self.showLoginVC()
        }
    }
    
    public
    func showLoginVC(isAdmin: Bool = false) {
        let vc = StaffLoginVC()
        vc.isAdminLogOut = isAdmin
        let mainNav = UINavigationController(rootViewController: vc)
        mainNav.navigationBar.setupNavigationBarWithColor()
        self.window?.rootViewController = mainNav
    }
    
    public
    func showHomeTabbar(isAdmin: Bool = false) {
        let tabbarVC = UITabBarControllerManager.shared.mainTabBarController(isAdmin: isAdmin)
        self.window?.rootViewController = tabbarVC
    }
    
    fileprivate
    func voipRegistration() {
        let voipRegistry: PKPushRegistry = PKPushRegistry(queue: DispatchQueue.main)
        voipRegistry.delegate = self
        voipRegistry.desiredPushTypes = [PKPushType.voIP]
    }
}

// MARK: - Migrations

extension AppDelegate {
    // Current's version is 0
    func migrate() {
        Realm.Configuration.defaultConfiguration = Realm.Configuration(
            schemaVersion: 1,
            migrationBlock: { (migration, oldSchemaVersion) in
                if oldSchemaVersion < 1 {
                    migration.enumerateObjects(ofType: User.className()) { oldObject, newObject in
                        // TODO:
                    }
                    migration.enumerateObjects(ofType: TalkGroup.className()) { oldObject, newObject in
                        // TODO:
                    }
                    migration.enumerateObjects(ofType: Message.className()) { oldObject, newObject in
                        // TODO:
                    }
                }
        })
    }
}
