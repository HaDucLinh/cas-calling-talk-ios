//
//  Defines.swift
//  CallingTalk
//
//  Created by Vu Tran on 4/11/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit

// App Infor

// Date format
public let kDateGetListChannel = "yyyy-MM-dd HH:mm:ss"

// Default image's size that is send to server
public let defaultImageSize: Int = 5 // 5.0 MB
public let defaultImageResolution: CGSize = CGSize(width: 1280, height: 720)
public let defaultMessageLengh: Int = 1000
public let defaultTimeReconnect: Int = 35000 // Milisecond. Timereconnect = Time jitsi reconnect (about 5s) + time wait to reconnect internet.
