//
//  EditTalkPresenter.swift
//  CallingTalk
//
//  Created by Vu Tran on 2/18/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import Foundation
import UIKit

/// Protocol connect view and presenter
protocol EditTalkView: BaseViewProtocol {
    func updateMemberCollectionView()
    func disableEditButton()
    func didUpdateTalkGroup(talkGroup: TalkGroup, shouldDismiss: Bool)
    func dismissVC()
}

class EditTalkPresenter: NSObject {
    weak fileprivate var view : EditTalkView?
    var talkGroup: TalkGroup?
    var usersToInvite: [User] = []
    var avartarData: Data?
    var backgroundData: Data?
    
    override init() {
    }
    
    func attachView(_ view: EditTalkView) {
        self.view = view
    }
    
    func talkNameDidChange(text: String?){
        talkGroup?.name = text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
        if talkGroup?.name.count == 0 {
            // check validate later
            self.view?.disableEditButton()
        }
    }
    
    // APIs
    func editTalkGroup(name: String, talkImageData: Data?, avatarIndex: Int, backGroundData: Data?) {
        guard let talkGroup = self.talkGroup, talkGroup.id != 0 else { return }

        hud.show()
        API.editTalkGroup(id: talkGroup.id, name, avatar: talkImageData, avatarIndex: avatarIndex, background: backGroundData) { (result) in
            DispatchQueue.main.async { [weak self] in
                guard let `self` = self else { return }
                hud.dismiss()
                
                switch result {
                case .success(let object):
                    if let newTalkGroup = object as? TalkGroup {
                        talkGroup.name = newTalkGroup.name
                        talkGroup.avatar = newTalkGroup.avatar
                        talkGroup.background = newTalkGroup.background
                        talkGroup.avatarIndex = avatarIndex
                        self.view?.didUpdateTalkGroup(talkGroup: talkGroup, shouldDismiss: true)
                    }
                case .failure(let error):
                    self.view?.showErrorAlertWithError(error, completion: nil)
                }
            }
        }
    }
    
    func handleTalkChanged() {
        guard let talkGroupId = self.talkGroup?.id else { return }
        API.detailTalkGroup(talkGroupId) { (result) in
            DispatchQueue.main.async { [weak self] in
                guard let `self` = self else { return }
                switch result {
                case .success(let talkGroup):
                    guard let talkGroup = talkGroup as? TalkGroup else { return }
                    self.talkGroup = talkGroup
                    self.view?.updateMemberCollectionView()
                    self.view?.didUpdateTalkGroup(talkGroup: talkGroup, shouldDismiss: false)
                case .failure(_): break
                }
            }
        }
    }
    
    func showQRCode(fromVC: BaseViewController!){
        guard let talkGroupId = talkGroup?.id else {return}
        QRCodeHelpers.sharedInstance.getQRCode(withTalkId: talkGroupId, talkgroupName: talkGroup?.name, fromVC: fromVC)
    }

}
