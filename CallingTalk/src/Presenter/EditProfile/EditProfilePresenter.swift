//
//  EditProfilePresenter.swift
//  CallingTalk
//
//  Created by Vu Tran on 2/27/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import Foundation
/// Protocol connect view and presenter
protocol EditProfileView: BaseViewProtocol {
    func enableEditButton()
    func disableEditButton()
    func updateSuccessful(profile: User)
}

class EditProfilePresenter: NSObject {
    weak fileprivate var view : EditProfileView?
    var profile: User? {
        didSet {
            name = profile?.name ?? ""
        }
    }
    public var avatar: Data?
    fileprivate var name = ""
    
    override init() {
    }
    
    func attachView(_ view: EditProfileView) {
        self.view = view
    }
    
    func usernameTextChange(text: String?) {
        name = text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
        setEditButtonStatus(name: name)
    }
    
    /// update button status (enable/disable) depend on other input field
    func setEditButtonStatus(name: String) {
        if !name.isEmpty {
            view?.enableEditButton()
            return
        }
        view?.disableEditButton()
    }
    
    func updateProfile(avatarIndex: Int) {
        guard !name.isEmpty, let avatarData = avatar else { return }
        hud.show()
        API.updateProfile(name, avatar: avatarData, avatarIndex: avatarIndex, email: profile?.email) { (result) in
            DispatchQueue.main.async { [weak self] in
                guard let `self` = self else { return }
                hud.dismiss()
                switch result {
                case .success(let profileData):
                    guard let profile = profileData as? User else { return }
                    
                    // Save user's profile & token id
                    Helpers.saveProfile(profile)
                    
                    self.view?.updateSuccessful(profile: profile)
                case .failure(let error):
                    self.view?.showErrorAlertWithError(error, completion: nil)
                }
            }
        }
    }
}
