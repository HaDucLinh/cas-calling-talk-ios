//
//  AdminEditProfilePresenter.swift
//  CallingTalk
//
//  Created by Vu Tran on 2/27/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import Foundation

protocol AdminEditProfileView: BaseViewProtocol {
    func enableEditButton()
    func disableEditButton()
    func updateSuccessful(profile: User)
}

class AdminEditProfilePresenter: NSObject {
    weak fileprivate var view : AdminEditProfileView?
    public var avatar: Data?
    var profile: User? {
        didSet {
            name = profile?.name ?? ""
            email = profile?.email ?? ""
        }
    }
    var name = ""
    var email = ""
    
    override init() {
    }
    
    func attachView(_ view: AdminEditProfileView) {
        self.view = view
    }
    
    func usernameTextChange(text: String?){
        name = text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
        setEditButtonStatus()
    }
    
    func emailTextChange(text: String?){
        email = text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
        setEditButtonStatus()
    }
    
    /// update button status (enable/disable) depend on other input field
    func setEditButtonStatus(){
        if (name.count > 0) {
            view?.enableEditButton()
            return
        }
        view?.disableEditButton()
    }
    
    func updateProfile(avatarIndex: Int) {
        guard !name.isEmpty, let avatarData = avatar else { return }
        hud.show()
        API.updateProfile(name, avatar: avatarData, avatarIndex: avatarIndex, email: email) { (result) in
            DispatchQueue.main.async { [weak self] in
                guard let `self` = self else { return }
                
                hud.dismiss()
                switch result {
                case .success(let profileData):
                    guard let profile = profileData as? User else { return }
                    
                    // Save user's profile & token id
                    Helpers.saveProfile(profile)
                    
                    self.view?.updateSuccessful(profile: profile)
                case .failure(let error):
                    self.view?.showErrorAlertWithError(error, completion: nil)
                }
            }
        }
    }
}
