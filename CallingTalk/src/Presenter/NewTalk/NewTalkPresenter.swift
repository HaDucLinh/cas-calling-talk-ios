//
//  NewTalkPresenter.swift
//  CallingTalk
//
//  Created by Vu Tran on 2/14/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import Foundation
import RealmSwift
import UIKit

/// Protocol connect view and presenter
protocol NewTalkView: BaseViewProtocol {
    func enableStartButton(_ isEnable: Bool)
    func updateNumberOfMembers(_ number: String)
    func moveToIncomingVCWithTalkGroupId(_ id: Int)
    func showListOrganizationVC(with organizations: [Organization])
    func refreshMembers()
}

protocol NewTalkViewPresenter: class {
    func attachView(_ view: NewTalkView)
    func updateName(_ name: String)
    func updateAvatarData(_ data: Data?)
    func updateBackgroundData(_ data: Data?)
    func updateMembers(_ members: [User])
    func numberOfItems(inSection section: Int) -> Int
    func configure(_ cell: MemberCell, at indexPath: IndexPath)
    func getListOrganizationForAdmin()
    func createTalkGroup(with organizationId: Int?, avatarIndex: Int)
}

class NewTalkPresenter: NewTalkViewPresenter {
    weak fileprivate var view: NewTalkView?
    var members: [User] = [User]()
    fileprivate var name: String!
    fileprivate var avatarData: Data?
    fileprivate var backgroundData: Data?
    var defaultAvatarIndex: Int = 1

    func attachView(_ view: NewTalkView) {
        self.view = view
    }
    
    func updateName(_ name: String) {
        self.name = name
        self.view?.enableStartButton(name.count > 0)
    }
    
    func updateAvatarData(_ data: Data?) {
        self.avatarData = data
    }
    
    func updateBackgroundData(_ data: Data?) {
        self.backgroundData = data
    }
    
    func updateMembers(_ members: [User]) {
        self.members = members
        self.view?.refreshMembers()
    }
    
    func numberOfItems(inSection section: Int) -> Int {
        let membersCount = self.members.count + 1
        self.view?.updateNumberOfMembers("\(membersCount)")
        return membersCount
    }
    
    func configure(_ cell: MemberCell, at indexPath: IndexPath) {
        if indexPath.row == 0 {
            let user = User.getProfile()
            cell.user = user
            cell.roleType = .Author
        } else {
            cell.user = self.members[indexPath.row - 1]
        }
    }
    
    func getListOrganizationForAdmin() {
        if let profile = User.getProfile(), profile.isAdmin {
            hud.show()
            API.getMyOrganizations { (result) in
                DispatchQueue.main.async { [weak self] in
                    guard let `self` = self else { return }
                    hud.dismiss()
                    switch result {
                    case .success(let data):
                        guard let organizations = data as? [Organization] else { return }
                        if organizations.count == 1 {
                            self.createTalkGroup(with: organizations.first?.id, avatarIndex: self.defaultAvatarIndex)
                        }
                        else {
                            self.view?.showListOrganizationVC(with: organizations)
                        }
                    case .failure(let error):
                        self.view?.showErrorAlertWithError(error, completion: nil)
                    }
                }
            }
        }
        else { // If user is not admin, can not call API get organization's list
            self.createTalkGroup(avatarIndex: defaultAvatarIndex)
        }
    }
    
    func createTalkGroup(with organizationId: Int? = nil, avatarIndex: Int) {
        hud.show()
        let ids = self.members.map({ $0.id })
        API.createTalkGroup(self.name,
                            memberIds: ids,
                            avatar: self.avatarData,
                            avatarIndex: avatarIndex,
                            background: self.backgroundData,
                            organizationId: organizationId)
        { (result) in
            DispatchQueue.main.async { [weak self] in
                guard let `self` = self else { return }
                hud.dismiss()
                switch result {
                case .success(let result):
                    if let talkId = result as? Int {
                        self.view?.moveToIncomingVCWithTalkGroupId(talkId)
                    }
                case .failure(let error):
                    self.view?.showErrorAlertWithError(error, completion: nil)
                }
            }
        }
    }
}
