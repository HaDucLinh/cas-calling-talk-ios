//
//  InviteMemberPresenter.swift
//  CallingTalk
//
//  Created by Vu Tran on 3/19/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import Foundation

public enum InviteMemberForType {
    case Create
    case Edit
}

protocol InviteMemberView: BaseViewProtocol {
    func enableInviteButton(_ isEnable: Bool)
    func hideSelectedMemberLabel(_ isHidden: Bool)
    func updateNumberOfSelectedUser(_ number: Int)
    func didSelectedMembers(_ members: [User])
    func insertRowsToTableView(at indexPaths: [IndexPath])
    func refreshTableView()
    func refreshCollectionView()
}

protocol InviteMemberViewPresenter: class {
    func attachView(_ view: InviteMemberView)
    func numberOfUsers(inSection section: Int) -> Int
    func numberOfSelectedUsers(inSection section: Int) -> Int
    func configure(_ cell: SelectMemberCell, at indexPath: IndexPath)
    func configure(_ cell: TalkMemberCell, at indexPath: IndexPath)
    func inviteMembers()
    func selectUser(_ user: User)
    func unselectUser(_ user: User)
    func removeSelectedUser(_ user: User)
    func fetchData(_ query: String?)
}

class InviteMemberPresenter: InviteMemberViewPresenter {
    weak fileprivate var view : InviteMemberView?
    var inviteMemberForType: InviteMemberForType = .Create
    var talkGroup = TalkGroup()
    var usersToInvite: [User] = [User]()
    
    var allMembers: [User] = [User]()
    var searchedMembers: [User] = [User]()
    var isSearching: Bool = false
    
    fileprivate var hasMorePages = false
    fileprivate var hasMoreSearchPages = false
    fileprivate var searchHasResponed = false
    
    fileprivate var maxDateString = Date().stringToJapanTime(withFormat: kDateGetListChannel)   // Convert to JP's Timezone
    fileprivate var minDateString = Date().stringToJapanTime(withFormat: kDateGetListChannel)
    
    func attachView(_ view: InviteMemberView) {
        self.view = view
    }
    
    func numberOfUsers(inSection section: Int) -> Int {
        let count: Int = self.isSearching ? self.searchedMembers.count : self.allMembers.count
        self.view?.hideSelectedMemberLabel(count <= 0)
        return count
    }
    
    func numberOfSelectedUsers(inSection section: Int) -> Int {
        let count: Int = self.usersToInvite.count
        self.view?.enableInviteButton(count > 0)
        self.view?.updateNumberOfSelectedUser(count)
        return count
    }
    
    func configure(_ cell: SelectMemberCell, at indexPath: IndexPath) {
        var users: [User] = self.isSearching ? self.searchedMembers : self.allMembers
        let currentUser = users[indexPath.row]
        cell.member = currentUser
        cell.isMemberSelected = self.usersToInvite.index(where: {$0.id == currentUser.id}) != nil
    }
    
    func configure(_ cell: TalkMemberCell, at indexPath: IndexPath) {
        cell.user = self.usersToInvite[indexPath.row]
    }
    
    func selectUser(_ user: User) {
        self.usersToInvite.append(user)
        self.view?.refreshCollectionView()
    }
    
    func unselectUser(_ user: User) {
        if self.usersToInvite.count > 0 {
            if let index = self.usersToInvite.index(where: {$0.id == user.id}) {
                self.usersToInvite.remove(at: index)
                self.view?.refreshCollectionView()
            }
        }
    }
    
    func removeSelectedUser(_ user: User) {
        if self.usersToInvite.count > 0 {
            if let index = self.usersToInvite.index(where: {$0.id == user.id}) {
                self.usersToInvite.remove(at: index)
                self.view?.refreshTableView()
                self.view?.refreshCollectionView()
            }
        }
    }
    
    func inviteMembers() {
        switch self.inviteMemberForType {
        case .Create:
            self.view?.didSelectedMembers(self.usersToInvite)
        case .Edit:
            self.addOnMemberToTalkGroup()
        }
    }
    
    fileprivate var searchCompletion: UsersCompletion {
        return { (result, hasMorePages) in
            DispatchQueue.main.async { [weak self] in
                guard let `self` = self else { return }
                hud.dismiss()
                switch result {
                case .success(let users):
                    if !self.isSearching { return }
                    if let users = users as? [User] {
                        var indexPaths: [IndexPath] = [IndexPath]()
                        if self.hasMoreSearchPages {
                            for i in self.searchedMembers.count..<(self.searchedMembers.count + users.count) {
                                let indexPath = IndexPath(row: i, section: 0)
                                indexPaths.append(indexPath)
                            }
                            self.searchedMembers.append(contentsOf: users)
                            self.view?.insertRowsToTableView(at: indexPaths)
                        }
                        else {
                            self.searchedMembers = users
                            self.searchHasResponed = true
                            self.view?.refreshTableView()
                        }
                    }
                    
                    // Update load more
                    self.hasMoreSearchPages = hasMorePages
                case .failure(let error):
                    self.view?.showErrorAlertWithError(error, completion: nil)
                }
            }
        }
    }
    
    fileprivate var fetchCompletion: UsersCompletion {
        return { (result, hasMorePages) in
            DispatchQueue.main.async { [weak self] in
                guard let `self` = self else { return }
                hud.dismiss()
                switch result {
                case .success(let users):
                    if let users = users as? [User] {
                        var indexPaths: [IndexPath] = [IndexPath]()
                        if self.hasMorePages {
                            for i in self.allMembers.count..<(self.allMembers.count + users.count) {
                                let indexPath = IndexPath(row: i, section: 0)
                                indexPaths.append(indexPath)
                            }
                            self.allMembers.append(contentsOf: users)
                            self.view?.insertRowsToTableView(at: indexPaths)
                        }
                        else {
                            self.allMembers = users
                            self.view?.refreshTableView()
                        }
                    }
                    
                    // Update load more
                    self.hasMorePages = hasMorePages
                case .failure(let error):
                    self.view?.showErrorAlertWithError(error, completion: nil)
                }
            }
        }
    }
    
    func fetchData(_ query: String? = nil) {
        switch self.inviteMemberForType {
        case .Create:
            if self.isSearching {
                self.searchAllUser(query, completion: self.searchCompletion)
            }
            else {
                self.getAllUser(self.fetchCompletion)
            }
        case .Edit:
            if self.isSearching {
                self.searchUsersToAddOnTalkGroup(query, completion: self.searchCompletion)
            }
            else {
                self.getUsersToAddOnTalkGroup(self.fetchCompletion)
            }
        }
    }
    
    // MARK: APIs
    
    fileprivate func searchAllUser(_ query: String?, completion: @escaping UsersCompletion) {
        if !(self.searchHasResponed && !self.hasMoreSearchPages) {
            hud.show()
            var minDateAt: String? = nil
            if self.hasMoreSearchPages {
                if let minEventTime = self.searchedMembers.last?.createAt {
                    minDateAt = minEventTime.adding(.second, value: -1).string(withFormat: kDateGetListChannel)
                }
            }
            API.listUserToInvite(query, minDate: minDateAt, completion: completion)
        }
    }
    
    fileprivate func getAllUser(_ completion: @escaping UsersCompletion) {
        if !(self.allMembers.count > 0 && !self.hasMorePages) {
            hud.show()
            var minDateAt: String = self.minDateString
            if self.hasMorePages {
                if let minEventTime = self.allMembers.last?.createAt {
                    minDateAt = minEventTime.adding(.second, value: -1).string(withFormat: kDateGetListChannel)
                }
            }
            API.listUserToInvite(maxDate: self.maxDateString, minDate: minDateAt, completion: completion)
        }
    }
    
    fileprivate func searchUsersToAddOnTalkGroup(_ query: String?, completion: @escaping UsersCompletion) {
        guard self.talkGroup.id != 0 else { return }
        if !(self.searchHasResponed && !self.hasMoreSearchPages) {
            hud.show()
            var minDateAt: String? = nil
            if self.hasMoreSearchPages {
                if let minEventTime = self.searchedMembers.last?.createAt {
                    minDateAt = minEventTime.adding(.second, value: -1).string(withFormat: kDateGetListChannel)
                }
            }
            API.listUserToInviteTalkGroup(self.talkGroup.id, query: query, minDate: minDateAt, completion: completion)
        }
    }
    
    fileprivate func getUsersToAddOnTalkGroup(_ completion: @escaping UsersCompletion) {
        guard talkGroup.id != 0 else { return }
        if !(self.allMembers.count > 0 && !self.hasMorePages) {
            hud.show()
            var minDateAt: String = self.minDateString
            if self.hasMorePages {
                if let minEventTime = self.allMembers.last?.createAt {
                    minDateAt = minEventTime.adding(.second, value: -1).string(withFormat: kDateGetListChannel)
                }
            }
            API.listUserToInviteTalkGroup(talkGroup.id, maxDate: self.maxDateString, minDate: minDateAt, completion: completion)
        }
    }
    
    fileprivate func addOnMemberToTalkGroup() {
        guard !usersToInvite.isEmpty else {
            return
        }
        var ids: [Int] = []
        usersToInvite.forEach { (user) in
            ids.append(user.id)
        }
        hud.show()
        API.inviteMembersToTalkGroup(groupId: talkGroup.id, memberIDs: ids) { (result) in
            DispatchQueue.main.async { [weak self] in
                guard let `self` = self else { return }
                hud.dismiss()
                switch result {
                case .success( _):
                    self.view?.didSelectedMembers(Array(self.talkGroup.allUsers))
                case .failure(let error):
                    self.view?.showErrorAlertWithError(error, completion: nil)
                }
            }
        }
    }
    
    func searchUsers(text: String?) {
        self.hasMoreSearchPages = false
        self.searchHasResponed = false
        self.fetchData(text)
    }
}
