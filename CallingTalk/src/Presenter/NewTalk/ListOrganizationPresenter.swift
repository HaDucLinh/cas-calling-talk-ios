//
//  ListOrganizationPresenter.swift
//  CallingTalk
//
//  Created by Toof on 4/5/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import Foundation

protocol ListOrganizationView: BaseViewProtocol {
    func refresh()
}

protocol ListOrganizationViewPresenter {
    init(view: ListOrganizationView, organizations: [Organization])
    func numberOfRows(inSection section: Int) -> Int
    func configure(_ cell: OrganizationCell, at indexPath: IndexPath)
    func selectOrganization(at indexPath: IndexPath)
}

class ListOrganizationPresenter: ListOrganizationViewPresenter {
    
    var view: ListOrganizationView
    var organizations: [Organization]
    var organizationSelected: Organization?
    
    required init(view: ListOrganizationView, organizations: [Organization]) {
        self.view = view
        self.organizations = organizations
    }
    
    func numberOfRows(inSection section: Int) -> Int {
        return self.organizations.count
    }
    
    func configure(_ cell: OrganizationCell, at indexPath: IndexPath) {
        let organization = self.organizations[indexPath.row]
        cell.organization = organization
        cell.isSelected = false // Default is unselected
        
        // Check organization is selected.
        if let organizationSelected = self.organizationSelected {
            cell.isSelected = organization.id == organizationSelected.id
        }
    }
    
    func selectOrganization(at indexPath: IndexPath) {
        self.organizationSelected = self.organizations[indexPath.row]
    }
    
}
