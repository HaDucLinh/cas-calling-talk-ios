//
//  MemberDetailPresenter.swift
//  CallingTalk
//
//  Created by Toof on 3/1/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit

// Define function for view

protocol MemberDetailView: BaseViewProtocol {
    func showEmptyReportHistoryLabel(_ isShow: Bool)
    func refresh()
}

// Define function for presenter

protocol MemberDetailViewPresenter: class {
    init(view: MemberDetailView, member: User, noticeStatus: ReadNoticeStatus)
    func numberOfRows(inSection section: Int) -> Int
    func configure(_ view: MemberDetailCell, at indexPath: IndexPath)
    func fetch()
}

// Implement presenter

class MemberDetailPresenter: MemberDetailViewPresenter {
    
    // MARK: Properties
    
    var view: MemberDetailView
    var member: User
    var noticeStatus: ReadNoticeStatus
    fileprivate var memberNotifications: [MemberNotification] = [MemberNotification]()
    
    required init(view: MemberDetailView, member: User, noticeStatus: ReadNoticeStatus) {
        self.view = view
        self.member = member
        self.noticeStatus = noticeStatus
    }
    
    func numberOfRows(inSection section: Int) -> Int {
        self.view.showEmptyReportHistoryLabel(self.memberNotifications.count <= 0)
        return self.memberNotifications.count
    }
    
    func configure(_ view: MemberDetailCell, at indexPath: IndexPath) {
        view.updateDateLabel(Date())
        
        // Dummy Data
        switch self.noticeStatus {
        case .NoNotice:
            break
        case .NoticeNotRead:
            view.updateNoticeStatus(.NoticeNotRead)
        case .NoticeRead:
            view.updateNoticeStatus(.NoticeRead)
        }
    }
    
    func fetch() {
        // Dummy Data
        switch self.noticeStatus {
        case .NoNotice:
            self.memberNotifications = []
        case .NoticeNotRead:
            self.memberNotifications = [
                MemberNotification(), MemberNotification(),
                MemberNotification(), MemberNotification(),
                MemberNotification(), MemberNotification(),
                MemberNotification(), MemberNotification(),
                MemberNotification(), MemberNotification()
            ]
        case .NoticeRead:
            self.memberNotifications = [ MemberNotification() ]
        }
        
        // Update UI
        self.view.refresh()
    }
    
}

