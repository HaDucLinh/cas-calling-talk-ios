//
//  MemberPresenter.swift
//  CallingTalk
//
//  Created by Toof on 2/28/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit

// Define function for view

protocol MemberView: BaseViewProtocol {
    func showEmptyMemberView(_ isShow: Bool)
    func refresh()
}

// Define function for presenter

protocol MemberViewPresenter: class {
    init(view: MemberView)
    func numberOfRows(inSection section: Int) -> Int
    func configure(_ view: MemberRoleAdminCell, at indexPath: IndexPath)
    func transferToMemberDetailPresenter(_ view: MemberDetailView, at indexPath: IndexPath) -> MemberDetailPresenter
    func fetch()
}

// Implement presenter

class MemberPresenter: MemberViewPresenter {
    
    // MARK: Properties
    
    var view: MemberView
    fileprivate var members: [User] = [User]()
    
    required init(view: MemberView) {
        self.view = view
    }
    
    func numberOfRows(inSection section: Int) -> Int {
        self.view.showEmptyMemberView(self.members.count <= 0)
        return self.members.count
    }
    
    func configure(_ view: MemberRoleAdminCell, at indexPath: IndexPath) {
//        let member = self.allUsers[indexPath.row]
        
        if indexPath.row == 1 {
            view.updateNoticeForStatus(.NoticeNotRead)
        }
        else if indexPath.row == 3 {
            view.updateNoticeForStatus(.NoticeRead)
        }
        else {
            view.updateNoticeForStatus(.NoNotice)
        }
    }
    
    func transferToMemberDetailPresenter(_ view: MemberDetailView, at indexPath: IndexPath) -> MemberDetailPresenter {
        
        var noticeStatus: ReadNoticeStatus = .NoNotice
        if indexPath.row == 1 {
            noticeStatus = .NoticeNotRead
        }
        
        if indexPath.row == 3 {
            noticeStatus = .NoticeRead
        }
        
        return MemberDetailPresenter(view: view, member: members[indexPath.row], noticeStatus: noticeStatus)
    }
    
    func fetch() {
        // Handle
        self.members = [
            User(),User(),
            User(),User(),
            User(),User(),
            User(),User(),
            User(),User(),
            User(),User()
        ]
        
        // Update UI
        self.view.refresh()
    }
    
}
