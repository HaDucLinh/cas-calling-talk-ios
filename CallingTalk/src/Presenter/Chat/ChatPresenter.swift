//
//  ChatPresenter.swift
//  CallingTalk
//  Created by Toof on 3/7/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit
import Swinject
import ObjectMapper
// Define function for view

protocol ChatView: BaseViewProtocol {
    func insertMessage(messages: [Message])
    func reloadRowAt(index: Int)
    func updateLogAudioStatus()
    func updateListAudio()
    func willSendMessage(message: Message)
    func didSendMessage(message: Message, fakeId: Int?)
    func refresh()
}

// Define function for presenter

protocol ChatViewPresenter: class {
    init(view: ChatView, talkGroup: TalkGroup?, messages: [Message])
    func reloadData()
    func fetch()
    
    /// Send Message
    func sendMessage(_ content: Any?, type: MessageContentType, isMentionAll: Bool)
}

// Implement presenter

class ChatPresenter: ChatViewPresenter {
    
    // MARK: Properties
    
    var view: ChatView
    var talkGroup: TalkGroup?
    var messages: [Message] = [Message]()
    var voices: [Voice] = []
    var voiceHasNextPage = false
    
    var hasNextPage: Bool = true
    var isLoading: Bool = false
    var isFromTalk: Bool = false
    var voiceIdSelected: Int?
    
    var shouldShowAudioLogMessages: Bool = true {
        didSet {
            self.view.updateLogAudioStatus()
            self.reloadData()
        }
    }
    var audioRecorderService: AudioServicesRecordable {
        get {
            return Container.default.resolve(AudioServicesRecordable.self)!
        }
        set {}
    }
    
    required init(view: ChatView, talkGroup: TalkGroup?, messages: [Message]) {
        self.view = view
        self.talkGroup = talkGroup
        self.messages = messages
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleMarkAsReadNotification(notification:)), name: NSNotification.Name(rawValue: kMarkMessageAsReadEvent), object: nil)
        
        audioRecorderService.audioServicesDidCompletedUpload = { [weak self] result in
            guard let self = self else { return }
            let value = result.value
            if let resultDict = value as? Dictionary<String,Any> {
                guard let message = Mapper<Message>().map(JSON: resultDict["data"] as! [String : Any]) else {
                    return
                }
                if self.addNewMessages(messages: [message], isLoadingMore: false).count > 0 {
                    self.view.refresh()
                }
            }
        }
    }
    
    // MARK: API
    func fetch() {
        guard let id = talkGroup?.id, id != 0 else {
            return
        }
        hud.show()
        isLoading = true
        API.getListMessage(channelID: id, shouldHideAudioLogMessages: !shouldShowAudioLogMessages) { (result, isHasNextPage) in
            DispatchQueue.main.async { [weak self] in
                hud.dismiss()
                guard let `self` = self else { return }
                self.messages.removeAll()
                switch result {
                case .success(let objects):
                    if let messages = objects as? [Message], !messages.isEmpty {
                        _ = self.addNewMessages(messages: messages, isLoadingMore: false)
                    }
                    self.view.refresh()
                    self.isLoading = false // Warning: Set = false after view.refresh()
                case .failure(let error):
                    self.isLoading = false
                    self.view.showErrorAlertWithError(error, completion: { (_) in
                        AppDelegate.share()?.showHomeTabbar()
                    })
                }
            }
        }
    }
    
    func loadMoreMessage(lastMessageID: Int) {
        guard let id = talkGroup?.id, id != 0 else { return }
        hud.show()
        isLoading = true
        print("beginMessageID ", lastMessageID)
        API.loadMoreMessages(channelID: id, lastMessageID: lastMessageID, shouldHideAudioLogMessages: !shouldShowAudioLogMessages) { (result, hasNextPage) in
            hud.dismiss()
            switch result {
            case .success(let objects):
                if let messages = objects as? [Message], !messages.isEmpty {
                    let neededMessages = self.addNewMessages(messages: messages, isLoadingMore: true)
                    if neededMessages.count > 0 {
                        self.view.insertMessage(messages: neededMessages)
                    }
                    self.isLoading = false
                    print("Loadmored: ", messages.count)
                } else {
                    print("Loadmore Messages Empty")
                    self.hasNextPage = false
                }
            case .failure(let error):
                self.isLoading = false
                self.hasNextPage = false
                self.view.showErrorAlertWithError(error, completion: nil)
            }
        }
    }
    
    func sendMessage(_ content: Any?, type: MessageContentType = .text, isMentionAll: Bool = false) {
        guard let id = talkGroup?.id, id != 0 else { return }
        let fakeId = Message.fakeIdForSendingMessage()
        
        let completion: Completion = { [weak self] (result) in
            guard let `self` = self else { return }
            hud.dismiss()
            switch result {
            case .success(let message):
                if let message = message as? Message {
                    if type == .text {
                        self.view.didSendMessage(message: message, fakeId: fakeId)
                    } else {
                        self.view.didSendMessage(message: message, fakeId: nil)
                    }
                }
            case .failure(let error):
                self.view.showErrorAlertWithError(error, completion: nil)
            }
        }

        switch type {
        case .text:
            guard let text = content as? String, !text.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty else { return }
            let message = Message()

            if let users = talkGroup?.allUsers, !users.isEmpty {
                users.forEach { (user) in
                    let idText = "@" + user.id.description
                    if text.contains(idText) {
                        message.mention_users.append(user)
                    }
                }
            }
            message.isMentionAll = isMentionAll
            
            message.id = fakeId
            message.owner = User.getProfile()
            message.content = text
            message.messageType = 2
            message.createTime = Date()
            self.view.willSendMessage(message: message)
            API.sendMessage(channelId: id, content: text, completion: completion)
        case .image:
            guard let image = content as? UIImage else { return }
            do {
                hud.show()
                let imageData = try image.resizeImage()?.rotated(by: CGFloat.zero)?.compressToDataSize()
                API.sendMessage(channelId: id, type: .image, fileData: imageData, completion: completion)
            }
            catch {
                self.view.showErrorAlertWithError(error, completion: nil)
                return
            }
        case .voiceText:
            hud.show()
            guard let voiceId = content as? Int, voiceId != 0 else {
                hud.dismiss()
                return
            }
            API.sendMessage(channelId: id, type: .voiceText, voiceId: voiceId, completion: completion)
        default:
            break
        }
    }
    
    func markAsRead(messageID: Int, index: Int) {
        API.maskAsRead(messageID: messageID) { (result) in
            switch result {
            case .success(_):
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: kLocalMarkMessageAsReadEvent), object: self.talkGroup?.id)
                UITabBarControllerManager.shared.getNotifications()
            case .failure(_):
                break
            }
        }
    }
    
    @objc private func handleMarkAsReadNotification(notification: Notification) {
        guard let object = notification.object as? JSObject,
            let userRead = Mapper<User>().map(JSONObject: object["user"]) else { return }
        
        let channelId: Int = object["channelId"] as! Int
        if channelId != talkGroup?.id {
            return
        }
        
        if talkGroup?.allUsers.enumerated().filter({ $0.element.id == userRead.id }).isEmpty == true {
            return
        }
        
        let seenMessageId: Int = object["messageId"] as! Int
        if let seenMessageTuple = self.messages.enumerated().filter({ $0.element.id == seenMessageId }).first {
            let seenMessage = seenMessageTuple.1
            if seenMessage.seenUsers.index(where: {$0.id == userRead.id}) == nil {
                seenMessage.seenUsers.append(userRead)
                self.view.reloadRowAt(index: seenMessageTuple.0)
            }
        }
        
        let previousSeenMessageId: Int = object["previousReadMessageId"] as! Int
        if previousSeenMessageId != -1 {
            if let previousSeenMessageTuple = self.messages.enumerated().filter({ $0.element.id == previousSeenMessageId }).first {
                let previousSeenMessage = previousSeenMessageTuple.1
                if let userIndex = previousSeenMessage.seenUsers.index(where: {$0.id == userRead.id}) {
                    previousSeenMessage.seenUsers.remove(at: userIndex)
                    self.view.reloadRowAt(index: previousSeenMessageTuple.0)
                }
            }
        }
    }
    
    func reloadData() {
        // Reset then fetch again
        self.hasNextPage = true
        self.fetch()
    }
    
    func addNewMessages(messages: [Message], isLoadingMore: Bool) -> [Message] {
        var supportedFilteredMessages: [Message] = []
        
        messages.forEach({ (message) in
            if isSupportedFilteredMessage(message: message) {
                supportedFilteredMessages.append(message)
            }
        })
        
        if isLoadingMore {
            self.messages.insert(contentsOf: supportedFilteredMessages, at:  0)
        } else {
            self.messages.append(contentsOf: supportedFilteredMessages)
        }
        
        return supportedFilteredMessages
    }
    
    fileprivate func isSupportedMessage(message: Message) -> Bool {
        //  Display message Audio, Text, System Audio in this version
        // return message.messageType == MessageType.SystemAudio.rawValue || message.messageType == MessageType.LogAudio.rawValue
        //  return message.messageType != MessageType.Image.rawValue
        
        return true
    }
    
    fileprivate func isSupportedFilteredMessage(message: Message) -> Bool {
        let isValidFilteredMessageType = shouldShowAudioLogMessages || message.messageType != MessageType.LogAudio.rawValue
        return isSupportedMessage(message: message) && isValidFilteredMessageType
    }
    
    func getListVoiceText() {
        guard let id = talkGroup?.id, id != 0 else { return }
        hud.show()
        API.getListVoiceOfChannel(channelID: id) { (result, hasNextPage) in
            hud.dismiss()
            switch result {
            case .success(let objects):
                if let voices = objects as? [Voice] {
                    self.voices = voices
                    self.voiceHasNextPage = hasNextPage
                    self.view.updateListAudio()
                }
            case .failure(let error):
                self.view.showErrorAlertWithError(error, completion: nil)
            }
        }
    }
}
