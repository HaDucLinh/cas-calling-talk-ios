//
//  AddUserIncomingPresenter.swift
//  CallingTalk
//
//  Created by Toof on 2/21/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import Foundation

// Define invite-user's method

protocol SeenUsersPresenterDelegate: class {
    func didInvitedUsers(_ users: [User])
}

// Define function for view

protocol SeenUsersView: BaseViewProtocol {
    func refresh()
}

// Define function for presenter

protocol SeenUsersViewPresenter: class {
    init(view: SeenUsersView)
    func numberOfRows(inSection section: Int) -> Int
    func configure(_ cell: LargeSeenUserCell, at indexPath: IndexPath)
}

// Implement presenter

class SeenUsersPresenter: SeenUsersViewPresenter {
    
    // MARK: Properties
    
    var view: SeenUsersView
    var users: [User] = [User]()
    weak var delegate: SeenUsersPresenterDelegate?
    
    // MARK: Initial
    
    required init(view: SeenUsersView) {
        self.view = view
    }
    
    func numberOfRows(inSection section: Int) -> Int {
        return self.users.count
    }
    
    func configure(_ cell: LargeSeenUserCell, at indexPath: IndexPath) {
        cell.member = users[indexPath.row]
    }
    
}
