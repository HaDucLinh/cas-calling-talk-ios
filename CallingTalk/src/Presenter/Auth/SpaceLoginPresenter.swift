//
//  SpaceLoginPresenter.swift
//  CallingTalk
//
//  Created by Van Trung on 1/17/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import Foundation
import UIKit

/// Protocol connect main view and presenter
protocol SpaceLoginView: BaseViewProtocol {
    func enableLoginButton()
    func disableLoginButton()
}

class SpaceLoginPresenter: NSObject {
    weak fileprivate var view : SpaceLoginView?
    var spaceId = ""
    /// initialize
    ///
    
    override init() {
        // init state for first launch
    }
    
    /// attach the view to presenter
    ///
    /// - Parameter view: view to attack
    func attachView(_ view: SpaceLoginView){
        self.view = view
    }
    
    /// when text change in spaceid textfield
    ///
    /// - Parameter text: input text
    func spaceIdTextChange(text: String?){
        spaceId = text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
        setLoginButtonStatus()
    }
    
    /// update button status (enable/disable) depend on other input field
    func setLoginButtonStatus(){
        if(spaceId.count > 0){
            view?.enableLoginButton()
            return
        }
        view?.disableLoginButton()
    }
}
