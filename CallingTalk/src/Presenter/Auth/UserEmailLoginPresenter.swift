//
//  UserEmailLoginPresenter.swift
//  CallingTalk
//
//  Created by Van Trung on 1/17/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import Foundation

import UIKit

/// Protocol connect main view and presenter
protocol UserEmailLoginView: BaseViewProtocol {
    func enableLoginButton()
    func disableLoginButton()
}

class UserEmailLoginPresenter: NSObject {
    weak fileprivate var view : UserEmailLoginView?
    var email = ""
    var password = ""
    /// initialize
    ///
    
    override init() {
        // init state for first launch
    }
    
    /// attach the view to presenter
    ///
    /// - Parameter view: view to attack
    func attachView(_ view: UserEmailLoginView){
        self.view = view
    }
    
    /// when text change in email textfield
    ///
    /// - Parameter text: input text
    func emailTextChange(text: String?){
        email = text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
        setLoginButtonStatus()
    }
    
    /// when text change in password textfield
    ///
    /// - Parameter text: input text
    func passwordTextChange(text: String?){
        password = text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
        setLoginButtonStatus()
    }
    
    /// update button status (enable/disable) depend on other input field
    func setLoginButtonStatus(){
        if(email.count > 0 && password.count > 0 && email.isValidEmail()){
            view?.enableLoginButton()
            return
        }
        view?.disableLoginButton()
    }
}
