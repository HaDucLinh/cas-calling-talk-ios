//
//  AdminSMSPresenter.swift
//  CallingTalk
//
//  Created by Toof on 2/13/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit

protocol AdminSMSView: BaseViewProtocol {
    func showAlertForRetrievingError(_ error: Error)
    func showAlertForRetrievingSuccess(_ image: String, message: String, completion: @escaping () -> Void) 
    func updateUIForLoginSuccessWithRole(_ isAdmin: Bool)
}

protocol AdminSMSViewPresenter: class {
    init(view: AdminSMSView, phoneNumber: String)
    func verifyCode(_ smsCode: String)
}

class AdminSMSPresenter: AdminSMSViewPresenter {
    
    var view: AdminSMSView
    var phoneNumber: String
    
    required init(view: AdminSMSView, phoneNumber: String) {
        self.view = view
        self.phoneNumber = phoneNumber
    }
    
    func verifyCode(_ smsCode: String) {
        hud.show()
        API.adminVerify(self.phoneNumber, code: smsCode) { [weak self] (result) in
            guard let `self` = self else { return }
            
            hud.dismiss()
            switch result {
            case .success(let profileData):
                guard   let profileData = profileData as? JSObject,
                        let profile = profileData["profile"] as? User,
                        let message = profileData["message"] as? String
                else    { return }
                
                // Save user's profile & token id
                Helpers.saveProfile(profile)
                
                self.view.showAlertForRetrievingSuccess("ic_valid_sms_success", message: message, completion: { [weak self] in
                    guard let `self` = self else { return }
                    self.view.updateUIForLoginSuccessWithRole(profile.isAdmin)
                })
                // Connect socket
                SocketIOManager.sharedInstance.connectSocket()
            case .failure(let error):
                self.view.showAlertForRetrievingError(error)
            }
        }
    }
    
    func resendVerifyCode() {
        guard !self.phoneNumber.isEmpty else { return }
        
        hud.show()
        API.adminLogin(phoneNumber) { (result) in
            DispatchQueue.main.async { [weak self] in
                guard let `self` = self else { return }
                
                hud.dismiss()
                switch result {
                case .success(_): break
                case .failure(let error):
                    self.view.showErrorAlertWithError(error, completion: nil)
                }
            }
        }
    }
}

