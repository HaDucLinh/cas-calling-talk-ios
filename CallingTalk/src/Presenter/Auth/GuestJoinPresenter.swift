//
//  GuestJoinPresenter.swift
//  CallingTalk
//
//  Created by Van Trung on 1/17/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import Foundation
import UIKit

/// Protocol connect view and presenter
protocol GuestJoinView: BaseViewProtocol {
    func enableLoginButton()
    func disableLoginButton()
}

class GuestJoinPresenter: NSObject {
    weak fileprivate var view : GuestJoinView?
    var userName: String = ""
    var memberId: String = ""
    /// initialize
    ///
    
    override init() {
        // init state for first launch
    }
    
    /// attach the view to presenter
    ///
    /// - Parameter view: view to attack
    func attachView(_ view: GuestJoinView){
        self.view = view
    }
    
    /// when text change in username textfield
    ///
    /// - Parameter text: input text
    func usernameTextChange(text: String?){
        userName = text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
        setLoginButtonStatus()
    }
    
    /// when text change in memberId textfield
    ///
    /// - Parameter text: input text
    func memberIdTextChange(text: String?){
        memberId = text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
        setLoginButtonStatus()
    }
    
    /// update button status (enable/disable) depend on other input field
    func setLoginButtonStatus(){
        if(userName.count > 0 && memberId.count > 0){
            view?.enableLoginButton()
            return
        }
        view?.disableLoginButton()
    }
}
