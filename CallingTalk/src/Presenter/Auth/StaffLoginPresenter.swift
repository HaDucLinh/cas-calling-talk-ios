//
//  GuestJoinPresenter.swift
//  CallingTalk
//
//  Created by Van Trung on 1/17/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit

/// Protocol connect view and presenter
protocol StaffLoginView: BaseViewProtocol {
    func enableMettingButton()
    func disableMettingButton()
    func updateUIForLoginSuccessWithRole(_ isAdmin: Bool)
}

class StaffLoginPresenter: NSObject {
    
    weak fileprivate var view : StaffLoginView?
    var userName: String = "" {
        didSet {
            setLoginButtonStatus()
        }
    }
    var avatarImage: UIImage?
    
    override init() {
        
    }

    func attachView(_ view: StaffLoginView) {
        self.view = view
    }
 
    func usernameTextChange(text: String?) {
        userName = text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
//        setLoginButtonStatus()
    }
    
    /// update button status (enable/disable) depend on other input field
    func setLoginButtonStatus() {
        if (userName.count > 0) {
            view?.enableMettingButton()
            return
        }
        
        self.view?.disableMettingButton()
    }
    
    func login(username: String, avatarImage: UIImage?, avataIndex: Int, countryCode: String) {
        self.userName = username
        self.avatarImage = avatarImage
        
        do {
            hud.show()
            let imageData = try self.avatarImage?.resizeImage()?.compressToDataSize()
            API.staffLogin(username, avatar: imageData!, avatarIndex: avataIndex, countryCode: countryCode) { [weak self] (result) in
                guard let `self` = self else { return }
                
                hud.dismiss()
                switch result {
                case .success(let profileData):
                    guard let profile = profileData as? User else { return }
                    // Save user's profile & token id
                    Helpers.saveProfile(profile)
                    
                    self.view?.updateUIForLoginSuccessWithRole(profile.isAdmin)
                    
                    // Connect socket
                    SocketIOManager.sharedInstance.connectSocket()
                case .failure(let error):
                    self.view?.showErrorAlertWithError(error, completion: nil)
                }
            }
        }
        catch {
            self.view?.showErrorAlertWithError(error, completion: nil)
        }
    }
}
