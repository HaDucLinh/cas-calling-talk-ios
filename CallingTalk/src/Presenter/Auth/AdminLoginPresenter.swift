//
//  AdminLoginPresenter.swift
//  CallingTalk
//
//  Created by Toof on 2/13/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit

// Define function for view

protocol AdminLoginView: BaseViewProtocol {
    func enableSMSButton(_ isEnable: Bool)
    func showPhoneNumberErrorLabel(_ isShow: Bool)
    func updateUIForLoginSuccess()
}

// Define function for presenter

protocol AdminLoginViewPresenter: class {
    init(view: AdminLoginView)
    func setSMSButtonStatus()
    func setPhoneNumberErrorLabelStatus()
    func phoneNumberTextChange(_ text: String?)
    func transferToAdminSMSPresenter(_ view: AdminSMSVC) -> AdminSMSPresenter?
    func login()
}

// Implement presenter

class AdminLoginPresenter: AdminLoginViewPresenter {
    
    var view: AdminLoginView
    var phoneNumber: String?
    
    required init(view: AdminLoginView) {
        self.view = view
    }
    
    func setSMSButtonStatus() {
        if  let phoneNumber = self.phoneNumber,
            phoneNumber.isValidPhone() {
            self.view.enableSMSButton(true)
            return
        }
        self.view.enableSMSButton(false)
    }
    
    func setPhoneNumberErrorLabelStatus() {
        if  let phoneNumber = self.phoneNumber, phoneNumber.trimmingCharacters(in: .whitespacesAndNewlines) != "",
            !phoneNumber.isValidPhone() {
            self.view.showPhoneNumberErrorLabel(true)
        }
        else {
            self.view.showPhoneNumberErrorLabel(false)
        }
    }
    
    func phoneNumberTextChange(_ text: String?) {
        self.phoneNumber = text
        self.setPhoneNumberErrorLabelStatus()
        self.setSMSButtonStatus()
    }
    
    func transferToAdminSMSPresenter(_ view: AdminSMSVC) -> AdminSMSPresenter? {
        guard let phoneNumber = self.phoneNumber else { return nil }
        return AdminSMSPresenter(view: view, phoneNumber: phoneNumber)
    }
    
    func login() {
        guard let phoneNumber = self.phoneNumber else { return }
        
        hud.show()
        self.view.enableSMSButton(false)
        API.adminLogin(phoneNumber) { [weak self] (result) in
            guard let `self` = self else { return }
            
            hud.dismiss()
            self.view.enableSMSButton(true)
            switch result {
            case .success(_):
                self.view.updateUIForLoginSuccess()
            case .failure(let error):
                self.view.showErrorAlertWithError(error, completion: nil)
            }
        }
    }
    
}
