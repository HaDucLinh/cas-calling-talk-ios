//
//  QRScanPresenter.swift
//  CallingTalk
//
//  Created by Toof on 2/19/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit

// Define function for view

protocol QRScanView: BaseViewProtocol {
    func showAlertForRetrievingError(_ message: String)
    func dismissQRScanVCWithQRCode(_ code: String)
}

// Define function for presenter

protocol QRScanViewPresenter: class {
    init(view: QRScanView)
    func joinTalkGroupBy(qrCode: String)
}

// Implement presenter

class QRScanPresenter: QRScanViewPresenter {
    
    // MARK: Properties
    
    var view: QRScanView
    
    // MARK: Initial
    
    required init(view: QRScanView) {
        self.view = view
    }
    
    func joinTalkGroupBy(qrCode: String) {
        if qrCode.isValidQRCodeDomain() {
            self.view.dismissQRScanVCWithQRCode(qrCode)
        }
        else {
            self.view.showAlertForRetrievingError(NSLocalizedString("qr_scan_fail_notice_text", comment: ""))
        }
    }
    
}

