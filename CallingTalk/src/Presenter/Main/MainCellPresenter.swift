//
//  MainCellPresenter.swift
//  CallingTalk
//
//  Created by Toof on 2/15/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit

protocol MainCellPresenterDelegate: class {
    func transferToTalkDetailPresenter(_ presenter: TalkDetailPresenter)
    func transferToChatPresenter(_ talkGroup: TalkGroup)
    //etc...
}

// Define function for view

protocol MainCellView: NSObjectProtocol {
    func updateUI(talkGroup: TalkGroup)
}

// Define function for presenter

protocol MainCellViewPresenter: class {
    init(view: MainCellView, talkGroup: TalkGroup)
    func handleForShowingTalkGroupDetail()
    func handleForShowingChatInGroup()
    func updateUI()
}

// Implement presenter

class MainCellPresenter: MainCellViewPresenter {
    
    // MARK: Properties
    
    var view: MainCellView
    var talkGroup: TalkGroup
    weak var delegate: MainCellPresenterDelegate?
    
    // MARK: Initial
    
    required init(view: MainCellView, talkGroup: TalkGroup) {
        self.view = view
        self.talkGroup = talkGroup
    }
    
    func updateUI() {
        self.view.updateUI(talkGroup: talkGroup)
    }
    
    func handleForShowingTalkGroupDetail() {
        let talkDetailPresenter = TalkDetailPresenter()
        talkDetailPresenter.talkGroupId = self.talkGroup.id
        self.delegate?.transferToTalkDetailPresenter(talkDetailPresenter)
    }
    
    func handleForShowingChatInGroup() {
        self.delegate?.transferToChatPresenter(self.talkGroup)
    }
}


