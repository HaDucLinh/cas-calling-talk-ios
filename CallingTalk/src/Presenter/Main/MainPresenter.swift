//
//  MainPresenter.swift
//  CallingTalk
//
//  Created by Toof on 2/14/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit

// Define function for view

protocol MainView: BaseViewProtocol {
    func showConfirmAlertViewForJoinTalkGroup(_ talkGroup: TalkGroup, code: String)
    func showConfirmAlertViewForInviteTalkGroup(_ talkGroup: TalkGroup)
    func showIncomingVCWithTalkGroupId(_ id: Int)
    func showTalkDetailVC(_ presenter: TalkDetailPresenter)
    func showChatGroup(_ talkGroup: TalkGroup)
    func showTutorialPopup(_ isShow: Bool)
    func showEmptyTalkView(_ isShow: Bool)
    func insertTalkGroups(talkGroups: [TalkGroup])
    func reloadRowAt(indexPath: IndexPath)
    func updateView()
    func hasNewEvent()
    func clearSearchBar()
}

// Define function for presenter

protocol MainViewPresenter: class {
    init(view: MainView)
    func accessAtFirstTime()
    func numberOfRows(inSection section: Int) -> Int
    func didSelectRow(at indexPath: IndexPath)
    func transferToRowPresenter(_ view: MainCellView, at indexPath: IndexPath) -> MainCellPresenter
    func fetch(isPullToRefresh: Bool, completion: RefreshDoneCompletion?)
    func getMyProfile(_ completion: (() -> Void)?)
    func verifyTalkGroupByQRCode(_ code: String)
    func renewTalkGroup(channnelId: Int, userId: Int, numberOfCalling: Int, socketEvent: SocketEvent)
    func resetListTalkGroup(completion: RefreshDoneCompletion?)
}

// Implement presenter
typealias RefreshDoneCompletion = () -> Void
class MainPresenter: MainViewPresenter {
    
    // MARK: Properties
    
    var view: MainView
    var talkGroups: [TalkGroup] = [TalkGroup]()
    var searchedTalkGroups: [TalkGroup] = [TalkGroup]()
    var searchText: String = ""
    var isSearching: Bool = false
    
    var hasMorePages = false
    var hasMoreSearchPage = false
    var searchHasResponed = false

    fileprivate var maxDate = Date()
    fileprivate var minDate = Date()
    
    fileprivate var enableUpdateUISocket: Bool = true
    fileprivate var socketNotifications: [Notification] = [Notification]()
    
    var completion: RefreshDoneCompletion?
    
    required init(view: MainView) {
        self.view = view
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleNotification(notification:)), name: NSNotification.Name(rawValue: kMemberJoinOrLeaveTalkEvent), object: nil)
    }
    
    @objc private func handleNotification(notification: Notification) {
        if self.enableUpdateUISocket {
            self.executeNotification(notification)
        }
        else {
            self.socketNotifications.append(notification)
        }
    }
    
    fileprivate func executeNotification(_ notification: Notification) {
        guard let object = notification.object as? JSObject else { return }
        if  let channelId = object["channelId"] as? Int,
            let userId = object["userId"] as? Int,
            let numberOfCalling = object["inCalling"] as? Int,
            let socketEvent = object["socketEvent"] as? SocketEvent {
            // Don't need to handle the case when the current user left the talk group
            // Because it will be refreshed after leaving successfully
            if !(socketEvent == SocketEvent.LeftTalkGroup && User.getProfile()?.id == userId) {
                self.renewTalkGroup(channnelId: channelId, userId: userId, numberOfCalling: numberOfCalling, socketEvent: socketEvent)
            }
        }
    }
    
    func renewTalkGroup(channnelId: Int, userId: Int, numberOfCalling: Int, socketEvent: SocketEvent) {
        if isSearching {
            if let searchTuple = searchedTalkGroups.enumerated().filter({ $0.element.id == channnelId}).first {
                self.renewUsersInTalkGroup(searchTuple, userId: userId, numberOfCalling: numberOfCalling, socketEvent: socketEvent, needReloadRow: true)
            }
            if let tuple = self.talkGroups.enumerated().filter({ $0.element.id == channnelId }).first {
                self.renewUsersInTalkGroup(tuple, userId: userId, numberOfCalling: numberOfCalling, socketEvent: socketEvent, needReloadRow: false)
            }
        } else {
            if let tuple = self.talkGroups.enumerated().filter({ $0.element.id == channnelId }).first {
                self.renewUsersInTalkGroup(tuple, userId: userId, numberOfCalling: numberOfCalling, socketEvent: socketEvent, needReloadRow: true)
            }
        }
    }
    
    fileprivate
    func renewUsersInTalkGroup(_ tuple: (Int, TalkGroup), userId: Int, numberOfCalling: Int, socketEvent: SocketEvent, needReloadRow: Bool) {
        let talkGroup = tuple.1
        
        if let user = Array(talkGroup.allUsers).filter({$0.id == userId}).first {
            updateUserInTalk(tuple, user: user, numberOfCalling: numberOfCalling, socketEvent: socketEvent, needReloadRow: needReloadRow)
        } else {
            API.detailUserInTalkGroup(userId, talkId: talkGroup.id) { (result) in
                DispatchQueue.main.async { [weak self] in
                    guard let `self` = self else { return }
                    
                    switch result {
                    case .success(let user):
                        guard let user = user as? User else { return }
                        if socketEvent == SocketEvent.JoinedChannel {
                            talkGroup.joinUsersCount += 1
                            talkGroup.allUsers.insert(user, at: 0)
                        }
                        self.updateUserInTalk(tuple, user: user, numberOfCalling: numberOfCalling, socketEvent: socketEvent, needReloadRow: needReloadRow)
                    case .failure(_): return
                    }
                }
            }
        }
    }
    
    private func updateUserInTalk(_ tuple: (Int, TalkGroup), user: User, numberOfCalling: Int, socketEvent: SocketEvent, needReloadRow: Bool) {
        let talkGroup = tuple.1
        let index = tuple.0
        
        switch socketEvent {
        case .JoinedChannel:
            talkGroup.meetingUserCount = numberOfCalling
            if Array(talkGroup.meetingUsers).enumerated().filter({ $0.element.id == user.id }).first == nil {
                talkGroup.meetingUsers.insert(user, at: 0)
            }
        case .LeftChannel:
            talkGroup.meetingUserCount = numberOfCalling
            if let userTuple: (Int, User) = Array(talkGroup.meetingUsers).enumerated().filter({ $0.element.id == user.id }).first {
                let index = userTuple.0
                talkGroup.meetingUsers.remove(at: index)
            }
        case .LeftTalkGroup:
            talkGroup.joinUsersCount -= 1
            if let userTuple: (Int, User) = Array(talkGroup.allUsers).enumerated().filter({ $0.element.id == user.id }).first {
                let index = userTuple.0
                talkGroup.allUsers.remove(at: index)
            }
        default: break
        }
        
        if needReloadRow {
            let indexPath = IndexPath(row: index, section: 0)
            view.reloadRowAt(indexPath: indexPath)
        }
    }
    
    func accessAtFirstTime() {
        if numberOfRows(inSection: 0) == 0 {
            self.view.showTutorialPopup(true)
        } else {
            self.view.showTutorialPopup(false)
        }
    }
    
    func numberOfRows(inSection section: Int) -> Int {
        let talks = isSearching ? searchedTalkGroups : self.talkGroups
        self.view.showEmptyTalkView(talks.count == 0)
        return talks.count
    }
    
    func didSelectRow(at indexPath: IndexPath) {
        let talks = (isSearching && searchHasResponed) ? searchedTalkGroups : self.talkGroups
        if talks[indexPath.row].isInvited {
            self.view.showConfirmAlertViewForInviteTalkGroup(talks[indexPath.row])
        } else {
            self.view.showIncomingVCWithTalkGroupId(talks[indexPath.row].id)
        }
    }
    
    func transferToRowPresenter(_ view: MainCellView, at indexPath: IndexPath) -> MainCellPresenter {
        let talks = (isSearching && searchHasResponed) ? searchedTalkGroups : self.talkGroups
        let cellPresenter = MainCellPresenter(view: view, talkGroup: talks[indexPath.row])
        cellPresenter.delegate = self
        cellPresenter.updateUI()
        
        return cellPresenter
    }
    
    // MARK: APIs
    func fetch(isPullToRefresh: Bool = false, completion: RefreshDoneCompletion?) {
        if !isPullToRefresh {
            hud.show()
        }
        self.isSearching = false
        self.hasMorePages = false
        self.completion = completion
        self.minDate = Date()
        self.maxDate = Date()
        
        print("Start Time: ", self.maxDate.stringToJapanTime())
        print("End Time: ", self.maxDate.stringToJapanTime())
        
        let minDateString = self.minDate.stringToJapanTime()
        let maxDateString = self.maxDate.stringToJapanTime()

        self.enableUpdateUISocket = false
        API.listTalkGroups(searchText: nil, maxDate: maxDateString, minDate: minDateString) { (result, hasNewEvents, hasMorePage) in
            DispatchQueue.main.async { [weak self] in
                guard let `self` = self else { return }
                hud.dismiss()
                self.completion?()
                switch result {
                case .success(let talks):
                    self.hasMorePages = hasMorePage
                    if let talks = talks as? [TalkGroup] {
                        self.talkGroups = talks
                    }
                    self.view.updateView()
                    
                    // Update UI for receiving socket
                    for notification in self.socketNotifications {
                        self.executeNotification(notification)
                    }
                case .failure(let error):
                    if UIApplication.topViewController() is MainVC {
                        self.view.showErrorAlertWithError(error, completion: nil)
                    }
                }
                
                // Release cache for socket's notification
                self.socketNotifications = []
                self.enableUpdateUISocket = true
            }
        }
    }
    
    func willLoadMoreTalkGroups(completion: RefreshDoneCompletion?) {
        if self.isSearching {
            if !self.hasMoreSearchPage { return }
            if let minEventTime = searchedTalkGroups.last?.eventTime {
                self.completion = completion
                
                let minDate = minEventTime.adding(.second, value: -1)
                // searchRemoteTalkGroups(text: searchText, minDate: minDate) {}
                
                loadMoreSearchTalkgroups(text: searchText, minDate: minDate)
            }
        } else {
            if !self.hasMorePages { return }
            if let minEventTime = talkGroups.last?.eventTime {
                self.completion = completion
                loadMoreTalkGroups(maxTime: self.maxDate, minTime: minEventTime.adding(.second, value: -1), searchText: searchText, completion: completion)
            }
        }
    }
    
    @objc func loadMoreTalkGroups(maxTime: Date, minTime: Date, searchText: String, completion: RefreshDoneCompletion?) {
        print("Load More Start Time: ", maxTime.string(withFormat: "yyyy-MM-dd HH:mm:ss"))
        print("Load More End Time: ", minTime.string(withFormat: "yyyy-MM-dd HH:mm:ss"))
        
        let maxTimeString = maxTime.stringToJapanTime()
        // minTime receive from DB so it is JP time, Don't need convert to JP time again
        let minTimeString = minTime.string(withFormat: kDateGetListChannel)

        hud.show()
        API.listTalkGroups(searchText: searchText, maxDate: maxTimeString, minDate: minTimeString) { (result, hasNewEvents, hasMorePages) in
            DispatchQueue.main.async { [weak self] in
                guard let `self` = self else { return }
                
                hud.dismiss()
                self.completion?()
                switch result {
                case .success(let talks):
                    self.hasMorePages = hasMorePages

                    if let talks = talks as? [TalkGroup] {
                        self.view.insertTalkGroups(talkGroups: talks)
                    }
                case .failure( _): break
                }
            }
        }
    }
    
    // MARK: APIs Search Talk groups

    func searchTalkGroups(name: String?, isPullToRefresh: Bool = false, completion: RefreshDoneCompletion?) {
        guard let text = name?.lowercased(), !text.isEmpty else {
            self.searchHasResponed = true
            completion?()
            return
        }
        searchText = text
        self.hasMoreSearchPage = false
        
        searchRemoteTalkGroups(text: searchText, isPullToRefresh: isPullToRefresh, completion: completion)
    }
    
    private func searchLocalTalkGroups(text: String) {
        guard !talkGroups.isEmpty else { return }
        talkGroups.forEach { (talk) in
            var organizeName = ""
            if let organization = talk.organization {
                organizeName = organization.name
            }
            var isMember = false
            talk.allUsers.forEach({ (user) in
                isMember = isMember || user.name.lowercased().contains(text)
            })
            if talk.name.lowercased().contains(text) || organizeName.lowercased().contains(text) || isMember {
                self.searchedTalkGroups.append(talk)
            }
        }
        self.view.updateView()
    }
    
    private func searchRemoteTalkGroups(text: String, minDate: Date? = nil, isPullToRefresh: Bool = false, completion: RefreshDoneCompletion?) {
        print("Search: ", text)
        if !isPullToRefresh {
            hud.show()
        }
        searchHasResponed = false
        self.completion = completion
        let minDateString = minDate?.stringToJapanTime()

        API.listTalkGroups(searchText: text, maxDate:  nil, minDate: minDateString) { (result, hasNewEvents, hasMorePages) in
            DispatchQueue.main.async { [weak self] in
                guard let `self` = self else { return }
                
                hud.dismiss()
                self.completion?()
                
                switch result {
                case .success(let talks):
                    self.searchHasResponed = true
                    
                    var searchedTalks: [TalkGroup] = []
                    if let talks = talks as? [TalkGroup] {
                        searchedTalks = talks
                    }
                    
                    if text != self.searchText {
                        return
                    }
                    
                    if !self.isSearching {
                        return
                    }
                    
                    self.searchedTalkGroups = searchedTalks
                    self.view.updateView()
                    self.hasMoreSearchPage = hasMorePages
                    
                case .failure( _):
                    print("Error while seaching")
                    break
                }
            }
        }
    }
    
    private func loadMoreSearchTalkgroups(text: String, minDate: Date) {
        hud.show()
        self.searchHasResponed = false
        let minTimeString = minDate.string(withFormat: kDateGetListChannel)

        API.listTalkGroups(searchText: text, maxDate: nil, minDate: minTimeString) { (result, hasNewEvents, hasMorePages) in
            DispatchQueue.main.async { [weak self] in
                guard let `self` = self else { return }
                
                hud.dismiss()
                self.completion?()
                self.searchHasResponed = true

                switch result {
                case .success(let talks):
                    self.hasMoreSearchPage = hasMorePages

                    if let talks = talks as? [TalkGroup] {
                        self.view.insertTalkGroups(talkGroups: talks)
                    }
                case .failure( _): break
                }
            }
        }
    }
    
    func getMyProfile(_ completion: (() -> Void)? = nil) {
        hud.show()
        API.getMyProfile { (result) in
            DispatchQueue.main.async { [weak self] in
                guard let `self` = self else { return }
                hud.dismiss()
                switch result {
                case .success(let profileData):
                    guard let profile = profileData as? User else { return }
                    
                    // Save user's profile & token id
                    Helpers.saveProfile(profile)
                    completion?()
                case .failure(let error):
                    self.view.showErrorAlertWithError(error, completion: nil)
                }
            }
        }
    }
    
    func verifyTalkGroupByQRCode(_ code: String) {
        if let channelId = code.components(separatedBy: "/").last {
            hud.show()
            API.verifyQRCode(channelId) { (result) in
                DispatchQueue.main.async { [weak self] in
                    guard let `self` = self else { return }
                    hud.dismiss()
                    switch result {
                    case .success(let talkGroup):
                        guard let talkGroup = talkGroup as? TalkGroup else { return }
                        self.view.showConfirmAlertViewForJoinTalkGroup(talkGroup, code: code)
                    case .failure(let error):
                        self.view.showErrorAlertWithError(error, completion: nil)
                    }
                }
            }
        }
    }
    
    func resetListTalkGroup(completion: RefreshDoneCompletion? = nil) {
        // Clear searching's cache
        self.searchText = ""
        self.hasMoreSearchPage = false
        self.searchHasResponed = false
        self.searchedTalkGroups.removeAll()
        self.view.clearSearchBar()
        
        // Fetch all data
        self.fetch(completion: completion)
        // Re-fetch notifications
        UITabBarControllerManager.shared.getNotifications()
    }
}

// MARK: - MainCellPresenterDelegate

extension MainPresenter: MainCellPresenterDelegate {
    func transferToTalkDetailPresenter(_ presenter: TalkDetailPresenter) {
        self.view.showTalkDetailVC(presenter)
    }
    
    func transferToChatPresenter(_ talkGroup: TalkGroup) {
        self.view.showChatGroup(talkGroup)
    }
}
