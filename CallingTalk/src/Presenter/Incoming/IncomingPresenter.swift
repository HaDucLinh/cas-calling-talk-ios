//
//  IncomingPresenter.swift
//  CallingTalk
//
//  Created by Toof on 2/19/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit
import RealmS
import ObjectMapper
import Swinject
public enum IncomingMode {
    case Push
    case HandFree
}

public enum MicroState {
    case MicroON
    case MicroOFF
}

// Define function for view

protocol IncomingView: BaseViewProtocol {
    func reloadUI(talkGroup: TalkGroup)
    func reloadRowAt(indexPath: IndexPath)
    func updateHandFreeModeUIForMicroState(_ state: MicroState)
    func updateUIForIncomingMode(_ mode: IncomingMode)
    func showAlertForDetailOfMember(_ member: User, talkGroup: TalkGroup)
    func showAlertForLeavingOfMember(_ message: String)
    func showAlertForJoiningOfMember(_ message: String)
    func showAlertForRetrievingMessage(title: String, message: String)
    func joinTalkRoom(withChannelSid channelSid: String)
    func updateNumberOfUnreadMessage(_ number: Int?)
    func deletedMemberInTalkGroup(talkID: Int, userId: Int, shouldShowToast: Bool)
    func dismissVC()
}

// Define function for presenter

protocol IncomingViewPresenter: class {
    init(view: IncomingView, talkGroupId: Int)
    func handleSpeaking()
    func handleIncomingMode()
    func numberOfRows(inSection section: Int) -> Int
    func configure(_ cell: IncomingMemberCell, at indexPath: IndexPath)
    func handleSelectMember(at indexPath: IndexPath)
    func transferToAddUserIncomingPresenter(_ view: AddUserIncomingVC) -> AddUserIncomingPresenter
    func renewTalkGroup(userId: Int, numberOfCalling: Int, socketEvent: SocketEvent)
    func getChannelSid()
    func fetch()
    // MARK: callback delegate
    var meetingAudioChatServicesDidJoinedRoom: (() -> Void)? { get set }
    var meetingAudioChatServicesDidLeftRoom: (() -> Void)? { get set }
    var meetingAudioChatServicesDidErrorWithoutReconnect: (() -> Void)? { get set }
    var meetingAudioChatServicesDidErrorWithReconnect: (() -> Void)? { get set }
    func clearChatServices()
    func audioServicesCanTalking() -> Bool
}

// Implement presenter

class IncomingPresenter: IncomingViewPresenter {
    
    var meetingAudioChatServicesDidLeftRoom: (() -> Void)?
    var meetingAudioChatServicesDidJoinedRoom: (() -> Void)?
    var meetingAudioChatServicesDidErrorWithoutReconnect: (() -> Void)?
    var meetingAudioChatServicesDidErrorWithReconnect: (() -> Void)?
    var allTalkingUser: Set<Int> = Set<Int>()
    var shouldUpdateMemberStateContinuously: Bool = false
    // MARK: Properties
    
    var view: IncomingView
    var talkGroupId: Int
    
    var talkGroup: TalkGroup = TalkGroup()
    var mode: IncomingMode = .HandFree
    var microState: MicroState = .MicroOFF
    
    // Mark Chat Audio services injector
    private var meetingAudioChatServices: MeetingAudioServiceable? = nil {
        didSet {
            if let audioChatServices = meetingAudioChatServices {
                if audioChatServices.isMute {
                    self.updateJitsiAudioMuteMode(isIncomingModeChanged: false)
                } else {
                    self.microState = .MicroON
                    self.view.updateHandFreeModeUIForMicroState(self.microState)
                }
            }
        }
    }
    // Audio Recorder Service
    private var audioRecorderService: AudioServicesRecordable {
        return Container.default.resolve(AudioServicesRecordable.self)!
    }
    var broadcastClientServices: ClientBroadcastable {
        get {
            return Container.default.resolve(ClientBroadcastable.self)!
        }
        set {}
    }
    // MARK: Initial
    
    required init(view: IncomingView, talkGroupId: Int) {
        self.view = view
        self.talkGroupId = talkGroupId
        self.talkGroup.allUsers.forEach { (user) in
            user.isCalling = false
        }
        NotificationCenter.default.addObserver(self, selector: #selector(didReceiveReleaseChatNotification(_:)), name: Notification.Name.ReleaseChatNotification, object: nil)
    }
    
    func handleSpeaking() {
        self.updateJitsiAudioMuteMode(isIncomingModeChanged: false)
    }
    
    fileprivate func updateJitsiAudioMuteMode(isIncomingModeChanged: Bool) {
        if isIncomingModeChanged {
            handleMuteChat(shouldMuteChat: (self.mode == .Push))
        } else {
            handleMuteChat(shouldMuteChat: (self.microState == .MicroON))
            if self.mode == .HandFree {
                self.speechTextMicroMode(oldMode: self.microState)
            }
        }
    }
    
    fileprivate func handleMuteChat(shouldMuteChat: Bool) {
        if let audioChatServices = self.meetingAudioChatServices {
            shouldMuteChat ? audioChatServices.muteChat() : audioChatServices.unMuteChat()
        }
    }
    
    func setDefaultMicrophoneMode() {
        handleMuteChat(shouldMuteChat: (self.mode == .Push))
        handleUserRecord(isStartRecording: false)
    }
    
    func handleProximitySensorDetected() {
        handleMuteChat(shouldMuteChat: true)
        handleUserRecord(isStartRecording: false)
    }
    
    func handleIncomingMode() {
        self.mode =  (self.mode == .Push) ? .HandFree : .Push
        // Update default micro state based on incoming mode
        updateJitsiAudioMuteMode(isIncomingModeChanged: true)
        // Stop the current record if applicable
        handleUserRecord(isStartRecording: false)
        // Start/Stop the smart record
        handleSmartRecord(isStartRecording: self.mode == .HandFree)
        
        // Update UI
        self.view.updateUIForIncomingMode(self.mode)
        self.speechTextIncomingMode(mode: self.mode)
    }
    
    fileprivate func handleSmartRecord(isStartRecording: Bool) {
        if isStartRecording {
            let user = User.getProfile()
            let userName = user?.name ?? ""
            audioRecorderService.startSmartRecord(withID: talkGroupId, andUserName: userName)
        } else { // .Push
            audioRecorderService.releaseSmartRecord()
        }
    }
    
    func handleUserRecord(isStartRecording: Bool) {
        if isStartRecording {
            let user = User.getProfile()
            let userName = user?.name ?? ""
            audioRecorderService.startUserRecord(withID: talkGroupId, andUserName: userName)
        } else {
            audioRecorderService.stopUserRecord()
        }
    }
    
    func numberOfRows(inSection section: Int) -> Int {
        return self.talkGroup.allUsers.count
        //return self.talkGroup.allUsers.count + 1
    }
    
    func configure(_ cell: IncomingMemberCell, at indexPath: IndexPath) {
        //                if indexPath.item == 0 {
        //                    cell.updateUIWithAvatarImage(UIImage(named: "ic_add_user_item_incoming"), name: NSLocalizedString("add_member_label_text", comment: ""))
        //                }
        //                else {
        //                let member = self.talkGroup.allUsers[indexPath.item]
        //                cell.user = member
        //                }
        let userForCell = self.talkGroup.allUsers[indexPath.item]
        cell.user = userForCell
        cell.isFromIncomming = true // Status of online/offline
        cell.isSpeaking = self.allTalkingUser.contains(userForCell.id)// Status of talking or not
    }
    
    func handleSelectMember(at indexPath: IndexPath) {
        // let memberSelected = self.talkGroup.allUsers[indexPath.item - 1]
        let memberSelected = self.talkGroup.allUsers[indexPath.item]
        self.view.showAlertForDetailOfMember(memberSelected, talkGroup: self.talkGroup)
    }
    
    func transferToAddUserIncomingPresenter(_ view: AddUserIncomingVC) -> AddUserIncomingPresenter {
        let addUserIncomingPresenter = AddUserIncomingPresenter(view: view)
        addUserIncomingPresenter.delegate = self
        
        return addUserIncomingPresenter
    }
    
    // MARK: APIs
    // get channel sid by channel id
    func getChannelSid() {
        hud.show()
        API.getChannelSid(channelId: self.talkGroupId) { (result) in
            DispatchQueue.main.async { [weak self] in
                guard let `self` = self else { return }
                switch result {
                case .success(let channelSid):
                    print("ChannelSID....: ", channelSid)
                    guard let channelSid = channelSid as? String else { return }
                    self.view.joinTalkRoom(withChannelSid: channelSid)
                case .failure(let error):
                    hud.dismiss()
                    self.view.showErrorAlertWithError(error, completion: nil)
                }
            }
        }
    }
    
    func fetch() {
        hud.show()
        print("hide start requset")
        API.detailTalkGroup(self.talkGroupId) { (result) in
            DispatchQueue.main.async { [weak self] in
                guard let `self` = self else { return }
                hud.dismiss()
                print("hide huddddd")
                switch result {
                case .success(let talkGroup):
                    guard let talkGroup = talkGroup as? TalkGroup else { return }
                    self.talkGroup = talkGroup
                    self.notifyUI()
                    self.view.updateNumberOfUnreadMessage(talkGroup.isNewUnreadMessage ? talkGroup.numberOfUnreadMessageMention : nil)
                    self.getChannelSid()
                case .failure(let error):
                    self.view.showErrorAlertWithError(error, completion: { (_) in
                        self.view.dismissVC()
                    })
                }
            }
        }
    }
    
    func handleTalkGroupChanged() {
        API.detailTalkGroup(self.talkGroupId) { (result) in
            DispatchQueue.main.async { [weak self] in
                guard let `self` = self else { return }
                switch result {
                case .success(let talkGroup):
                    guard let talkGroup = talkGroup as? TalkGroup else { return }
                    self.talkGroup = talkGroup
                    self.notifyUI()
                case .failure(_): break
                }
            }
        }
    }
    
    func initializeMeetingAudioChatServices(withHost host: String, andRoom room:String, withView view: UIView) {
        guard let user = User.getProfile() else {
            return
        }
        self.meetingAudioChatServices = JitsiChatServices(withChatRoomId: room, andHost: host, andDisplayName: self.talkGroup.name, andCurrentUserName: user.name, andGroupChatID: talkGroupId, insideView: view)
        self.meetingAudioChatServices?.servicesReady = { [weak self] in
            guard let self = self else { return }
            if let concreteDelegate = self.meetingAudioChatServicesDidJoinedRoom {
                concreteDelegate()
            }
        }
        self.meetingAudioChatServices?.servicesDidChangeState = { [weak self] newState in
            guard let self = self else { return }
            switch newState {
            case .soundOn, .soundOff:
                let isSoundEnable = (newState == ChatServicesState.soundOn) ? true : false
                self.microState = isSoundEnable ? .MicroON : .MicroOFF
                if self.mode == .HandFree {
                    if let chatService = self.meetingAudioChatServices, chatService.isConnected {
                        self.handleSmartRecord(isStartRecording: isSoundEnable)
                    }
                    self.view.updateHandFreeModeUIForMicroState(self.microState)
                }
            case .roomConnectFall:
                fallthrough
            case .roomConnectOK:
                print("[AudioServices] Room join successed...!!!")
            }
        }
        self.meetingAudioChatServices?.servicesDidReceivedAnError = { [weak self] error in
            guard let self = self else { return }
            switch error {
            case .interupt:
                if let delegateDisconnect = self.meetingAudioChatServicesDidErrorWithoutReconnect {
                    delegateDisconnect()
                }
            case .internetDown:
                if let delegateRetryConnect = self.meetingAudioChatServicesDidErrorWithReconnect {
                    delegateRetryConnect()
                }
            }
        }
        self.meetingAudioChatServices?.servicesMembersDidChanged = { [weak self] notifiedState in
            guard let self = self else { return }
            let myCurrentUserID = User.getProfile()?.id ?? 0
            API.findAllUserIDFromJitsiID(jitsiID: Array(notifiedState.allCurrentMembers)){ [weak self] items in
                guard let userIdsFromJitsiIds = items.value as? [AudioServicesInfo],
                    let self = self else { return }
                let setOfAllUsers = Set(self.talkGroup.allUsers.map{ $0.id})
                let setOfActiveUserInRoom = Set(userIdsFromJitsiIds.map{ $0.user_id })
                let setOfModifiedUserInRoom = Set(userIdsFromJitsiIds.filter({ (item) -> Bool in
                    return notifiedState.modifiedMembers.contains(item.jitsi_id)
                }).map{ $0.user_id })
                let usersIncomingManager = UsersIncomingManager(withAllUsers: setOfAllUsers,
                                                                andUsersInRoom: setOfActiveUserInRoom,
                                                                withMyCurrentID: myCurrentUserID,
                                                                allModifiedMembers:setOfModifiedUserInRoom)
                let allOnlineUserIDs = usersIncomingManager.getAllOnlineMembersInRoom()
                let allOfflineUserIDs = usersIncomingManager.getAllOfflineMembersInRoom()
                /*
                 Special cases if the user is just join room after we call API, so the intersection number is different with the list of user(plus 1 meaning the currentuse), we should call the api in order to get the room list again
                 */
                let userJoinedMissingFromList = Set(userIdsFromJitsiIds.map{ $0.user_id })
                    .subtracting (Set(self.talkGroup.allUsers.map{ $0.id}))
                if notifiedState.action == .join { //some new user(s) joined, need to reload the api
                    userJoinedMissingFromList.forEach({ (userID) in
                        self.reloadUserInRoom(withUserID: userID)
                    })
                }
                /*
                 Change the order of joiing user!
                 It's quite cheating in the case swap online user to the first item in list
                 We should to implement better way to change the order of this list
                 */
                if notifiedState.action == .join {
                    usersIncomingManager.handleIncomming(users: allOnlineUserIDs,
                                                         forState: .join,
                                                         withListOfUsers: self.talkGroup.allUsers,
                                                         completion: { [weak self] (index,user) in
                                                            guard let self = self else { return }
                                                            self.talkGroup.allUsers.remove(at: index)
                                                            self.talkGroup.allUsers.insert(user, at: 0)
                    })
                }
                if notifiedState.action == .left {
                    usersIncomingManager.handleIncomming(users: allOfflineUserIDs,
                                                         forState: .left,
                                                         withListOfUsers: self.talkGroup.allUsers,
                                                         completion: { (index,user) in
                                                            self.talkGroup.allUsers.remove(at: index)
                                                            self.talkGroup.allUsers.insert(user, at: self.talkGroup.allUsers.count)
                    })
                }
                self.talkGroup.meetingUserCount = allOnlineUserIDs.count
                self.talkGroup.joinUsersCount = self.talkGroup.allUsers.count
                DispatchQueue.main.async {
                    self.notifyUI()
                }
            }
        }
        self.meetingAudioChatServices?.userDidChangeTalkingstatus = {[weak self] (talkingStatus) in
            guard let self = self else { return }
            let myCurrentUserID = User.getProfile()?.id ?? 0
            let objectIntIDForBroadcastMessage = NSNumber.init(value: myCurrentUserID)
            
            switch talkingStatus {
            case .talking:
                self.broadcastClientServices.sendBroadcastEvent(event: ClientBroadcastEvent.talking,
                                                                withData: [objectIntIDForBroadcastMessage])
                if !self.allTalkingUser.contains(myCurrentUserID) {
                    self.addTalkingUser(withID: myCurrentUserID)
                    self.reloadUser(userId: myCurrentUserID)
                }
            case .idle:
                self.broadcastClientServices.sendBroadcastEvent(event: ClientBroadcastEvent.idle,
                                                                withData: [objectIntIDForBroadcastMessage])
                if self.allTalkingUser.contains(myCurrentUserID) {
                    self.removeTalkingUser(withID: myCurrentUserID)
                    self.reloadUser(userId: myCurrentUserID)
                }
            }
        }
        self.setupSocketListener()
        self.meetingAudioChatServices?.connectChat()
    }
    func tearDownSocketListener() {
        self.allTalkingUser.removeAll()
    }
    private func startTimerToGetMemberState() {
        guard shouldUpdateMemberStateContinuously == false else { return }
        shouldUpdateMemberStateContinuously = true
        _ = Timer.scheduledTimer(withTimeInterval: 2, repeats: false) {[weak self] (timer) in
            guard let self = self else { return }
            self.shouldUpdateMemberStateContinuously = false
        }
    }
    func setupSocketListener(){
        startTimerToGetMemberState()
        self.broadcastClientServices.didReceivedMessageInformation = { [weak self] (packetName, data) in
            guard let self = self else { return }
            let myCurrentUserID = (data as! Array<Int>)[0]
            switch packetName {
            case .talking:
                if !self.allTalkingUser.contains(myCurrentUserID) || self.shouldUpdateMemberStateContinuously {
                    self.addTalkingUser(withID: myCurrentUserID)
                    self.reloadUser(userId: myCurrentUserID)
                }
            case .idle:
                if self.allTalkingUser.contains(myCurrentUserID) || self.shouldUpdateMemberStateContinuously {
                    self.removeTalkingUser(withID: myCurrentUserID)
                    self.reloadUser(userId: myCurrentUserID)
                }
            }
        }
    }
    func audioServicesCanTalking() -> Bool {
        guard let concreteMeetingAudioChatServices = meetingAudioChatServices else { return false }
        return concreteMeetingAudioChatServices.isConnected
    }
    func clearChatServices() {
        guard let audioChatServices = self.meetingAudioChatServices else {
            return
        }
        audioChatServices.releaseChat()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
        self.clearChatServices()
    }
    
    func removeMember(member: User) {
        hud.show()
        API.removeUserFromTalkGroup(talkID: talkGroup.id, userID: member.id) { (result) in
            DispatchQueue.main.async { [weak self] in
                hud.dismiss()
                guard let `self` = self else { return }
                switch result {
                case .success(_):
                    self.view.deletedMemberInTalkGroup(talkID: self.talkGroup.id, userId: member.id, shouldShowToast: true)
                    UITabBarControllerManager.shared.getNotifications()
                case .failure(let error):
                    self.view.showErrorAlertWithError(error, completion: nil)
                }
            }
        }
    }
    
    func banishMember(member: User) { // API for Band member
    }
    
    func reportMember(member: User) { // API for Report member
    }
    
    func blockMember(member: User) { // API for Unlock member
    }
    
    func unlockMember(member: User) { // API for Unlock member
    }
    
    func renewTalkGroup(userId: Int, numberOfCalling: Int, socketEvent: SocketEvent) {
        if let userTuple: (Int, User) = Array(self.talkGroup.allUsers).enumerated().filter({ $0.element.id == userId }).first {
            self.updateUserInTalk(userTuple: userTuple, numberOfCalling: numberOfCalling, socketEvent: socketEvent)
        } else {
            API.detailUserInTalkGroup(userId, talkId: talkGroupId) { (result) in
                DispatchQueue.main.async { [weak self] in
                    guard let `self` = self else { return }
                    switch result {
                    case .success(let user):
                        guard let user = user as? User else { return }
                        let indexOfNewMember = 0
                        if socketEvent == SocketEvent.JoinedChannel {
                            self.talkGroup.joinUsersCount += 1
                            self.talkGroup.allUsers.insert(user, at: indexOfNewMember)
                        }
                        self.updateUserInTalk(userTuple: (indexOfNewMember, user), numberOfCalling: numberOfCalling, socketEvent: socketEvent)
                    case .failure(_): return
                    }
                }
            }
        }
    }
    
    private func reloadUserInRoom(withUserID userID: Int) {
        API.detailUserInTalkGroup(userID, talkId: self.talkGroupId) { (result) in
            DispatchQueue.main.async { [weak self] in
                guard let `self` = self else { return }
                switch result {
                case .success(let user):
                    guard let user = user as? User else { return }
                    let userExisted = self.talkGroup.allUsers.first(where: { (user) -> Bool in
                        return user.id == userID
                    })
                    guard userExisted != nil else {
                        self.talkGroup.joinUsersCount += 1
                        self.talkGroup.allUsers.insert(user, at: 0)
                        self.talkGroup.meetingUsers.insert(user, at: 0)
                        UsersIncomingManager.showToast(forUser: user,withState: .join)
                        DispatchQueue.main.async {
                            self.notifyUI()
                        }
                        return
                    }
                    if Array(self.talkGroup.meetingUsers).enumerated().filter({ $0.element.id == user.id }).first == nil {
                        self.talkGroup.meetingUsers.insert(user, at: 0)
                    }
                case .failure(_): return
                }
            }
        }
    }
    private func forceHighLight() {
        //TODO: cheat here to force set the user become highlight
        // must be recheck with the api sync call
        let myCurrentUserID = User.getProfile()?.id ?? 0
        let currentUser = self.talkGroup.allUsers.enumerated().first { (index,user) -> Bool in
            return user.id == myCurrentUserID
        }
        
        if let currentUserExisted = currentUser {
            self.talkGroup.allUsers.remove(at: currentUserExisted.offset)
            currentUserExisted.element.isCalling = true
            self.talkGroup.allUsers.insert(currentUserExisted.element, at: 0)
        }
        if self.talkGroup.meetingUserCount == 0 {
            self.talkGroup.meetingUserCount = 1
        }
    }
    private func updateUserInTalk(userTuple: (Int, User), numberOfCalling: Int, socketEvent: SocketEvent) {
        let user = userTuple.1
        let index = userTuple.0
        
        switch socketEvent {
        case .LeftTalkGroup:
            self.talkGroup.joinUsersCount -= 1
            self.talkGroup.allUsers.remove(at: index)
        default: break
        }
        self.notifyUI()
    }
    
    func showQRCode(fromVC: BaseViewController!) {
        QRCodeHelpers.sharedInstance.getQRCode(withTalkId: talkGroupId, talkgroupName: talkGroup.name, fromVC: fromVC)
    }
    
    // MARK: Speech text changed Mode
    private func speechTextIncomingMode(mode: IncomingMode) {
        AVSpeechManager.shared.cancelSpeech()
        var text = ""
        if mode == .Push {
            text = NSLocalizedString("push_mode_speech_text", comment: "")
        } else {
            text = NSLocalizedString("hand_free_mode_speech_text", comment: "")
        }
        AVSpeechManager.shared.speechText(text: text)
    }
    
    private func speechTextMicroMode(oldMode: MicroState) {
        AVSpeechManager.shared.cancelSpeech()
        var text = ""
        if oldMode == .MicroON {
            text = NSLocalizedString("mike_off_text", comment: "")
        } else {
            text = NSLocalizedString("mike_on_text", comment: "")
        }
        AVSpeechManager.shared.speechText(text: text)
    }
    private func notifyUI() {
        self.forceHighLight()
        self.view.reloadUI(talkGroup: self.talkGroup)
    }
    
    private func reloadUser(userId: Int) {
        if let index = self.talkGroup.allUsers.index(where: { (user) -> Bool in
            user.id == userId
        }) {
            let indexPath = IndexPath(item: index, section: 0)
            self.view.reloadRowAt(indexPath: indexPath)
        }
    }
}

// MARK: - AddUserIncomingDelegate

extension IncomingPresenter: AddUserIncomingPresenterDelegate {
    func didInvitedUsers(_ users: [User]) {
        self.fetch()
    }
}

// MARK: - Handle Receiving Notification
extension IncomingPresenter {
    @objc
    fileprivate func didReceiveReleaseChatNotification(_ notification: Notification) {
        self.clearChatServices()
    }
}
