//
//  AddUserIncomingPresenter.swift
//  CallingTalk
//
//  Created by Toof on 2/21/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import Foundation

// Define invite-user's method

protocol AddUserIncomingPresenterDelegate: class {
    func didInvitedUsers(_ users: [User])
}

// Define function for view

protocol AddUserIncomingView: BaseViewProtocol {
    func refresh()
}

// Define function for presenter

protocol AddUserIncomingViewPresenter: class {
    init(view: AddUserIncomingView)
    func numberOfRows(inSection section: Int) -> Int
    func configure(_ cell: SelectMemberCell, at indexPath: IndexPath)
    func inviteUser(_ cell: SelectMemberCell, at indexPath: IndexPath)
    func handleInviteUser()
    func fetch()
}

// Implement presenter

class AddUserIncomingPresenter: AddUserIncomingViewPresenter {
    
    // MARK: Properties
    
    var view: AddUserIncomingView
    var users: [User] = [User]()
    var userSelecteds: [User] = [User]()
    weak var delegate: AddUserIncomingPresenterDelegate?
    
    // MARK: Initial
    
    required init(view: AddUserIncomingView) {
        self.view = view
    }
    
    func numberOfRows(inSection section: Int) -> Int {
        return self.users.count
    }
    
    func configure(_ cell: SelectMemberCell, at indexPath: IndexPath) {
        
    }
    
    func inviteUser(_ cell: SelectMemberCell, at indexPath: IndexPath) {
        
        
        cell.isMemberSelected = !cell.isMemberSelected
    }
    
    func handleInviteUser() {
        // TO DO: Handel add user to room
        
        // Call delegate for update Incoming's Screen
        self.delegate?.didInvitedUsers(self.userSelecteds)
    }
    
    func fetch() {
        self.users = [
            User(),
            User(),
            User(),
            User(),
            User(),
            User(),
            User(),
            User(),
            User()
        ]
        
        // Update UI
        self.view.refresh()
    }
    
}
