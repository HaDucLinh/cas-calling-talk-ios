//
//  UsersIncomingManager.swift
//  CallingTalk
//
//  Created by NeoLabx on 5/14/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit
import RealmSwift
/*
 We have 2 kind of users:
 - members: NORMAL MEMBERS, who inside calling talk app system control and can talk together
 - users: SPECIAL MEMBERS,who joined to room but may be just for observing problems, it can be bosses, observers, admins (they will not available in UI)...
 This manage is use to control them.
 */
class UsersIncomingManager {
    private var allMembers:Set<Int>! // all member (who can chat together)
    private var allModifiedMembers:Set<Int>!
    private var allValidUserInRoom:Set<Int>! // all valid user, contains admins,bosses, observers...
    private var myID: Int!
    init(withAllUsers allValidMembers: Set<Int>, andUsersInRoom :Set<Int>, withMyCurrentID: Int,allModifiedMembers: Set<Int>){
        self.allMembers = allValidMembers
        self.allValidUserInRoom = andUsersInRoom
        self.myID = withMyCurrentID
        self.allModifiedMembers = allModifiedMembers
    }
    func handleIncomming(users: Set<Int>,forState state: ActionState,withListOfUsers allUsers: List<User>,completion :@escaping(Int,User) -> ()) {
        users.forEach { (userIdInt) in
            guard let userWithCorrectID = (allUsers.enumerated().first(where: { (index,user) -> Bool in
                return user.id == userIdInt
            })) else { return }
            let oldUserUIState = userWithCorrectID.element.isCalling
            let newUserUIState = (state == .join) ? true : false
            if oldUserUIState != newUserUIState && !self.isInitialInformation() {
                UsersIncomingManager.showToast(forUser: userWithCorrectID.element,
                                               withState: state)
            }
            userWithCorrectID.element.isCalling = newUserUIState
            completion(userWithCorrectID.offset,userWithCorrectID.element)
        }
    }
    func reloadRoomInfoByAPI() {
    }
    func isInitialInformation() -> Bool{
        print("\(self.allModifiedMembers) and my id \(self.myID)")
        return self.allModifiedMembers.contains(self.myID)
    }
    /*
     All valid and currenly online members in room mean user must be in room excluding specials members(check at top defination)
     */
    func getAllOnlineMembersInRoom() -> Set<Int> {
        return  allMembers.intersection(allValidUserInRoom).union(Set([self.myID]))
    }
    /*
     All valid members but in offline status in room
     */
    func getAllOfflineMembersInRoom() -> Set<Int> {
        return  allMembers.subtracting(allValidUserInRoom)
    }
}
extension UsersIncomingManager {
    static func showToast(forUser user: User,withState state: ActionState) {
        let myCurrentUserID = User.getProfile()?.id ?? 0
        if user.id == myCurrentUserID { return }
        let messageDisplay = (state == .join) ? "member_joined_talk" : "member_left_talk"
        let sound = (state == .join) ? "audio_marimba_alert_in" : "audio_marimba_alert_out"
      
        DispatchQueue.main.async {
            let toastView = ToastView.loadViewFromNib()
            toastView.toastViewColor = UIColor.ct.incomingToastViewColor.withAlphaComponent(0.94)
            toastView.show(text: String(format: NSLocalizedString(messageDisplay, comment: ""), user.name))
            AVPlayerManager.shared.playLocalAudio(name: sound, type: "mp3")
        }
    }
}
