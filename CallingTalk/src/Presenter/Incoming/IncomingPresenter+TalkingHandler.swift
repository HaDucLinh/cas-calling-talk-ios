//
//  IncomingPresenter+TalkingHandler.swift
//  CallingTalk
//
//  Created by NeoLabx on 5/31/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit

extension IncomingPresenter {
    func addTalkingUser(withID id: Int) {
        self.allTalkingUser.insert(id)
    }
    func removeTalkingUser(withID id: Int) {
        self.allTalkingUser.remove(id)
    }
}
