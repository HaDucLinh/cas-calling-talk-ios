//
//  SettingPresenter.swift
//  CallingTalk
//
//  Created by Vu Tran on 5/30/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import Foundation

protocol SettingView: BaseViewProtocol {
    func updateView()
}

class SettingPresenter: NSObject {
    weak fileprivate var view : SettingView?
    var user: User?
    
    override init() {
    }
    
    func attachView(_ view: SettingView) {
        self.view = view
    }
    
    // MARK: APIs
    func getMyProfile(_ completion: (() -> Void)? = nil) {
        hud.show()
        API.getMyProfile { (result) in
            DispatchQueue.main.async { [weak self] in
                hud.dismiss()
                guard let _ = self else { return }
                switch result {
                case .success(let profileData):
                    guard let profile = profileData as? User else { return }
                    
                    // Save user's profile & token id
                    self?.user = profile
                    Helpers.saveProfile(profile)
                    self?.view?.updateView()
                case .failure( _):
                    self?.view?.updateView()
                }
            }
        }
    }
}

