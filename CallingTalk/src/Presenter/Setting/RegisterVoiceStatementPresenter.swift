//
//  RegisterSpeechPresenter.swift
//  CallingTalk
//
//  Created by Vu Tran on 3/4/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit
/// Protocol connect view and presenter
protocol RegisterVoiceView: BaseViewProtocol {
    func setUpdateButtonState(isEnable: Bool)
    func createVoiceSuccessfull(voice: Voice)
    func updateListOrganization()
}

class RegisterVoiceStatementPresenter: NSObject {
    weak fileprivate var view : RegisterVoiceView?
    var voice: Voice = Voice()
    var organizations: [Organization] = []
    
    override init() {
    }
    
    func attachView(_ view: RegisterVoiceView) {
        self.view = view
    }
    
    func voiceTextChange(text: String?) {
        voice.content = text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
        setRegisterButtonStatus()
    }
    
    /// update button status (enable/disable) depend on other input field
    func setRegisterButtonStatus() {
        if (voice.content.count > 0 && !voice.organizations.isEmpty) {
            view?.setUpdateButtonState(isEnable: true)
            return
        }
        self.view?.setUpdateButtonState(isEnable: false)
    }
    
    // MARK: APIs
    func getListOrganization() {
        hud.show()
        API.getAllOrganization { (result) in
            hud.dismiss()
            switch result {
            case .success(let objects):
                if let orgs = objects as? [Organization] {
                    self.organizations = orgs
                    self.view?.updateListOrganization()
                }
            case .failure(let error):
                self.view?.showErrorAlertWithError(error, completion: nil)
            }
        }
    }
    
    func registerVoiceStatement() {
        var ids: [Int] = []
        voice.organizations.forEach { (org) in
            ids.append(org.id)
        }
        
        hud.show()
        API.createVoiceStatement(name: voice.content, orgIDs: ids) { (result) in
            hud.dismiss()
            switch result {
            case .success(let object):
                if let voice = object as? Voice {
                    self.view?.createVoiceSuccessfull(voice: voice)
                }
            case .failure(let error):
                self.view?.showErrorAlertWithError(error, completion: nil)
            }
        }
    }
}
