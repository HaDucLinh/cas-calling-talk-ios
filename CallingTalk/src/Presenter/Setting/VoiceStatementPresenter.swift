//
//  VoiceStatementPresenter.swift
//  CallingTalk
//
//  Created by Vu Tran on 3/6/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit
protocol VoiceStatementView: BaseViewProtocol {
    func updateView()
}

class VoiceStatementPresenter: NSObject {
    weak fileprivate var view : VoiceStatementView?
    var voices: [Voice] = []
    var hasNextPage = false
    
    override init() {
    }
    
    func attachView(_ view: VoiceStatementView) {
        self.view = view
    }
    
    // MARK: APIs
    func getAllVoiceStatements() {
        hud.show()
        API.getListVoiceStatements { (result, hasNextPage) in
            hud.dismiss()
            switch result {
            case .success(let objects):
                if let voices = objects as? [Voice] {
                    self.voices = voices
                }
                self.view?.updateView()
            case .failure(let error):
                self.view?.showErrorAlertWithError(error, completion: nil)
            }
        }
    }
}
