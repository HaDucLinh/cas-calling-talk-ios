//
//  EditSpeechPresenter.swift
//  CallingTalk
//
//  Created by Vu Tran on 3/6/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit
/// Protocol connect view and presenter
protocol EditVoiceView: BaseViewProtocol {
    func setUpdateButtonState(isEnable: Bool)
    func updateListOrganization()
    func updateVoiceSuccessfull(voice: Voice)
    func deletedVoiceStatement(voice: Voice)
}

class EditVoicePresenter: NSObject {
    weak fileprivate var view : EditVoiceView?
    var voice: Voice?
    var organizations: [Organization] = []
    var organizationForUpdate: [Organization] = []
    
    override init() {
    }
    
    func attachView(_ view: EditVoiceView) {
        self.view = view
    }
    
    func voiceTextChange(text: String?) {
        voice?.content = text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
        setRegisterButtonStatus()
    }
    
    /// update button status (enable/disable) depend on other input field
    func setRegisterButtonStatus() {
        guard let voice = self.voice else {
            self.view?.setUpdateButtonState(isEnable: false)
            return
        }
        
        if (voice.content.count > 0 && !organizationForUpdate.isEmpty) {
            view?.setUpdateButtonState(isEnable: true)
            return
        }
        self.view?.setUpdateButtonState(isEnable: false)
    }
    
    // APIs
    func getListOrganization() {
        hud.show()
        API.getAllOrganization { (result) in
            hud.dismiss()
            switch result {
            case .success(let objects):
                if let orgs = objects as? [Organization] {
                    self.organizations = orgs
                    self.view?.updateListOrganization()
                }
            case .failure(let error):
                self.view?.showErrorAlertWithError(error, completion: nil)
            }
        }
    }
    
    func updateVoiceStatement() {
        guard let voice = self.voice else { return }
        var ids: [Int] = []
        organizationForUpdate.forEach { (org) in
            ids.append(org.id)
        }
        hud.show()
        API.updateVoiceStatement(id: voice.id, name: voice.content, ids: ids) { (result) in
            hud.dismiss()
            switch result {
            case .success(let object):
                if let voice = object as? Voice {
                    self.view?.updateVoiceSuccessfull(voice: voice)
                }
            case .failure(let error):
                self.view?.showErrorAlertWithError(error, completion: nil)
            }
        }
    }
    
    func deleteVoiceStatement() {
        guard let voice = self.voice else { return }
        hud.show()
        API.deleteVoiceStatement(id: voice.id) { (result) in
            hud.dismiss()
            switch result {
            case .success(_):
                self.view?.deletedVoiceStatement(voice: voice)
            case .failure(let error):
                self.view?.showErrorAlertWithError(error, completion: nil)
            }
        }
    }
}
