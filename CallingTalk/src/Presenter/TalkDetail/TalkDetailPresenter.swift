//
//  TalkDetailPresenter.swift
//  CallingTalk
//
//  Created by Vu Tran on 2/18/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//


import UIKit
import ObjectMapper

/// Protocol connect view and presenter
protocol TalkDetailView: BaseViewProtocol {
    func updateUI()
    func dismissVC()
    func leaveTalkGroup(id: Int)
    func deleteTalkGroup(id: Int)
    func gotoChatVC(withTalk talkGroup: TalkGroup)
    func updateNumberOfUnreadMessage(_ number: Int?)
    func deletedMember(talkID: Int, member: User)
}

class TalkDetailPresenter: NSObject {
    
    weak fileprivate var view: TalkDetailView?
    var talkGroupId: Int = 0
    var talkGroup: TalkGroup?
    fileprivate var numberOfUnreadMessage: Int?
    
    override init() {
    }
    
    func attachView(_ view: TalkDetailView) {
        self.view = view
    }
    
    // MARK: APIs
    func fetch() {
        hud.show()
        API.detailTalkGroup(self.talkGroupId) { (result) in
            DispatchQueue.main.async { [weak self] in
                guard let `self` = self else { return }
                hud.dismiss()
                switch result {
                case .success(let talkGroup):
                    guard let talkGroup = talkGroup as? TalkGroup else { return }
                    self.talkGroup = talkGroup
                    self.view?.updateUI()
                    self.view?.updateNumberOfUnreadMessage(talkGroup.isNewUnreadMessage ? talkGroup.numberOfUnreadMessageMention : nil)
                case .failure(let error):
                    self.view?.showErrorAlertWithError(error, completion: { (_) in
                        self.view?.dismissVC()
                    })
                }
            }
        }
    }
    
    func handleTalkGroupChanged() {
        API.detailTalkGroup(self.talkGroupId) { (result) in
            DispatchQueue.main.async { [weak self] in
                guard let `self` = self else { return }
                switch result {
                case .success(let talkGroup):
                    guard let talkGroup = talkGroup as? TalkGroup else { return }
                    self.talkGroup = talkGroup
                    self.view?.updateUI()
                case .failure(_): break
                }
            }
        }
    }
    
    func getMyProfile(_ completion: (() -> Void)? = nil) {
        API.getMyProfile { (result) in
            DispatchQueue.main.async { [weak self] in
                guard let _ = self else { return }
                switch result {
                case .success(let profileData):
                    guard let profile = profileData as? User else { return }
                    
                    // Save user's profile & token id
                    Helpers.saveProfile(profile)
                    completion?()
                case .failure( _): break
                }
            }
        }
    }
    
    func leaveTalkGroup(id: Int) {
        hud.show()
        API.leaveTalkGroup(talkGroupId: id, completion: { (result) in
            DispatchQueue.main.async { [weak self] in
                hud.dismiss()
                guard let `self` = self else { return }

                switch result {
                case .success(_):
                    UITabBarControllerManager.shared.getNotifications()
                    self.view?.leaveTalkGroup(id: id)
                case .failure(let error):
                    self.view?.showErrorAlertWithError(error, completion: nil)
                }
            }
        })
    }
    
    func deleteTalkGroup(id: Int) {
        hud.show()
        API.deleteTalkGroup(talkGroupId: id) { (result) in
            DispatchQueue.main.async { [weak self] in
                hud.dismiss()
                guard let `self` = self else { return }
                
                switch result {
                case .success(_):
                    UITabBarControllerManager.shared.getNotifications()
                    self.view?.deleteTalkGroup(id: id)
                case .failure(let error):
                    print("Delete Error")
                    self.view?.showErrorAlertWithError(error, completion: nil)
                }
            }
        }
    }
    
    private func removeMember(member: User) { // API for Remove member
        // Success ->>
        guard let id = talkGroup?.id, id != 0 else { return }
        hud.show()
        API.removeUserFromTalkGroup(talkID: id, userID: member.id) { (result) in
            DispatchQueue.main.async { [weak self] in
                hud.dismiss()
                guard let `self` = self else { return }
                
                switch result {
                case .success(_):
                    self.view?.deletedMember(talkID: id, member: member)
                    UITabBarControllerManager.shared.getNotifications()
                case .failure(let error):
                    self.view?.showErrorAlertWithError(error, completion: nil)
                }
            }
        }
    }
    
    private func banishMember(member: User) { // API for Band member
         // Success ->>
        print("Banded: show toast")
        let toastView = ToastView.loadViewFromNib()
        toastView.show(text: NSLocalizedString("banded_message", comment: ""))
    }
    
    private func reportMember(member: User) { // API for Report member
        // Success ->>
        print("Reported: show toast")
        let toastView = ToastView.loadViewFromNib()
        toastView.show(text: NSLocalizedString("report_member_infor_sent_to_admin", comment: ""))
    }
    
    private func blockMember(member: User) { // API for Unlock member
        // Success ->>
        print("Blocked: show toast")
        let toastView = ToastView.loadViewFromNib()
        toastView.show(text: NSLocalizedString("blocked_message", comment: ""))
    }
    
    private func unlockMember(member: User) { // API for Unlock member
        // Success ->>
        print("Unlocked: show toast")
        let toastView = ToastView.loadViewFromNib()
        toastView.show(text: NSLocalizedString("un_block_message", comment: ""))
    }
    
    // MARK: Functions
    func willShowTalkMemberAlert(index: Int) {
        guard let talkGroup = talkGroup, index < talkGroup.allUsers.count else {
            return
        }
        let member = talkGroup.allUsers[index]
        showMemberAlert(member: member, talkGroup: talkGroup)
    }
    
    func showMemberAlert(member: User, talkGroup: TalkGroup) {
        let alertMember = BlockMemberAlertView.loadFromNib()
        alertMember.show(member: member, talkGroup: talkGroup) { (action) in
            switch action {
            case .RemoveMember:
                self.showDeleteMemberAlert(member: member, talkGroup: talkGroup)
            case .Banish:
                self.showBandMemberConfirmAlert(member: member, talkGroup: talkGroup)
            case .Report:
                self.showReportMemberConfirmView(member: member)
            case .UnBlock:
                self.unlockMember(member: member)
            case .Block:
                self.blockMember(member: member)
            }
        }
    }
    
    func showQRCode(fromVC: BaseViewController!){
        QRCodeHelpers.sharedInstance.getQRCode(withTalkId: talkGroupId, talkgroupName: talkGroup?.name, fromVC: fromVC)
    }
    
    private func showDeleteMemberAlert(member: User, talkGroup: TalkGroup) { // CT-C-5-4
        let confirmAlertView = ConfirmDeleteTalkView.loadFromNib()
        confirmAlertView.isDeleteMember = true
        confirmAlertView.backgroundImage = UIImage(named: "bg_alert")
        confirmAlertView.member = member
        
        confirmAlertView.message = NSLocalizedString("delete_member_confirm_text", comment: "")
        confirmAlertView.confirmButtonTitle = NSLocalizedString("delete_button_alert_text", comment: "")
        confirmAlertView.show {
            self.removeMember(member: member)
        }
    }
    
    private func showBandMemberConfirmAlert(member: User, talkGroup: TalkGroup) { // CT-C-5-4-1
        let confirmAlertView = ConfirmDeleteTalkView.loadFromNib()
        confirmAlertView.isDeleteMember = true
        confirmAlertView.backgroundImage = UIImage(named: "bg_alert")
        confirmAlertView.member = member

        confirmAlertView.message = NSLocalizedString("band_member_message", comment: "")
        let bandText = NSLocalizedString("band_infor_text", comment: "")
        confirmAlertView.moreInfor = String(format: bandText, "Organize-Name")
        confirmAlertView.confirmButtonTitle = NSLocalizedString("band_button_title", comment: "")
        confirmAlertView.show {
            self.banishMember(member: member)
        }
    }
    
    private func showReportMemberConfirmView(member: User) { // CT-C-5-4-2
        let confirmAlertView = ReportMemberConfirmView.loadFromNib()
        confirmAlertView.backgroundImage = UIImage(named: "bg_alert")
        confirmAlertView.organize = member.organizations.first?.name
        confirmAlertView.memberName = member.name
        confirmAlertView.show {
            self.reportMember(member: member)
        }
    }
    
    func gotoChatVC(){
        if let talkGroup = self.talkGroup{
            self.view?.gotoChatVC(withTalk: talkGroup)
        }
    }
}
