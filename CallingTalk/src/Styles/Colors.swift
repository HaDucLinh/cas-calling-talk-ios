//
//  Colors.swift
//  CallingTalk
//
//  Created by Vu Tran on 2/12/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import Foundation
import UIKit
import Swinject

extension CTTheme {
    /*
     Injector of Swinject. Swinject is a dependency injection frame work that can help us inject or object
     to the system.
     I try to inject the theme system to the existed extension.
     */
    static var systemSetting = Container.default.resolve(SystemThemeable.self)
    
    static var mainBackgroundColor: UIColor {
        get {
            return CTTheme.systemSetting?.appStyleTheme().mainBackgroundColor ?? .red
        }
    }
    static var redDisableButton : UIColor {
        get {
            return CTTheme.systemSetting?.appStyleTheme().mainBackgroundColor.withAlphaComponent(0.4) ?? .red
        }
    }
    static var selectItemColor: UIColor {// TabBar Color
        get {
            return CTTheme.systemSetting?.appStyleTheme().selectItemColor ?? .red
        }
    }
    static var iconHightLight: UIColor {// icon Highligh
        get {
            return CTTheme.systemSetting?.appStyleTheme().iconHightLight ?? .red
        }
    }
    static var headerText: UIColor {
        get {
            return CTTheme.systemSetting?.appStyleTheme().headerText ?? .white
        }
    }
    static var headerTextSubtitle: UIColor {
        get {
            return CTTheme.systemSetting?.appStyleTheme().headerTextSubtitle ?? .white
        }
    }
    static var bottomTabbarColor: UIColor {
        get {
            return CTTheme.systemSetting?.appStyleTheme().bottomTabbarColor ?? .white
        }
    }
    static var badgeBackgroundColor: UIColor {
        get {
            return CTTheme.systemSetting?.appStyleTheme().badgeBackgroundColor ?? .red
        }
    }
    static let roleAuthorViewColor = UIColor.rgb(red: 85.0, green: 204.0, blue: 204.0)
    static let roleAdminViewColor = UIColor.rgb(red: 217, green: 9, blue: 9)
    // AdminSMSVC's screen
    static let underlineTitleButtonColor = UIColor.rgb(red: 153.0, green: 153.0, blue: 153.0)
    static let grayBorderColor = UIColor.rgb(red: 204.0, green: 204.0, blue: 204.0)
    static let okAlertButtonColor = UIColor.rgb(red: 85.0, green: 204.0, blue: 204.0)
    static let unselectedItemColor = UIColor.rgb(red: 153.0, green: 153.0, blue: 153.0)
    // Main's screen
    static let borderImageColor = UIColor.rgb(red: 85.0, green: 204.0, blue: 204.0)
    static let searchBarBackgroundColor = UIColor.rgb(red: 240.0, green: 240.0, blue: 240.0)
    static let searchFieldBackgroundColor = UIColor.rgb(red: 251.0, green: 252.0, blue: 253.0)
    static let placeholderSearchTextColor = UIColor.rgb(red: 197.0, green: 197.0, blue: 197.0)
    static let borderSearchTextFieldColor = UIColor.rgb(red: 204.0, green: 204.0, blue: 204.0)
    static let popTipBackgroundColor = UIColor.rgb(red: 128.0, green: 128.0, blue: 128.0)
    static let totalPersonTextColor = UIColor.rgb(red: 153.0, green: 153.0, blue: 153.0)
    static let numberOfPersonJoiningTextColor = UIColor.rgb(red: 85.0, green: 204.0, blue: 204.0)
    static let redKeyColor = UIColor.rgb(red: 217, green: 9, blue: 9)
    static let grayPlaceHolderTextColor = UIColor.rgb(red: 204.0, green: 204.0, blue: 204.0)
    // Member's screen
    static let textNoticeReadColor = UIColor.rgb(red: 197.0, green: 197.0, blue: 197.0)
    // NewTalk
    static let grayInvitedTextColor = UIColor.rgb(red: 128, green: 128, blue: 128)
    static let aboutMemberTextColor = UIColor.rgb(red: 153, green: 153, blue: 153)
    // Incomming
    static let tipToTalkTextColor = UIColor.rgb(red: 153.0, green: 153.0, blue: 153.0)
    static let pushModeColor = UIColor.rgb(red: 255.0, green: 238.0, blue: 17.0)
    static let handFreeModeColor = UIColor.rgb(red: 85.0, green: 204.0, blue: 204.0)
    static let pushModeBlurColor = UIColor.rgb(red: 65.0, green: 55.0, blue: 1.0)
    static let handFreeModeBlurColor = UIColor.rgb(red: 18.0, green: 39.0, blue: 60.0)
    static let recordOnTitleButtonColor = UIColor.rgb(red: 85.0, green: 204.0, blue: 204.0)
    static let recordOffTitleButtonColor = UIColor.rgb(red: 255.0, green: 68.0, blue: 68.0)
    static let incomingToastViewColor = UIColor.rgb(red: 85.0, green: 85.0, blue: 85.0)
    // Setting
    static let grayVersionColor = UIColor.rgb(red: 204.0, green: 204.0, blue: 204.0)
    // Chat
    static let placeHolderChatTextColor = UIColor.rgb(red: 153.0, green: 153.0, blue: 153.0)
    static let highlightMessageTextColor = UIColor.rgb(red: 85.0, green: 204.0, blue: 204.0)
    static let grayMessageTextColor = UIColor.rgb(red: 197.0, green: 197.0, blue: 197.0)
    static let mentionBackgroundColor = UIColor.rgb(red: 68.0, green: 68.0, blue: 68.0)
    static let tagViewBackgroundColor = UIColor.rgb(red: 248, green: 250, blue: 251)
    static let boderTagViewColor = UIColor.rgb(red: 153, green: 153, blue: 153)
    static let selectAudioSentenceBackgroundColor = UIColor.rgb(red: 222.0, green: 255.0, blue: 255.0)
    static let talkConnectionIssusAlert = UIColor.rgb(red: 85.0, green: 85.0, blue: 85.0)
    // Incoming's background
    static let defaultIncomingBackgroundColor = UIColor.rgb(red: 195.0, green: 214.0, blue: 0.0)
    static let defaultIncomingBlurBackgroundColor = UIColor.rgb(red: 1.0, green: 1.0, blue: 1.0)
    class func blurColorWithAlpha(_ alpha: CGFloat) -> UIColor {
        return defaultIncomingBlurBackgroundColor.withAlphaComponent(alpha)
    }
}
