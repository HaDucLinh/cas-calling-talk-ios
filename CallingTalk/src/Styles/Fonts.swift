//
//  Fonts.swift
//  CallingTalk
//
//  Created by Vu Tran on 2/12/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit

enum FontType: String {
    case bold = "-Bold"
    case light = "-Light"
    case regular = "-Regular"
}

enum FontName: String {
    case NotoSansCJKjp = "NotoSansCJKjp"
}

enum PSDFontScale: CGFloat {
    case iPhone5 = 0.88
    case iPhone6 = 1
    case iPhone6p = 1.0132
    case iPhoneX = 1.15
    case iPad = 1.3
}

var fontScale: PSDFontScale {
    switch kScreenSize {
    case DeviceType.iPhone4.size, DeviceType.iPhone5.size:          return .iPhone5
    case DeviceType.iPhone6.size:                                    return .iPhone6
    case DeviceType.iPhone6p.size:                                   return .iPhone6p
    case DeviceType.iPhoneX.size, DeviceType.iPhoneXSMax.size:      return .iPhoneX
    case DeviceType.iPad.size:                                       return .iPad
    default:
        return .iPhone6
    }
}

extension CTTheme {
    
    @nonobjc class func font(_ name: FontName, type: FontType, size: CGFloat) -> UIFont! {
        let fontName = name.rawValue + type.rawValue
        let fontSize = size
        let font = UIFont(name: fontName, size: fontSize)
        if let font = font {
            return font
        } else {
            return UIFont.systemFont(ofSize: fontSize)
        }
    }
}

