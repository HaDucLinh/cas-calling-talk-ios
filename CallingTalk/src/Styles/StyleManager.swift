//
//  StyleManager.swift
//  CallingTalk
//
//  Created by Vu Tran on 2/12/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit

class CTTheme {}

extension UIColor {
    static var ct: CTTheme.Type {
        return CTTheme.self
    }
}

extension UIFont {
    static var ct: CTTheme.Type {
        return CTTheme.self
    }
}
