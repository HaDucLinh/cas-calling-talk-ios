//
//  IncomingVC.swift
//  CallingTalk
//
//  Created by Toof on 2/19/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit
import Reachability
import ObjectMapper
import AVFoundation

protocol IncomingVCDelegate: class {
    func reloadListTalkGroup()
    func deletedMemberFromInComing(groupID: Int, userId: Int)
}

class IncomingVC: BaseViewController {
    
    // MARK: Properties
    
    public var presenter: IncomingPresenter!
    
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var dismissButton: UIButton!
    @IBOutlet weak var settingsButton: UIButton!
    
    @IBOutlet weak var nameTalkGroupLabel: UILabel!
    @IBOutlet weak var numberOfUserLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var chatTitleButtonLabel: UILabel!
    @IBOutlet weak var chatButton: BadgeButton!
    
    @IBOutlet weak var talkTitleButtonLabel: UILabel!
    @IBOutlet weak var talkButton: UIButton!
    
    @IBOutlet weak var recordTitleButtonLabel: UILabel!
    @IBOutlet weak var recordButton: UIButton!
    
    @IBOutlet weak var modeSwitch: CustomSwitch!
    @IBOutlet weak var pushModeLabel: UILabel!
    @IBOutlet weak var handFreeModeLabel: UILabel!
    
    fileprivate let blurBackgroundLayer: CALayer = CALayer()
    
    weak var delegate: IncomingVCDelegate?
    var shouldShowAudioLogMessages: Bool = true
    private var talkConnection: TalkConnectionAlert = TalkConnectionAlert.loadFromNib()
    
    fileprivate var isWaitingForReconnect: Bool = false
    
    fileprivate var numberOfUnreadMessage: Int? {
        didSet {
            self.chatButton.badgeString = self.numberOfUnreadMessage.toUnreadNumberBadgeString()
        }
    }
    
    // MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.registerReachability()
        hud.setDefaultMaskType(.clear)
        self.setupSubviews()
        self.presenter.fetch()
        NotificationCenter.default.addObserver(self, selector: #selector(reconnectSocket(notification:)), name: NSNotification.Name.ReconnectSocket, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(disconnectSocket(notification:)), name: NSNotification.Name.DisconnectSocket, object: nil)
        
        NotificationCenter.default.addObserver(self,
                                       selector: #selector(handleRouteChange),
                                       name: AVAudioSession.routeChangeNotification,
                                       object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(proximityChanged(notification:)), name: NSNotification.Name(rawValue: "UIDeviceProximityStateDidChangeNotification"), object: UIDevice.current)
        
        NotificationCenter.default.addObserver(self, selector: #selector(appBecomeActive(notification:)), name: UIApplication.didBecomeActiveNotification, object: nil)

        GlobalVariable.didComeToIncomingVC = true
        
        // Connect BlueParrott headset to listen for button events
        connectHeadset()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        hud.setDefaultMaskType(.none)
    }
    
    deinit {
        self.talkConnection.dismiss()
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        // Set position for badge message
        self.chatButton.badgeEdgeInsets = UIEdgeInsets(top: self.chatButton.bounds.size.height / 3.5, left: 0, bottom: 0, right: self.chatButton.bounds.size.width / 2.55)
        self.blurBackgroundLayer.frame = self.backgroundImageView.bounds
    }
    
    override func notifyForUnreadMessage(_ isMention: Bool, of talkGroupId: Int) {
        super.notifyForUnreadMessage(isMention, of: talkGroupId)
        guard self.presenter != nil else { return }

        if talkGroupId == self.presenter.talkGroup.id {
            let numberOfMentionMessage = isMention ? 1 : 0
            self.numberOfUnreadMessage = (self.numberOfUnreadMessage ?? 0) + numberOfMentionMessage
        }
    }
    
    override func notifyForMarkAsReadMessage(of talkGroupId: Int) {
        super.notifyForMarkAsReadMessage(of: talkGroupId)
        guard self.presenter != nil else { return }
        
        if talkGroupId == self.presenter.talkGroup.id {
            self.numberOfUnreadMessage = nil
        }
    }
    
    override func notifyForNewMessage(_ message: Message) {
        super.notifyForNewMessage(message)
        guard self.presenter != nil else { return }

        // Just play system audio messages in the current talk
        if message.channelId == self.presenter.talkGroupId {
            if message.messageType == MessageType.SystemAudio.rawValue {
                AVPlayerManager.shared.playRemoteAudio(urlString: message.fileURL as NSString)
            }
                
            if message.messageType == MessageType.Text.rawValue || message.messageType == MessageType.Image.rawValue {
                if let userId = User.getProfile()?.id, message.owner?.id != userId {
                    if Array(message.mentionIds).contains(userId) || message.isMentionAll {
                        var mentionUsers: [User] = []
                        message.mention_users.forEach { (user) in
                            mentionUsers.append(user)
                        }
                        if message.isMentionAll {
                            mentionUsers.append(User.allUser())
                        }
                        
                        let textContentTuple = Helpers.replaceUserIdByName(textMessage: message.content, mentionUsers: mentionUsers, isMentionAll:  message.isMentionAll)
                        let content = textContentTuple.0
                        
                        if User.getProfile()?.enableNotification == true {
                            self.showAlertForRetrievingMessage(title: message.owner?.name ?? "", message: message.messageType == MessageType.Text.rawValue ? content : NSLocalizedString("image_alert_message", comment: ""))
                            AVPlayerManager.shared.playLocalAudio(name: "audio-message-arrive", type: "mp3")
                        }
                    }
                }
            }
        }
    }
    
    override func reachabilityChanged(note: Notification) {
        super.reachabilityChanged(note: note)
        guard let reachability =  note.object as? Reachability else { return }
        switch reachability.connection {
        case .none:
            print("Incomming lost Internet")
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
               self.talkConnection.show()
            }
        default:
            /*
             The connection will be restored,so if the audio services still connected
             it will dissmiss the popup, otherwise a new joining request will triggered
             */
            if self.presenter.audioServicesCanTalking() {
                self.talkConnection.dismiss()
            }
            print("Will reconnect Socket")
        }
    }
    
    // MARK: Reconnect Socket Notification
    @objc private func reconnectSocket(notification: Notification) {
        print("Socket Reconected")
        if self.isWaitingForReconnect {
            self.presenter.clearChatServices()
        }
        self.presenter.setupSocketListener()
    }
    @objc private func disconnectSocket(notification: Notification) {
        self.presenter.tearDownSocketListener()
    }
    @objc private func proximityChanged(notification: Notification) {
        if let device = notification.object as? UIDevice {
            if device.proximityState && self.presenter.mode == .Push && !self.isHeadsetConnectedToApp() {
                self.presenter.handleProximitySensorDetected()
            }
        }
    }
    
    @objc private func appBecomeActive(notification: Notification) {
        // Do nothing in HandFree mode
        if self.presenter.mode == .HandFree { return }
        // Do nothing if user is holding headset button to talk
        if self.isHeadsetConnectedToApp() && talkButton.isHighlighted { return }
        
        presenter.setDefaultMicrophoneMode()
    }
    
    fileprivate
    func setupSubviews() {
        // Setup talk's button
        self.talkButton.setBackgroundImage(UIImage(named: "img_talk_button_normal"), for: .normal)
        self.talkButton.setBackgroundImage(UIImage(named: "img_talk_button_focus"), for: .highlighted)
        self.talkButton.addTarget(self, action: #selector(talkButtonTouchDown(_:)), for: .touchDown)
        self.talkButton.addTarget(self, action: #selector(talkButtonTouchUp(_:)), for: [.touchUpInside, .touchUpOutside])
        self.recordButton.adjustsImageWhenHighlighted = false
        self.chatButton.badgeBackgroundColor = UIColor.ct.badgeBackgroundColor
        
        self.blurBackgroundLayer.backgroundColor = UIColor.ct.blurColorWithAlpha(0.6).cgColor
        self.backgroundImageView.layer.addSublayer(self.blurBackgroundLayer)
        
        // Setup collection view
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        self.collectionView.register(IncomingMemberCell.self)
        self.collectionView.backgroundColor = UIColor.white.withAlphaComponent(0.0)
        
        // Add listener's events for Button
        self.dismissButton.addTarget(self, action: #selector(dismissButtonPressed(_:)), for: .touchUpInside)
        self.settingsButton.addTarget(self, action: #selector(settingsButtonPressed(_:)), for: .touchUpInside)
        self.recordButton.addTarget(self, action: #selector(recordButtonPressed(_:)), for: .touchUpInside)
        self.chatButton.addTarget(self, action: #selector(chatButtonPressed(_:)), for: .touchUpInside)
        self.modeSwitch.addTarget(self, action: #selector(modeSwitchChanged(_:)), for: .touchUpInside)
        
        self.setHiddenFeature()
        self.enableFuncWhenJoinedRoom(isJoined: false)
        self.updateUIForIncomingMode(presenter.mode)
        self.updateHandFreeModeUIForMicroState(presenter.microState)
    }
    
    override func localizeString() {
        self.chatTitleButtonLabel.text = NSLocalizedString("chat_button_title", comment: "")
        self.talkTitleButtonLabel.text = NSLocalizedString("talk_button_title", comment: "")
        self.recordTitleButtonLabel.text = NSLocalizedString("record_on_button_title", comment: "")
        self.pushModeLabel.text = NSLocalizedString("push_mode_label_text", comment: "")
        self.handFreeModeLabel.text = NSLocalizedString("hand_free_mode_label_text", comment: "")
    }
    /*
     Request an audio meeting service creation inside presenter.
     We also listen for the changing of data once the presenter delegate their notify.
     */
    fileprivate func initMeetingAudioChat(withChannelSid channelSid: String) {
        talkConnection.setupToast()
        self.presenter.initializeMeetingAudioChatServices(withHost: jitsiDomain, andRoom: channelSid,withView: self.view)
        self.presenter.meetingAudioChatServicesDidJoinedRoom = { [weak self] in
            guard let self = self else {
                return
            }
            DispatchQueue.main.async {
                self.talkConnection.dismiss()
                hud.dismiss()
                self.isWaitingForReconnect = false
                self.enableFuncWhenJoinedRoom(isJoined: true)
            }
        }
        self.presenter.meetingAudioChatServicesDidLeftRoom = {
            // the services audio did left
        }
        self.presenter.meetingAudioChatServicesDidErrorWithoutReconnect = { [weak self] in
            guard let self = self else { return }
            self.checkConnectAndLeaveRoom(forceLeave: true, withStatus:self.presenter.audioServicesCanTalking())
        }
        self.presenter.meetingAudioChatServicesDidErrorWithReconnect = {
            /*
             The reconnect is in process, waiting it!
             if failed a delegate : meetingAudioChatServicesDidErrorWithoutReconnect should be called
            */
            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(defaultTimeReconnect)) { [weak self] in
                guard let self = self else { return }
                self.checkConnectAndLeaveRoom(forceLeave: false, withStatus:self.presenter.audioServicesCanTalking())
            }
        }
    }
    private func checkConnectAndLeaveRoom(forceLeave: Bool,withStatus canTalking: Bool) {
        let shouldShowPopupAndBack = forceLeave || !canTalking
        if !shouldShowPopupAndBack {
            return
        }
        self.talkConnection.dismiss()
        let alertController = UIAlertController(title: "",
                                                message: NSLocalizedString("jitsi_disconnect_without_reason_message_text", comment: ""),
                                                preferredStyle: .alert)
        let dismissAction = UIAlertAction(title: "OK", style: .default, handler: { [weak self] (action) in
            guard let `self` = self else { return }
            self.disconnectRoom()
        })
        alertController.addAction(dismissAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    private func setHiddenFeature() {
//        self.settingsButton.isHidden = true
//        self.chatButton.isHidden = true
//        self.chatTitleButtonLabel.isHidden = true
        
//        self.modeSwitch.isHidden = true
//        self.pushModeLabel.isHidden = true
//        self.handFreeModeLabel.isHidden = true
    }
    
    private func enableFuncWhenJoinedRoom(isJoined: Bool) {
        self.recordButton.isEnabled = isJoined
    }
    
    private func handlePushToRecord(isStartRecording: Bool) {
        let audioName = isStartRecording ? "audio-start-Speaking" : "audio-finish-Speaking"
        AVPlayerManager.shared.playLocalAudio(name: audioName, type: "mp3")
        self.presenter.handleSpeaking()
        self.presenter.handleUserRecord(isStartRecording: isStartRecording)
    }
}
// MARK: - Handle Events

extension IncomingVC {
    // Disconnect to talk room
    @objc fileprivate func  disconnectRoom() {
        NotificationCenter.default.removeObserver(self)
//        self.presenter.clearChatServices()
        self.presenter = nil
        self.dismiss(animated: true) { [ weak self ] in
            guard let self = self else { return }
            GlobalVariable.didComeToIncomingVC = false
            self.delegate?.reloadListTalkGroup()
        }
    }
    
    @objc
    fileprivate
    func dismissButtonPressed(_ sender: UIButton) {
        self.disconnectRoom()
        
        // Disconnect BlueParrott headset to stop listening for button events
        BPHeadsetManager.shared.disconnectHeadset()
    }
    
    @objc
    fileprivate
    func settingsButtonPressed(_ sender: UIButton) {
        let settingsIncomingVC = SettingsIncomingVC()
        settingsIncomingVC.talkGroup = presenter.talkGroup
        settingsIncomingVC.firstNumberOfUnreadMessage = self.numberOfUnreadMessage
        settingsIncomingVC.delegate = self
        settingsIncomingVC.modalPresentationStyle = .overCurrentContext
        self.present(settingsIncomingVC, animated: false, completion: nil)
    }
    
    @objc
    fileprivate
    func chatButtonPressed(_ sender: UIButton) {
        self.showChatVC()
    }
    
    @objc
    fileprivate
    func talkButtonTouchDown(_ sender: UIButton) {
        self.handlePushToRecord(isStartRecording: true)
    }
    
    @objc
    fileprivate
    func talkButtonTouchUp(_ sender: UIButton) {
        self.handlePushToRecord(isStartRecording: false)
    }
    
    @objc
    fileprivate
    func recordButtonPressed(_ sender: UIButton) {
        self.presenter.handleSpeaking()
    }
    
    @objc
    fileprivate
    func modeSwitchChanged(_ sender: CustomSwitch) {
        self.presenter.handleIncomingMode()
    }
    
    fileprivate
    func showInviteUserVC() {
        let addUserIncomingVC = AddUserIncomingVC()
        addUserIncomingVC.modalPresentationStyle = .overCurrentContext
        addUserIncomingVC.presenter = self.presenter.transferToAddUserIncomingPresenter(addUserIncomingVC)
        self.present(addUserIncomingVC, animated: true, completion: nil)
    }
    
    fileprivate
    func showShareQRCode() {
        self.presenter.showQRCode(fromVC: self)
    }
    
    fileprivate func showChatVC() {
        let vc = ChatVC()
        vc.chatPresenter = ChatPresenter(view: vc, talkGroup: presenter.talkGroup, messages: [])
        vc.chatPresenter.shouldShowAudioLogMessages = shouldShowAudioLogMessages
        vc.chatPresenter.isFromTalk = true
        vc.delegate = self
        self.present(vc, animated: true, completion: nil)
    }
}

// MARK: - SettingsIncomingDelegate

extension IncomingVC: SettingsIncomingDelegate {
    func didPressedInviteUserButton(in settingsIncomingVC: SettingsIncomingVC) {
        settingsIncomingVC.dismiss(animated: false, completion: nil)
        self.showInviteUserVC()
    }
    
    func didPressedShareQRButton(in settingsIncomingVC: SettingsIncomingVC) {
        settingsIncomingVC.dismiss(animated: false, completion: nil)
        self.showShareQRCode()
    }
    
    func didPressedChatButton(in settingsIncomingVC: SettingsIncomingVC) {
        settingsIncomingVC.dismiss(animated: false, completion: nil)
        self.showChatVC()
    }
}

// MARK: ChatVCDelegate

extension IncomingVC: ChatVCDelegate {
    func chatVCDismissed(shouldShowAudioLogMessages: Bool) {
        self.shouldShowAudioLogMessages = shouldShowAudioLogMessages
    }
}

// MARK: - UICollectionViewDataSource

extension IncomingVC: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.presenter.numberOfRows(inSection: section)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeue(IncomingMemberCell.self, forIndexPath: indexPath)
        self.presenter.configure(cell, at: indexPath)
        
        return cell
    }
}

// MARK: - UICollectionViewDelegateFlowLayout

extension IncomingVC: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 14.5, bottom: 0, right: 14.5)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let widthCell = (GlobalVariable.screenWidth - 30) / 4
        return CGSize(width: widthCell, height: widthCell + 10)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.collectionView.deselectItem(at: indexPath, animated: true)
//        if indexPath.item == 0 {
//            self.showInviteUserVC()
//        }
//        else {
//            self.presenter.handleSelectMember(at: indexPath)
//        }
        self.presenter.handleSelectMember(at: indexPath)
    }
}

// MARK: - Implement View

extension IncomingVC: IncomingView {
    func reloadUI(talkGroup: TalkGroup) {
        self.nameTalkGroupLabel.text = talkGroup.name
        self.backgroundImageView.loadImageFromURL(talkGroup.background, placeholder: UIImage(named: "bg-newtalk01"))
        let allOnlineUserCount = talkGroup.allUsers.filter { (user) -> Bool in
            return user.isCalling == true
        }.count
        let totalUserCount = talkGroup.allUsers.count
        self.numberOfUserLabel.updateActiveNumber(allOnlineUserCount, inTotalNumber: totalUserCount)
        self.collectionView.reloadData()
    }
    
    func reloadRowAt(indexPath: IndexPath) {
        self.collectionView.performBatchUpdates({
            self.collectionView.reloadItems(at: [indexPath])
        }, completion: nil)
    }
    
    func joinTalkRoom(withChannelSid channelSid: String) {  /*
         REMOVED socket listening in main screen, all thing will he handled by services it self.
         */
        NotificationCenter.default.addObserver(self, selector: #selector(handleNotificationForJoinOrLeave(notification:)), name: NSNotification.Name(rawValue: kMemberJoinOrLeaveTalkEvent), object: nil)

        DispatchQueue.main.async {
            self.initMeetingAudioChat(withChannelSid: channelSid)
        }
    }
    
    @objc private func handleNotificationForJoinOrLeave(notification: Notification) {
        let myDict = notification.object as! [AnyHashable : Any]
        let channelId = myDict["channelId"] as! Int
        let userId = myDict["userId"] as! Int
        let numberOfCalling = myDict["inCalling"] as! Int
        let socketEvent = myDict["socketEvent"] as! SocketEvent
        
        guard self.presenter != nil else { return }
        
        if channelId == self.presenter.talkGroupId {
            self.presenter.renewTalkGroup(userId: userId, numberOfCalling: numberOfCalling, socketEvent: socketEvent)
        }
    }
    
    func updateNumberOfUnreadMessage(_ number: Int?) {
        self.numberOfUnreadMessage = number
    }
    
    func updateHandFreeModeUIForMicroState(_ state: MicroState) {
        switch state {
        case .MicroON:
            self.recordTitleButtonLabel.textColor = UIColor.ct.recordOnTitleButtonColor
            self.recordTitleButtonLabel.text = NSLocalizedString("record_on_button_title", comment: "")
            self.recordButton.setBackgroundImage(UIImage(named: "ic_record_on"), for: .normal)
        case .MicroOFF:
            self.recordTitleButtonLabel.textColor = UIColor.ct.recordOffTitleButtonColor
            self.recordTitleButtonLabel.text = NSLocalizedString("record_off_button_title", comment: "")
            self.recordButton.setBackgroundImage(UIImage(named: "ic_record_off"), for: .normal)
        }
    }
    
    func updateUIForIncomingMode(_ mode: IncomingMode) {
        switch mode {
        case .Push:
            self.talkButton.isHidden = false
            self.talkTitleButtonLabel.isHidden = false
            
            self.recordButton.isHidden = true
            self.recordTitleButtonLabel.isHidden = true
            
            self.modeSwitch.setOn(on: false, animated: true)
            self.pushModeLabel.textColor = UIColor.white
            self.handFreeModeLabel.textColor = UIColor.white.withAlphaComponent(0.7)
        case .HandFree:
            self.talkButton.isHidden = true
            self.talkTitleButtonLabel.isHidden = true
            
            self.recordButton.isHidden = false
            self.recordTitleButtonLabel.isHidden = false
            
            self.modeSwitch.setOn(on: true, animated: true)
            self.pushModeLabel.textColor = UIColor.white.withAlphaComponent(0.7)
            self.handFreeModeLabel.textColor = UIColor.white
        }
    }
    
    func showAlertForDetailOfMember(_ member: User, talkGroup: TalkGroup) {
        let alertMember = BlockMemberAlertView.loadFromNib()
        alertMember.show(member: member, talkGroup: talkGroup) { (action) in
            switch action {
            case .RemoveMember:
                self.showDeleteMemberAlert(member: member, talkGroup: talkGroup)
            case .Banish:
                self.showBandMemberConfirmAlert(member: member, talkGroup: talkGroup)
            case .Report:
                self.showReportMemberConfirmView(member: member)
            case .UnBlock:
                self.unlockMember(member: member)
            case .Block:
                self.blockMember(member: member)
            }
        }
    }
    
    func showAlertForLeavingOfMember(_ message: String) {
        let toastView = ToastView.loadViewFromNib()
        toastView.toastViewColor = UIColor.ct.incomingToastViewColor.withAlphaComponent(0.94)
        toastView.show(text: message)
    }
    
    func showAlertForJoiningOfMember(_ message: String) {
        let toastView = ToastView.loadViewFromNib()
        toastView.toastViewColor = UIColor.ct.incomingToastViewColor.withAlphaComponent(0.94)
        toastView.show(text: message)
    }
    
    func showAlertForRetrievingMessage(title: String, message: String) {
        let toastView = MessageToastView.loadViewFromNib()
        toastView.toastViewColor = UIColor.ct.incomingToastViewColor.withAlphaComponent(0.94)
        toastView.show(title: title, message: message)
    }
    
    func deletedMemberInTalkGroup(talkID: Int, userId: Int, shouldShowToast: Bool) {
        guard self.presenter != nil else { return }

        if let index = self.presenter.talkGroup.allUsers.index(where: {$0.id == userId}) {
            if shouldShowToast {
                let toastView = ToastView.loadViewFromNib()
                toastView.show(text: NSLocalizedString("deleteted_message", comment: ""))
            }
            
            let indexPath = IndexPath(row: index, section: 0)
            self.presenter.talkGroup.allUsers.remove(at: index)
            collectionView.deleteItems(at: [indexPath])
            
            let allOnlineUserCount = self.presenter.talkGroup.allUsers.filter { (user) -> Bool in
                return user.isCalling == true
                }.count
            let totalUserCount = self.presenter.talkGroup.allUsers.count
            self.numberOfUserLabel.updateActiveNumber(allOnlineUserCount, inTotalNumber: totalUserCount)
            
            if shouldShowToast {
                delegate?.deletedMemberFromInComing(groupID: talkID, userId: userId)
            }
        }
    }
    
    func dismissVC() {
        self.dismiss(animated: true) {
            self.delegate?.reloadListTalkGroup()
        }
    }
}

// MARK: - Show member's detail

extension IncomingVC {
    
    fileprivate
    func showDeleteMemberAlert(member: User, talkGroup: TalkGroup) { // CT-C-5-4
        let confirmAlertView = ConfirmDeleteTalkView.loadFromNib()
        confirmAlertView.isDeleteMember = true
        confirmAlertView.backgroundImage = UIImage(named: "bg_alert")
        confirmAlertView.member = member

        confirmAlertView.title = ""
        confirmAlertView.message = NSLocalizedString("delete_member_confirm_text", comment: "")
        confirmAlertView.confirmButtonTitle = NSLocalizedString("delete_button_alert_text", comment: "")
        confirmAlertView.show {
            self.presenter.removeMember(member: member)
        }
    }
    
    fileprivate
    func showBandMemberConfirmAlert(member: User, talkGroup: TalkGroup) { // CT-C-5-4-1
        let confirmAlertView = ConfirmDeleteTalkView.loadFromNib()
        confirmAlertView.isDeleteMember = true
        confirmAlertView.backgroundImage = UIImage(named: "bg_alert")
        confirmAlertView.member = member

        confirmAlertView.title = ""
        confirmAlertView.message = NSLocalizedString("band_member_message", comment: "")
        let bandText = NSLocalizedString("band_infor_text", comment: "")
        confirmAlertView.moreInfor = String(format: bandText, "Organize-Name")
        confirmAlertView.confirmButtonTitle = NSLocalizedString("band_button_title", comment: "")
        confirmAlertView.show {
            self.presenter.banishMember(member: member)
            // success -->
            print("Band: will show toast")
            let toastView = ToastView.loadViewFromNib()
            toastView.show(text: NSLocalizedString("banded_message", comment: ""))
        }
    }
    
    fileprivate
    func showReportMemberConfirmView(member: User) { // CT-C-5-4-2
        let confirmAlertView = ReportMemberConfirmView.loadFromNib()
        confirmAlertView.backgroundImage = UIImage(named: "bg_alert")
        confirmAlertView.organize = "Organize-Name "
        confirmAlertView.show {
            self.presenter.reportMember(member: member)
            // success -->
            print("Report: will show toast")
            let toastView = ToastView.loadViewFromNib()
            toastView.show(text: NSLocalizedString("reported_message", comment: ""))
        }
    }
    
    private func blockMember(member: User) {
        self.presenter.blockMember(member: member)
        // Success ->>
        print("Blocked: show toast")
        let toastView = ToastView.loadViewFromNib()
        toastView.show(text: NSLocalizedString("blocked_message", comment: ""))
    }
    
    private func unlockMember(member: User) {
        self.presenter.unlockMember(member: member)
        // Success ->>
        print("Unlocked: show toast")
        let toastView = ToastView.loadViewFromNib()
        toastView.show(text: NSLocalizedString("un_block_message", comment: ""))
    }
}

// MARK: - Bluetooth BlueParrott Headsets
extension IncomingVC {
    @objc func handleRouteChange(notification: Notification) {
        guard let userInfo = notification.userInfo,
            let reasonValue = userInfo[AVAudioSessionRouteChangeReasonKey] as? UInt,
            let reason = AVAudioSession.RouteChangeReason(rawValue: reasonValue) else {
                return
        }
        if reason == .newDeviceAvailable {
            if isHeadsetConnectedToDevice() && !BPHeadsetManager.shared.isHeadsetConnected() {
                connectHeadset()
            }
        }
    }
    
    fileprivate func isHeadsetConnectedToDevice() -> Bool {
        for output in AVAudioSession.sharedInstance().currentRoute.outputs where isHeadsetPortSupported(output: output) {
            return true
        }
        return false
    }
    
    fileprivate func isHeadsetConnectedToApp() -> Bool {
        return BPHeadsetManager.shared.isHeadsetConnected()
    }
    
    fileprivate func isHeadsetPortSupported(output: AVAudioSessionPortDescription) -> Bool {
        return (output.portType == AVAudioSession.Port.bluetoothA2DP) || (output.portType == AVAudioSession.Port.bluetoothHFP)
    }
    
    fileprivate func connectHeadset() {
        BPHeadsetManager.shared.connectHeadset()
        BPHeadsetManager.shared.bpButtonCommandHandler = { [weak self] buttonCommand in
            guard let self = self else {
                return
            }
            
            switch buttonCommand {
            case .tap:
                if self.presenter.mode == .HandFree {
                    self.presenter.handleSpeaking()
                } else {
                    self.presenter.handleUserRecord(isStartRecording: false)
                }
            case .doubleTap:
                self.presenter.handleIncomingMode()
            case .longTap:
                if self.presenter.mode == .Push {
                    self.talkButton.isHighlighted = true
                    self.handlePushToRecord(isStartRecording: true)
                }
            case .finishLongTap:
                if self.presenter.mode == .Push {
                    self.talkButton.isHighlighted = false
                    self.handlePushToRecord(isStartRecording: false)
                }
            }
        }
    }
}
