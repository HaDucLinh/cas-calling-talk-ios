//
//  AddUserIncomingVC.swift
//  CallingTalk
//
//  Created by Toof on 2/21/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit

class AddUserIncomingVC: BaseViewController {

    // MARK: Properties
    
    public var presenter: AddUserIncomingPresenter!
    
    @IBOutlet weak var containView: UIView!
    @IBOutlet weak var memberImageView: UIImageView!
    @IBOutlet weak var ratioHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var inviteHeightConstraint: NSLayoutConstraint!

    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var inviteButton: UIButton!
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var blurView: GradientView!
    var isFromSeenUser = false
    
    // MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupSubviews()
        self.presenter.fetch()
    }
    
    fileprivate
    func setupSubviews() {
        self.containView.corner = 20.0
        
        // Setup close's Button
        self.closeButton.addTarget(self, action: #selector(closeButtonPressed(_:)), for: .touchUpInside)
        
        // Setup invite's Button
        self.inviteButton.layer.cornerRadius = 10.0
        // Setup theme appearance: background color and text display
        self.inviteButton.backgroundColor = UIColor.ct.mainBackgroundColor
        self.inviteButton.setTitleColor(UIColor.ct.headerText, for: .normal)
        self.inviteButton.addTarget(self, action: #selector(inviteButtonPressed(_:)), for: .touchUpInside)
        if isFromSeenUser {
            self.memberImageView.image = UIImage(named: "ic-member-small")
            self.inviteButton.isHidden = true
            self.titleLabel.text = NSLocalizedString("Read_member_text", comment: "")
            
            self.ratioHeightConstraint.constant = 460 * GlobalVariable.ratioHeight
            self.inviteHeightConstraint.constant = 0
        } else {
            self.ratioHeightConstraint.constant = 530 * GlobalVariable.ratioHeight
        }
        self.view.layoutIfNeeded()
        self.view.updateConstraints()
        
        // Setup blur view
        self.blurView.gradientLayer.colors = [
            UIColor.white.withAlphaComponent(0.0).cgColor,
            UIColor.white.cgColor
        ]
        self.blurView.gradientLayer.gradient = GradientPoint.topBottom.draw()
        
        // Setup table view
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.register(SelectMemberCell.self)
        self.tableView.separatorColor = UIColor.white.withAlphaComponent(0.0)
        self.tableView.backgroundColor = UIColor.white.withAlphaComponent(0.0)
        self.tableView.tableFooterView = UIView()
    }
    
    override func localizeString() {
        self.titleLabel.text = NSLocalizedString("adding_user_talk_group_label_text", comment: "")
        self.inviteButton.setTitle(NSLocalizedString("invite_member_button_title", comment: ""), for: .normal)
    }
    
}

// MARK: - Handle Events

extension AddUserIncomingVC {
    
    @objc
    fileprivate
    func closeButtonPressed(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc
    fileprivate
    func inviteButtonPressed(_ sender: UIButton) {
        self.presenter.handleInviteUser()
        self.dismiss(animated: true, completion: nil)
    }
    
}

// MARK: - UITableViewDataSource

extension AddUserIncomingVC: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.presenter.numberOfRows(inSection: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeue(SelectMemberCell.self)
        self.presenter.configure(cell, at: indexPath)
        cell.checkImageView.isHidden = isFromSeenUser
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
}

// MARK: - UITableViewDelegate

extension AddUserIncomingVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! SelectMemberCell
        self.presenter.inviteUser(cell, at: indexPath)
    }
    
}

// MARK: - Implement View

extension AddUserIncomingVC: AddUserIncomingView {
    
    func refresh() {
        self.tableView.reloadData()
    }
    
}
