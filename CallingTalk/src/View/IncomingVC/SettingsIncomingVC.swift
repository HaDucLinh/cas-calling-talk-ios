//
//  SettingIncomingVC.swift
//  CallingTalk
//
//  Created by Toof on 2/20/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit
import Swinject
import ObjectMapper

protocol SettingsIncomingDelegate: class {
    func didPressedInviteUserButton(in settingsIncomingVC: SettingsIncomingVC)
    func didPressedShareQRButton(in settingsIncomingVC: SettingsIncomingVC)
    func didPressedChatButton(in settingsIncomingVC: SettingsIncomingVC)
}

class SettingsIncomingVC: BaseViewController {

    // MARK: Properties
    
    @IBOutlet weak var containView: UIView!
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var addUserButton: UIButton!
    @IBOutlet weak var shareQRButton: UIButton!
    @IBOutlet weak var chatButton: BadgeButton!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var numberOfMemberLabel: UILabel!
    @IBOutlet weak var closeButton: UIButton!
    weak var delegate: SettingsIncomingDelegate?
    open var talkGroup: TalkGroup!
    open var firstNumberOfUnreadMessage: Int?
    fileprivate var numberOfUnreadMessage: Int? {
        didSet {
            self.chatButton.badgeString = self.numberOfUnreadMessage.toUnreadNumberBadgeString()
        }
    }
    
    // MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupSubviews()
        self.setUpData()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.avatarImageView.roundedCornerRadius()
        
        // Set position for badge message
        self.numberOfUnreadMessage = self.firstNumberOfUnreadMessage
        self.chatButton.badgeEdgeInsets = UIEdgeInsets(top: self.chatButton.bounds.size.height / 3.5, left: 0, bottom: 0, right: self.chatButton.bounds.size.width / 3.5)
    }
    
    override func notifyForUnreadMessage(_ isMention: Bool, of talkGroupId: Int) {
        super.notifyForUnreadMessage(isMention, of: talkGroupId)
        if talkGroupId == self.talkGroup.id {
            let numberOfMentionMessage = isMention ? 1 : 0
            self.numberOfUnreadMessage = (self.numberOfUnreadMessage ?? 0) + numberOfMentionMessage
        }
    }
    
    override func notifyForMarkAsReadMessage(of talkGroupId: Int) {
        super.notifyForMarkAsReadMessage(of: talkGroupId)
        if talkGroupId == self.talkGroup.id {
            self.numberOfUnreadMessage = nil
        }
    }
    
    fileprivate
    func setupSubviews() {
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        self.containView.corner = 20.0
        
        if let systemSetting = Container.default.resolve(SystemThemeable.self) {
            let currentImageConverted = systemSetting.convertImageToTheme(withName: "bg_alert")
            self.backgroundImageView.image = UIImage(named: currentImageConverted)
        }
        backgroundImageView.contentMode = .scaleAspectFill

        avatarImageView.corner = avatarImageView.frame.size.width / 2
        
        // Setup close's button
        self.closeButton.addTarget(self, action: #selector(closeButtonPressed(_:)), for: .touchUpInside)
        
        // Setup add's button
        self.addUserButton.addTarget(self, action: #selector(addUserButtonPressed(_:)), for: .touchUpInside)
        
        // Setup qr's button
        self.shareQRButton.addTarget(self, action: #selector(shareQRButtonPressed(_:)), for: .touchUpInside)
        
        // Setup chat's button
        self.chatButton.addTarget(self, action: #selector(chatButtonPressed(_:)), for: .touchUpInside)
        self.chatButton.badgeBackgroundColor = UIColor.ct.badgeBackgroundColor
        self.setHiddenFeatures()
    }
    
    fileprivate func setUpData() {
        self.avatarImageView.loadImageFromURL(self.talkGroup.avatar, placeholder: UIImage(named: "img_avatar_talkgroup_empty"))
        self.nameLabel.text = self.talkGroup.name
        self.numberOfMemberLabel.updateActiveNumber(self.talkGroup.meetingUserCount, inTotalNumber: self.talkGroup.joinUsersCount)
    }
    
    private func setHiddenFeatures() {
        self.addUserButton.isHidden = true
    }
}

// MARK: - Handle Events

extension SettingsIncomingVC {
    @objc
    fileprivate
    func closeButtonPressed(_ sender: UIButton) {
        self.dismiss(animated: false, completion: nil)
    }
    
    @objc
    fileprivate
    func addUserButtonPressed(_ sender: UIButton) {
        self.delegate?.didPressedInviteUserButton(in: self)
    }
    
    @objc
    fileprivate
    func shareQRButtonPressed(_ sender: UIButton) {
        self.delegate?.didPressedShareQRButton(in: self)
    }
    
    @objc
    fileprivate
    func chatButtonPressed(_ sender: UIButton) {
        self.delegate?.didPressedChatButton(in: self)
    }
}
