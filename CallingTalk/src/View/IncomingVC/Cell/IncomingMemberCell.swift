//
//  IncomingMemberCell.swift
//  CallingTalk
//
//  Created by Toof on 4/2/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit
class IncomingMemberCell: UICollectionViewCell {
    @IBOutlet private weak var speakIndicatorView: RoundCicleAnimation!
    @IBOutlet private weak var memberAvatar: UIImageView!
    @IBOutlet private weak var memberNameLabel: UILabel!
    @IBOutlet private weak var memberRoleLabel: UILabel!
    @IBOutlet private weak var memberRoleView: UIView!
    
    let placeholderImage = UIImage(named: "img_avatar_user_empty")
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.memberRoleView.isHidden = true
        self.memberRoleView.corner = self.memberRoleView.frame.size.height / 2
        self.memberAvatar.corner = self.memberAvatar.frame.size.width / 2
        self.memberNameLabel.textColor = UIColor.white
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.memberAvatar.corner = memberAvatar.frame.size.width / 2
        self.speakIndicatorView.setLayoutCompletedLoad()
    }
    
    override func willMove(toSuperview newSuperview: UIView?) {
        self.layoutIfNeeded()
    }
    override func prepareForReuse() {
        super.prepareForReuse()
        self.speakIndicatorView.hideAnimation()
        self.memberAvatar.cancelRequestImage()
        self.memberAvatar.image = placeholderImage
        self.setSpeachIndicator(hidden: true)
    }
    
    fileprivate var roleType: UserRoleType = .Member {
        didSet {
            switch self.roleType {
            case .Admin:
                self.memberRoleView.isHidden = false
                self.memberRoleView.backgroundColor = UIColor.ct.redKeyColor
                self.memberRoleLabel.text = NSLocalizedString("admin_role_text", comment: "")
            case .Author:
                self.memberRoleView.isHidden = false
                self.memberRoleView.backgroundColor = UIColor.ct.okAlertButtonColor
                self.memberRoleLabel.text = NSLocalizedString("author_text", comment: "")
            case .Member:
                self.memberRoleView.isHidden = true
                self.memberRoleLabel.text = "\t"
            }
        }
    }
    
    public var user: User! {
        didSet {
            guard let user = user else {
                self.memberAvatar.image = nil
                self.memberNameLabel.text = "\t"
                self.memberRoleView.isHidden = true
                return
            }
            
            self.memberNameLabel.text = user.name
            self.memberAvatar.loadImageFromURL(user.avatarUrl, placeholder: placeholderImage)
            
            if user.isAdmin {
                self.roleType = .Admin
            }
            else {
                self.roleType = .Member
            }
            if user.isOwner {
                self.roleType = .Author
            }
        }
    }
    
    public var isFromIncomming = false {
        didSet {
            if self.isFromIncomming {
                if self.user.isCalling {
                    self.memberAvatar.layer.opacity = 1
                } else {
                    self.memberAvatar.layer.opacity = 0.3
                    self.setSpeachIndicator(hidden: true)
                }
            }
        }
    }
    
    public var isSpeaking = false {
        didSet {
            if self.isSpeaking {
                if !self.user.isCalling {
                    return
                }
                self.speakIndicatorView.resizeRoundCirleView()
                self.setSpeachIndicator(hidden: false)
                if self.isFromIncomming {
                    self.speakIndicatorView.showAnimation()
                }
            }
        }
    }
    
    func setSpeachIndicator(hidden: Bool){
        if !hidden {
            self.speakIndicatorView.resizeRoundCirleView()
        }
        self.speakIndicatorView.isHidden = hidden
    }
}
