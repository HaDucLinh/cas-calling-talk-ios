//
//  MemberVC.swift
//  CallingTalk
//
//  Created by Toof on 2/14/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit

class MemberVC: BaseViewController {
    
    // MARK: Properties
    
    fileprivate var presenter: MemberPresenter!
    
    @IBOutlet weak var emptyMemberView: UIView!
    @IBOutlet weak var emptyMemberLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    fileprivate let refreshControl = UIRefreshControl()
    
    // MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.presenter = MemberPresenter(view: self)
        
        self.setupSubviews()
        self.presenter.fetch()
    }
    
    fileprivate
    func setupSubviews() {
        // Custom navigation's transition
//        self.navigationController?.delegate = self
        
        // Setup font for member empty label
        self.emptyMemberLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .bold, size: 14.0)
        
        // Setup TableView
        self.tableView.dataSource = self
        self.tableView.delegate = self
        
        self.tableView.register(MemberRoleAdminCell.self)
        
        self.tableView.backgroundColor = UIColor.white.withAlphaComponent(0.0)
        self.tableView.separatorColor = UIColor.white.withAlphaComponent(0.0)
        self.tableView.contentInset.top = 22.0
        self.tableView.tableFooterView = UIView()
        
        // Setup RefreshControl
        self.refreshControl.tintColor = UIColor.ct.mainBackgroundColor
        self.refreshControl.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)
        if #available(iOS 10.0, *) {
            self.tableView.refreshControl = self.refreshControl
        } else {
            self.tableView.addSubview(self.refreshControl)
        }
    }
    
    override func localizeString() {
        self.navigationItem.title = NSLocalizedString("member_navigation_item_title", comment: "")
        self.emptyMemberLabel.text = NSLocalizedString("empty_member_label_text", comment: "")
    }
    
}

// MARK: - Handle Events

extension MemberVC {
    
    @objc
    fileprivate
    func refreshData(_ sender: UIRefreshControl) {
        
    }
    
}

// MARK: - UITableViewDataSource

extension MemberVC: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.presenter.numberOfRows(inSection: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeue(MemberRoleAdminCell.self)
        self.presenter.configure(cell, at: indexPath)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75.0
    }
    
}

// MARK: - UITableViewDelegate

extension MemberVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let memberDetailVC = MemberDetailVC()
        memberDetailVC.presenter = self.presenter.transferToMemberDetailPresenter(memberDetailVC, at: indexPath)
        self.navigationController?.pushViewController(memberDetailVC, animated: true)
    }
    
}

// MARK: - UINavigationControllerDelegate

//extension MemberVC: UINavigationControllerDelegate {
//
//    func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationController.Operation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
//        switch operation {
//        case .push:
//            return PushUpAnimator(type: .navigation)
//        case .pop:
//            return PopDownAnimator(type: .navigation)
//        default:
//            return nil
//        }
//    }
//
//}

// MARK: Implement View

extension MemberVC: MemberView {
    
    func showEmptyMemberView(_ isShow: Bool) {
        self.emptyMemberView.isHidden = !isShow
    }
    
    func refresh() {
        self.tableView.reloadData()
    }
    
}
