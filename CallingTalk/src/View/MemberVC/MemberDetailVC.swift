//
//  MemberDetailViewController.swift
//  CallingTalk
//
//  Created by Toof on 2/28/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit

class MemberDetailVC: BaseViewController {
    
    // MARK: Properties
    
    open var presenter: MemberDetailPresenter!
    
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var organizationNameLabel: UILabel!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var deleteMemberButton: UIButton!
    
    @IBOutlet weak var reportHistoryTitleLabel: UILabel!
    @IBOutlet weak var emptyReportHistoryLabel: UILabel!
    
    @IBOutlet weak var tableView: UITableView!
    
    fileprivate let refreshControl = UIRefreshControl()
    
    // MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupSubviews()
        self.presenter.fetch()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.setupNavigationBarWithColor()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.navigationController?.navigationBar.setupNavigationBarWithColor(.appTheme)
    }
    
    fileprivate
    func setupSubviews() {
        // Setup navigation's items
        let settings = CustomButtonBarSetting(imageName: "ic-close", style: .plain, action: #selector(closeButtonPressed(_:)), target: self, possition: .left)
        _ = CustomUIItemFactory().createAndLoadButtonBar(withSetting: settings)
        // Setup avatar's image
        self.avatarImageView.roundedCornerRadius()
        
        // Setup label
        self.usernameLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .bold, size: 24.0)
        self.organizationNameLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .regular, size: 14.0)
        self.reportHistoryTitleLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .bold, size: 16.0)
        self.emptyReportHistoryLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .regular, size: 14.0)
        
        // Setup button
        self.deleteMemberButton.setUnderlineTitle(NSLocalizedString("delete_member_button_title", comment: ""),
                                                  font: UIFont.ct.font(.NotoSansCJKjp, type: .regular, size: 15.0),
                                                  color: UIColor.ct.redKeyColor)
        
        // Setup TableView
        self.tableView.dataSource = self
        self.tableView.delegate = self
        
        self.tableView.register(MemberDetailCell.self)
        
        self.tableView.backgroundColor = UIColor.white.withAlphaComponent(0.0)
        self.tableView.separatorColor = UIColor.white.withAlphaComponent(0.0)
        self.tableView.tableFooterView = UIView()
        
        // Setup RefreshControl
        self.refreshControl.tintColor = UIColor.ct.mainBackgroundColor
        self.refreshControl.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)
        if #available(iOS 10.0, *) {
            self.tableView.refreshControl = self.refreshControl
        } else {
            self.tableView.addSubview(self.refreshControl)
        }
        
        // Add events
        self.deleteMemberButton.addTarget(self, action: #selector(deleteMemberButtonPressed(_:)), for: .touchUpInside)
    }
    
    override func localizeString() {
        self.reportHistoryTitleLabel.text = NSLocalizedString("report_title_label_text", comment: "")
        self.emptyReportHistoryLabel.text = NSLocalizedString("empty_report_label_text", comment: "")
    }
    
}

// MARK: - Handle Events

extension MemberDetailVC {
    
    @objc
    fileprivate
    func closeButtonPressed(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc
    fileprivate
    func deleteMemberButtonPressed(_ sender: UIButton) {
        self.showBandMemberConfirmAlert()
    }
    
    @objc
    fileprivate
    func refreshData(_ sender: UIRefreshControl) {
        
    }
    
    fileprivate
    func showBandMemberConfirmAlert() { // CT-C-5-4-1
        let bandText = NSLocalizedString("band_infor_text", comment: "")
        let confirmAlertView = ConfirmDeleteTalkView.loadFromNib()
        confirmAlertView.isDeleteMember = true
        confirmAlertView.backgroundImage = UIImage(named: "bg_alert")
        confirmAlertView.organize = "Organize-Name "
        confirmAlertView.title = ""
        confirmAlertView.message = NSLocalizedString("band_member_message", comment: "")
        confirmAlertView.moreInfor = String(format: bandText, "Organize-Name")
        confirmAlertView.confirmButtonTitle = NSLocalizedString("band_button_title", comment: "")
        confirmAlertView.show {
            print("Band: will show toast")
            let toastView = ToastView.loadViewFromNib()
            toastView.show(text: NSLocalizedString("banded_message", comment: ""))
        }
    }
    
}

// MARK: - UITableViewDataSource

extension MemberDetailVC: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.presenter.numberOfRows(inSection: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeue(MemberDetailCell.self)
        self.presenter.configure(cell, at: indexPath)
        
        return cell
    }
    
}

// MARK: - UITableViewDelegate

extension MemberDetailVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        //
    }
    
}

// MARK: Implement View

extension MemberDetailVC: MemberDetailView {
    
    func showEmptyReportHistoryLabel(_ isShow: Bool) {
        self.emptyReportHistoryLabel.isHidden = !isShow
    }
    
    func refresh() {
        self.tableView.reloadData()
    }
    
}
