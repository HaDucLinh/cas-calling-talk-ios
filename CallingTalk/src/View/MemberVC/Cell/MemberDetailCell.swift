//
//  MemberDetailTableViewCell.swift
//  CallingTalk
//
//  Created by Toof on 2/28/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit

class MemberDetailCell: UITableViewCell {

    // MARK: Properties
    
    @IBOutlet weak var readNoticeImageView: UIImageView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var reasonLabel: UILabel!
    
    @IBOutlet weak var borderView: UIView!
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var callerLabel: UILabel!
    @IBOutlet weak var callerView: UIView!
    
    // MARK: Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.avatarImageView.roundedCornerRadius()
        self.callerView.corner = self.callerView.bounds.size.height / 2
        self.borderView.corner = 8.0
        
        self.dateLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .regular, size: 13.0)
        self.reasonLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .bold, size: 15.0)
        self.usernameLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .bold, size: 15.0)
        self.callerLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .bold, size: 11.0)
        
        self.callerLabel.text = NSLocalizedString("caller_label_text", comment: "")
    }
    
    public
    func updateInfoLabel(_ text: String) {
        //
    }
    
    public func updateNoticeStatus(_ status: ReadNoticeStatus) {
        switch status {
        case .NoNotice:
            break
        case .NoticeNotRead:
            self.readNoticeImageView.image = UIImage(named: "ic_notice_member")
        case .NoticeRead:
            self.readNoticeImageView.image = UIImage()
        }
    }
    
    public
    func updateDateLabel(_ date: Date) {
        self.dateLabel.text = date.stringLocale
    }
    
}
