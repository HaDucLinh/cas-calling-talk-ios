//
//  MemberRoleAdminCell.swift
//  CallingTalk
//
//  Created by Toof on 3/1/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit

public enum ReadNoticeStatus: Int {
    case NoNotice
    case NoticeNotRead
    case NoticeRead
}

class MemberRoleAdminCell: UITableViewCell {

    // MARK: Properties
    
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var infoLabel: UILabel!
    
    @IBOutlet weak var noticeImageView: UIImageView!
    @IBOutlet weak var noticeLabel: UILabel!
    @IBOutlet weak var noticeView: UIView!
    
    // MARK: Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.avatarImageView.roundedCornerRadius()
        
        self.usernameLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .regular, size: 15.0)
        self.noticeLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .bold, size: 12.0)
        
        self.noticeLabel.text = NSLocalizedString("read_notice_label_text", comment: "")
    }
    
    public
    func updateNoticeForStatus(_ status: ReadNoticeStatus) {
        switch status {
        case .NoNotice:
            self.noticeView.isHidden = true
        case .NoticeNotRead:
            self.noticeView.isHidden = false
            self.noticeImageView.image = UIImage(named: "ic_notice_member")
            self.noticeLabel.textColor = UIColor.ct.redKeyColor//UIColor.ct.mainBackgroundColor
        case .NoticeRead:
            self.noticeView.isHidden = false
            self.noticeImageView.image = UIImage(named: "ic_notice_member_inactive")
            self.noticeLabel.textColor = UIColor.ct.textNoticeReadColor
        }
    }
    
}
