//
//  EditTalkVC.swift
//  CallingTalk
//
//  Created by Vu Tran on 2/18/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit
import Kingfisher

protocol EditTalkVCDelegate: NSObjectProtocol {
    func didUpdateTalk(isEditName: Bool, talk: TalkGroup)
}

class EditTalkVC: BaseViewController {
    let editTalkPresenter = EditTalkPresenter()

    @IBOutlet private weak var headerView: UIView!
    @IBOutlet private weak var talkImageView: UIImageView!
    @IBOutlet private weak var talkNameTextView: UITextView!

    @IBOutlet private weak var numberOfMembersLabel: UILabel!
    @IBOutlet private weak var talkNameCharactersLabel: UILabel!
    @IBOutlet private weak var organizationLabel: UILabel!

    @IBOutlet private weak var collectionView: UICollectionView!
    
    @IBOutlet private weak var chatButton: UIButton!
    @IBOutlet private weak var shareQRCode: UIButton!
    @IBOutlet private weak var actionButton: UIButton!
    
    private var selectedTalkImageID = 1
    fileprivate var defaultAvatarIndex = 1
    
    weak var delegate: EditTalkVCDelegate?
    @IBOutlet private weak var talkNameHeightConstraint: NSLayoutConstraint!

    override func viewDidLoad() {
        super.viewDidLoad()
        editTalkPresenter.attachView(self)
        setUpUI()
        setUpData()
    }
    // MARK: Life circle
    private func setUpUI() {
        title = NSLocalizedString("edit_talk_title", comment: "")
        let buttonBarSettingClose = CustomButtonBarSetting.init(imageName: "ic-close",
                                                                style: .plain,
                                                                action: #selector(close),
                                                                target: self,
                                                                possition: .left)
        let buttonBarSettingEdit = CustomButtonBarSetting.init(tittleLabel:  NSLocalizedString("done_button_text", comment: ""),
                                                               style: .plain,
                                                               action: #selector(editTalkGroup),
                                                               target: self,
                                                               possition: .right)
        _ = CustomUIItemFactory().createAndLoadButtonBar(withSetting: buttonBarSettingClose)
        _ = CustomUIItemFactory().createAndLoadButtonBar(withSetting: buttonBarSettingEdit)
        
        numberOfMembersLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .bold, size: 12)
        talkNameCharactersLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .regular, size: 12)
        
        talkImageView.corner = talkImageView.frame.size.width / 2
        let tapShowIcon = UITapGestureRecognizer(target: self, action: #selector(selectIcon))
        talkImageView.isUserInteractionEnabled = true
        talkImageView.addGestureRecognizer(tapShowIcon)
        
        talkNameTextView.font = UIFont.ct.font(.NotoSansCJKjp, type: .regular, size: 14)
        talkNameTextView.delegate = self
        talkNameTextView.isScrollEnabled = false
        
        if GlobalVariable.screenWidth == 320 {
            headerView.widthAnchor.constraint(equalTo: headerView.heightAnchor, multiplier: 75/53).isActive = true
        }
        
        collectionView.register(MemberCell.self)
        chatButton.isEnabled = false
        shareQRCode.isEnabled = false
        actionButton.isEnabled = false
    }
    
    fileprivate func setUpData() {
        if let avatar = editTalkPresenter.talkGroup?.avatar {
            talkImageView.loadImageFromURL(avatar, placeholder: UIImage(named: "img_avatar_talkgroup_empty"))
        }
        talkNameTextView.text = editTalkPresenter.talkGroup?.name
        
        var totalMembers = 0
        if let members = editTalkPresenter.talkGroup?.allUsers {
            totalMembers = members.count
        }
        numberOfMembersLabel.text = NSLocalizedString("number_of_member_text", comment: "") + totalMembers.description
        
        talkNameTextView.dynamicHeight(heightConstraint: talkNameHeightConstraint)
        self.view.updateConstraints()
        organizationLabel.text = editTalkPresenter.talkGroup?.organization?.name
        
        defaultAvatarIndex = editTalkPresenter.talkGroup?.avatarIndex ?? 1
        collectionView.dataSource = self
        collectionView.delegate = self
    }
    
    fileprivate func setPlaceHolderText() {
        let placeholderText = NSLocalizedString("enter_name_of_the_talk_text", comment: "")
        talkNameTextView.font = UIFont.ct.font(.NotoSansCJKjp, type: .regular, size: 14)
        talkNameTextView.text = placeholderText
        talkNameTextView.textColor = UIColor.ct.grayPlaceHolderTextColor
    }
    
    private func showLeaveConfirmAlert() { // CT-C-5-1-2
        let confirmAlertView = ConfirmDeleteTalkView.loadFromNib()
        confirmAlertView.isDeleteMember = false
        confirmAlertView.title = NSLocalizedString("leave_alert_title", comment: "")
        confirmAlertView.message = NSLocalizedString("leave_delete_history_chat", comment: "")
        confirmAlertView.confirmButtonTitle = NSLocalizedString("leave_button_alert_text", comment: "")
        confirmAlertView.show {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    private func showDeleteConfirmAlert() {  // CT-C-5-1-3
        let confirmAlertView = ConfirmDeleteTalkView.loadFromNib()
        confirmAlertView.isDeleteMember = false
        confirmAlertView.title = NSLocalizedString("delete_alert_title", comment: "")
        confirmAlertView.message = NSLocalizedString("delete_talk_message_text", comment: "")
        confirmAlertView.confirmButtonTitle = NSLocalizedString("delete_button_alert_text", comment: "")
        confirmAlertView.show {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    private func showToast(isEditName: Bool) {
        let text = isEditName ? NSLocalizedString("talk_updated_text", comment: "") : NSLocalizedString("invited_member_text", comment: "")
        let toastView = ToastView.loadViewFromNib()
        toastView.show(text: text)
    }
    
    // MARK: Actions

    @IBAction private func selectIcon(sender: UIButton) {
        let selectIconVC = SelectIconVC()
        selectIconVC.delegate = self
        selectIconVC.oldAvatarId = selectedTalkImageID
        selectIconVC.defaultAvatarIndex = defaultAvatarIndex
        selectIconVC.delegate = self
        selectIconVC.isFromNewTalk = true
        
        selectIconVC.transitioningDelegate = self
        self.definesPresentationContext = true
        selectIconVC.view.backgroundColor = .clear
        selectIconVC.modalPresentationStyle = .overFullScreen
        present(selectIconVC, animated: true, completion: nil)
    }
    
    @objc func close() {
        dismiss(animated: true, completion: nil)
    }
    
    @objc func editTalkGroup() {
        editTalkPresenter.editTalkGroup(name: talkNameTextView.text, talkImageData: editTalkPresenter.avartarData, avatarIndex: defaultAvatarIndex, backGroundData: editTalkPresenter.backgroundData)
    }
    
    @IBAction private func inviteMember(sender: UIButton) {
        let inviteMemberVC = InviteMemberVC()
        inviteMemberVC.delegate = self
        inviteMemberVC.presenter.inviteMemberForType = .Edit
        if let talkGroup = editTalkPresenter.talkGroup {
            inviteMemberVC.presenter.talkGroup = talkGroup
        }
        present(UINavigationController(rootViewController: inviteMemberVC), animated: true, completion: nil)
    }
    
    @IBAction private func openConversation(sender: UIButton) {
        
    }
    
    @IBAction private func shareTalk(sender: UIButton) {
        editTalkPresenter.showQRCode(fromVC: self)
    }
    
    @IBAction private func moreAction(sender: UIButton) {
        let leaveTitle = NSLocalizedString("leave_talk_action_text", comment: "")
        let leaveAction = UIAlertAction(title: leaveTitle, style: .default) { (action) in
            print("leaveAction")
            self.showLeaveConfirmAlert()
        }
        let deleteTitle = NSLocalizedString("delete_talk_action_text", comment: "")
        let deleteAction = UIAlertAction(title: deleteTitle, style: .default) { (action) in
            print("deleteAction")
            self.showDeleteConfirmAlert()
        }
        let cancelTitle = NSLocalizedString("cancel_text", comment: "")
        let cancelAction = UIAlertAction(title: cancelTitle, style: .cancel) { (action) in
        }
        var actions: [UIAlertAction] = [leaveAction, deleteAction, cancelAction]
        if  let userProfile = User.getProfile(),
            userProfile.isAdmin && userProfile.id != editTalkPresenter.talkGroup?.author?.id {
            actions.remove(at: 1)
        }
        Helpers.showActionSheet(title: nil, actions: actions, sourceView: sender)
    }
}

// MARK: SelectIconVC Delegate
extension EditTalkVC: SelectIconVCDelegate {
    func selectAvatar(avatar: Avatar, at index: Int) {
        selectedTalkImageID = avatar.id
        defaultAvatarIndex = index + 1
        
        talkImageView.image = UIImage(named: avatar.name)
        editTalkPresenter.avartarData = UIImage(named: avatar.name)?.pngData()
        editTalkPresenter.backgroundData = (UIImage(named: avatar.name.replacingOccurrences(of: "ic", with: "bg"))!.pngData())
    }
    
    func didSelectImage(image: UIImage) {
        let resizeImage = image.resizeImage()
        // Update background
        do {
            let imageBackgroundData = try resizeImage?.compressToDataSize()
            self.editTalkPresenter.backgroundData = imageBackgroundData
        }
        catch {
            self.showErrorAlertWithError(error, completion: nil)
        }
        
        let vc = EditPhotoVC()
        vc.delegate = self
        vc.sourceImage = resizeImage
        let navi = UINavigationController(rootViewController: vc)
        navi.navigationBar.setupNavigationBarWithColor(.white)
        present(navi, animated: true, completion: nil)
        selectedTalkImageID = 0
    }
}
// MARK: UITextView Delegate
extension EditTalkVC: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.ct.grayPlaceHolderTextColor {
            textView.text = ""
            textView.textColor = UIColor.black
        }
    }
    
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        textView.endEditing(true)
        return true
    }
    
    func textViewDidChange(_ textView: UITextView) {
        editTalkPresenter.talkNameDidChange(text: textView.text)
        if let talkName = textView.text {
            talkNameCharactersLabel.text = talkName.count.description + "/30"
        }
        
        textView.dynamicHeight(heightConstraint: talkNameHeightConstraint)
        self.view.updateConstraints()
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            textView.dynamicHeight(heightConstraint: talkNameHeightConstraint, defaultHeight: 40)
            setPlaceHolderText()
            self.view.updateConstraints()
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        guard let textOld = textView.text else { return true }
        let count = textOld.count + text.count - range.length
        return count <= 30
    }
}
// MARK: UITextView Delegate
extension EditTalkVC: EditTalkView {
    func dismissVC() {
    }
    
    func updateMemberCollectionView() {
        var totalMembers = 0
        if let members = editTalkPresenter.talkGroup?.allUsers {
            totalMembers = members.count
        }
        numberOfMembersLabel.text = NSLocalizedString("number_of_member_text", comment: "") + totalMembers.description
        collectionView.reloadData()
    }
    
    func disableEditButton() {
    }
    
    func didUpdateTalkGroup(talkGroup: TalkGroup, shouldDismiss: Bool) {
        delegate?.didUpdateTalk(isEditName: true, talk: talkGroup)
        if shouldDismiss {
            dismiss(animated: true)
        }
    }
}
extension EditTalkVC: EditPhotoVCDelegate {
    func didCropImage(croppedImage: UIImage) {
        talkImageView.image = croppedImage
        defaultAvatarIndex = 0
        
        do {
            let imageData = try croppedImage.compressToDataSize()
            editTalkPresenter.avartarData = imageData
        }
        catch {
            self.showErrorAlertWithError(error, completion: nil)
        }
    }
}
// MARK: UICollectionView DataSource & Delegate
extension EditTalkVC: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let members = editTalkPresenter.talkGroup?.allUsers {
            return members.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeue(MemberCell.self, forIndexPath: indexPath)
        cell.user = editTalkPresenter.talkGroup?.allUsers[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let widthCell = (GlobalVariable.screenWidth - 30) / 4
        return CGSize(width: widthCell, height: widthCell + 10)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}
extension EditTalkVC: InviteMemberVCDelegate {
    func invitedMembers(_ members: [User], in inviteMemberVC: InviteMemberVC) {
        inviteMemberVC.dismiss(animated: true, completion: nil)
        self.showToast(isEditName: false)
    }
}
// MARK: Transitioning Delegate
extension EditTalkVC: UIViewControllerTransitioningDelegate {
    
    func animationController(forPresented presented: UIViewController,
                             presenting: UIViewController,
                             source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return FadePushAnimator(type: .modal)
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        var interactionController: UIPercentDrivenInteractiveTransition?
        if let viewController = dismissed as? SelectIconVC {
            interactionController = viewController.interactionController
        }
        return FadePopAnimator(type: .modal, interactionController: interactionController)
    }
    
    func interactionControllerForDismissal(using animator: UIViewControllerAnimatedTransitioning)
        -> UIViewControllerInteractiveTransitioning? {
            guard let animator = animator as? FadePopAnimator,
                let interactionController = animator.interactionController as? LeftEdgeInteractionController, interactionController.inProgress else {
                    return nil
            }
            return interactionController
    }
}
