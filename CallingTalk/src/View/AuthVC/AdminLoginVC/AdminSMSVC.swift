//
//  AdminSMSVC.swift
//  CallingTalk
//
//  Created by Toof on 2/13/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit

class AdminSMSVC: BaseViewController {
    
    // MARK: Properties
    
    open var presenter: AdminSMSPresenter!
    
    @IBOutlet weak var confirmSMSLabel: UILabel!
    @IBOutlet weak var confirmSMSNoticeLabel: UILabel!
    @IBOutlet weak var resendSMSView: UIView!
    @IBOutlet weak var resendSMSTitleLabel: UILabel!
    @IBOutlet weak var resendingButton: UIButton!
    @IBOutlet weak var callingButton: UIButton!
    @IBOutlet weak var smsCodeView: SwiftyCodeView!
    
    // MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupSubviews()
    }
    
    fileprivate
    func setupSubviews() {
        let buttonBarSetting = CustomButtonBarSetting.init(imageName: "ic_back",
                                                           style: .plain,
                                                           action: #selector(backButtonPressed(_:)),
                                                           target: self,
                                                           possition: .left)
        _ = CustomUIItemFactory().createAndLoadButtonBar(withSetting: buttonBarSetting)
        self.smsCodeView.delegate = self
        self.resendSMSView.corner = 20.0
        
        self.confirmSMSLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .bold, size: 24.0)
        self.confirmSMSNoticeLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .bold, size: 16.0)
        self.resendSMSTitleLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .bold, size: 14)
        
        self.resendingButton.addTarget(self, action: #selector(resendingButtonPressed(_:)), for: .touchUpInside)
        self.callingButton.addTarget(self, action: #selector(callingButtonPressed(_:)), for: .touchUpInside)
        
        setHiddenFeature()
    }
    
    override func localizeString() {
        self.confirmSMSLabel.text = NSLocalizedString("confirm_sms_label_text", comment: "")
        self.confirmSMSNoticeLabel.text = NSLocalizedString("confirm_sms_notice_label_text", comment: "")
        self.resendSMSTitleLabel.text = NSLocalizedString("resend_sms_label_text", comment: "")
        self.confirmSMSLabel.textColor = UIColor.ct.headerText
        self.confirmSMSNoticeLabel.textColor = UIColor.ct.headerText
        let underlineAttribute: [NSAttributedString.Key : Any] = [
            .underlineStyle: NSUnderlineStyle.single.rawValue,
            .underlineColor: UIColor.ct.underlineTitleButtonColor,
            .font: UIFont.ct.font(.NotoSansCJKjp, type: .regular, size: 13.0),
            .foregroundColor: UIColor.ct.underlineTitleButtonColor
        ]
        
        self.resendingButton.setAttributedTitle(NSAttributedString(string: NSLocalizedString("resending_button_title", comment: ""), attributes: underlineAttribute), for: .normal)
        self.callingButton.setAttributedTitle(NSAttributedString(string: NSLocalizedString("calling_button_title", comment: ""), attributes: underlineAttribute), for: .normal)
    }
    
    private func setHiddenFeature() {
        self.callingButton.isHidden = true
    }
}

// MARK: - Handle Events
extension AdminSMSVC {
    
    @objc
    fileprivate
    func backButtonPressed(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc
    fileprivate
    func resendingButtonPressed(_ sender: UIButton) {
        presenter.resendVerifyCode()
    }
    
    @objc
    fileprivate
    func callingButtonPressed(_ sender: UIButton) {
        
    }
    
}

// MARK: - SwiftyCodeViewDelegate

extension AdminSMSVC: SwiftyCodeViewDelegate {
    
    func codeView(sender: SwiftyCodeView, didFinishInput code: String) {
        self.presenter.verifyCode(code)
    }
    
}

// MARK: - Implement AdminSMSView

extension AdminSMSVC: AdminSMSView {
    
    func showAlertForRetrievingError(_ error: Error) {
        self.showErrorAlertWithError(error) { [weak self] _ in
            guard let `self` = self else { return }
            
            self.smsCodeView.deleteAll()
        }
    }
    
    func showAlertForRetrievingSuccess(_ image: String, message: String, completion: @escaping () -> Void) {
        SuccessValidSMSAlertView(image: image, message: message).show(completion: completion)
    }
    
    func updateUIForLoginSuccessWithRole(_ isAdmin: Bool) {
        AppDelegate.share()?.showHomeTabbar(isAdmin: isAdmin)
    }
    
}
