//
//  AdminLoginVC.swift
//  CallingTalk
//
//  Created by Toof on 2/13/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit

class AdminLoginVC: BaseViewController {
    
    // MARK: Properties
    
    fileprivate var presenter: AdminLoginPresenter!
    
    @IBOutlet weak var adminLoginLabel: UILabel!
    @IBOutlet weak var phoneNumberNoticeLabel: UILabel!
    @IBOutlet weak var phoneNumberLabel: UILabel!
    @IBOutlet weak var phoneNumberErrorLabel: UILabel!
    @IBOutlet weak var phoneNumberTextField: LeftPaddedTextField!
    @IBOutlet weak var smsButton: UIButton!
    @IBOutlet weak var staffTitleButton: UIButton!
    var isShowBackButton = true
    
    // MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.presenter = AdminLoginPresenter(view: self)
        
        self.setupSubviews()
        self.presenter.setSMSButtonStatus()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.phoneNumberTextField.resignFirstResponder()
    }
    
    fileprivate
    func setupSubviews() {
        // setup navigation's bar & item
        if isShowBackButton {
            let buttonBarSetting = CustomButtonBarSetting.init(imageName: "ic_back",
                                                               style: .plain,
                                                               action: #selector(backButtonPressed(_:)),
                                                               target: self,
                                                               possition: .left)
            _ = CustomUIItemFactory().createAndLoadButtonBar(withSetting: buttonBarSetting)
        } else {
            self.navigationItem.hidesBackButton = true
        }
        
        // set font for label or title's button
        self.adminLoginLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .bold, size: 24.0)
        self.phoneNumberNoticeLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .bold, size: 16.0)
        self.phoneNumberLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .bold, size: 14)
        self.phoneNumberErrorLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .regular, size: 14)
        self.phoneNumberTextField.font = UIFont.ct.font(.NotoSansCJKjp, type: .regular, size: 15)
        self.smsButton.titleLabel?.font = UIFont.ct.font(.NotoSansCJKjp, type: .bold, size: 16)
        self.staffTitleButton.titleLabel?.font = UIFont.ct.font(.NotoSansCJKjp, type: .regular, size: 15)
        
        // set type for textfield
        self.phoneNumberTextField.keyboardType = .phonePad
        self.phoneNumberTextField.becomeFirstResponder()
        
        // set corner's radius for button
        self.smsButton.corner = 10
        
        // add events for button
        self.smsButton.addTarget(self, action: #selector(smsButtonPressed(_:)), for: .touchUpInside)
        self.staffTitleButton.addTarget(self, action: #selector(staffTitleButtonPressed(_:)), for: .touchUpInside)
        self.phoneNumberTextField.addTarget(self, action: #selector(phoneNumberTextFieldChanged(_:)), for: .editingChanged)
    }
    
    override func localizeString() {
        self.adminLoginLabel.text = NSLocalizedString("admin_login_label_text", comment: "")
        self.adminLoginLabel.textColor = UIColor.ct.headerText
        self.phoneNumberNoticeLabel.text = NSLocalizedString("phone_number_notice_label_text", comment: "")
        self.phoneNumberNoticeLabel.textColor = UIColor.ct.headerText
        self.phoneNumberLabel.text = NSLocalizedString("phone_number_label_text", comment: "")
        self.phoneNumberTextField.placeholder = NSLocalizedString("phone_number_placeholder_text", comment: "")
        self.smsButton.setTitle(NSLocalizedString("sms_button_title", comment: ""), for: .normal)
        self.smsButton.setTitleColor(UIColor.ct.headerText, for: .normal)
        self.staffTitleButton.setTitle(NSLocalizedString("staff_label_text", comment: ""), for: .normal)
    }
    
}

// MARK: - Handle Events

extension AdminLoginVC {
    
    @objc
    fileprivate
    func backButtonPressed(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc
    fileprivate
    func smsButtonPressed(_ sender: UIButton) {
        self.presenter.login()
    }
    
    @objc
    fileprivate
    func staffTitleButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc
    fileprivate
    func phoneNumberTextFieldChanged(_ sender: UITextField) {
        self.presenter.phoneNumberTextChange(sender.text)
    }
    
}

// MARL: - Implement AdminLoginView

extension AdminLoginVC: AdminLoginView {
    
    func enableSMSButton(_ isEnable: Bool) {
        self.smsButton.isEnabled = isEnable
        self.smsButton.backgroundColor = isEnable ? UIColor.ct.mainBackgroundColor : UIColor.ct.redDisableButton
        let colorText = isEnable ? UIColor.ct.headerText : UIColor.ct.headerText.withAlphaComponent(0.37)
        self.smsButton.setTitleColor(colorText, for: .normal)

    }
    
    func showPhoneNumberErrorLabel(_ isShow: Bool) {
        self.phoneNumberErrorLabel.text = isShow ? NSLocalizedString("error_validation_label_text", comment: "") : ""
        UIView.animate(withDuration: 0.15) { [weak self] in
            guard let `self` = self else { return }
            self.view.layoutIfNeeded()
        }
    }
    
    func updateUIForLoginSuccess() {
        let adminSMSVC = AdminSMSVC()
        adminSMSVC.presenter = self.presenter.transferToAdminSMSPresenter(adminSMSVC)
        self.navigationController?.pushViewController(adminSMSVC, animated: true)
    }
    
}
