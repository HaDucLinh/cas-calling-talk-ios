//
//  StaffLoginVC.swift
//  CallingTalk
//
//  Created by Vu Tran on 2/12/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit
import Swinject
class StaffLoginVC: BaseViewController {
    fileprivate let staffLoginPresenter = StaffLoginPresenter()

    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var talkLabel: UILabel!
    @IBOutlet weak var noticeStartLabel: UILabel!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var userNameTextView: UITextView!
    @IBOutlet weak var textView: UIView!

    @IBOutlet weak var meetingButton: UIButton!
    @IBOutlet weak var adminTitleButton: UIButton!
    @IBOutlet weak var textViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomAdminConstraint: NSLayoutConstraint!

    var isAdminLogOut = false
    var selectedAvatarID = 1
    var defaultAvatarIndex = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        staffLoginPresenter.attachView(self)
        let tap = UITapGestureRecognizer(target: self, action: #selector(endEditting))
        self.view.addGestureRecognizer(tap)
        staffLoginPresenter.setLoginButtonStatus()

        setupUI()
        if isAdminLogOut {
            let adminLoginVC = AdminLoginVC()
            adminLoginVC.isShowBackButton = false
            self.navigationController?.pushViewController(adminLoginVC, animated: false)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        userNameTextView.centerVertically()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        userNameTextView.centerVertically()
    }
    
    private func setupUI(){
        talkLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .bold, size: 26)
        noticeStartLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .bold, size: 14)
        noticeStartLabel.textColor = UIColor.ct.headerText
        usernameLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .bold, size: 14)
        
        meetingButton.titleLabel?.font = UIFont.ct.font(.NotoSansCJKjp, type: .bold, size: 14)
        adminTitleButton.titleLabel?.font = UIFont.ct.font(.NotoSansCJKjp, type: .regular, size: 15)
        
        avatarImageView.roundedCornerRadius()
        meetingButton.corner = 8
        
        userNameTextView.isScrollEnabled = false
        textView.corner = 8
        textView.border(color: UIColor.ct.borderSearchTextFieldColor, width: 1)
        userNameTextView.delegate = self
        userNameTextView.font = UIFont.ct.font(.NotoSansCJKjp, type: .regular, size: 15)
        userNameTextView.textContainerInset = UIEdgeInsets(top: 0, left: 18, bottom: 0, right: 5)
        setPlaceHolderText()
        
        let tapShowIcon = UITapGestureRecognizer(target: self, action: #selector(showIconView))
        avatarImageView.isUserInteractionEnabled = true
        avatarImageView.addGestureRecognizer(tapShowIcon)
        avatarImageView.image = UIImage(named: "icon01")
        avatarImageView.contentMode = .scaleAspectFit
        
        if iPad {
            bottomAdminConstraint.constant = 100
        }
    }
    
    override func localizeString() {
        guard let systemSetting = Container.default.resolve(SystemThemeable.self) else {
            return
        }
        let currentImageConverted = systemSetting.convertImageToTheme(withName: "ic-talk-begin")
        let noticeText = systemSetting.convertTextToTheme(withName: "notice_start_label_text")
        noticeStartLabel.text = NSLocalizedString(noticeText, comment: "")
        
        usernameLabel.text = NSLocalizedString("username_label_text", comment: "")
        meetingButton.setTitle(NSLocalizedString("meeting_button_title", comment: ""), for: .normal)
        adminTitleButton.setTitle(NSLocalizedString("administrator_label_text", comment: ""), for: .normal)
        
        let talkString = NSLocalizedString("talk_label_text", comment: "")
        let attributedString = NSMutableAttributedString(string: talkString, attributes: [NSAttributedString.Key.font: UIFont.ct.font(.NotoSansCJKjp, type: .bold, size: 26)])
        let textAttachment = NSTextAttachment()
        textAttachment.image = UIImage(named:currentImageConverted)
        let attrStringWithImage = NSAttributedString(attachment: textAttachment)
        var code = ""
        if let langCode = Locale.current.languageCode {
            code = langCode
        } else {
            code = "ja"
        }
        if code == "ja" {
            attributedString.replaceCharacters(in: NSMakeRange(0, 4), with: attrStringWithImage)
        } else {
            attributedString.replaceCharacters(in: NSMakeRange(11, 4), with: attrStringWithImage)
        }
        talkLabel.attributedText = attributedString
        talkLabel.textColor = UIColor.ct.headerText
    }
    
    fileprivate func setPlaceHolderText() {
        let placeholderText = NSLocalizedString("username_placeholder_text", comment: "")
        userNameTextView.font = UIFont.ct.font(.NotoSansCJKjp, type: .regular, size: 14)
        userNameTextView.text = placeholderText
        userNameTextView.textColor = UIColor.ct.grayPlaceHolderTextColor
        userNameTextView.centerVertically()
    }
    
    // MARK: Actions
    @IBAction private func showIconView(sender: UIButton) {
        let selectIconVC = SelectIconVC()
        selectIconVC.oldAvatarId = selectedAvatarID
        selectIconVC.defaultAvatarIndex = defaultAvatarIndex
        selectIconVC.delegate = self
        
        selectIconVC.transitioningDelegate = self
        self.definesPresentationContext = true
        selectIconVC.view.backgroundColor = .clear
        selectIconVC.modalPresentationStyle = .overFullScreen
        present(selectIconVC, animated: true, completion: nil)
    }
    
    @IBAction private func staffLogin(sender: UIButton) {
        if let username = self.userNameTextView.text,
            let avatarImage = avatarImageView.image {
            let countryCode = Locale.current.regionCode ?? "JP"
            self.staffLoginPresenter.login(username: username, avatarImage: avatarImage, avataIndex: defaultAvatarIndex, countryCode: countryCode)
        }
    }
    
    @IBAction private func adminLogin(sender: UIButton) {
        self.navigationController?.pushViewController(AdminLoginVC(), animated: true)
    }
}

extension StaffLoginVC: StaffLoginView {
    func enableMettingButton() {
        meetingButton.isEnabled = true
        meetingButton.backgroundColor = UIColor.ct.mainBackgroundColor
        meetingButton.setTitleColor(UIColor.ct.headerText, for: .normal)
    }
    
    func disableMettingButton() {
        meetingButton.isEnabled = false
        meetingButton.setTitleColor(UIColor.ct.headerText.withAlphaComponent(0.37), for: .normal)
        meetingButton.backgroundColor = UIColor.ct.redDisableButton
    }
    
    func updateUIForLoginSuccessWithRole(_ isAdmin: Bool) {
        AppDelegate.share()?.showHomeTabbar(isAdmin: isAdmin)
    }
}
extension StaffLoginVC: SelectIconVCDelegate {
    func selectAvatar(avatar: Avatar, at index: Int) {
        selectedAvatarID = avatar.id
        defaultAvatarIndex = index + 1 // Because begin index is start at 1
        avatarImageView.image = UIImage(named: avatar.name)
    }
    
    func didSelectImage(image: UIImage) {
        let vc = EditPhotoVC()
        vc.delegate = self
        vc.sourceImage = image
        let navi = UINavigationController(rootViewController: vc)
        navi.navigationBar.setupNavigationBarWithColor(.white)
        present(navi, animated: true, completion: nil)
    }
}
extension StaffLoginVC: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.ct.grayPlaceHolderTextColor {
            textView.text = ""
            textView.textColor = UIColor.black
        }
    }
    
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        textView.endEditing(true)
        return true
    }
    
    func textViewDidChange(_ textView: UITextView) {
        staffLoginPresenter.usernameTextChange(text: textView.text)
        
        textView.dynamicHeight(heightConstraint: textViewHeightConstraint, minHeight: 50)
        userNameTextView.centerVertically()
        self.view.updateConstraints()
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            textView.dynamicHeight(heightConstraint: textViewHeightConstraint, defaultHeight: 50)
            setPlaceHolderText()
            self.view.updateConstraints()
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        guard let textOld = textView.text else { return true }
        if(text == "\n") {
            view.endEditing(true)
            return false
        }
        let count = textOld.count + text.count - range.length
        return count <= 30
    }
}
extension StaffLoginVC: EditPhotoVCDelegate {
    func didCropImage(croppedImage: UIImage) {
        avatarImageView.image = croppedImage
        defaultAvatarIndex = 0
    }
}

// MARK: Transitioning Delegate
extension StaffLoginVC: UIViewControllerTransitioningDelegate {

    func animationController(forPresented presented: UIViewController,
                             presenting: UIViewController,
                             source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return FadePushAnimator(type: .modal)
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        var interactionController: UIPercentDrivenInteractiveTransition?
        if let viewController = dismissed as? SelectIconVC {
            interactionController = viewController.interactionController
        }
        return FadePopAnimator(type: .modal, interactionController: interactionController)
    }
    
    func interactionControllerForDismissal(using animator: UIViewControllerAnimatedTransitioning)
        -> UIViewControllerInteractiveTransitioning? {
            guard let animator = animator as? FadePopAnimator,
                let interactionController = animator.interactionController as? LeftEdgeInteractionController, interactionController.inProgress else {
                    return nil
            }
            return interactionController
    }
}
