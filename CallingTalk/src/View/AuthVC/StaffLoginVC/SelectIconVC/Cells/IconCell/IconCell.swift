//
//  IconCell.swift
//  CallingTalk
//
//  Created by Vu Tran on 2/13/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit

class IconCell: UICollectionViewCell {
    @IBOutlet weak var iconImageView: UIImageView!
    
    override var isSelected: Bool {
        didSet {
            let color = isSelected ? UIColor.red : UIColor.clear
            iconImageView.border(color: color, width: 2.0)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        iconImageView.corner = iconImageView.frame.size.width / 2
        layoutIfNeeded()
    }
    
    override func willMove(toSuperview newSuperview: UIView?) {
        layoutIfNeeded()
    }

}
