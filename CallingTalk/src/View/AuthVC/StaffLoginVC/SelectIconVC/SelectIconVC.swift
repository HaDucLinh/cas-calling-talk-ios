//
//  SelectIconVC.swift
//  CallingTalk
//
//  Created by Vu Tran on 2/13/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit
import Photos
import ObjectMapper

protocol SelectIconVCDelegate: NSObjectProtocol {
    func selectAvatar(avatar: Avatar, at index: Int)
    func didSelectImage(image: UIImage)
}
class SelectIconVC: BaseViewController {
    @IBOutlet private weak var emptyView: UIView!
    @IBOutlet private weak var containerView: UIView!
    @IBOutlet private weak var sourceView: UIView!
    @IBOutlet private weak var collectionView: UICollectionView!
    @IBOutlet private weak var selectIconLabel: UILabel!
    @IBOutlet private weak var heightConstraint: NSLayoutConstraint!
    private var avatars: [Avatar] = []
    var isFromNewTalk = false
    var interactionController: LeftEdgeInteractionController?

    var oldAvatarId = 0
    var selectedIndex: Int?
    var defaultAvatarIndex: Int = 1

    var width: CGFloat = 0
    
    weak var delegate: SelectIconVCDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.interactionController = LeftEdgeInteractionController(viewController: self)
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        emptyView.addGestureRecognizer(tap)
        setUpUI()
        getData()
    }
    
    private func setUpUI() {
        emptyView.backgroundColor = UIColor.black.withAlphaComponent(0.23)
        containerView.corner = 16
        selectIconLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .bold, size: 18)
        selectIconLabel.text = NSLocalizedString("selectIcon_label_text", comment: "")
        collectionView.register(IconCell.self)
        width = containerView.bounds.width
    }
    
    private func getData() {
        if isFromNewTalk {
            if let avatars = Mapper<Avatar>().mapArray(JSONObject: JSONTalkImage["list"]) {
                self.avatars = avatars
            }
        } else {
            if let avatars = Mapper<Avatar>().mapArray(JSONObject: JSONSAvatar["list"]) {
                self.avatars = avatars
            }
        }
        collectionView.dataSource = self
        collectionView.delegate = self
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        switch UIScreen.main.bounds.size {
        case DeviceType.iPhone5.size:
            heightConstraint.constant = 448 * GlobalVariable.ratioHeight
        case DeviceType.iPhoneXSMax.size:
            heightConstraint.constant = 410 * GlobalVariable.ratioHeight
        default:
            heightConstraint.constant = 440 * GlobalVariable.ratioHeight
        }

        view.updateConstraints()
        width = containerView.bounds.width
        print("Width SCrene: ", UIScreen.main.bounds.width)
        collectionView.reloadData()
    }
    
    @objc func handleTap(){
        dismiss(animated: true, completion: nil)
    }
    
    private func selectIconAt(index: Int) {
        delegate?.selectAvatar(avatar: avatars[index], at: index)
        self.dismiss(animated: true, completion: nil)
    }
    
    private func checkAuthorForPhotoLibrary(){
        let status = PHPhotoLibrary.authorizationStatus()
        if (status == PHAuthorizationStatus.authorized) {
            openPhotoLibrary()
        } else if status == .denied {
            willShowChangePermissionAlert(isCamera: false)
        } else {
            PHPhotoLibrary.requestAuthorization({ (newStatus) in
                DispatchQueue.main.async {
                    if (newStatus == PHAuthorizationStatus.authorized) {
                        self.openPhotoLibrary()
                    } else {
                        self.willShowChangePermissionAlert(isCamera: false)
                    }
                }
            })
        }
    }
    
    func checkAuthorForCamera() {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.isEditing = true
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            present(imagePicker, animated: true, completion: nil)
        } else {
            self.willShowChangePermissionAlert(isCamera: true)
        }
    }
    
    fileprivate func willShowChangePermissionAlert(isCamera: Bool) {
        let message = isCamera ? NSLocalizedString("camera_permission", comment: "") : NSLocalizedString("photo_library_permission", comment: "")
        self.showConfirmAlertWithMessage(message) { (action) in
            if action {
                self.openSetting()
            } else {
                // show empty
            }
        }
    }
    
    fileprivate func openPhotoLibrary() {
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            print("Button capture")
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .savedPhotosAlbum;
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    private func openSetting() {
        guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
            return
        }
        if UIApplication.shared.canOpenURL(settingsUrl) {
            UIApplication.shared.open(settingsUrl, options: [:], completionHandler: nil)
        }
    }
    
    private func selectCamera() {
        let takePhotoTitle = NSLocalizedString("take_photo", comment: "")
        let choosePhotoTitle = NSLocalizedString("select_avatar_from_Album", comment: "")
        let cancelTitle = NSLocalizedString("cancel_text", comment: "")
        
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        alert.popoverPresentationController?.sourceView = sourceView

        alert.addAction(UIAlertAction(title: cancelTitle, style: UIAlertAction.Style.cancel, handler: { (action) in
            self.collectionView.reloadData()
        }))
        alert.addAction(UIAlertAction(title: takePhotoTitle, style: UIAlertAction.Style.default, handler: { (action) in
            self.checkAuthorForCamera()
        }))
        alert.addAction(UIAlertAction(title: choosePhotoTitle, style: UIAlertAction.Style.default, handler: { (action) in
            self.checkAuthorForPhotoLibrary()
        }))
        alert.show()
    }
}
extension SelectIconVC: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
         return avatars.count  + 1
//        return avatars.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeue(IconCell.self, forIndexPath: indexPath)
        
        if indexPath.row == avatars.count {
            cell.iconImageView.image = UIImage(named: "camera")
            cell.isSelected = defaultAvatarIndex == 0
            return cell
        }
        
        cell.isSelected = indexPath.row == defaultAvatarIndex - 1
        cell.iconImageView.image = UIImage(named: avatars[indexPath.row].name)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        if indexPath.row == avatars.count {
            selectCamera()
        }
        else {
            selectIconAt(index: indexPath.row)
            oldAvatarId = avatars[indexPath.row].id
            collectionView.reloadData()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let widthCell = (width - 40) / 4
        return CGSize(width: widthCell, height: widthCell)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
}
extension SelectIconVC: UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true, completion: nil)
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            print("oriented: ", image.imageOrientation.rawValue)
            dismiss(animated: true) {
                self.delegate?.didSelectImage(image: image)
            }
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
        self.collectionView.reloadData()
    }
}
