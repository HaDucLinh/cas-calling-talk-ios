//
//  EditPhotoVC.swift
//  CallingTalk
//
//  Created by Vu Tran on 2/19/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit
import Swinject
protocol EditPhotoVCDelegate: NSObjectProtocol {
    func didCropImage(croppedImage: UIImage)
}
class EditPhotoVC: BaseViewController {
    @IBOutlet private weak var contentView: UIView!
    @IBOutlet private weak var cropButton: UIButton!
    @IBOutlet private weak var cancelButton: UIButton!
    
    var sourceImage: UIImage?
    private var cropView: CropView!
    weak var delegate: EditPhotoVCDelegate?
    var isAdd = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUI()
    }
    
    private func setUpUI() {
        cropButton.corner = 10
        let confirmText = NSLocalizedString("confirm_crop_button_text", comment: "")
        cropButton.setTitle(confirmText, for: .normal)
        cropButton.setTitleColor(UIColor.white, for: .normal)
        cropButton.backgroundColor = UIColor.ct.redKeyColor
        cropButton.titleLabel?.font = UIFont.ct.font(.NotoSansCJKjp, type: .bold, size: 16)
        
        let cancelText = NSLocalizedString("cancel_crop_button_text", comment: "")
        self.setUpStyleForCancelButton()
        cancelButton.setTitle(cancelText, for: .normal)
        cancelButton.corner = 10
        
        cancelButton.titleLabel?.font = UIFont.ct.font(.NotoSansCJKjp, type: .regular, size: 15)
        cropButton.backgroundColor = UIColor.ct.mainBackgroundColor
        cropButton.setTitleColor(UIColor.ct.headerText, for: .normal)
    }
    func setUpStyleForCancelButton() {
        //TODO: this is 2 difference style of button, we can't just modify the value
        //So make hard-code right here!! we need to fix it later if has more than this place
        let systemTheme = Container.default.resolve(SystemThemeable.self)
        switch systemTheme!.getCurrentMode() {
        case ThemeMode.normal:
            cancelButton.backgroundColor = UIColor.white
            cancelButton.border(color: UIColor.ct.redKeyColor, width: 1)
            cancelButton.corner = 10
            cancelButton.setTitleColor(UIColor.ct.redKeyColor, for: .normal)
        case ThemeMode.omiseno:
            cancelButton.backgroundColor = UIColor.ct.headerTextSubtitle
            cancelButton.setTitleColor(UIColor.white, for: .normal)
            break
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = true
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if isAdd == true { return }
        guard let image = sourceImage else { return }
        isAdd = true
        cropView = CropView(frame: contentView.bounds)
        cropView.sourceImage = image
        cropView.setupCropView()
        contentView.addSubview(cropView)
    }
    
    @IBAction private func cropButton(sender: UIButton) {
        if let image = cropView.cropImage() {
            delegate?.didCropImage(croppedImage: image)
            dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction private func cancelButton(sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
}
