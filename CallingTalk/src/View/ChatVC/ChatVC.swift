//
//  ChatVC.swift
//  CallingTalk
//
//  Created by Toof on 3/5/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit
import ObjectMapper
import Swinject

public enum ChatBarStatus {
    case Normal
    case TextEdited
    case AudioEdited
}

protocol ChatVCDelegate: class {
    func chatVCDismissed(shouldShowAudioLogMessages: Bool)
}

class ChatVC: BaseViewController {
    
    // MARK: Properties
    public var chatPresenter: ChatPresenter!
    weak var delegate: ChatVCDelegate?
    
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var enableLogAudioButton: UIButton!
    @IBOutlet weak var enableLogAudioLabel: UILabel!
    
    @IBOutlet weak var avatarBorderView: UIView!
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var nameTalkGroupLabel: UILabel!
    @IBOutlet fileprivate weak var tableView: UITableView!
    
    @IBOutlet weak var endEditButton: UIButton!
    @IBOutlet weak var collectImageButton: UIButton!
    @IBOutlet weak var collectAudioButton: UIButton!
    @IBOutlet weak var imageAudioMessageContainView: UIView!
    
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var sendAudioButton: UIButton!
    @IBOutlet weak var chatTextView: GrowingTextView!
    @IBOutlet weak var inputMessageView: UIView!
    
    @IBOutlet weak var bottomChatBarConstraint: NSLayoutConstraint?
    
    @IBOutlet var sendButtonLeadingConstraint: NSLayoutConstraint?
    @IBOutlet var sendAudioButtonLeadingConstraint: NSLayoutConstraint?
    
    @IBOutlet var chatTextViewTrailingConstraint: NSLayoutConstraint?
    @IBOutlet var chatTextViewTrailingForEditConstraint: NSLayoutConstraint?
    fileprivate var indicatorView = UIActivityIndicatorView()
    fileprivate var mentionView: MentionView?
    fileprivate var chatStatusBarView: ChatStatusBarView?
    
    fileprivate var audioSentenceKeyboard: AudioSentenceKeyboard?
    fileprivate var keyboardHeight: CGFloat = 0.0
    fileprivate var replaceTextTuple: (textOld: String, range: NSRange, replaceText: String) = ("", NSRange(), "")
    
    private var matchedList: [User] = []
    private var taggedList: [TaggingModel] = []
    
    fileprivate var chatBarStatus: ChatBarStatus = .Normal {
        didSet {
            self.chatTextViewTrailingConstraint?.isActive = self.chatBarStatus == .Normal
            self.chatTextViewTrailingForEditConstraint?.isActive = self.chatBarStatus != .Normal
            
            self.sendButtonLeadingConstraint?.isActive = self.chatBarStatus != .AudioEdited
            self.sendAudioButtonLeadingConstraint?.isActive = self.chatBarStatus == .AudioEdited

            // Update UI with animation
            UIView.animate(withDuration: 0.25, animations: { [weak self] in
                guard let `self` = self else { return }
                self.view.layoutIfNeeded()
            }) { (done) in
            }
            
            switch self.chatBarStatus {
            case .Normal:
                self.chatTextView.isEditable = true
                self.chatTextView.tintColor = UIColor.ct.handFreeModeColor
                self.sendButton.isHidden = false
                self.sendAudioButton.isHidden = true
                self.endEditButton.isHidden = true
                self.imageAudioMessageContainView.isHidden = false
                
                self.chatTextView.inputView = nil
                self.chatTextView.textColor = UIColor.white
                self.chatTextView.attributedPlaceholder = NSAttributedString(
                    string: NSLocalizedString("chat_text_message_placeholder_text", comment: ""),
                    attributes: [
                        .font: self.chatTextView.font!,
                        .foregroundColor: UIColor.ct.placeHolderChatTextColor
                    ]
                )
            case .TextEdited:
                self.sendButton.isHidden = false
                self.sendAudioButton.isHidden = true
                self.endEditButton.isHidden = false
                self.imageAudioMessageContainView.isHidden = true
                
                self.chatTextView.inputView = nil
                self.chatTextView.textColor = UIColor.white
                self.chatTextView.attributedPlaceholder = NSAttributedString(
                    string: NSLocalizedString("chat_text_message_placeholder_text", comment: ""),
                    attributes: [
                        .font: self.chatTextView.font!,
                        .foregroundColor: UIColor.ct.placeHolderChatTextColor
                    ]
                )
            case .AudioEdited:
                self.chatTextView.tintColor = .clear
                self.sendButton.isHidden = true
                self.sendAudioButton.isHidden = false
                self.endEditButton.isHidden = false
                self.imageAudioMessageContainView.isHidden = true
                
                self.chatTextView.textColor = UIColor.ct.highlightMessageTextColor
                self.chatTextView.attributedPlaceholder = NSAttributedString(
                    string: NSLocalizedString("chat_audio_message_placeholder_text", comment: ""),
                    attributes: [
                        .font: self.chatTextView.font!,
                        .foregroundColor: UIColor.ct.highlightMessageTextColor.withAlphaComponent(0.5)
                    ]
                )
            }
        }
    }
    
    fileprivate var isFirstLoadMessage: Bool = false
    
    fileprivate var playingAudioCell: LogVoiceCell?
    
    fileprivate let blurBackgroundLayer: CALayer = CALayer()
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupSubviews()
        
        // Show status's bar
        self.showStatusBarViewIfNeeded()
        
        // Fetch data
        self.chatPresenter.fetch()
        
        // Add listener's notification
        NotificationCenter.default.addObserver(self, selector: #selector(handleRecieveKeyboardNotification(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleRecieveKeyboardNotification(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleCheckMaskAsRead), name: NSNotification.Name(rawValue: "CallMarkAsRead"), object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.chatTextView.resignFirstResponder()
        self.dismissMentionView()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        // Setup avatar image view
        self.avatarBorderView.corner = self.avatarBorderView.bounds.size.height/2
        self.avatarImageView.roundedCornerRadius()
        self.blurBackgroundLayer.frame = self.backgroundImageView.bounds
    }
    
    override func notifyForNewMessage(_ message: Message) {
        super.notifyForNewMessage(message)
        
        // Just play system audio messages in the current talk
        if let talkGroup = self.chatPresenter.talkGroup, message.channelId == talkGroup.id  {
            if chatPresenter.addNewMessages(messages: [message], isLoadingMore: false).count != 0 {
                
                let indexPath = IndexPath(row: chatPresenter.messages.count - 1, section: 0)
                tableView.performBatchUpdates({
                    tableView.insertRows(at: [indexPath], with: .fade)
                }, completion: { [weak self] _ in
                    guard let self = self else {
                        return
                    }
                    self.tableView.scrollsToBottom(true)
                })
                
                let state = UIApplication.shared.applicationState
                if state == .active {
                    self.checkForMarkAsRead()
                }
            }
        }
    }
    
    // MARK: Functions
    fileprivate
    func setupSubviews() {
        // Setup blur background layer
        self.blurBackgroundLayer.backgroundColor = UIColor.ct.blurColorWithAlpha(0.6).cgColor
        self.backgroundImageView.layer.addSublayer(self.blurBackgroundLayer)
        
        // Setup table view
        self.setupTableView()
        
        // Setup chat's text view
        self.setupChatTextView()
        
        // Setup chat's buttons
        self.sendButton.isEnabled = false
        self.enableLogAudioButton.adjustsImageWhenHighlighted = false
        self.endEditButton.isHidden = true
        self.sendAudioButton.isHidden = true
        self.sendAudioButton.corner = 8.0
        
        // Add button's events
        self.backButton.addTarget(self, action: #selector(backButtonPressed(_:)), for: .touchUpInside)
        self.enableLogAudioButton.addTarget(self, action: #selector(enableLogAudioButtonPressed(_:)), for: .touchUpInside)
        self.endEditButton.addTarget(self, action: #selector(endEditButtonPressed(_:)), for: .touchUpInside)
        self.collectImageButton.addTarget(self, action: #selector(collectImageButtonPressed(_:)), for: .touchUpInside)
        self.collectAudioButton.addTarget(self, action: #selector(collectAudioButtonPressed(_:)), for: .touchUpInside)
        self.sendAudioButton.addTarget(self, action: #selector(sendAudioButtonPressed(_:)), for: .touchUpInside)
        self.sendButton.addTarget(self, action: #selector(sendButtonPressed(_:)), for: .touchUpInside)
        
        self.hiddenFeature()
        self.updateLogAudioStatus()
    }
    
    override func localizeString() {
        self.sendAudioButton.alignmentCenter(8.0,
                                             title: NSLocalizedString("send_audio_message_button_title", comment: ""),
                                             image: UIImage(named: "ic_audio_message_white"))
    }
    
    fileprivate func hiddenFeature() {
        self.collectAudioButton.isHidden = true
    }
    
    fileprivate
    func setupTableView() {
        self.tableView.backgroundColor = UIColor.white.withAlphaComponent(0.0)
        self.tableView.separatorColor = UIColor.white.withAlphaComponent(0.0)
        self.tableView.tableFooterView = UIView()
        
        self.tableView.contentInset.bottom = 15.0
        self.tableView.estimatedRowHeight = 100.0
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.contentInsetAdjustmentBehavior = .always
        
        self.tableView.register(MineImageMessageCell.self)
        self.tableView.register(TheirImageMessageCell.self)
        self.tableView.register(MineTextMessageCell.self)
        self.tableView.register(TheirTextMessageCell.self)
        self.tableView.register(MineLogVoiceCell.self)
        self.tableView.register(TheirLogVoiceCell.self)
        self.tableView.dataSource = self
        self.tableView.delegate = self
        
        // Add Tap Gesture for dismiss keyboard
        let tapGesturer = UITapGestureRecognizer(target: self, action: #selector(handleTappingGesturer(_:)))
        self.tableView.addGestureRecognizer(tapGesturer)
    }
    
    private func showStatusBarViewIfNeeded() {
        if self.chatStatusBarView == nil && self.chatPresenter.isFromTalk {
            self.chatStatusBarView = ChatStatusBarView()
            self.chatStatusBarView?.show(completion: {
                self.dismiss()
            })
        }
    }
    
    public func releaseStatusBarView() {
        self.chatStatusBarView?.dismiss()
        self.chatStatusBarView = nil
    }
    
    fileprivate
    func setupChatTextView() {
        // Setup chat's text view
        self.chatTextView.delegate = self
        self.chatTextView.layer.cornerRadius = 6.0
        self.chatTextView.font = UIFont.ct.font(.NotoSansCJKjp, type: .regular, size: 14.0)
        self.chatTextView.textColor = UIColor.white
        self.chatTextView.tintColor = UIColor.ct.highlightMessageTextColor
        self.chatTextView.keyboardAppearance = .dark
        self.chatTextView.textContainerInset = UIEdgeInsets(top: 10.0,
                                                                     left: 10.0,
                                                                     bottom: 10.0,
                                                                     right: 10.0)
        self.chatTextView.attributedPlaceholder = NSAttributedString(
            string: NSLocalizedString("chat_text_message_placeholder_text", comment: ""),
            attributes: [
                .font: self.chatTextView.font!,
                .foregroundColor: UIColor.ct.placeHolderChatTextColor
            ]
        )
        
        let attributes: [NSAttributedString.Key: Any] = [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: UIFont.ct.font(.NotoSansCJKjp, type: .regular, size: 14.0)]
        chatTextView.defaultAttributes = attributes
        chatTextView.symbolAttributes = attributes
        chatTextView.taggedAttributes = attributes
        chatTextView.dataSource = self
        
        chatTextView.symbol = "@"
        if let users = chatPresenter.talkGroup?.allUsers {
            let allUsers = Array(users)
            chatTextView.tagableList = allUsers
        }
    }
    
    fileprivate func initialMentionView(hasMentionAll: Bool, mentionUsers: [User]) {
        
        let topTextViewConstraint: CGFloat = 15
        var allCellHeight: CGFloat = CGFloat(50 * mentionUsers.count)
        if hasMentionAll {
            allCellHeight += 50
        }
        
        let mentionViewHeight: CGFloat = min(allCellHeight + topTextViewConstraint, tableView.bounds.height)
        let totalSpace = inputMessageView.bounds.height
        let originMention = GlobalVariable.screenHeight - mentionViewHeight - totalSpace
                
        let frameMention = CGRect(x: 0, y: originMention,
                                  width: GlobalVariable.screenWidth, height: mentionViewHeight)
        if self.mentionView == nil {
            mentionView = MentionView.loadFromNib(frame: frameMention)
        } else {
            mentionView?.updateFrameView(newFrame: frameMention)
        }
        mentionView?.hasMentionAll = hasMentionAll
    }
    
    // MARK: call APIs
    fileprivate func checkForMarkAsRead() {
        guard let myID = User.getProfile()?.id, myID != 0 else {
            return
        }
        let lastIndex = chatPresenter.messages.count - 1
        guard let messageId = chatPresenter.messages.last?.id, lastIndex >= 0 else {
            return
        }
        let seenUserOfLast = chatPresenter.messages.last?.seenUsers
        if seenUserOfLast?.index(where: { $0.id == myID }) == nil {
            // Call Mark As Read
            chatPresenter.markAsRead(messageID: messageId, index: lastIndex)
        }
    }
    
    fileprivate func stopPreviousPlayingAudio() {
        guard let playingAudioCell = playingAudioCell else { return }
        playingAudioCell.stopPlaying()
    }
}

// MARK: - Handle events
extension ChatVC {
    fileprivate func dismiss() {
        stopPreviousPlayingAudio()
        self.delegate?.chatVCDismissed(shouldShowAudioLogMessages: chatPresenter.shouldShowAudioLogMessages)
        NotificationCenter.default.removeObserver(self)
        self.dismiss(animated: true, completion: nil)
    }
    
    fileprivate func dismissMentionView() {
        self.mentionView?.dismiss()
        self.mentionView = nil
    }
    
    fileprivate func mentionUsersForText(text: String) -> [User] {
        let lowercasedText = text.lowercased()
        var allUsers: [User] = []
        if let users = chatPresenter.talkGroup?.allUsers, !users.isEmpty {
            allUsers = Array(users).filter({ (user) -> Bool in
                return user.id != User.getProfile()?.id
            })
        }
        if text.isEmpty {
            return allUsers
        }
        if allUsers.isEmpty { return [] }
        let mentionUsers: [User] = allUsers.filter { (user) -> Bool in
            return user.name.lowercased().contains(lowercasedText)
        }
        return mentionUsers
    }
    
    @objc
    fileprivate
    func handleTappingGesturer(_ sender: UIGestureRecognizer) {
        self.chatTextView.resignFirstResponder()
    }
    
    @objc
    fileprivate
    func backButtonPressed(_ sender: UIButton) {
        chatStatusBarView?.dismiss()
        chatStatusBarView = nil
        self.dismissMentionView()
        self.dismiss()
    }
    
    @objc
    fileprivate
    func enableLogAudioButtonPressed(_ sender: UIButton) {
        chatPresenter.shouldShowAudioLogMessages = !chatPresenter.shouldShowAudioLogMessages
    }
    
    @objc fileprivate func endEditButtonPressed(_ sender: UIButton) {
        self.chatBarStatus = .Normal
        self.chatPresenter.voiceIdSelected = nil
        self.chatTextView.reloadInputViews()
    }
    
    @objc fileprivate func collectImageButtonPressed(_ sender: UIButton) {
        self.openPhotoLibrary()
    }
    
    @objc fileprivate func collectAudioButtonPressed(_ sender: UIButton) {
        // Call service
        self.chatBarStatus = .AudioEdited
        self.chatTextView.text = ""
        self.chatTextView.becomeFirstResponder()

        self.chatPresenter.getListVoiceText()
        
        self.chatTextView.inputView = self.audioSentenceKeyboard
        self.chatTextView.reloadInputViews()
    }
    
    @objc fileprivate func sendAudioButtonPressed(_ sender: UIButton) {
        if let id = chatPresenter.voiceIdSelected {
            self.chatTextView.text = ""
            self.audioSentenceKeyboard?.reloadData()
            self.chatPresenter.sendMessage(id, type: .voiceText)
        }
    }
    
    @objc fileprivate func sendButtonPressed(_ sender: UIButton) {
        print("Tag Model list: ", chatTextView.taggedList.count)
       
        if chatPresenter.talkGroup?.allUsers.isEmpty == false, !chatTextView.taggedList.isEmpty {
            var sortedTaggedList = chatTextView.taggedList
            sortedTaggedList.sort { (modelA, modelB) -> Bool in
                return modelA.range.location < modelB.range.location
            }
            sortedTaggedList.forEach { (model) in
                print("Text: \(model.user.name), range: \(model.range.location)")
            }
            var isMentionAll = false
            isMentionAll = !chatTextView.taggedList.filter({ $0.user.id == -1 }).isEmpty
            
            let messageReplaced = Helpers.replaceUserNameById(textMessage: chatTextView.text, taggedUsers: sortedTaggedList)
            
            print("messageReplaced: ", messageReplaced)
            
            self.chatPresenter.sendMessage(messageReplaced, type: .text, isMentionAll: isMentionAll)
        } else {
            self.chatPresenter.sendMessage(self.chatTextView.text)
        }
    
        self.chatTextView.taggedList.removeAll()
        self.chatTextView.text = ""
        self.dismissMentionView()
    }
}

extension ChatVC: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView is UITableView {
            let hasContent = scrollView.contentSize.height > 0
            if scrollView.contentOffset.y <= 0 && hasContent {
                if !isFirstLoadMessage || chatPresenter.isLoading || !chatPresenter.hasNextPage {
                    return
                }
                if let firstMessage = chatPresenter.messages.first {
                    chatPresenter.loadMoreMessage(lastMessageID: firstMessage.id)
                }
            }
        }
    }
}

// MARK: - UITableViewDataSource
extension ChatVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.chatPresenter.messages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let message = chatPresenter.messages[indexPath.row]
        let currentUserIsOwner = (User.getProfile()?.id == message.owner?.id)
        
        switch message.messageType {
        case MessageType.Image.rawValue: // Image
            let cellClass = currentUserIsOwner ? MineImageMessageCell.self : TheirImageMessageCell.self
            let cell = tableView.dequeue(cellClass)
            cell.imageMessageDelegate = self
            cell.delegate = self
            cell.message = message
            return cell

        case MessageType.LogAudio.rawValue: // Log Audio
            let cellClass = currentUserIsOwner ? MineLogVoiceCell.self : TheirLogVoiceCell.self
            let cell = tableView.dequeue(cellClass)
            cell.logVoiceCellDelegate = self
            cell.delegate = self
            cell.message = message
            return cell

        default: // Text, System Audio and Others
            let cellClass = currentUserIsOwner ? MineTextMessageCell.self : TheirTextMessageCell.self
            let cell = tableView.dequeue(cellClass)
            cell.message = message
            cell.delegate = self
            return cell
        }
    }
}

// MARK: - Photo Library
extension ChatVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    fileprivate
    func openPhotoLibrary() {
        _ = self.chatTextView.resignFirstResponder()
        
        let cancelTitle = NSLocalizedString("cancel_button_title", comment: "")
        let cancelAction = UIAlertAction(title: cancelTitle, style: .cancel) { (action) in
        }
        let cameraTitle = NSLocalizedString("take_photo", comment: "")
        let cameraAction = UIAlertAction(title: cameraTitle, style: .default) { (action) in
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                let myPickerController = UIImagePickerController()
                myPickerController.delegate = self
                myPickerController.sourceType = .camera
                self.present(myPickerController, animated: true, completion: nil)
            }
        }
        
        let photoLibrariesTitle = NSLocalizedString("select_avatar_from_Album", comment: "")
        let photoAction = UIAlertAction(title: photoLibrariesTitle, style: .default) { (action) in
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
                self.releaseStatusBarView()
                let myPickerController = UIImagePickerController()
                myPickerController.delegate = self
                myPickerController.sourceType = .photoLibrary
                self.present(myPickerController, animated: true, completion: nil)
            }
        }
        let actions: [UIAlertAction] = [cameraAction, photoAction, cancelAction]
        Helpers.showActionSheet(title: nil, actions: actions, sourceView: self.collectImageButton)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
        self.showStatusBarViewIfNeeded()
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let image = info[.originalImage] as? UIImage else { return }
        DispatchQueue.main.async { [weak self] in
            guard let `self` = self else { return }
            self.chatPresenter.sendMessage(image, type: .image)
        }
        picker.dismiss(animated: true, completion: nil)
        self.showStatusBarViewIfNeeded()
    }
}

// MARK: AudioSentenceKeyboardDelegate
extension ChatVC: AudioSentenceKeyboardDelegate {
    func numberOfItems(in audioSentenceKeyboard: AudioSentenceKeyboard) -> Int {
        return chatPresenter.voices.count
    }
    
    func audioSentenceKeyboard(_ audioSentenceKeyboard: AudioSentenceKeyboard, audioSentenceForItemAt indexPath: IndexPath) -> String {
        return chatPresenter.voices[indexPath.row].content
    }
    
    func audioSentenceKeyboard(_ audioSentenceKeyboard: AudioSentenceKeyboard, didSelectItemAt indexPath: IndexPath) {
        self.chatTextView.text = chatPresenter.voices[indexPath.row].content
        self.chatPresenter.voiceIdSelected = chatPresenter.voices[indexPath.row].id
    }
}

// MARK: - ImageMessageCellDelegate
extension ChatVC: ImageMessageCellDelegate {
    func imageMessageCell(_ imageMessageCell: ImageMessageCell, didTapMessageImage image: UIImage?) {
        let imagePreviewVC = ImagePreviewVC()
        imagePreviewVC.image = image
        self.present(imagePreviewVC, animated: false, completion: nil)
    }
}

// MARK: - UITextViewDelegate
extension ChatVC: UITextViewDelegate {
    func textViewDidEndEditing(_ textView: UITextView) {
        // self.chatTextView.textView.text = ""
        self.chatPresenter.voiceIdSelected = nil
    }
    
    func textViewDidChange(_ textView: UITextView) {
        self.chatBarStatus = .TextEdited
        self.chatTextView.reloadInputViews()
        guard let text = textView.text else { return }
        self.sendButton.isEnabled = text.count > 0
        
        chatTextView.tagging(textView: textView)
        
        if text.count > defaultMessageLengh {
            textView.text = String(text.prefix(defaultMessageLengh))
        }
    }
    
    public func textViewDidChangeSelection(_ textView: UITextView) {
        chatTextView.tagging(textView: textView)
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if self.chatBarStatus == .AudioEdited { return false }
        
        chatTextView.updateTaggedList(range: range, textCount: text.utf16.count)
        return true
    }
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        return true
    }
}

// MARK: - Handle Receive Notification
extension ChatVC {
    @objc func handleCheckMaskAsRead() {
        guard let topViewController = UIApplication.topViewController() as? BaseViewController,
        topViewController is ChatVC else { return }
        
        self.checkForMarkAsRead()
    }
}
extension ChatVC {
    @objc func handleRecieveKeyboardNotification(_ notif: Notification) {
        if let userInfo = (notif as NSNotification).userInfo {
            let keyboardFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as AnyObject).cgRectValue
            _ = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? Double) ?? 0
            _ = UIView.AnimationOptions.init(rawValue: (userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as? UInt) ?? UIView.AnimationOptions.curveEaseInOut.rawValue)
            
            let isKeyboardShowing = notif.name == UIResponder.keyboardWillShowNotification
            var bottomPadding: CGFloat = CGFloat.zero
            
            if #available(iOS 11.0, *) {
                bottomPadding = self.view.safeAreaInsets.bottom
            }
            
            let keyboardSizeHeight: CGFloat = keyboardFrame!.size.height - bottomPadding
            
            self.bottomChatBarConstraint?.constant = isKeyboardShowing ? keyboardSizeHeight : 0
            
            let audioFrame = CGRect(x: 0, y: 0, width: GlobalVariable.screenWidth, height: keyboardSizeHeight)
            if audioSentenceKeyboard == nil {
                audioSentenceKeyboard = AudioSentenceKeyboard(frame: audioFrame)
            } else {
                audioSentenceKeyboard?.frame = audioFrame
            }
    
            self.audioSentenceKeyboard?.delegate = self
            if !chatPresenter.voices.isEmpty {
                self.audioSentenceKeyboard?.reloadData()
            }
            
            if isKeyboardShowing {
                self.bottomChatBarConstraint?.constant = keyboardSizeHeight
                self.view.updateConstraints()
                self.view.layoutIfNeeded()
                
                if let text = mentionView?.mentionText {
                    let mentionUsers = mentionUsersForText(text: text)
                    initialMentionView(hasMentionAll: chatTextView.hasMentionAll, mentionUsers: mentionUsers)
                }
            } else {
                self.bottomChatBarConstraint?.constant = 0
                self.chatBarStatus = .Normal
                self.dismissMentionView()
            }
        }
    }
}

// MARK: ChatView Delegate

extension ChatVC: ChatView {
    func insertMessage(messages: [Message]) {
        let oldContentHeight: CGFloat = self.tableView.contentSize.height
        let oldOffsetY: CGFloat = self.tableView.contentOffset.y
        self.tableView.reloadData()
        let newContentHeight: CGFloat = self.tableView.contentSize.height
        self.tableView.contentOffset.y = oldOffsetY + (newContentHeight - oldContentHeight)
    }
    
    func refresh() {
        self.updateInfoChatFor(talkGroup: self.chatPresenter.talkGroup)
        
        // Avoid crash at the first time `shouldShowAudioLogMessages` is set, when view has not been loaded yet
        if !self.isViewLoaded { return }
        self.tableView.reloadData()
        
        if self.chatPresenter.messages.count > 3 {
            let endIndexPath = IndexPath(row: self.chatPresenter.messages.count - 1, section: 0)
            self.tableView.scrollToRow(at: endIndexPath, at: .middle, animated: false)
        }
        
        if !self.isFirstLoadMessage {
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                self.isFirstLoadMessage = true
            }
        }
        
        self.checkForMarkAsRead()
    }
    
    fileprivate func updateInfoChatFor(talkGroup: TalkGroup?) {
        self.avatarImageView.cancelRequestImage()
        self.backgroundImageView.cancelRequestImage()
        self.nameTalkGroupLabel.text = nil
        
        if let talkGroup = talkGroup {
            self.nameTalkGroupLabel.text = talkGroup.name
            self.avatarImageView.loadImageFromURL(talkGroup.avatar, placeholder: UIImage(named: "img_avatar_talkgroup_empty"))
            self.backgroundImageView.loadImageFromURL(talkGroup.background, placeholder: UIImage(named: "bg-newtalk01"))
        }
    }
    
    func reloadRowAt(index: Int) {
        let indexPath = IndexPath(row: index, section: 0)
        self.tableView.reloadRows(at: [indexPath], with: .fade)
    }
    
    func updateLogAudioStatus() {
        // Avoid crash at the first time `shouldShowAudioLogMessages` is set, when view has not been loaded yet
        if !self.isViewLoaded {
            return
        }
        if chatPresenter.shouldShowAudioLogMessages {
            enableLogAudioButton.setBackgroundImage(UIImage(named: "ic_show_log_audio_mesage"), for: .normal)
            enableLogAudioLabel.text = NSLocalizedString("show_log_audio_button_title", comment: "")
        } else {
            enableLogAudioButton.setBackgroundImage(UIImage(named: "ic_hide_log_audio_mesage"), for: .normal)
            enableLogAudioLabel.text = NSLocalizedString("hide_log_audio_button_title", comment: "")
        }
    }
    
    func updateListAudio() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
            self.audioSentenceKeyboard?.reloadData()
        }
    }
    
    func willSendMessage(message: Message) {
        let indexPath = IndexPath(row: chatPresenter.messages.count, section: 0)
        self.chatPresenter.messages.append(message)
        self.tableView.performBatchUpdates({
            self.tableView.insertRows(at: [indexPath], with: .fade)
        })
        let endIndexPath = IndexPath(row: chatPresenter.messages.count - 1, section: 0)
        self.tableView.scrollToRow(at: endIndexPath, at: .bottom, animated: true)
    }
    
    func didSendMessage(message: Message, fakeId: Int?) {
        guard let fakeId = fakeId else {
            let indexPath = IndexPath(row: chatPresenter.messages.count, section: 0)
            self.chatPresenter.messages.append(message)
            self.tableView.performBatchUpdates({
                self.tableView.insertRows(at: [indexPath], with: .fade)
            })
            self.tableView.scrollsToBottom(true)
            self.checkForMarkAsRead()
            return
        }
        if let index = self.chatPresenter.messages.index(where: {$0.id == fakeId}) {
            chatPresenter.messages[index] = message
            let indexPath = IndexPath(row: index, section: 0)
            self.tableView.reloadRows(at: [indexPath], with: .fade)
            self.checkForMarkAsRead()
        }
    }
}

// MARK: BaseMessageCell Delegate

extension ChatVC: BaseMessageCellDelegate {
    func willShowSeenUser(users: [User]) {
        let vc = SeenUsersVC()
        vc.modalPresentationStyle = .overCurrentContext
        let presenter = SeenUsersPresenter(view: vc)
        presenter.users = users
        vc.presenter = presenter
        self.present(vc, animated: true, completion: nil)
    }
}

// MARK: LogVoiceCell Delegate
extension ChatVC: LogVoiceCellDelegate {
    func logVoiceCellDidPlay(_ logVoiceCell: LogVoiceCell) {
        stopPreviousPlayingAudio()
        playingAudioCell = logVoiceCell
    }
}

// MARK: - TaggingDataSource
extension ChatVC: TaggingDataSource {
    
    func tagging(_ taggingTextView: GrowingTextView, didChangedTagableList tagableList: [User]) {
        matchedList = tagableList
        
        if let me = User.getProfile(), let index = matchedList.firstIndex(where: { $0.id == me.id }) {
            matchedList.remove(at: index)
        }
        
        if matchedList.isEmpty && !taggingTextView.hasMentionAll {
            self.dismissMentionView()
            return
        }
        initialMentionView(hasMentionAll: taggingTextView.hasMentionAll, mentionUsers: matchedList)
        mentionView?.show(text: "", users: matchedList, isUpdateFrame: false, completion: { (user) in
            self.dismissMentionView()
            self.chatTextView.updateTaggedList(allText: taggingTextView.text, tagUser: user)
        })
    }
    
    func tagging(_ taggingTextView: GrowingTextView, didChangedTaggedList taggedList: [TaggingModel]) {
        self.taggedList = taggedList
    }
}
