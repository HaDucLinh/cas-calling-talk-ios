//
//  ImagePreviewVC.swift
//  CallingTalk
//
//  Created by Toof on 3/14/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit

class ImagePreviewVC: BaseViewController {
    open var image: UIImage?
    
    @IBOutlet weak var imageScrollView: ImageScrollView!
    @IBOutlet weak var closeButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.imageScrollView.imageScrollViewDelegate = self
        self.imageScrollView.imageContentMode = .aspectFit
        self.imageScrollView.initialOffset = .center
        
        self.closeButton.addTarget(self, action: #selector(closeButtonPressed(_:)), for: .touchUpInside)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.imageScrollView.display(image: self.image)
    }
}

// MARK: - Handle Events

extension ImagePreviewVC {
    @objc
    fileprivate
    func closeButtonPressed(_ sender: UIButton) {
        self.dismiss(animated: false, completion: nil)
    }
}

// MARK: - Handle Events

extension ImagePreviewVC: ImageScrollViewDelegate {
    func imageScrollViewDidChangeOrientation(imageScrollView: ImageScrollView) {
        print("Did change orientation")
    }
}
