//
//  AddUserIncomingVC.swift
//  CallingTalk
//
//  Created by Toof on 2/21/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit

class SeenUsersVC: BaseViewController {

    // MARK: Properties
    
    public var presenter: SeenUsersPresenter!
    
    @IBOutlet weak var containView: UIView!
    @IBOutlet weak var memberImageView: UIImageView!
    @IBOutlet weak var ratioHeightConstraint: NSLayoutConstraint!

    @IBOutlet weak var closeButton: UIButton!
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var blurView: GradientView!
    
    // MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupSubviews()
    }
    
    fileprivate
    func setupSubviews() {
        
        self.containView.corner = 20.0
        self.closeButton.addTarget(self, action: #selector(closeButtonPressed(_:)), for: .touchUpInside)
                self.memberImageView.image = UIImage(named: "ic-member-small")
        self.titleLabel.text = NSLocalizedString("Read_member_text", comment: "")
        
//        let totalHeight: CGFloat = CGFloat(84 + presenter.users.count * 60)
//        self.ratioHeightConstraint.constant = min(totalHeight, 460 * GlobalVariable.ratioHeight)
        self.ratioHeightConstraint.constant = 460 * GlobalVariable.ratioHeight
        
        self.view.layoutIfNeeded()
        self.view.updateConstraints()
        
        // Setup blur view
        self.blurView.gradientLayer.colors = [
            UIColor.white.withAlphaComponent(0.0).cgColor,
            UIColor.white.cgColor
        ]
        self.blurView.gradientLayer.gradient = GradientPoint.topBottom.draw()
        
        // Setup table view
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.register(LargeSeenUserCell.self)
        self.tableView.separatorColor = UIColor.white.withAlphaComponent(0.0)
        self.tableView.backgroundColor = UIColor.white.withAlphaComponent(0.0)
        self.tableView.tableFooterView = UIView()
    }
    
    override func localizeString() {
        self.titleLabel.text = NSLocalizedString("adding_user_talk_group_label_text", comment: "")
    }
}

// MARK: - Handle Events

extension SeenUsersVC {
    
    @objc
    fileprivate
    func closeButtonPressed(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}

// MARK: - UITableViewDataSource

extension SeenUsersVC: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.presenter.numberOfRows(inSection: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeue(LargeSeenUserCell.self)
        self.presenter.configure(cell, at: indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
}

// MARK: - UITableViewDelegate

extension SeenUsersVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}

// MARK: - Implement View

extension SeenUsersVC: SeenUsersView {
    
    func refresh() {
        self.tableView.reloadData()
    }
    
}
