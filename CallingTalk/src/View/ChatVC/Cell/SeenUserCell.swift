//
//  SeenUserCell.swift
//  CallingTalk
//
//  Created by Vu Tran on 3/7/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit

class SeenUserCell: UICollectionViewCell {
    
    @IBOutlet private weak var avatarImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        avatarImageView.corner = avatarImageView.frame.size.width / 2
        //self.contentView setTransform:CGAffineTransformMakeScale(-1, 1)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        avatarImageView.corner = avatarImageView.frame.size.width / 2
        layoutIfNeeded()
    }

    var user: User? {
        didSet {
            if let seenUser = self.user {
                avatarImageView.loadImageFromURL(seenUser.avatarUrl, placeholder: UIImage(named: "img_avatar_user_empty"))
            }
        }
    }
}
