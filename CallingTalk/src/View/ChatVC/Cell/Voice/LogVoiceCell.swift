//
//  LogVoiceCell.swift
//  CallingTalk
//
//  Created by Chinh Le on 5/3/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit
import AVKit

protocol LogVoiceCellDelegate: class {
    func logVoiceCellDidPlay(_ logVoiceCell: LogVoiceCell)
}

class LogVoiceCell: BaseMessageCell {
    @IBOutlet private weak var myAvatarView: UIImageView!
    @IBOutlet weak var voiceNameLabel: UILabel!
    @IBOutlet weak var playImageView: UIImageView!
    @IBOutlet private weak var durationLabel: UILabel!
    @IBOutlet private weak var timeSentLabel: UILabel!
    @IBOutlet private weak var loadingIndicatorView: UIActivityIndicatorView!
    
    weak var logVoiceCellDelegate: LogVoiceCellDelegate?
    var playerManager: AVPlayerManager?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setUpView()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    private func setUpView() {
        selectionStyle = .none
        myAvatarView.roundedCornerRadius()
        voiceNameLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .bold, size: 12)
        durationLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .regular, size: 20)
        timeSentLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .regular, size: 12)
    }
    
    override func setUpData() {
        super.setUpData()
        if let user = message?.owner {
            myAvatarView.loadImageFromURL(user.avatarUrl, placeholder: UIImage(named: "img_avatar_user_empty"))
            voiceNameLabel.text = user.name
        }
        timeSentLabel.text = message?.createTime?.string(withFormat: "MM/dd HH:mm")
        updateUI()
    }
    
    func stopPlaying() {
        playerManager?.stopAudio()
        playerManager?.playingAudioMessageId = nil
        playerManager = nil
        updateUI()
        if loadingIndicatorView.isAnimating {
            loadingIndicatorView.stopAnimating()
        }
    }
    
    @IBAction func playButtonTapped(_ sender: Any) {
        if playerManager == nil {
            setupPlayer()
        } else {
            stopPlaying()
        }
    }
    
    // MARK: Private
    fileprivate
    func updateUI() {
        let isCurrentPlayingAudio = (AVPlayerManager.shared.playingAudioMessageId == message?.id)
        if isCurrentPlayingAudio && (playerManager == nil) {
            playerManager = AVPlayerManager.shared
            setupCallbacks()
        }
        let isPlaying = isCurrentPlayingAudio && playerManager!.isPlayerReady()
        let imageName = isPlaying ? "ic_playing_audio" : "ic-log-audio"
        let timeText = isPlaying ? playerManager?.audioReverseCurrentTimeString() : AVPlayerManager.shared.stringFromTimeInterval(interval: message?.duration)
        playImageView.image = UIImage.init(named: imageName)
        durationLabel.text = timeText
        if isCurrentPlayingAudio && !playerManager!.isPlayerReady() {
            loadingIndicatorView.startAnimating()
        }
    }
    
    fileprivate
    func setupPlayer() {
        logVoiceCellDelegate?.logVoiceCellDidPlay(self)
        playerManager = AVPlayerManager.shared
        if let audioURL = message?.fileURL, let playerManager = playerManager {
            playerManager.playingAudioMessageId = message?.id
            loadingIndicatorView.startAnimating()
            playerManager.playAudio(url: URL(string: audioURL))
            setupCallbacks()
        }
    }
    
    fileprivate
    func setupCallbacks() {
        guard let playerManager = playerManager else {
            return
        }
        playerManager.completion = { [weak self] in
            guard let self = self else {
                return
            }
            
            self.stopPlaying()
        }
        
        // Notify every 1 second
        let interval = CMTime(seconds: 1,
                              preferredTimescale: CMTimeScale(NSEC_PER_SEC))
        playerManager.player?.addPeriodicTimeObserver(forInterval: interval, queue: DispatchQueue.main) { [weak self] (CMTime) -> Void in
            guard let self = self else {
                return
            }
            if playerManager.isPlayerReady() {
                if self.loadingIndicatorView.isAnimating {
                    self.loadingIndicatorView.stopAnimating()
                }
                self.updateUI()
            }
        }
    }
}
