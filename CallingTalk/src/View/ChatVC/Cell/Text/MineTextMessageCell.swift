//
//  MineTextMessageCell.swift
//  CallingTalk
//
//  Created by Toof on 3/7/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit

class MineTextMessageCell: TextMessageCell {
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setUpData() {
        super.setUpData()
        guard let message = self.message else { return }
        if message.id <= 0 {
            self.messageTextView.backgroundColor = .clear
            self.contentMainView?.backgroundColor = UIColor.white.withAlphaComponent(0.9)
        } else {
            self.messageTextView.backgroundColor = .white
            self.contentMainView?.backgroundColor = UIColor.white
        }
    }
}
