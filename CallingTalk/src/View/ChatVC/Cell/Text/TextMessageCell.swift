//
//  TextMessageCell.swift
//  CallingTalk
//
//  Created by Toof on 3/7/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit

public enum TextMessageType {
    case Normal
    case Audio
}

class TextMessageCell: BaseMessageCell {
    
    // MARK: Properties
    
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var dateSendMessageLabel: UILabel!
    @IBOutlet weak var messageTextView: UITextView!
    @IBOutlet weak var speakerButton: UIButton!
    
    open var messageType: TextMessageType = .Normal
    fileprivate var isPlayingAudio = false
    
    // MARK: Lifecycles
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupSubviews()
    }
    
    fileprivate
    func setupSubviews() {
        self.selectionStyle = .none
        self.backgroundColor = UIColor.white.withAlphaComponent(0.0)
        self.contentView.backgroundColor = UIColor.white.withAlphaComponent(0.0)
        
        self.avatarImageView.roundedCornerRadius()
        self.speakerButton.addTarget(self, action: #selector(speakerButtonPressed(_:)), for: .touchUpInside)
        self.setupMessageTextView()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.avatarImageView.cancelRequestImage()
    }
    
    fileprivate
    func setupMessageTextView() {
        self.messageTextView.textContainerInset = UIEdgeInsets.zero
        self.messageTextView.isScrollEnabled = false
        self.messageTextView.isEditable = false
        self.messageTextView.delegate = self
    }
    
    override func setUpData() {
        super.setUpData()
        guard let message = message else {
            usernameLabel.text = ""
            messageTextView.text = ""
            avatarImageView.image = nil
            dateSendMessageLabel.text = ""
            return
        }
        if let owner = message.owner {
            usernameLabel.text = owner.name
            avatarImageView.loadImageFromURL(owner.avatarUrl, placeholder: UIImage(named: "img_avatar_user_empty"))
        } else {
            usernameLabel.text = NSLocalizedString("system_text", comment: "")
            avatarImageView.image = UIImage(named: "ic-admin-chat")
        }
        setUpMessageType(message: message)
        
        dateSendMessageLabel.text = message.createTime?.string(withFormat: "MM/dd HH:mm")
    }
    
    private func setUpMessageType(message: Message) {
        let type = message.messageType
        if type == MessageType.SystemAudio.rawValue || type == MessageType.VoiceText.rawValue {
            self.speakerButton.isHidden = false
            self.messageTextView.textColor = UIColor.ct.highlightMessageTextColor
            self.messageTextView.text = message.content
        } else {
            self.speakerButton.isHidden = true
            self.messageTextView.textColor = UIColor.black
            
            // Add Attribute Color for user who was mentioned.
            var mentionUsers: [User] = []
            message.mention_users.forEach { (user) in
                mentionUsers.append(user)
            }
            
            print("message: ", message.content)
            
            if message.isMentionAll {
                mentionUsers.append(User.allUser())
            }
        
            if mentionUsers.isEmpty {
                messageTextView.text = message.content
            } else {
                let contentTuple = Helpers.replaceUserIdByName(textMessage: message.content, mentionUsers: mentionUsers, isMentionAll: message.isMentionAll)
                
                let attributesString = addAttributesText(ranges: contentTuple.1, allText: contentTuple.0)
                messageTextView.attributedText = attributesString
            }
        }
    }
    
    func addAttributesText(ranges: [NSRange], allText: String) -> NSMutableAttributedString {
        let attributedString = allText.attributesString(color: UIColor.black,
                                    font: UIFont.ct.font(.NotoSansCJKjp, type: .regular, size: 14.0))
       
        ranges.forEach { (range) in
            attributedString.addAttributes([
                .font: UIFont.ct.font(.NotoSansCJKjp, type: .regular, size: 14.0),
                .foregroundColor: UIColor.ct.roleAuthorViewColor
                ], range: NSRange(location: range.location, length: range.length))
        }
        return attributedString
    }
}

// MARK: - Handle events
extension TextMessageCell {
    @objc
    fileprivate
    func speakerButtonPressed(_ sender: UIButton) {
        if isPlayingAudio {
            isPlayingAudio = false
            AVPlayerManager.shared.pauseAudio()
            self.speakerButton.setImage(UIImage(named: "ic_audio_message_green"), for: .normal)
            return
        }
        if let urlString = message?.fileURL, let url = URL(string: urlString) {
            isPlayingAudio = true
            let playerManager = AVPlayerManager.shared
            playerManager.playAudio(url: url)
            playerManager.completion = {
                // Done
                self.isPlayingAudio = false
                self.speakerButton.setImage(UIImage(named: "ic_audio_message_green"), for: .normal)
            }
        }
    }
}

// MARK: - UITextViewDelegate
extension TextMessageCell: UITextViewDelegate {
    // TO DO:
}
