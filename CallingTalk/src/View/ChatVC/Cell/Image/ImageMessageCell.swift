//
//  ImageMessageCell.swift
//  CallingTalk
//
//  Created by Toof on 3/7/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit

protocol ImageMessageCellDelegate: class {
    func imageMessageCell(_ imageMessageCell: ImageMessageCell, didTapMessageImage image: UIImage?)
}

class ImageMessageCell: BaseMessageCell {

    // MARK: Properties
    
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var messageImageView: UIImageView!
    weak var imageMessageDelegate: ImageMessageCellDelegate?
    
    // MARK: Lifecycles
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupSubviews()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.messageImageView.cancelRequestImage()
        self.avatarImageView.cancelRequestImage()
    }
    
    override func setUpData() {
        super.setUpData()
        guard let message = message else {
            avatarImageView.image = nil
            return
        }
        if let owner = message.owner {
            avatarImageView.loadImageFromURL(owner.avatarUrl, placeholder: UIImage(named: "img_avatar_user_empty"))
        } else {
            avatarImageView.image = UIImage(named: "ic-admin-chat")
        }
        self.messageImageView.loadImageFromURL(message.fileURL, placeholder: UIImage(named: "ic_image_placeholder"))
    }
    
    fileprivate
    func setupSubviews() {
        self.selectionStyle = .none
        self.backgroundColor = UIColor.white.withAlphaComponent(0.0)
        self.contentView.backgroundColor = UIColor.white.withAlphaComponent(0.0)
        
        self.avatarImageView.roundedCornerRadius()
        self.messageImageView.corner = 10.0
        
        // Add tap gesture for contentview
        let tapGesture: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleTapGesture(_:)))
        self.contentMainView?.addGestureRecognizer(tapGesture)
    }
    
    @objc
    fileprivate
    func handleTapGesture(_ sender: UITapGestureRecognizer) {
        self.imageMessageDelegate?.imageMessageCell(self, didTapMessageImage: self.messageImageView.image)
    }
}
