//
//  SelectMemberCell.swift
//  CallingTalk
//
//  Created by Vu Tran on 2/15/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit

class LargeSeenUserCell: UITableViewCell {
    
    @IBOutlet private weak var avatarImageView: UIImageView!
    @IBOutlet private weak var nicknameLabel: UILabel!
    @IBOutlet private weak var aboutLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        self.avatarImageView.corner = avatarImageView.frame.size.width / 2
        
        self.nicknameLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .regular, size: 15)
        self.aboutLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .bold, size: 11)
        self.aboutLabel.textColor = UIColor.ct.aboutMemberTextColor
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.avatarImageView.corner = avatarImageView.frame.size.width / 2
    }
    
    override func willMove(toSuperview newSuperview: UIView?) {
        super.willMove(toSuperview: newSuperview)
        self.avatarImageView.corner = avatarImageView.frame.size.width / 2
        self.layoutIfNeeded()
    }
    
    var member: User? {
        didSet {
            guard let member = member else {
                self.avatarImageView.image = nil
                self.nicknameLabel.text = ""
                self.aboutLabel.text = ""
                return
            }
            self.avatarImageView.loadImageFromURL(member.avatarUrl, placeholder: UIImage(named: "img_avatar_user_empty"))
            self.nicknameLabel.text = member.name
            self.updateOrganizations(Array(member.organizations))
        }
    }
    
    fileprivate func updateOrganizations(_ organizations: [Organization]) {
        guard organizations.count > 0 else {
            self.aboutLabel.text = ""
            return
        }
        
        let organizationsString = organizations.map({ $0.name }).joined(separator: ", ")
        let title = NSLocalizedString("member_for_organizations_label_text", comment: "")
        
        let attributedString = NSMutableAttributedString(string: "\(title)\(organizationsString)", attributes: [
            .font: UIFont.ct.font(.NotoSansCJKjp, type: .regular, size: 11.0),
            .foregroundColor: UIColor.ct.aboutMemberTextColor,
            .kern: 0.0
            ])
        attributedString.addAttribute(.font,
                                      value: UIFont.ct.font(.NotoSansCJKjp, type: .bold, size: 11.0),
                                      range: NSRange(location: 0, length: title.count))
        
        self.aboutLabel.attributedText = attributedString
    }
    
}
