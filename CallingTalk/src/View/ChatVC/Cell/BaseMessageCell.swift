//
//  BaseMessageCell.swift
//  CallingTalk
//
//  Created by Vu Tran on 3/7/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit

protocol BaseMessageCellDelegate: NSObjectProtocol {
    func willShowSeenUser(users: [User])
}
class BaseMessageCell: UITableViewCell {
    @IBOutlet public weak var contentMainView: UIView?
    @IBOutlet public weak var otherSeenUserLabel: UILabel!
    @IBOutlet public weak var collectionView: UICollectionView!
    @IBOutlet public weak var collectionHeightConstraint: NSLayoutConstraint!
    @IBOutlet public weak var contentBottomHeightConstraint: NSLayoutConstraint!
    fileprivate let maxNumberUserSeen = 10
    weak var delegate: BaseMessageCellDelegate?

    var message: Message? {
        didSet {
            setUpData()
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        contentMainView?.corner = 10
        otherSeenUserLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .regular, size: 12)
        otherSeenUserLabel.textColor = UIColor.ct.grayBorderColor
        otherSeenUserLabel.text = ""
        
        let tapLabel = UITapGestureRecognizer(target: self, action: #selector(self.handleTap))
        otherSeenUserLabel.isUserInteractionEnabled = true
        otherSeenUserLabel.addGestureRecognizer(tapLabel)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap))
        collectionView.addGestureRecognizer(tap)
        
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.register(SeenUserCell.self)
        collectionView.dataSource = self
        collectionView.delegate = self
    }

    public func setUpData() {
        guard let message = self.message else {
            updateHeightConstraint(isHideBottom: true)
            return
        }
        
        updateHeightConstraint(isHideBottom: message.seenUsers.isEmpty == true)
        if message.seenUsers.count > maxNumberUserSeen {
            let extendNumber = message.seenUsers.count - maxNumberUserSeen
            otherSeenUserLabel.text = "+" + extendNumber.description + NSLocalizedString("people_text", comment: "")
        } else {
            otherSeenUserLabel.text = ""
        }
        updateHeightConstraint(isHideBottom: message.seenUsers.isEmpty == true)
        
        collectionView.semanticContentAttribute = .forceRightToLeft
        collectionView.reloadData()
    }

    public func updateHeightConstraint(isHideBottom: Bool) {
        if isHideBottom {
            otherSeenUserLabel.text = ""
            collectionHeightConstraint.constant = 0
            contentBottomHeightConstraint.constant = 0
        } else {
            collectionHeightConstraint.constant = 20
            contentBottomHeightConstraint.constant = 14
        }
        contentView.updateConstraints()
    }
    
    @objc private func handleTap() {
        var seenUsers: [User] = []
        message?.seenUsers.forEach({ (user) in
            seenUsers.append(user)
        })
        delegate?.willShowSeenUser(users: seenUsers)
    }
}
extension BaseMessageCell: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let total = message?.seenUsers.count, total > 0 else {
            return 0
        }
        if total > maxNumberUserSeen { return maxNumberUserSeen }
        return total
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeue(SeenUserCell.self, forIndexPath: indexPath)
        cell.user = message?.seenUsers[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        handleTap()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 20.0, height: 20.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 2.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 2.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 19)
    }
}
