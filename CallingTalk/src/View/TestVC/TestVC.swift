//
//  TestVC.swift
//  CallingTalk
//
//  Created by Van Trung on 1/15/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit

class TestVC: UIViewController {

    @IBOutlet weak var jitsiMeetView: JitsiMeetView!
    
    private func initJitsiView(){
//        jitsiMeetView.welcomePageEnabled = false
//        jitsiMeetView.load(URL(string: "https://meet.jit.si/vvtt"))
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initJitsiView()
        // Do any additional setup after loading the view.
    }

}

extension TestVC: JitsiMeetViewDelegate{
    func conferenceJoined(_ data: [AnyHashable : Any]!) {
        print(data)
    }
    
    func conferenceLeft(_ data: [AnyHashable : Any]!) {
        print(data)
    }
    func conferenceFailed(_ data: [AnyHashable : Any]!) {
        print(data)
    }
}
