//
//  StartNewTalkVC.swift
//  CallingTalk
//
//  Created by Vu Tran on 2/14/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit
import RealmSwift
import Swinject
protocol StartNewTalkVCDelegate: class {
    func didCreateNewTalkGroupWithId(_ id: Int, in startNewTalkVC: StartNewTalkVC)
}
class StartNewTalkVC: BaseViewController {
    fileprivate let newTalkPresenter = NewTalkPresenter()
    
    @IBOutlet private weak var headerView: UIView!
    @IBOutlet private weak var talkNameTextView: UITextView!
    @IBOutlet weak var shortButtonView: UIView!
    @IBOutlet weak var shortButton: UIButton!
    
    @IBOutlet private weak var talkImageView: UIImageView!
    @IBOutlet private weak var numberOfMembersLabel: UILabel!
    @IBOutlet private weak var talkNameCharactersLabel: UILabel!
    @IBOutlet private weak var startTalkButton: UIButton!
    @IBOutlet fileprivate weak var collectionView: UICollectionView!
    @IBOutlet private weak var talkNameHeightConstraint: NSLayoutConstraint!
    
    weak var delegate: StartNewTalkVCDelegate?
    
    fileprivate var width: CGFloat = 0
    fileprivate var selectedTalkImageID: Int = 1

    // MARK: Life circle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.newTalkPresenter.attachView(self)
        self.newTalkPresenter.updateMembers([])
        self.newTalkPresenter.updateAvatarData(UIImage(named: "ic-newtalk01")!.pngData())
        self.newTalkPresenter.updateBackgroundData(UIImage(named: "bg-newtalk01")!.pngData())
        self.setUpUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = false
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.width = UIScreen.main.bounds.width
        self.refreshMembers()
    }
    
    // MARK: Functions
    func setUpStyleForBuild() {
        //TODO: this is 2 difference style of button, we can't just modify the value
        //So make hard-code right here!! we need to fix it later if has more than this place
        let systemTheme = Container.default.resolve(SystemThemeable.self)
        switch systemTheme!.getCurrentMode() {
        case ThemeMode.normal:
            self.startTalkButton.isHidden = false
            self.shortButtonView.isHidden = true
        case ThemeMode.omiseno:
            self.startTalkButton.isHidden = true
            self.shortButtonView.isHidden = false
//            self.shortButton.backgroundColor = UIColor.ct.mainBackgroundColor
            self.shortButton.layer.cornerRadius = 10
            self.shortButton.clipsToBounds = true
            break
        }
    }
    private func setUpUI() {
        title = NSLocalizedString("start_NewTalk_title_text", comment: "")
        let settings = CustomButtonBarSetting(imageName: "ic-close",
                                              style: .plain,
                                              action: #selector(close),
                                              target: self,
                                              possition: .left)
        _ = CustomUIItemFactory().createAndLoadButtonBar(withSetting: settings)
        
        self.enableStartButton(false)
        
        numberOfMembersLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .bold, size: 12)
        talkNameCharactersLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .regular, size: 12)
        
        talkImageView.contentMode = .scaleAspectFit
        talkImageView.corner = talkImageView.frame.size.width / 2
        let tapShowIcon = UITapGestureRecognizer(target: self, action: #selector(selectIcon))
        talkImageView.isUserInteractionEnabled = true
        talkImageView.addGestureRecognizer(tapShowIcon)
        
        setPlaceHolderText()
        talkNameTextView.font = UIFont.ct.font(.NotoSansCJKjp, type: .regular, size: 14)
        talkNameTextView.returnKeyType = .done
        talkNameTextView.delegate = self
        talkNameTextView.isScrollEnabled = false
        
        let startTalkTitle = NSLocalizedString("start_talk_button_title", comment: "")
        startTalkButton.setTitle(startTalkTitle, for: .normal)
        shortButton.setTitle(startTalkTitle, for: .normal)
        startTalkButton.titleLabel?.attributedText = NSAttributedString(string: startTalkTitle,
                                                                        attributes: [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: UIFont.ct.font(.NotoSansCJKjp, type: .bold, size: 16)])
        shortButton.titleLabel?.attributedText = NSAttributedString(string: startTalkTitle,
                                                                    attributes: [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: UIFont.ct.font(.NotoSansCJKjp, type: .bold, size: 16)])
        if GlobalVariable.screenWidth == 320 {
            headerView.widthAnchor.constraint(equalTo: headerView.heightAnchor, multiplier: 75/53).isActive = true
        }
        
        collectionView.register(MemberCell.self)
        collectionView.dataSource = self
        collectionView.delegate = self
        setUpStyleForBuild()
        self.view.layoutIfNeeded()
    }
    
    fileprivate func setPlaceHolderText() {
        let placeholderText = NSLocalizedString("enter_name_of_the_talk_text", comment: "")
        talkNameTextView.font = UIFont.ct.font(.NotoSansCJKjp, type: .regular, size: 14)
        talkNameTextView.text = placeholderText
        talkNameTextView.textColor = UIColor.ct.grayPlaceHolderTextColor
    }
    
    fileprivate func startNewTalk() {
        self.newTalkPresenter.getListOrganizationForAdmin()
    }
    
    // MARK: Actions
    @objc func close() {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction private func selectIcon(sender: UIButton) {
        let selectIconVC = SelectIconVC()
        selectIconVC.oldAvatarId = selectedTalkImageID
        selectIconVC.defaultAvatarIndex = newTalkPresenter.defaultAvatarIndex
        selectIconVC.delegate = self
        selectIconVC.isFromNewTalk = true
        
        selectIconVC.transitioningDelegate = self
        self.definesPresentationContext = true
        selectIconVC.view.backgroundColor = .clear
        selectIconVC.modalPresentationStyle = .overFullScreen
        present(selectIconVC, animated: true, completion: nil)
    }
    
    @IBAction private func inviteMember(sender: UIButton) {
        let inviteMemberVC = InviteMemberVC()
        inviteMemberVC.delegate = self
        inviteMemberVC.presenter.inviteMemberForType = .Create
        inviteMemberVC.presenter.usersToInvite = self.newTalkPresenter.members
        present(UINavigationController(rootViewController: inviteMemberVC), animated: true, completion: nil)
    }
    
    @IBAction func shortButtonStartTalk(_ sender: Any) {
        self.startNewTalk()
    }
    
    @IBAction private func startTalk(sender: UIButton) {
        self.startNewTalk()
    }
}
// MARK: NewTalkView
extension StartNewTalkVC: NewTalkView {
    func enableStartButton(_ isEnable: Bool) {
        self.startTalkButton.isEnabled = isEnable
        self.startTalkButton.backgroundColor = isEnable ? UIColor.ct.mainBackgroundColor : UIColor.ct.redDisableButton
        self.shortButton.isEnabled = isEnable
        self.shortButton.backgroundColor = isEnable ? UIColor.ct.mainBackgroundColor : UIColor.ct.mainBackgroundColor.withAlphaComponent(0.47)
    }
    
    func updateNumberOfMembers(_ number: String) {
        self.numberOfMembersLabel.text = NSLocalizedString("number_of_member_text", comment: "") + number
    }
    
    func moveToIncomingVCWithTalkGroupId(_ id: Int) {
        self.dismiss(animated: true, completion: nil)
        self.delegate?.didCreateNewTalkGroupWithId(id, in: self)
    }
    
    func showListOrganizationVC(with organizations: [Organization]) {
        let listOrganizationVC = ListOrganizationVC()
        listOrganizationVC.presenter = ListOrganizationPresenter(view: listOrganizationVC, organizations: organizations)
        listOrganizationVC.delegate = self
        listOrganizationVC.transitioningDelegate = self
        listOrganizationVC.modalPresentationStyle = .overFullScreen
        self.present(listOrganizationVC, animated: true, completion: nil)
    }
    
    func refreshMembers() {
        self.collectionView.reloadData()
    }
}
// MARK: UICollectionView DataSource & Delegate
extension StartNewTalkVC: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.newTalkPresenter.numberOfItems(inSection: section)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeue(MemberCell.self, forIndexPath: indexPath)
        self.newTalkPresenter.configure(cell, at: indexPath)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let widthCell = (self.width - 30) / 4
        return CGSize(width: widthCell, height: widthCell + 10)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}

// MARK: SelectIconVC Delegate
extension StartNewTalkVC: SelectIconVCDelegate {
    func selectAvatar(avatar: Avatar, at index: Int) {
        self.selectedTalkImageID = avatar.id
        newTalkPresenter.defaultAvatarIndex = index + 1
        self.talkImageView.image = UIImage(named: avatar.name)
        self.newTalkPresenter.updateAvatarData(UIImage(named: avatar.name)!.pngData())
        self.newTalkPresenter.updateBackgroundData(UIImage(named: avatar.name.replacingOccurrences(of: "ic", with: "bg"))!.pngData())
    }
    
    func didSelectImage(image: UIImage) {
        let resizeImage = image.resizeImage()
        
        // Update background
        do {
            let imageBackgroundData = try resizeImage?.compressToDataSize()
            self.newTalkPresenter.updateBackgroundData(imageBackgroundData)
        }
        catch {
            self.showErrorAlertWithError(error, completion: nil)
        }
        
        // Update avatar
        let vc = EditPhotoVC()
        vc.delegate = self
        vc.sourceImage = resizeImage
        let navi = UINavigationController(rootViewController: vc)
        navi.navigationBar.setupNavigationBarWithColor(.white)
        present(navi, animated: true, completion: nil)
        self.selectedTalkImageID = 0
    }
}
extension StartNewTalkVC: EditPhotoVCDelegate {
    func didCropImage(croppedImage: UIImage) {
        newTalkPresenter.defaultAvatarIndex = 0
        self.talkImageView.image = croppedImage
        do {
            let imageData = try croppedImage.compressToDataSize()
            self.newTalkPresenter.updateAvatarData(imageData)
        }
        catch {
            self.showErrorAlertWithError(error, completion: nil)
        }
    }
}
extension StartNewTalkVC: InviteMemberVCDelegate {
    func invitedMembers(_ members: [User], in inviteMemberVC: InviteMemberVC) {
        inviteMemberVC.dismiss(animated: true, completion: nil)
        self.newTalkPresenter.updateMembers(members)
    }
}
extension StartNewTalkVC: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.ct.grayPlaceHolderTextColor {
            textView.text = ""
            textView.textColor = UIColor.black
        }
    }
    
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        textView.endEditing(true)
        return true
    }
    
    func textViewDidChange(_ textView: UITextView) {
        self.newTalkPresenter.updateName(textView.text)
        if let talkName = textView.text {
            talkNameCharactersLabel.text = talkName.count.description + "/30"
        }
        
        textView.dynamicHeight(heightConstraint: talkNameHeightConstraint)
        self.view.updateConstraints()
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            textView.dynamicHeight(heightConstraint: talkNameHeightConstraint, defaultHeight: 40)
            self.setPlaceHolderText()
            self.view.updateConstraints()
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        
        guard let textOld = textView.text else { return true }
        let count = textOld.count + text.count - range.length
        return count <= 30
    }
}
// MARK: ListOrganizationVCDelegate
extension StartNewTalkVC: ListOrganizationVCDelegate {
    func didSelectOrganization(_ organization: Organization, in listOrganizationVC: ListOrganizationVC) {
        listOrganizationVC.dismiss(animated: true, completion: nil)
        self.newTalkPresenter.createTalkGroup(with: organization.id, avatarIndex: newTalkPresenter.defaultAvatarIndex)
    }
}
// MARK: Transitioning Delegate
extension StartNewTalkVC: UIViewControllerTransitioningDelegate {
    
    func animationController(forPresented presented: UIViewController,
                             presenting: UIViewController,
                             source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return FadePushAnimator(type: .modal)
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        var interactionController: UIPercentDrivenInteractiveTransition?
        if let viewController = dismissed as? SelectIconVC {
            interactionController = viewController.interactionController
        }
        return FadePopAnimator(type: .modal, interactionController: interactionController)
    }
    
    func interactionControllerForDismissal(using animator: UIViewControllerAnimatedTransitioning)
        -> UIViewControllerInteractiveTransitioning? {
            guard let animator = animator as? FadePopAnimator,
                let interactionController = animator.interactionController as? LeftEdgeInteractionController, interactionController.inProgress else {
                    return nil
            }
            return interactionController
    }
}
