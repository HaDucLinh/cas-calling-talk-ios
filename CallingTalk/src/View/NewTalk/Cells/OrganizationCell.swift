//
//  OrganizationCell.swift
//  CallingTalk
//
//  Created by Toof on 4/5/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit

class OrganizationCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var chechImageView: UIImageView!
    
    public var organization: Organization! {
        didSet {
            self.titleLabel.text = self.organization.name
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.chechImageView.image = UIImage(named: selected ? "ic_select_organization" : "ic-NonSelected")
    }
    
}
