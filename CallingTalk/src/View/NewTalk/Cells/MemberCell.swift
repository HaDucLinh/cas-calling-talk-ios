//
//  MemberCell.swift
//  CallingTalk
//
//  Created by Vu Tran on 2/14/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit

public enum UserRoleType {
    case Admin
    case Author
    case Member
}

class MemberCell: UICollectionViewCell {
    
    @IBOutlet weak var memberAvatar: UIImageView!
    @IBOutlet private weak var memberNameLabel: UILabel!
    @IBOutlet private weak var memberRoleLabel: UILabel!
    @IBOutlet private weak var memberRoleView: UIView!
    
    let placeholderImage = UIImage(named: "img_avatar_user_empty")
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.memberRoleView.isHidden = true
        self.memberRoleView.corner = self.memberRoleView.frame.size.height / 2
        self.memberAvatar.corner = self.memberAvatar.frame.size.width / 2
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.memberAvatar.corner = memberAvatar.frame.size.width / 2
    }
    
    override func willMove(toSuperview newSuperview: UIView?) {
        self.layoutIfNeeded()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.memberAvatar.cancelRequestImage()
        self.memberAvatar.image = placeholderImage
    }
    
    var roleType: UserRoleType = .Member {
        didSet {
            switch self.roleType {
            case .Admin:
                self.memberRoleView.isHidden = false
                self.memberRoleView.backgroundColor = UIColor.ct.redKeyColor
                self.memberRoleLabel.text = NSLocalizedString("admin_role_text", comment: "")
            case .Author:
                self.memberRoleView.isHidden = false
                self.memberRoleView.backgroundColor = UIColor.ct.okAlertButtonColor
                self.memberRoleLabel.text = NSLocalizedString("author_text", comment: "")
            case .Member:
                self.memberRoleView.isHidden = true
                self.memberRoleLabel.text = "\t"
            }
        }
    }
    
    var user: User? {
        didSet {
            guard let user = user else {
                self.memberAvatar.image = nil
                self.memberNameLabel.text = "\t"
                self.memberRoleView.isHidden = true
                return
            }
            
            self.memberNameLabel.text = user.name
            self.memberAvatar.loadImageFromURL(user.avatarUrl, placeholder: placeholderImage)
            
            if user.isAdmin {
                self.roleType = .Admin
            }
            else {
                self.roleType = .Member
            }
            if user.isOwner {
                self.roleType = .Author
            }
        }
    }
}
