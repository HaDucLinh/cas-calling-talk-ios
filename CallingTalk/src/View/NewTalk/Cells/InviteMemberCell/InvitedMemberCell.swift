//
//  InvitedMemberCell.swift
//  CallingTalk
//
//  Created by Vu Tran on 2/15/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit
protocol InvitedMemberCellDelegate: NSObjectProtocol {
    func removeMember(user: User)
}
class InvitedMemberCell: UITableViewHeaderFooterView {
    @IBOutlet private weak var collectionView: UICollectionView!
    @IBOutlet private weak var invitedMemberLabel: UILabel!
    @IBOutlet private weak var selectedMemberLabel: UILabel!
    weak var delegate: InvitedMemberCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        invitedMemberLabel.textColor = UIColor.ct.grayInvitedTextColor
        invitedMemberLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .bold, size: 12)
        
        selectedMemberLabel.textColor = UIColor.ct.grayInvitedTextColor
        selectedMemberLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .bold, size: 12)
        selectedMemberLabel.text = NSLocalizedString("select_member_text", comment: "")
        
        collectionView.register(TalkMemberCell.self)
        collectionView.dataSource = self
        collectionView.delegate = self
    }
    
    var invitedMember: [User] = [] {
        didSet {
            invitedMemberLabel.text = NSLocalizedString("member_to_invite_text", comment: "") + " (\(invitedMember.count)/30)"
            collectionView.reloadData()
        }
    }
}
extension InvitedMemberCell: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return invitedMember.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeue(TalkMemberCell.self, forIndexPath: indexPath)
        cell.user = invitedMember[indexPath.row]
        cell.delegate = self
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let widthCell: CGFloat = 70
        return CGSize(width: widthCell, height: widthCell * 1.2)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
}
extension InvitedMemberCell: TalkMemberCellDelegate {
    func willDeleteMember(user: User) {
        if let index = invitedMember.index(where: {$0.id == user.id}) {
            invitedMember.remove(at: index)
            delegate?.removeMember(user: user)
        }
    }
}
