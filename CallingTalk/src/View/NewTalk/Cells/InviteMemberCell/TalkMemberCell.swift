//
//  TalkMemberCell.swift
//  CallingTalk
//
//  Created by Vu Tran on 2/15/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit
protocol TalkMemberCellDelegate: NSObjectProtocol {
    func willDeleteMember(user: User)
}
class TalkMemberCell: UICollectionViewCell {

    @IBOutlet private weak var memberAvatar: UIImageView!
    @IBOutlet private weak var memberNameLabel: UILabel!
    
    weak var delegate: TalkMemberCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        memberAvatar.corner = memberAvatar.frame.size.width / 2
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        memberAvatar.corner = memberAvatar.frame.size.width / 2
        layoutIfNeeded()
    }
    
    override func willMove(toSuperview newSuperview: UIView?) {
        layoutIfNeeded()
    }
    
    var user: User? {
        didSet {
            guard let user = user else {
                memberAvatar.image = nil
                memberNameLabel.text = ""
                return
            }
            memberAvatar.loadImageFromURL(user.avatarUrl, placeholder: UIImage(named: "img_avatar_user_empty"))
            memberNameLabel.text = user.name
        }
    }
    
    @IBAction private func deleteMember(sender: UIButton) {
        guard let user = user else { return }
        delegate?.willDeleteMember(user: user)
    }

}
