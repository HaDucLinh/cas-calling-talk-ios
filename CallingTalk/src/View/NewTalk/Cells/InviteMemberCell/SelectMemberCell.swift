//
//  SelectMemberCell.swift
//  CallingTalk
//
//  Created by Vu Tran on 2/15/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit

class SelectMemberCell: UITableViewCell {
    
    @IBOutlet private weak var avatarImageView: UIImageView!
    @IBOutlet private weak var memberRoleLabel: UILabel!
    @IBOutlet private weak var memberRoleView: UIView!
    @IBOutlet private weak var nicknameLabel: UILabel!
    @IBOutlet private weak var aboutLabel: UILabel!
    @IBOutlet weak var checkImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        self.memberRoleView.isHidden = true
        self.memberRoleView.corner = self.memberRoleView.frame.size.height / 2
        self.avatarImageView.corner = avatarImageView.frame.size.width / 2
        self.nicknameLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .regular, size: 15)
        self.aboutLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .bold, size: 11)
        self.aboutLabel.textColor = UIColor.ct.aboutMemberTextColor
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.avatarImageView.cancelRequestImage()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.avatarImageView.corner = avatarImageView.frame.size.width / 2
    }
    
    override func willMove(toSuperview newSuperview: UIView?) {
        super.willMove(toSuperview: newSuperview)
        self.avatarImageView.corner = avatarImageView.frame.size.width / 2
        self.layoutIfNeeded()
    }
    
    fileprivate var roleType: UserRoleType = .Member {
        didSet {
            switch self.roleType {
            case .Admin:
                self.memberRoleView.isHidden = false
                self.memberRoleView.backgroundColor = UIColor.ct.redKeyColor
                self.memberRoleLabel.text = NSLocalizedString("admin_role_text", comment: "")
            default:
                self.memberRoleView.isHidden = true
                self.memberRoleLabel.text = " "
            }
        }
    }
    
    var isMemberSelected = false {
        didSet {
            self.changeButtonState()
        }
    }
    
    var member: User? {
        didSet {
            self.avatarImageView.image = UIImage(named: "img_avatar_user_empty")
            guard let member = member else {
                self.nicknameLabel.text = ""
                self.aboutLabel.text = ""
                return
            }
            self.avatarImageView.loadImageFromURL(member.avatarUrl, placeholder: UIImage(named: "img_avatar_user_empty"))
            self.nicknameLabel.text = member.name
            self.updateOrganizations(Array(member.organizations))
            self.roleType = member.isAdmin ? .Admin : .Member
        }
    }
    
    fileprivate func changeButtonState() {
        let image = UIImage(named: self.isMemberSelected == false ? "ic-NonSelected" : "ic-Selected")
        self.checkImageView.image = image
    }
    
    fileprivate func updateOrganizations(_ organizations: [Organization]) {
        guard organizations.count > 0 else {
            self.aboutLabel.text = ""
            return
        }
        
        let organizationsString = organizations.map({ $0.name }).joined(separator: ", ")
        let title = NSLocalizedString("member_for_organizations_label_text", comment: "")
        
        let attributedString = NSMutableAttributedString(string: "\(title)\(organizationsString)", attributes: [
            .font: UIFont.ct.font(.NotoSansCJKjp, type: .regular, size: 11.0),
            .foregroundColor: UIColor.ct.aboutMemberTextColor,
            .kern: 0.0
            ])
        attributedString.addAttribute(.font,
                                      value: UIFont.ct.font(.NotoSansCJKjp, type: .bold, size: 11.0),
                                      range: NSRange(location: 0, length: title.count))
        
        self.aboutLabel.attributedText = attributedString
    }
    
}
