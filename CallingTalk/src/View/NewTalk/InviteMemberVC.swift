//
//  InviteMemberVC.swift
//  CallingTalk
//
//  Created by Vu Tran on 2/15/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit
import Swinject
protocol InviteMemberVCDelegate: NSObjectProtocol {
    func invitedMembers(_ members: [User], in inviteMemberVC: InviteMemberVC)
}
class InviteMemberVC: BaseViewController {
    var presenter = InviteMemberPresenter()
    
    @IBOutlet weak var inviteShortFormView: UIView!
    @IBOutlet private weak var searchBar: UISearchBar!
    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var collectionView: UICollectionView!
    @IBOutlet private weak var invitedMemberLabel: UILabel!
    @IBOutlet private weak var selectedMemberLabel: UILabel!
    @IBOutlet private weak var inviteButton: UIButton!
    @IBOutlet weak var inviteButtonShortform: UIButton!
    weak var delegate: InviteMemberVCDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpUI()
        self.presenter.attachView(self)
        self.presenter.fetchData()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    func setUpStyleForBuild() {
        //TODO: this is 2 difference style of button, we can't just modify the value
        //So make hard-code right here!! we need to fix it later if has more than this place
        let systemTheme = Container.default.resolve(SystemThemeable.self)
        var iconDisplayedLeft = "ic-close"
        var tintColorBarButtonItem: UIColor?
        switch systemTheme!.getCurrentMode() {
        case ThemeMode.normal:
            self.navigationController?.navigationBar.barTintColor = .white
            self.navigationController?.navigationBar.titleTextAttributes = [
                .font: UIFont.ct.font(.NotoSansCJKjp, type: .bold, size: 16.0),
                .foregroundColor: UIColor.ct.redKeyColor
            ]
            self.navigationController?.navigationBar.isTranslucent = true
            self.navigationController?.navigationBar.shadowImage = UIImage()
            self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
            inviteButtonShortform.isHidden = true
            inviteShortFormView.isHidden = true
            tintColorBarButtonItem = UIColor.ct.redKeyColor
        case ThemeMode.omiseno:
            self.navigationController?.navigationBar.titleTextAttributes = [
                .font: UIFont.ct.font(.NotoSansCJKjp, type: .bold, size: 16.0),
                .foregroundColor: UIColor.ct.headerText
            ]
            self.navigationController?.navigationBar.barTintColor = UIColor.ct.mainBackgroundColor
            inviteButton.isHidden = true
            inviteButtonShortform.setTitleColor(UIColor.ct.headerText, for: .normal)
            inviteButtonShortform.layer.cornerRadius = 10
            inviteButtonShortform.clipsToBounds = true
            iconDisplayedLeft = "ic_back"
            
            let inviteButtonAttributedString = NSAttributedString(string: NSLocalizedString("invite_member_text", comment: ""),
                                                                  attributes: [
                                                                    .foregroundColor: UIColor.black,
                                                                    .font: UIFont.ct.font(.NotoSansCJKjp, type: .bold, size: 16)])
            self.inviteButtonShortform.setAttributedTitle(inviteButtonAttributedString, for: .normal)
            
            break
        }
        let settings = CustomButtonBarSetting(imageName: iconDisplayedLeft,
                                              tintColor: tintColorBarButtonItem,
                                              style: .plain,
                                              action: #selector(close),
                                              target: self,
                                              possition: .left)
        _ = CustomUIItemFactory().createAndLoadButtonBar(withSetting: settings)
    }
    private func setUpUI() {
        self.setUpStyleForBuild()
        
        self.searchBar.barTintColor = UIColor.ct.searchBarBackgroundColor
        self.searchBar.frame = CGRect(x: 0, y: 1, width: GlobalVariable.screenWidth, height: 59)
        self.searchBar.delegate = self
        self.searchBar.returnKeyType = .done
        
        self.invitedMemberLabel.textColor = UIColor.ct.grayInvitedTextColor
        self.invitedMemberLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .bold, size: 12)
        
        self.selectedMemberLabel.textColor = UIColor.ct.grayInvitedTextColor
        self.selectedMemberLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .bold, size: 12)
        
        self.collectionView.register(TalkMemberCell.self)
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        
        self.tableView.backgroundColor = UIColor.white.withAlphaComponent(0.0)
        self.tableView.estimatedRowHeight = UITableView.automaticDimension
        self.tableView.register(InvitedMemberCell.self)
        self.tableView.register(SelectMemberCell.self)
        self.tableView.tableFooterView = UIView(frame: CGRect.zero)
        self.tableView.dataSource = self
        self.tableView.delegate = self
    }
    
    override func localizeString() {
        self.navigationItem.title = NSLocalizedString("invite_member_text", comment: "")
        self.searchBar.customWithPlaceholder(NSLocalizedString("invite_search_placeholder_text", comment: ""))
        self.selectedMemberLabel.text = NSLocalizedString("select_member_text", comment: "")
        

        let inviteButtonAttributedString = NSAttributedString(string: NSLocalizedString("invite_member_text", comment: ""),
                                                              attributes: [
                                                                .foregroundColor: UIColor.white,
                                                                .font: UIFont.ct.font(.NotoSansCJKjp, type: .bold, size: 16)])
        self.inviteButton.setAttributedTitle(inviteButtonAttributedString, for: .normal)
        self.inviteButtonShortform.setAttributedTitle(inviteButtonAttributedString, for: .normal)
    }
    
    @objc func willSearchMembers() {
        self.presenter.searchUsers(text: self.searchBar.text)
    }
    
    // MARK: Actions
    @objc func close() {
        dismiss(animated: true, completion: nil)
    }
    @IBAction func shortButtonInviteMembers(_ sender: Any) {
        self.presenter.inviteMembers()
    }
    @IBAction private func inviteMember(sender: UIButton) {
        self.presenter.inviteMembers()
    }
}
extension InviteMemberVC: InviteMemberView {
    func enableInviteButton(_ isEnable: Bool) {
        self.inviteButton.isEnabled = isEnable
        self.inviteButton.backgroundColor = isEnable ? UIColor.ct.mainBackgroundColor : UIColor.ct.redDisableButton
        inviteButtonShortform.isEnabled = isEnable
        inviteButtonShortform.backgroundColor = isEnable ? UIColor.ct.mainBackgroundColor : UIColor.ct.redDisableButton
    }
    
    func hideSelectedMemberLabel(_ isHidden: Bool) {
        self.selectedMemberLabel.isHidden = isHidden
    }
    
    func updateNumberOfSelectedUser(_ number: Int) {
        self.invitedMemberLabel.text = NSLocalizedString("member_to_invite_text", comment: "") + " (\(number))"
    }
    
    func didSelectedMembers(_ members: [User]) {
        self.delegate?.invitedMembers(members, in: self)
    }
    
    func insertRowsToTableView(at indexPaths: [IndexPath]) {
        self.tableView.performBatchUpdates({ [weak self] in
            guard let `self` = self else { return }
            self.tableView.setContentOffset(self.tableView.contentOffset, animated: false)
            self.tableView.insertRows(at: indexPaths, with: .fade)
        }, completion: nil)
    }
    
    func refreshTableView() {
        self.tableView.reloadData()
    }
    
    func refreshCollectionView() {
        self.collectionView.reloadData()
    }
}
// MARK: UICollectionView DataSource & Delegate
extension InviteMemberVC: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.presenter.numberOfSelectedUsers(inSection: section)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeue(TalkMemberCell.self, forIndexPath: indexPath)
        cell.delegate = self
        self.presenter.configure(cell, at: indexPath)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let widthCell: CGFloat = 70
        return CGSize(width: widthCell, height: widthCell * 1.2)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
}
// MARK: UITableView DataSource & Delegate
extension InviteMemberVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.presenter.numberOfUsers(inSection: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeue(SelectMemberCell.self)
        cell.selectionStyle = .none
        self.presenter.configure(cell, at: indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var users: [User] = presenter.isSearching ? presenter.searchedMembers : presenter.allMembers
        let user = users[indexPath.row]
        let cell = tableView.cellForRow(at: indexPath) as! SelectMemberCell
        
        cell.isMemberSelected = !cell.isMemberSelected
        if cell.isMemberSelected {
            self.presenter.selectUser(user)
        } else {
            self.presenter.unselectUser(user)
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if self.presenter.isSearching {
            if indexPath.row == self.presenter.searchedMembers.count - 1 {
                self.presenter.fetchData(self.searchBar.text)
            }
        }
        else {
            if indexPath.row == self.presenter.allMembers.count - 1 {
                self.presenter.fetchData()
            }
        }
    }
}

// MARK: TalkMemberCell Delegate
extension InviteMemberVC: TalkMemberCellDelegate {
    func willDeleteMember(user: User) {
        self.presenter.removeSelectedUser(user)
    }
}

extension InviteMemberVC: UISearchBarDelegate {
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        if searchBar.text == "" || searchBar.text == nil {
            self.presenter.isSearching = false
            self.refreshTableView()
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        self.presenter.isSearching = true
        self.refreshTableView()
        return true
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.willSearchMembers), object: nil)
        self.perform(#selector(self.willSearchMembers), with: nil, afterDelay: 0.3)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
    }
}
