//
//  ListOrganizationViewController.swift
//  CallingTalk
//
//  Created by Toof on 4/5/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit
import Swinject

protocol ListOrganizationVCDelegate: class {
    func didSelectOrganization(_ organization: Organization, in listOrganizationVC: ListOrganizationVC)
}

class ListOrganizationVC: BaseViewController {
    var presenter: ListOrganizationPresenter!
    
    @IBOutlet weak var darkView: UIView!
    @IBOutlet weak var containView: UIView!
    @IBOutlet weak var selectTitleLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var blurView: GradientView!
    @IBOutlet weak var selectButton: UIButton!
    weak var delegate: ListOrganizationVCDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupSubviews()
    }
    
    fileprivate func setupSubviews() {
        self.view.backgroundColor = UIColor.white.withAlphaComponent(0.0)
        
        self.containView.layer.cornerRadius = 20.0
        self.containView.layer.shadowColor = UIColor.black.withAlphaComponent(0.2).cgColor
        self.containView.layer.shadowOffset = CGSize.zero
        self.containView.layer.shadowRadius = 10.0
        self.containView.layer.shadowOpacity = 1.0
        
        self.tableView.backgroundColor = UIColor.white.withAlphaComponent(0.0)
        self.tableView.estimatedRowHeight = UITableView.automaticDimension
        self.tableView.separatorColor = UIColor.white.withAlphaComponent(0.0)
        self.tableView.register(OrganizationCell.self)
        self.tableView.tableFooterView = UIView(frame: CGRect.zero)
        self.tableView.dataSource = self
        self.tableView.delegate = self
        
        // Setup blur view
        self.blurView.gradientLayer.colors = [
            UIColor.white.withAlphaComponent(0.0).cgColor,
            UIColor.white.cgColor
        ]
        self.blurView.gradientLayer.gradient = GradientPoint.topBottom.draw()
        
        self.selectTitleLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .bold, size: 18.0)
        self.selectButton.titleLabel?.font = UIFont.ct.font(.NotoSansCJKjp, type: .bold, size: 16.0)
        self.selectButton.layer.cornerRadius = 10.0
        self.selectButton.addTarget(self, action: #selector(selectButtonPressed(_:)), for: .touchUpInside)
        self.setUpStyleForBuild()
        
        let tapGesture: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleTapGesture(_:)))
        self.darkView.addGestureRecognizer(tapGesture)
    }
    
    override func localizeString() {
        self.selectTitleLabel.text = NSLocalizedString("select_the_organization_label_text", comment: "")
        self.selectButton.setTitle(NSLocalizedString("select_the_organization_button_title", comment: ""), for: .normal)
    }
    
    fileprivate func setUpStyleForBuild() {
        let systemTheme = Container.default.resolve(SystemThemeable.self)
        switch systemTheme!.getCurrentMode() {
        case ThemeMode.normal:
            self.selectButton.setTitleColor(.white, for: .normal)
        case ThemeMode.omiseno:
            self.selectButton.setTitleColor(.black, for: .normal)
        }
        self.selectButton.backgroundColor = UIColor.ct.mainBackgroundColor
    }
}

// MARK: Handel Events

extension ListOrganizationVC {
    
    @objc
    fileprivate
    func selectButtonPressed(_ sender: UIButton) {
        if let organizationSelected = self.presenter.organizationSelected {
            self.delegate?.didSelectOrganization(organizationSelected, in: self)
        }
        else {
            self.showErrorAlertWithError(NSLocalizedString("Please_select_organize", comment: ""), completion: nil)
        }
    }
    
    @objc
    fileprivate
    func handleTapGesture(_ sender: UITapGestureRecognizer) {
        self.dismiss(animated: true, completion: nil)
    }
    
}

// MARK: - UITableViewDataSource

extension ListOrganizationVC: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.presenter.numberOfRows(inSection: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeue(OrganizationCell.self)
        self.presenter.configure(cell, at: indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 64.0
    }
    
}

// MARK: - UITableViewDelegate

extension ListOrganizationVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.presenter.selectOrganization(at: indexPath)
    }
    
}

// MARK: - ListOrganizationView

extension ListOrganizationVC: ListOrganizationView {
    
    func refresh() {
        self.tableView.reloadData()
    }
    
}
