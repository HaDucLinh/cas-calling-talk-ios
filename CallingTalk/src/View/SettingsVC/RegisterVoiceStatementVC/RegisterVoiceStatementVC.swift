//
//  RegisterSpeechSentenceVC.swift
//  CallingTalk
//
//  Created by Vu Tran on 3/4/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit
import DropDown
protocol RegisterVoiceStatementVCDelegate: NSObjectProtocol {
    func registerdVoiceSentence(voice: Voice)
}
class RegisterVoiceStatementVC: BaseViewController {
    let presenter = RegisterVoiceStatementPresenter()
    @IBOutlet private weak var voiceTextView: UITextView!
    @IBOutlet private weak var voiceTextLabel: UILabel!
    @IBOutlet private weak var selectVoiceView: UIView!
    @IBOutlet private weak var organizeRegisterdLabel: UILabel!
    @IBOutlet private weak var selectedOrganizeLabel: UILabel!
    @IBOutlet private weak var signupButton: UIButton!
    @IBOutlet private weak var organizeTagListView: TagListView!
    @IBOutlet private weak var tagViewHeightConstraint: NSLayoutConstraint!
    
    weak var delegate: RegisterVoiceStatementVCDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.attachView(self)
        setUpUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.setupNavigationBarWithColor(.appTheme)
    }
    
    private func setUpUI() {
        title = NSLocalizedString("Register_Speech_Sentence_title", comment: "")
        let settings = CustomButtonBarSetting(imageName: "ic_back",
                                              style: .plain,
                                              action: #selector(backButtonPressed(_:)),
                                              target: self,
                                              possition: .left)
        _ = CustomUIItemFactory().createAndLoadButtonBar(withSetting: settings)
        
        voiceTextLabel.text = NSLocalizedString("Voice_text", comment: "")
        voiceTextView.corner = 4
        voiceTextView.border(color: UIColor.ct.borderSearchTextFieldColor, width: 1)
        voiceTextView.delegate = self
        voiceTextView.text = NSLocalizedString("Voice_text_placeHolder", comment: "")
        voiceTextView.textColor = UIColor.ct.grayPlaceHolderTextColor
        voiceTextView.font = UIFont.ct.font(.NotoSansCJKjp, type: .regular, size: 15)
        voiceTextView.textContainerInset = UIEdgeInsets.init(top: 7, left: 21, bottom: 7, right: 11)
        voiceTextLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .bold, size: 14)
        
        selectVoiceView.corner = 4
        selectVoiceView.border(color: UIColor.ct.borderSearchTextFieldColor, width: 1)
        organizeRegisterdLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .bold, size: 14)
        organizeRegisterdLabel.text = NSLocalizedString("Organization_be_registered_text",
                                                        comment: "")
        selectedOrganizeLabel.text = NSLocalizedString("Please_select_organize", comment: "")
        selectedOrganizeLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .regular, size: 15)
        
        signupButton.corner = 10
        signupButton.titleLabel?.font = UIFont.ct.font(.NotoSansCJKjp, type: .bold, size: 16)
        signupButton.setTitle(NSLocalizedString("Register_button_text", comment: ""), for: .normal)
        signupButton.backgroundColor = UIColor.ct.redKeyColor
        signupButton.setTitleColor(UIColor.ct.headerText, for: .normal)
        setUpdateButtonState(isEnable: false)
        
        organizeTagListView.textFont = UIFont.ct.font(.NotoSansCJKjp, type: .regular, size: 13)
        organizeTagListView.alignment = .left
        organizeTagListView.tagHeight = 36
        organizeTagListView.removeButtonIconSize = 22
        organizeTagListView.borderColor = UIColor.ct.boderTagViewColor
        organizeTagListView.delegate = self
    }
    
    fileprivate func setUpListOrganization() {
//        if voice.organizations.isEmpty {
//            tagViewHeightConstraint.constant = 0
//            return
//        }
        
        let dropDown = DropDown()
        dropDown.customCellConfiguration = { [weak self] (index, string, cell) in
            guard let _ = self else { return }
            cell.optionLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .regular, size: 14)
        }
        dropDown.anchorView = organizeTagListView
        presenter.organizations.forEach { (organization) in
            dropDown.dataSource.append(organization.name)
        }
        dropDown.width = organizeTagListView.bounds.width
        
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            let organization = self.presenter.organizations[index]
            self.presenter.voice.organizations.append(organization)
            self.presenter.setRegisterButtonStatus()
            
            let tagView = self.organizeTagListView.addTag(organization.name)
            tagView.tagBackgroundColor = UIColor.ct.tagViewBackgroundColor
            self.organizeTagListView.removeIconImage = UIImage(named: "ic-tag-remove")
            tagView.onTap = { tagView in
                print("Don’t tap me!")
            }
        }
        dropDown.show()
    }
    
    // MARK: Actions
    @objc fileprivate func backButtonPressed(_ sender: UIBarButtonItem) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction private func addOrganization(sender: UIButton) {
        presenter.getListOrganization()
    }
    
    @IBAction private func registerVocie(sender: UIButton) {
        presenter.registerVoiceStatement()
    }
}
// MARK: RegisterSpeechView
extension RegisterVoiceStatementVC: RegisterVoiceView {
    func setUpdateButtonState(isEnable: Bool) {
        signupButton.isEnabled = isEnable
        signupButton.backgroundColor = isEnable ? UIColor.ct.mainBackgroundColor : UIColor.ct.redDisableButton
        let color = isEnable ? UIColor.ct.headerText : UIColor.ct.headerText.withAlphaComponent(0.47)
        signupButton.setTitleColor(color, for: .normal)
    }
    
    func updateListOrganization() {
        setUpListOrganization()
    }
    
    func createVoiceSuccessfull(voice: Voice) {
        delegate?.registerdVoiceSentence(voice: voice)
        navigationController?.popViewController(animated: true)
    }
}
// MARK: UITextView Delegate
extension RegisterVoiceStatementVC: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.ct.grayPlaceHolderTextColor {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = NSLocalizedString("Voice_text_placeHolder", comment: "")
            textView.textColor = UIColor.ct.grayPlaceHolderTextColor
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
        presenter.voiceTextChange(text: textView.text)
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        guard let textOld = textView.text else { return true }
        let count = textOld.count + text.count - range.length
        return count <= 50
    }
}
// MARK: TagListView Delegate
extension RegisterVoiceStatementVC: TagListViewDelegate {
    func tagPressed(_ title: String, tagView: TagView, sender: TagListView) -> Void {
        print("tagPressed: ", title)
    }
    
    func tagRemoveButtonPressed(_ title: String, tagView: TagView, sender: TagListView) -> Void {
        if let index = presenter.voice.organizations.index(where: {$0.name == tagView.titleLabel?.text}) {
            presenter.voice.organizations.remove(at: index)
            organizeTagListView.removeTag(title)
            presenter.setRegisterButtonStatus()
        }
    }
}
