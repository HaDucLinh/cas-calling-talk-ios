//
//  MemberListCell.swift
//  CallingTalk
//
//  Created by Vu Tran on 2/28/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit

class MemberListCell: UITableViewCell {
    @IBOutlet private weak var avatarImageView: UIImageView!
    @IBOutlet private weak var nicknameLabel: UILabel!
    @IBOutlet private weak var aboutLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.selectionStyle = .none
        
        avatarImageView.corner = avatarImageView.frame.size.width / 2
        nicknameLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .regular, size: 15)
        aboutLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .bold, size: 11)
        aboutLabel.textColor = UIColor.ct.aboutMemberTextColor
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.avatarImageView.corner = avatarImageView.frame.size.width / 2
    }
    
    override func willMove(toSuperview newSuperview: UIView?) {
        super.willMove(toSuperview: newSuperview)
        self.avatarImageView.corner = avatarImageView.frame.size.width / 2
        self.layoutIfNeeded()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
}
