//
//  MemberListVC.swift
//  CallingTalk
//
//  Created by Vu Tran on 2/28/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit

class MemberListVC: BaseViewController {
    @IBOutlet private weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.setupNavigationBarWithColor(.appTheme)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.navigationBar.setupNavigationBarWithColor()
    }
    
    // MARK: Alert for Actions
    private func showDeleteMemberAlert(member: User) { // CT-C-5-4
        let confirmAlertView = ConfirmDeleteTalkView.loadFromNib()
        confirmAlertView.isDeleteMember = true
        confirmAlertView.backgroundImage = UIImage(named: "bg_alert")
        confirmAlertView.organize = member.organizations.first?.name
        confirmAlertView.groupName = member.name //'groupName': also using for leave Talkgroup
        confirmAlertView.title = ""
        confirmAlertView.message = NSLocalizedString("delete_member_confirm_text", comment: "")
        confirmAlertView.confirmButtonTitle = NSLocalizedString("delete_button_alert_text", comment: "")
        confirmAlertView.show {
            // API
        }
    }
    
    private func showBandMemberConfirmAlert(member: User) { // CT-C-5-4-1
        let bandText = NSLocalizedString("band_infor_text", comment: "")
        let confirmAlertView = ConfirmDeleteTalkView.loadFromNib()
        confirmAlertView.isDeleteMember = true
        confirmAlertView.backgroundImage = UIImage(named: "bg_alert")
        confirmAlertView.organize = member.organizations.first?.name
        confirmAlertView.groupName = member.name //'groupName': also using for leave Talkgroup
        confirmAlertView.title = ""
        confirmAlertView.message = NSLocalizedString("band_member_message", comment: "")
        confirmAlertView.moreInfor = String(format: bandText, "Organize-Name")
        confirmAlertView.confirmButtonTitle = NSLocalizedString("band_button_title", comment: "")
        confirmAlertView.show {
            // API
        }
    }
    
    private func showReportMemberConfirmView(member: User) { // CT-C-5-4-2
        let confirmAlertView = ReportMemberConfirmView.loadFromNib()
        confirmAlertView.backgroundImage = UIImage(named: "bg_alert")
        confirmAlertView.organize = member.organizations.first?.name
        confirmAlertView.memberName = member.name
        confirmAlertView.show {
            // API
        }
    }
    
    fileprivate func setUpUI() {
        title = NSLocalizedString("Member_list_title", comment: "")
        let settings = CustomButtonBarSetting(imageName: "ic_back",
                                              style: .plain,
                                              action: #selector(backButtonPressed(_:)),
                                              target: self,
                                              possition: .left)
        _ = CustomUIItemFactory().createAndLoadButtonBar(withSetting: settings)
        
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        tableView.register(MemberListCell.self)
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    @objc fileprivate func backButtonPressed(_ sender: UIBarButtonItem) {
        navigationController?.popViewController(animated: true)
    }
}
extension MemberListVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeue(MemberListCell.self)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let memberView = BlockMemberAlertView.loadFromNib()
        let member = User()
        memberView.show(member: member, talkGroup: nil) { (action) in
            switch action {
            case .RemoveMember:
                self.showDeleteMemberAlert(member: member)
            case .Banish:
                self.showBandMemberConfirmAlert(member: member)
            case .Report:
                self.showReportMemberConfirmView(member: member)
            case .UnBlock:
                // API for Unlock User
                let toastView = ToastView.loadViewFromNib()
                toastView.show(text: NSLocalizedString("un_block_message", comment: ""))
            case .Block:
                // API for Lock User
                // success -->
                let toastView = ToastView.loadViewFromNib()
                toastView.show(text: NSLocalizedString("blocked_message", comment: ""))
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
}
