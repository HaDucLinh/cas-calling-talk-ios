//
//  SettingNotificationVC.swift
//  CallingTalk
//
//  Created by Vu Tran on 2/28/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit
class SettingNotificationVC: BaseViewController {
    @IBOutlet private weak var notificationLabel: UILabel!
    @IBOutlet weak var labelSwitch: LabelSwitch!

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUI()
    }
    
    private func setUpUI() {
        title = NSLocalizedString("notification_text", comment: "")
        let settings = CustomButtonBarSetting(imageName: "ic_back",
                                              style: .plain,
                                              action: #selector(backButtonPressed(_:)),
                                              target: self,
                                              possition: .left)
        _ = CustomUIItemFactory().createAndLoadButtonBar(withSetting: settings)
        
        notificationLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .bold, size: 16)
        notificationLabel.textColor = UIColor.ct.grayInvitedTextColor
        notificationLabel.text = NSLocalizedString("push_notification_text", comment: "")
        
        labelSwitch.delegate = self
        labelSwitch.circlePadding = 3
        labelSwitch.edge = 3
        labelSwitch.circleShadow = false
        labelSwitch.fullSizeTapEnabled = true
        
        // Update value
        labelSwitch.curState = (User.getProfile()?.enableNotification ?? true) ? .R : .L
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.setupNavigationBarWithColor(.appTheme)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidAppear(animated)
        navigationController?.navigationBar.setupNavigationBarWithColor()
    }
    
    @objc fileprivate func backButtonPressed(_ sender: UIBarButtonItem) {
        navigationController?.popViewController(animated: true)
    }

}
extension SettingNotificationVC: LabelSwitchDelegate {
    func switchChangToState(sender: LabelSwitch) {
        // Update Status
        self.updateStatus(sender.curState == .L)
    }
}
extension SettingNotificationVC {
    func updateStatus(_ isOn: Bool) {
        hud.show()
        API.updateNotificationState(isOn) { [weak self] (result) in
            guard let `self` = self else { return }
            switch result {
            case .success(let status):                
                guard let status = status as? StatusCode else { return }
                if status == .success {
                    API.getMyProfile(completion: { (result) in
                        DispatchQueue.main.async { [weak self] in
                            guard let `self` = self else { return }
                            hud.dismiss()
                            switch result {
                            case .success(let profileData):
                                guard let profile = profileData as? User else { return }
                                Helpers.saveProfile(profile)
                            case .failure(let error):
                                self.showErrorAlertWithError(error, completion: nil)
                            }
                        }
                    })
                }
            case .failure(let error):
                hud.dismiss()
                self.showErrorAlertWithError(error, completion: nil)
            }
        }
    }
}
