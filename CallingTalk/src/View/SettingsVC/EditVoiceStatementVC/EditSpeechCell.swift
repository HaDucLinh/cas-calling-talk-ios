//
//  EditSpeechCell.swift
//  CallingTalk
//
//  Created by Vu Tran on 3/5/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit
import DropDown

protocol EditSpeechCellDelegate: NSObjectProtocol {
    func removeTag(tagView: TagView)
    func willUpdateVoice(voice: Voice)
    func willAddOrganization(organization: Organization)
    func willDeleteVoice(voice: Voice)
}
class EditSpeechCell: UITableViewCell {
    @IBOutlet private weak var voiceTextView: UITextView!
    @IBOutlet private weak var voiceTextLabel: UILabel!
    @IBOutlet private weak var selectVoiceView: UIView!
    @IBOutlet private weak var organizeRegisterdLabel: UILabel!
    @IBOutlet private weak var selectedOrganizeLabel: UILabel!
    @IBOutlet private weak var editButton: UIButton!
    @IBOutlet private weak var organizeTagListView: TagListView!
    @IBOutlet private weak var deleteButton: UIButton!
    @IBOutlet private weak var tagViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet private weak var deleteHeightConstraint: NSLayoutConstraint!
    weak var delegate: EditSpeechCellDelegate?
    var organizations: [Organization] = []
    var currentOrganizations: [Organization] = []

    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        voiceTextLabel.text = NSLocalizedString("Voice_text", comment: "")
        voiceTextView.corner = 4
        voiceTextView.border(color: UIColor.ct.borderSearchTextFieldColor, width: 1)
        voiceTextView.delegate = self
        voiceTextView.text = NSLocalizedString("Voice_text_placeHolder", comment: "")
        voiceTextView.textColor = UIColor.ct.grayPlaceHolderTextColor
        voiceTextView.font = UIFont.ct.font(.NotoSansCJKjp, type: .regular, size: 15)
        voiceTextView.textContainerInset = UIEdgeInsets.init(top: 7, left: 21, bottom: 7, right: 11)
        voiceTextLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .bold, size: 14)
        
        selectVoiceView.corner = 4
        selectVoiceView.border(color: UIColor.ct.borderSearchTextFieldColor, width: 1)
        organizeRegisterdLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .bold, size: 14)
        organizeRegisterdLabel.text = NSLocalizedString("Organization_be_registered_text",
                                                        comment: "")
        selectedOrganizeLabel.text = NSLocalizedString("Please_select_organize", comment: "")
        selectedOrganizeLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .regular, size: 15)
        
        editButton.corner = 10
        editButton.titleLabel?.font = UIFont.ct.font(.NotoSansCJKjp, type: .bold, size: 16)
        editButton.setTitle(NSLocalizedString("Change_voice_statement", comment: ""), for: .normal)
        editButton.backgroundColor = UIColor.ct.redKeyColor
        
        deleteButton.setUnderlineTitle(NSLocalizedString("Deleted_voice_text", comment: ""), font: UIFont.ct.font(.NotoSansCJKjp, type: .regular, size: 14), color: UIColor.ct.boderTagViewColor)
        setUpdateButtonState(isEnable: false)
        
        organizeTagListView.textFont = UIFont.ct.font(.NotoSansCJKjp, type: .regular, size: 13)
        organizeTagListView.alignment = .left
        organizeTagListView.tagHeight = 36
        organizeTagListView.removeButtonIconSize = 22
        organizeTagListView.borderColor = UIColor.ct.boderTagViewColor
        organizeTagListView.delegate = self
    }
    
    var isRegister = false {
        didSet {
            if isRegister {
                deleteHeightConstraint.constant = 0
                editButton.setTitle(NSLocalizedString("Register_button_text", comment: ""), for: .normal)
            } else {
                deleteHeightConstraint.constant = 30
            }
            self.contentView.updateConstraints()
        }
    }
    var voice: Voice? {
        didSet {
            organizeTagListView.removeAllTags()
            guard let voice = voice else {
                return
            }
            if !voice.content.isEmpty {
                voiceTextView.text = voice.content
                voiceTextView.textColor = UIColor.black
                setUpdateButtonState(isEnable: true)
            }
            voice.organizations.forEach { (org) in
                currentOrganizations.append(org)
                organizations.append(org)
            }
            
            if voice.organizations.isEmpty {
                tagViewHeightConstraint.constant = 0
                return
            }
            voice.organizations.forEach { (organization) in
                let tagView = organizeTagListView.addTag(organization.name)
                tagView.tagBackgroundColor = UIColor.ct.tagViewBackgroundColor
                tagView.onTap = { tagView in
                    print("Don’t tap me!")
                }
            }
            organizeTagListView.removeIconImage = UIImage(named: "ic-tag-remove")
        }
    }
    
    private func setUpdateButtonState(isEnable: Bool) {
        editButton.isEnabled = isEnable
        editButton.backgroundColor = isEnable ? UIColor.ct.mainBackgroundColor : UIColor.ct.redDisableButton
    }
    
    @IBAction private func addOrganization(sender: UIButton) {
        let dropDown = DropDown()
        dropDown.customCellConfiguration = { [unowned self] (index, string, cell) in
            cell.optionLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .regular, size: 14)
        }
        dropDown.anchorView = organizeTagListView
        organizations.forEach { (organization) in
            dropDown.dataSource.append(organization.name)
        }
        dropDown.width = organizeTagListView.bounds.width
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            let organization = self.organizations[index]
            self.delegate?.willAddOrganization(organization: organization)
        }
        dropDown.show()
    }
    
    @IBAction private func updateVoice(sender: UIButton) {
        guard let voice = self.voice else { return }
        voice.content = voiceTextView.text
        voice.organizations.removeAll()
        currentOrganizations.forEach { (org) in
            voice.organizations.append(org)
        }
        delegate?.willUpdateVoice(voice: voice)
    }
    
    @IBAction private func deleteVoice(sender: UIButton) {
        if let voice = self.voice {
            delegate?.willDeleteVoice(voice: voice)
        }
    }
}
extension EditSpeechCell: TagListViewDelegate {
    func tagPressed(_ title: String, tagView: TagView, sender: TagListView) -> Void {
        print("tagPressed: ", title)
    }
    
    func tagRemoveButtonPressed(_ title: String, tagView: TagView, sender: TagListView) -> Void {
        delegate?.removeTag(tagView: tagView)
    }
}
extension EditSpeechCell: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.ct.grayPlaceHolderTextColor {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = NSLocalizedString("Voice_text_placeHolder", comment: "")
            textView.textColor = UIColor.ct.grayPlaceHolderTextColor
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
        setUpdateButtonState(isEnable: textView.text.isEmpty == false)
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        guard let textOld = textView.text else { return true }
        let count = textOld.count + text.count - range.length
        return count <= 50
    }
}
