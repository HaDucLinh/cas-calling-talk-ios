//
//  EditSpeechSentenceVC.swift
//  CallingTalk
//
//  Created by Vu Tran on 3/5/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit
import DropDown

protocol EditVoiceStatementVCDelegate: NSObjectProtocol {
    func updatedVoiceStatement(voice: Voice)
    func deletedVoiceStatement(voice: Voice)
}
class EditVoiceStatementVC: BaseViewController {
    let presenter = EditVoicePresenter()
    @IBOutlet private weak var voiceTextView: UITextView!
    @IBOutlet private weak var voiceTextLabel: UILabel!
    @IBOutlet private weak var selectVoiceView: UIView!
    @IBOutlet private weak var organizeRegisterdLabel: UILabel!
    @IBOutlet private weak var selectedOrganizeLabel: UILabel!
    @IBOutlet private weak var editButton: UIButton!
    @IBOutlet private weak var deleteButton: UIButton!
    @IBOutlet private weak var organizeTagListView: TagListView!
    @IBOutlet private weak var tagViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet private weak var deleteHeightConstraint: NSLayoutConstraint!

    weak var delegate: EditVoiceStatementVCDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.attachView(self)
        setUpUI()
        setUpForVoice()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.setupNavigationBarWithColor(.appTheme)
    }

    private func setUpUI() {
        title = NSLocalizedString("Edit_voice_text_title", comment: "")
        let settings = CustomButtonBarSetting(imageName: "ic_back",
                                              style: .plain,
                                              action: #selector(backButtonPressed(_:)),
                                              target: self,
                                              possition: .left)
        _ = CustomUIItemFactory().createAndLoadButtonBar(withSetting: settings)
        
        voiceTextLabel.text = NSLocalizedString("Voice_text", comment: "")
        voiceTextView.corner = 4
        voiceTextView.border(color: UIColor.ct.borderSearchTextFieldColor, width: 1)
        voiceTextView.delegate = self
        voiceTextView.text = NSLocalizedString("Voice_text_placeHolder", comment: "")
        voiceTextView.textColor = UIColor.ct.grayPlaceHolderTextColor
        voiceTextView.font = UIFont.ct.font(.NotoSansCJKjp, type: .regular, size: 15)
        voiceTextView.textContainerInset = UIEdgeInsets.init(top: 7, left: 21, bottom: 7, right: 11)
        voiceTextLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .bold, size: 14)
        
        selectVoiceView.corner = 4
        selectVoiceView.border(color: UIColor.ct.borderSearchTextFieldColor, width: 1)
        organizeRegisterdLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .bold, size: 14)
        organizeRegisterdLabel.text = NSLocalizedString("Organization_be_registered_text",
                                                        comment: "")
        selectedOrganizeLabel.text = NSLocalizedString("Please_select_organize", comment: "")
        selectedOrganizeLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .regular, size: 15)
        
        editButton.corner = 10
        editButton.titleLabel?.font = UIFont.ct.font(.NotoSansCJKjp, type: .bold, size: 16)
        editButton.setTitle(NSLocalizedString("Change_voice_statement", comment: ""), for: .normal)
        editButton.setTitleColor(UIColor.ct.headerText, for: .normal)
        editButton.backgroundColor = UIColor.ct.mainBackgroundColor
         deleteButton.setUnderlineTitle(NSLocalizedString("Deleted_voice_text", comment: ""), font: UIFont.ct.font(.NotoSansCJKjp, type: .regular, size: 14), color: UIColor.ct.boderTagViewColor)
                
        organizeTagListView.textFont = UIFont.ct.font(.NotoSansCJKjp, type: .regular, size: 13)
        organizeTagListView.alignment = .left
        organizeTagListView.tagHeight = 36
        organizeTagListView.removeButtonIconSize = 22
        organizeTagListView.borderColor = UIColor.ct.boderTagViewColor
        organizeTagListView.delegate = self
    }

    fileprivate func setUpForVoice() {
        if presenter.voice?.content.isEmpty == false {
            voiceTextView.text = presenter.voice?.content
            voiceTextView.textColor = UIColor.black
        }
        organizeTagListView.removeAllTags()
        guard let voice = presenter.voice else { return }
        if voice.organizations.isEmpty {
            tagViewHeightConstraint.constant = 0
            return
        }
        voice.organizations.forEach { (organization) in
            self.presenter.organizationForUpdate.append(organization)
            
            let tagView = organizeTagListView.addTag(organization.name)
            tagView.tagBackgroundColor = UIColor.ct.tagViewBackgroundColor
            tagView.onTap = { tagView in
                print("Don’t tap me!")
            }
        }
        organizeTagListView.removeIconImage = UIImage(named: "ic-tag-remove")
    }
    
    fileprivate func showOrganizationView() {
        guard let _ = presenter.voice else { return }
        
        let dropDown = DropDown()
        dropDown.customCellConfiguration = { [weak self] (index, string, cell) in
            guard let _ = self else { return }
            
            cell.optionLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .regular, size: 14)
        }
        dropDown.anchorView = organizeTagListView
        presenter.organizations.forEach { (organization) in
            dropDown.dataSource.append(organization.name)
        }
        dropDown.width = organizeTagListView.bounds.width
        
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            let organization = self.presenter.organizations[index]
            if self.presenter.organizationForUpdate.filter({ $0.id == organization.id }).isEmpty {
                self.presenter.organizationForUpdate.append(organization)
                self.presenter.setRegisterButtonStatus()
                
                let tagView = self.organizeTagListView.addTag(organization.name)
                tagView.tagBackgroundColor = UIColor.ct.tagViewBackgroundColor
                self.organizeTagListView.removeIconImage = UIImage(named: "ic-tag-remove")
                tagView.onTap = { tagView in
                    print("Don’t tap me!")
                }
            }
        }
        dropDown.show()
    }
    
    // MARK: Actions

    @objc fileprivate func backButtonPressed(_ sender: UIBarButtonItem) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction private func addOrganization(sender: UIButton) {
        presenter.getListOrganization()
    }
    
    @IBAction private func update(sender: UIButton) {
        presenter.updateVoiceStatement()
    }
    
    @IBAction private func deleteVoice(sender: UIButton) {
        let deleteView = ConfirmLogoutView.loadFromNib()
        let title = NSLocalizedString("Confirm_delete_voice_text", comment: "")
        deleteView.confirmButtonTitle = NSLocalizedString("delete_button_alert_text", comment: "")
        deleteView.iconImageView.isHidden = true
        deleteView.closeButton.isHidden = true
        deleteView.show(title: title, message: "") {
            self.presenter.deleteVoiceStatement()
        }
    }
}
// MARK: EditSpeechView
extension EditVoiceStatementVC: EditVoiceView {
    func setUpdateButtonState(isEnable: Bool) {
        editButton.isEnabled = isEnable
        editButton.backgroundColor = isEnable ? UIColor.ct.mainBackgroundColor : UIColor.ct.redDisableButton
    }
    
    func updateListOrganization() {
        showOrganizationView()
    }
    
    func updateVoiceSuccessfull(voice: Voice) {
        delegate?.updatedVoiceStatement(voice: voice)
        navigationController?.popViewController(animated: true)
    }
    
    func deletedVoiceStatement(voice: Voice) {
        self.delegate?.deletedVoiceStatement(voice: voice)
        self.navigationController?.popViewController(animated: true)
    }
}
// MARK: UITextView Delegate
extension EditVoiceStatementVC: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.ct.grayPlaceHolderTextColor {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = NSLocalizedString("Voice_text_placeHolder", comment: "")
            textView.textColor = UIColor.ct.grayPlaceHolderTextColor
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
        presenter.voiceTextChange(text: textView.text)
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        guard let textOld = textView.text else { return true }
        let count = textOld.count + text.count - range.length
        return count <= 50
    }
}
// MARK: TagListView Delegate
extension EditVoiceStatementVC: TagListViewDelegate {
    func tagPressed(_ title: String, tagView: TagView, sender: TagListView) -> Void {
        print("tagPressed: ", title)
    }
    
    func tagRemoveButtonPressed(_ title: String, tagView: TagView, sender: TagListView) -> Void {
        if let index = presenter.organizationForUpdate.index(where: {$0.name == tagView.titleLabel?.text}) {
            presenter.organizationForUpdate.remove(at: index)
            organizeTagListView.removeTag(title)
            
            if presenter.organizations.isEmpty == true {
                tagViewHeightConstraint.constant = 0
            }
            presenter.setRegisterButtonStatus()
        }
    }
}
