//
//  EditProfileVC.swift
//  CallingTalk
//
//  Created by Vu Tran on 2/27/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit
import Kingfisher

protocol EditProfileDelegate: NSObjectProtocol {
    func staffUpdatedProfile(profile: User)
}

class EditProfileVC: BaseViewController {
    var editProfilePresenter = EditProfilePresenter()
    
    @IBOutlet private weak var avatarImageView: UIImageView!
    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var userNameTextView: UITextView!
    @IBOutlet private weak var textView: UIView!

    @IBOutlet private weak var updateButton: UIButton!
    @IBOutlet private weak var selectIconButton: UIButton!
    @IBOutlet private weak var textViewHeightConstraint: NSLayoutConstraint!
    
    fileprivate var selectedTalkImageID = 1
    fileprivate var defaultAvatarIndex = 1

    weak var delegate: EditProfileDelegate?
    var user: User?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        editProfilePresenter.attachView(self)
        
        setUpUI()
        setUpData()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        userNameTextView.centerVertically()
    }
    
    private func setUpUI() {
        let settings = CustomButtonBarSetting(imageName: "ic_back",
                                              style: .plain,
                                              action: #selector(backButtonPressed(_:)),
                                              target: self,
                                              possition: .left)
        _ = CustomUIItemFactory().createAndLoadButtonBar(withSetting: settings)
        
        selectIconButton.addTarget(self, action: #selector(selectIcon), for: .touchUpInside)
        avatarImageView.corner = avatarImageView.frame.size.width / 2
        avatarImageView.contentMode = .scaleAspectFit
        let tapShowIcon = UITapGestureRecognizer(target: self, action: #selector(selectIcon))
        avatarImageView.isUserInteractionEnabled = true
        avatarImageView.addGestureRecognizer(tapShowIcon)
        
        nameLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .bold, size: 14)
        nameLabel.text = NSLocalizedString("name_label_text", comment: "")
        
        userNameTextView.isScrollEnabled = false
        textView.corner = 8
        textView.border(color: UIColor.ct.borderSearchTextFieldColor, width: 1)
        userNameTextView.delegate = self
        userNameTextView.font = UIFont.ct.font(.NotoSansCJKjp, type: .regular, size: 15)
        userNameTextView.textContainerInset = UIEdgeInsets(top: 0, left: 18, bottom: 0, right: 5)

        updateButton.backgroundColor = UIColor.ct.mainBackgroundColor
        updateButton.titleLabel?.font = UIFont.ct.font(.NotoSansCJKjp, type: .bold, size: 16)
        updateButton.corner = 10
        updateButton.setTitle(NSLocalizedString("update_button_text", comment: ""), for: .normal)
        updateButton.setTitleColor(UIColor.ct.headerText, for: .normal)
    }
    
    fileprivate func setUpData() {
        guard let user = self.user else { return }
        editProfilePresenter.profile = user
        defaultAvatarIndex = user.avatarIndex
        
        userNameTextView.text = user.name
        guard let url = URL(string: user.avatarUrl) else { return }
        
        let placeHolder = UIImage(named: "img_avatar_user_empty")
        avatarImageView.kf.setImage(with: url, placeholder: placeHolder, options: nil,
                                    progressBlock: nil) { (image, _, _, _) in
            if let imageDownloaded = image {
                self.avatarImageView.image = image
                self.avatarImageView.contentMode = .scaleAspectFit
                self.editProfilePresenter.avatar = imageDownloaded.pngData()
            } else {
                self.editProfilePresenter.avatar = placeHolder!.pngData()
            }
        }
    }
    
    fileprivate func setPlaceHolderText() {
        let placeholderText = NSLocalizedString("username_placeholder_text", comment: "")
        userNameTextView.font = UIFont.ct.font(.NotoSansCJKjp, type: .regular, size: 14)
        userNameTextView.text = placeholderText
        userNameTextView.textColor = UIColor.ct.grayPlaceHolderTextColor
        userNameTextView.centerVertically()
    }
    
    // MARK: Actions
    @objc fileprivate func backButtonPressed(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func userNameTextFieldDidChange(_ textField: UITextField){
        editProfilePresenter.usernameTextChange(text: textField.text)
    }
    
    @objc fileprivate func selectIcon() {
        let selectIconVC = SelectIconVC()
        selectIconVC.oldAvatarId = selectedTalkImageID
        selectIconVC.defaultAvatarIndex = defaultAvatarIndex
        selectIconVC.delegate = self
        selectIconVC.isFromNewTalk = false
        
        selectIconVC.transitioningDelegate = self
        self.definesPresentationContext = true
        selectIconVC.view.backgroundColor = .clear
        selectIconVC.modalPresentationStyle = .overFullScreen
        present(selectIconVC, animated: true, completion: nil)
    }
    
    @IBAction private func editProfile(sender: UIButton) {
        editProfilePresenter.updateProfile(avatarIndex: self.defaultAvatarIndex)
    }
}
extension EditProfileVC: EditProfileView {
    func enableEditButton() {
        updateButton.isEnabled = true
        updateButton.backgroundColor = UIColor.ct.mainBackgroundColor
        updateButton.setTitleColor(UIColor.ct.headerText, for: .normal)
    }
    
    func disableEditButton() {
        updateButton.isEnabled = false
        updateButton.backgroundColor = UIColor.ct.redDisableButton
        updateButton.setTitleColor(UIColor.ct.headerText.withAlphaComponent(0.47), for: .normal)
    }
    
    func updateSuccessful(profile: User) {
        delegate?.staffUpdatedProfile(profile: profile)
        self.navigationController?.popViewController(animated: true)
    }
}
extension EditProfileVC: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.ct.grayPlaceHolderTextColor {
            textView.text = ""
            textView.textColor = UIColor.black
        }
    }
    
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        textView.endEditing(true)
        return true
    }
    
    func textViewDidChange(_ textView: UITextView) {
        editProfilePresenter.usernameTextChange(text: textView.text)
        
        textView.dynamicHeight(heightConstraint: textViewHeightConstraint, minHeight: 50)
        userNameTextView.centerVertically()
        self.view.updateConstraints()
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            textView.dynamicHeight(heightConstraint: textViewHeightConstraint, defaultHeight: 50)
            setPlaceHolderText()
            self.view.updateConstraints()
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        guard let textOld = textView.text else { return true }
        if(text == "\n") {
            view.endEditing(true)
            return false
        }
        let count = textOld.count + text.count - range.length
        return count <= 30
    }
}

extension EditProfileVC: SelectIconVCDelegate {
    func selectAvatar(avatar: Avatar, at index: Int) {
        selectedTalkImageID = avatar.id
        defaultAvatarIndex = index + 1
        
        if let image = UIImage(named: avatar.name) {
            avatarImageView.image = image
            editProfilePresenter.avatar = image.pngData()
        }
    }
    
    func didSelectImage(image: UIImage) {
        let vc = EditPhotoVC()
        vc.delegate = self
        vc.sourceImage = image.resizeImage()
        let navi = UINavigationController(rootViewController: vc)
        navi.navigationBar.setupNavigationBarWithColor(.white)
        present(navi, animated: true, completion: nil)
    }
}
extension EditProfileVC: EditPhotoVCDelegate {
    func didCropImage(croppedImage: UIImage) {
        avatarImageView.image = croppedImage
        defaultAvatarIndex = 0

        do {
            let imageData = try croppedImage.compressToDataSize()
            editProfilePresenter.avatar = imageData
        }
        catch {
            self.showErrorAlertWithError(error, completion: nil)
        }
    }
}
// MARK: Transitioning Delegate
extension EditProfileVC: UIViewControllerTransitioningDelegate {
    
    func animationController(forPresented presented: UIViewController,
                             presenting: UIViewController,
                             source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return FadePushAnimator(type: .modal)
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        var interactionController: UIPercentDrivenInteractiveTransition?
        if let viewController = dismissed as? SelectIconVC {
            interactionController = viewController.interactionController
        }
        return FadePopAnimator(type: .modal, interactionController: interactionController)
    }
    
    func interactionControllerForDismissal(using animator: UIViewControllerAnimatedTransitioning)
        -> UIViewControllerInteractiveTransitioning? {
            guard let animator = animator as? FadePopAnimator,
                let interactionController = animator.interactionController as? LeftEdgeInteractionController, interactionController.inProgress else {
                    return nil
            }
            return interactionController
    }
}
