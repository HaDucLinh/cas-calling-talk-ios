//
//  AdminEditProfileVC.swift
//  CallingTalk
//
//  Created by Vu Tran on 2/27/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit
import Kingfisher

protocol AdminEditProfileVCDelagate: NSObjectProtocol {
    func adminUpdatedProfile(profile: User)
}

class AdminEditProfileVC: BaseViewController {
    var adminEditProfilePresenter = AdminEditProfilePresenter()
    
    @IBOutlet private weak var avatarImageView: UIImageView!
    @IBOutlet private weak var nameLabel: UILabel!
    
    @IBOutlet private weak var userNameTextView: UITextView!
    @IBOutlet private weak var textView: UIView!
    
    @IBOutlet private weak var emailIconImageView: UIImageView!
    @IBOutlet private weak var emailLabel: UILabel!
    @IBOutlet private weak var emailTextField: LeftPaddedTextField!

    @IBOutlet private weak var updateButton: UIButton!
    @IBOutlet private weak var selectIconButton: UIButton!
    @IBOutlet private weak var textViewHeightConstraint: NSLayoutConstraint!
    fileprivate var selectedTalkImageID = 1
    fileprivate var defaultAvatarIndex = 1

    weak var delegate: AdminEditProfileVCDelagate?
    var user: User?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        adminEditProfilePresenter.attachView(self)
        setUpUI()
        setUpData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        userNameTextView.centerVertically()
    }
    
    private func setUpUI() {
        let settings = CustomButtonBarSetting(imageName: "ic_back",
                                              style: .plain,
                                              action: #selector(backButtonPressed(_:)),
                                              target: self,
                                              possition: .left)
        _ = CustomUIItemFactory().createAndLoadButtonBar(withSetting: settings)
        
        selectIconButton.addTarget(self, action: #selector(selectIcon), for: .touchUpInside)
        selectIconButton.addTarget(self, action: #selector(selectIcon), for: .touchUpInside)
        avatarImageView.corner = avatarImageView.frame.size.width / 2
        avatarImageView.contentMode = .scaleAspectFit
        let tapShowIcon = UITapGestureRecognizer(target: self, action: #selector(selectIcon))
        avatarImageView.isUserInteractionEnabled = true
        avatarImageView.addGestureRecognizer(tapShowIcon)
        
        nameLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .bold, size: 14)
        nameLabel.text = NSLocalizedString("name_label_text", comment: "")
        emailLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .bold, size: 14)
        emailLabel.text = NSLocalizedString("email_label_text", comment: "")
    
        userNameTextView.isScrollEnabled = false
        textView.corner = 8
        textView.border(color: UIColor.ct.borderSearchTextFieldColor, width: 1)
        userNameTextView.delegate = self
        userNameTextView.font = UIFont.ct.font(.NotoSansCJKjp, type: .regular, size: 15)
        userNameTextView.textContainerInset = UIEdgeInsets(top: 0, left: 18, bottom: 0, right: 5)
        
        let emailHolder = NSLocalizedString("email_placeholder_text", comment: "")
        let attributes =  [NSAttributedString.Key.foregroundColor: UIColor.ct.borderSearchTextFieldColor, NSAttributedString.Key.font: UIFont.ct.font(.NotoSansCJKjp, type: .regular, size: 14)]
        emailTextField.attributedPlaceholder = NSAttributedString(string: emailHolder, attributes: attributes as [NSAttributedString.Key : Any])
        
        emailTextField.delegate = self
        emailTextField.addTarget(self, action: #selector(emailTextFieldDidChange(_:)), for: .editingChanged)
        
        updateButton.backgroundColor = UIColor.ct.mainBackgroundColor
        updateButton.titleLabel?.font = UIFont.ct.font(.NotoSansCJKjp, type: .bold, size: 16)
        updateButton.corner = 10
        updateButton.setTitle(NSLocalizedString("update_button_text", comment: ""), for: .normal)
        updateButton.setTitleColor(UIColor.ct.headerText, for: .normal)
        
        setHiddenFetures()
    }
    
    fileprivate func setPlaceHolderText() {
        let placeHolder = NSLocalizedString("username_placeholder_text", comment: "")
        userNameTextView.font = UIFont.ct.font(.NotoSansCJKjp, type: .regular, size: 14)
        userNameTextView.text = placeHolder
        userNameTextView.textColor = UIColor.ct.grayPlaceHolderTextColor
        userNameTextView.centerVertically()
    }
    
    fileprivate func setUpData() {
        guard let user = self.user else { return }
        adminEditProfilePresenter.profile = user
        defaultAvatarIndex = user.avatarIndex
        
        userNameTextView.text = user.name
        emailTextField.text = user.email
        let url = URL(string: user.avatarUrl)
        let placeHolder = UIImage(named: "img_avatar_user_empty")
        avatarImageView.kf.setImage(with: url, placeholder: placeHolder) { (image, _, _, _) in
            if let imageDownloaded = image {
                self.avatarImageView.image = image
                self.avatarImageView.contentMode = .scaleAspectFit
                self.adminEditProfilePresenter.avatar = imageDownloaded.pngData()
            } else {
                self.adminEditProfilePresenter.avatar = placeHolder!.pngData()
            }
        }
    }
    
    private func setHiddenFetures() {
        emailIconImageView.isHidden = true
        emailLabel.isHidden = true
        emailTextField.isHidden = true
    }
    
    // MARK: Actions
    @objc fileprivate func selectIcon() {
        let selectIconVC = SelectIconVC()
        selectIconVC.oldAvatarId = selectedTalkImageID
        selectIconVC.defaultAvatarIndex = defaultAvatarIndex
        
        selectIconVC.delegate = self
        selectIconVC.isFromNewTalk = false
        
        selectIconVC.transitioningDelegate = self
        self.definesPresentationContext = true
        selectIconVC.view.backgroundColor = .clear
        selectIconVC.modalPresentationStyle = .overFullScreen
        present(selectIconVC, animated: true, completion: nil)
    }
    
    @objc func emailTextFieldDidChange(_ textField: UITextField){
        adminEditProfilePresenter.emailTextChange(text: textField.text)
    }
    
    @objc fileprivate func backButtonPressed(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction private func updateProfile(sender: UIButton) {
        adminEditProfilePresenter.updateProfile(avatarIndex: defaultAvatarIndex)
    }
}
// MARK: AdminEditProfileView
extension AdminEditProfileVC: AdminEditProfileView {
    func enableEditButton() {
        updateButton.isEnabled = true
        updateButton.backgroundColor = UIColor.ct.mainBackgroundColor
        updateButton.setTitleColor(UIColor.ct.headerText, for: .normal)
    }
    
    func disableEditButton() {
        updateButton.isEnabled = false
        updateButton.backgroundColor = UIColor.ct.redDisableButton
        updateButton.setTitleColor(UIColor.ct.headerText.withAlphaComponent(0.47), for: .normal)
    }
    
    func updateSuccessful(profile: User) {
        delegate?.adminUpdatedProfile(profile: profile)
        self.navigationController?.popViewController(animated: true)
    }
}
// MARK: UITextView Delegate
extension AdminEditProfileVC: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.ct.grayPlaceHolderTextColor {
            textView.text = ""
            textView.textColor = UIColor.black
        }
    }
    
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        textView.endEditing(true)
        return true
    }
    
    func textViewDidChange(_ textView: UITextView) {
        adminEditProfilePresenter.usernameTextChange(text: textView.text)
        
        textView.dynamicHeight(heightConstraint: textViewHeightConstraint, minHeight: 50)
        userNameTextView.centerVertically()
        self.view.updateConstraints()
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            textView.dynamicHeight(heightConstraint: textViewHeightConstraint, defaultHeight: 50)
            setPlaceHolderText()
            self.view.updateConstraints()
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        guard let textOld = textView.text else { return true }
        if(text == "\n") {
            view.endEditing(true)
            return false
        }
        let count = textOld.count + text.count - range.length
        return count <= 30
    }
}
// MARK: UITextField Delegate
extension AdminEditProfileVC: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        if (text == "\n") {
            view.endEditing(true)
            return false
        }
        let count = text.count + string.count - range.length
        return count <= 30
    }
}
// MARK: SelectIconVC Delegate
extension AdminEditProfileVC: SelectIconVCDelegate {
    func selectAvatar(avatar: Avatar, at index: Int) {
        selectedTalkImageID = avatar.id
        defaultAvatarIndex = index + 1
        
        if let image = UIImage(named: avatar.name) {
            avatarImageView.image = UIImage(named: avatar.name)
            adminEditProfilePresenter.avatar = image.pngData()
        }
    }
    
    func didSelectImage(image: UIImage) {
        let vc = EditPhotoVC()
        vc.delegate = self
        vc.sourceImage = image.resizeImage()
        let navi = UINavigationController(rootViewController: vc)
        navi.navigationBar.setupNavigationBarWithColor(.white)
        present(navi, animated: true, completion: nil)
    }
}
// MARK: EditPhotoVC Delegate
extension AdminEditProfileVC: EditPhotoVCDelegate {
    func didCropImage(croppedImage: UIImage) {
        avatarImageView.image = croppedImage
        defaultAvatarIndex = 0
        
        do {
            let imageData = try croppedImage.compressToDataSize()
            adminEditProfilePresenter.avatar = imageData
        }
        catch {
            self.showErrorAlertWithError(error, completion: nil)
        }
    }
}
// MARK: Transitioning Delegate
extension AdminEditProfileVC: UIViewControllerTransitioningDelegate {
    
    func animationController(forPresented presented: UIViewController,
                             presenting: UIViewController,
                             source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return FadePushAnimator(type: .modal)
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        var interactionController: UIPercentDrivenInteractiveTransition?
        if let viewController = dismissed as? SelectIconVC {
            interactionController = viewController.interactionController
        }
        return FadePopAnimator(type: .modal, interactionController: interactionController)
    }
    
    func interactionControllerForDismissal(using animator: UIViewControllerAnimatedTransitioning)
        -> UIViewControllerInteractiveTransitioning? {
            guard let animator = animator as? FadePopAnimator,
                let interactionController = animator.interactionController as? LeftEdgeInteractionController, interactionController.inProgress else {
                    return nil
            }
            return interactionController
    }
}
