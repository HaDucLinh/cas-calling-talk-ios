//
//  HelpVC.swift
//  CallingTalk
//
//  Created by Vu Tran on 2/28/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit

class HelpVC: BaseViewController {
    @IBOutlet private weak var helpLabel: UILabel!
    @IBOutlet private weak var helpPersonLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.setupNavigationBarWithColor(.appTheme)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidAppear(animated)
        navigationController?.navigationBar.setupNavigationBarWithColor()
    }
    
    private func setUpUI() {
        title = NSLocalizedString("help_text", comment: "")
        let settings = CustomButtonBarSetting(imageName: "ic_back",
                                              style: .plain,
                                              action: #selector(backButtonPressed(_:)),
                                              target: self,
                                              possition: .left)
        _ = CustomUIItemFactory().createAndLoadButtonBar(withSetting: settings)
        
        helpLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .bold, size: 14)
        helpLabel.text = NSLocalizedString("help_text", comment: "")
        helpPersonLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .bold, size: 80)
        helpPersonLabel.textColor = UIColor.ct.grayPlaceHolderTextColor
    }
    
    @objc fileprivate func backButtonPressed(_ sender: UIBarButtonItem) {
        navigationController?.popViewController(animated: true)
    }
}
