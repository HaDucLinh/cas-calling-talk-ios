//
//  VoiceStatementCell.swift
//  CallingTalk
//
//  Created by Vu Tran on 3/4/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit
protocol VoiceStatementCellDelegate: NSObjectProtocol {
    func willEditVoice(voice: Voice)
    func willSpeakVoice(voice: Voice)
}
class VoiceStatementCell: UITableViewCell {
    @IBOutlet weak var contentTextLabel: UILabel!
    @IBOutlet private weak var organizeLabel: UILabel!
    @IBOutlet private weak var editVocieButton: UIButton!

    weak var delegate: VoiceStatementCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = .clear
        contentTextLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .bold, size: 14)
        organizeLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .regular, size: 12)
        organizeLabel.textColor = UIColor.ct.aboutMemberTextColor
    }
    
    var voice: Voice? {
        didSet {
            guard let voice = voice else {
                contentTextLabel.text = ""
                organizeLabel.text = ""
                return
            }
            contentTextLabel.text = voice.content
            var organizationText = ""
            voice.organizations.forEach { (organization) in
                organizationText += organization.name + ", "
            }
            organizationText = String(organizationText.dropLast())
            organizationText = String(organizationText.dropLast())
            organizeLabel.text = organizationText
            editVocieButton.isHidden = voice.type == VoiceType.Default.rawValue
        }
    }
    
    @IBAction private func willEditVoice(sender: UIButton) {
        if let voice = voice {
            delegate?.willEditVoice(voice: voice)
        }
    }
    
    @IBAction private func willSpeakVoice(sender: UIButton) {
        if let voice = voice {
            delegate?.willSpeakVoice(voice: voice)
        }
    }
}
