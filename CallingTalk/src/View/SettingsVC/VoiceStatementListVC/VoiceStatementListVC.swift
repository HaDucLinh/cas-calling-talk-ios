//
//  VoiceStatementListVC.swift
//  CallingTalk
//
//  Created by Vu Tran on 3/4/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit

class VoiceStatementListVC: BaseViewController {
    let voiceStatementPresenter = VoiceStatementPresenter()
    @IBOutlet private weak var tableview: UITableView!
    @IBOutlet private weak var emptyView: UIView!
    @IBOutlet private weak var emptyLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        voiceStatementPresenter.attachView(self)
        setUpUI()
        
        voiceStatementPresenter.getAllVoiceStatements()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.setupNavigationBarWithColor(.appTheme)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.navigationBar.setupNavigationBarWithColor()
    }
    
    private func setUpUI() {
        title = NSLocalizedString("Statement_list_title", comment: "")
        let settingsButtonLeft = CustomButtonBarSetting(imageName: "ic_back",
                                              style: .plain,
                                              action: #selector(backButtonPressed(_:)),
                                              target: self,
                                              possition: .left)
        let settingsButtonRight = CustomButtonBarSetting(imageName: "ic_add",
                                              style: .plain,
                                              action: #selector(addVoiceStatement),
                                              target: self,
                                              possition: .right)
        _ = CustomUIItemFactory().createAndLoadButtonBar(withSetting: settingsButtonLeft)
        _ = CustomUIItemFactory().createAndLoadButtonBar(withSetting: settingsButtonRight)
    
        emptyLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .bold, size: 14)
        emptyLabel.textColor = UIColor.ct.placeholderSearchTextColor
        tableview.estimatedRowHeight = 80.0
        tableview.rowHeight = UITableView.automaticDimension
        tableview.register(VoiceStatementCell.self)
        tableview.tableFooterView = UIView(frame: CGRect.zero)
        tableview.dataSource = self
        tableview.delegate = self
        
        setEmptyState(isEmpty: false)
    }
    
    private func setEmptyState(isEmpty: Bool) {
        tableview.isHidden = isEmpty
        emptyView.isHidden = !isEmpty
    }
    
    // MARK: Actions
    @objc fileprivate func backButtonPressed(_ sender: UIBarButtonItem) {
        navigationController?.popViewController(animated: true)
    }
    
    @objc fileprivate func addVoiceStatement() {
        let vc = RegisterVoiceStatementVC()
        vc.delegate = self
        navigationController?.pushViewController(vc, animated: true)
    }
}
// MARK: VoiceStatemen tView
extension VoiceStatementListVC: VoiceStatementView {
    func updateView() {
        self.tableview.reloadData()
    }
}
extension VoiceStatementListVC: RegisterVoiceStatementVCDelegate {
    func registerdVoiceSentence(voice: Voice) {
        voiceStatementPresenter.voices.append(voice)
        tableview.reloadData()
        let toastView = ToastView.loadViewFromNib()
        toastView.show(text: NSLocalizedString("Registered_voice_text", comment: ""))
    }
}
extension VoiceStatementListVC: VoiceStatementCellDelegate {
    func willEditVoice(voice: Voice) {
        let vc = EditVoiceStatementVC()
        vc.presenter.voice = voice
        vc.delegate = self
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func willSpeakVoice(voice: Voice) {
        AVPlayerManager.shared.playAudio(url: URL(string: voice.voicePathUrl))
    }
}
extension VoiceStatementListVC: EditVoiceStatementVCDelegate {
    func deletedVoiceStatement(voice: Voice) {
        if let index = voiceStatementPresenter.voices.index(where: {$0.id == voice.id}) {
            voiceStatementPresenter.voices.remove(at: index)
            self.tableview.reloadData()
            let toastView = ToastView.loadViewFromNib()
            toastView.show(text: NSLocalizedString("Deleted_voice_text", comment: ""))
        }
    }
    
    func updatedVoiceStatement(voice: Voice) {
        if let index = voiceStatementPresenter.voices.index(where: {$0.id == voice.id}) {
            voiceStatementPresenter.voices[index] = voice
            tableview.reloadData()
            let toastView = ToastView.loadViewFromNib()
            toastView.show(text: NSLocalizedString("Updated_voice_text", comment: ""))
        }
    }
}
// MARK: UITableView DataSource & Delegate
extension VoiceStatementListVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return voiceStatementPresenter.voices.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeue(VoiceStatementCell.self)
        cell.delegate = self
        cell.voice = voiceStatementPresenter.voices[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
