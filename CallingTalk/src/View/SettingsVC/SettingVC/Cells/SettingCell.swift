//
//  SettingCell.swift
//  CallingTalk
//
//  Created by Vu Tran on 2/26/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit

enum SettingType: Int {
    case Notification
    case Logout
    case List
    case Help
    
    // case List = 0, Notification, Help, Logout

    var text: String? {
        switch self {
        case .List:
            if  let user = User.getProfile(),
                user.isAdmin {
                return NSLocalizedString("List_voice_statements", comment: "")
            }
            else {
                return NSLocalizedString("member_list_text", comment: "")
            }
        case .Notification: return NSLocalizedString("notification_text", comment: "")
        case .Help: return NSLocalizedString("help_text", comment: "")
        case .Logout:
            if  let user = User.getProfile(),
                user.isAdmin {
                return NSLocalizedString("Logout_text", comment: "")
            }
            else {
                return NSLocalizedString("Reset_label_text", comment: "")
            }
        }
    }
    
    var image: UIImage? {
        switch self {
        case .List:
            if let user = User.getProfile(),
                user.isAdmin {
                return UIImage(named: "ic-list-voice")
            }
            else {
                return UIImage(named: "ic-setting-listMember")
            }
        case .Notification: return UIImage(named: "ic-notification")
        case .Help: return UIImage(named: "ic-setting-help")
        case .Logout: return UIImage(named: "ic-logout")
        }
    }
}
class SettingCell: UITableViewCell {
    @IBOutlet private weak var arrowImageView: UIImageView!
    @IBOutlet private weak var iconImageView: UIImageView!
    @IBOutlet private weak var titleLabel: UILabel!
    var type: SettingType? {
        didSet {
            iconImageView.image = type?.image?.withRenderingMode(.alwaysTemplate)
            iconImageView.tintColor = UIColor.ct.iconHightLight
            titleLabel.text = type?.text
            let templateArrowImage = arrowImageView.image?.withRenderingMode(.alwaysTemplate)
            arrowImageView.image = templateArrowImage
            arrowImageView.tintColor = UIColor.ct.mainBackgroundColor
            arrowImageView.isHidden = type == SettingType.Logout
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        titleLabel.textColor = UIColor.ct.grayInvitedTextColor
        titleLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .bold, size: 14)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
