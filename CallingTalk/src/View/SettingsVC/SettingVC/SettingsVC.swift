//
//  SettingsVC.swift
//  CallingTalk
//
//  Created by Toof on 2/14/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit
import Kingfisher

class SettingsVC: BaseViewController {
    fileprivate var presenter = SettingPresenter()
    @IBOutlet private weak var avatarImageView: UIImageView!
    @IBOutlet private weak var organizeLabel: UILabel!
    @IBOutlet private weak var nickNameLabel: UILabel!
    @IBOutlet private weak var emailLabel: UILabel!
    @IBOutlet private weak var versionLabel: UILabel!
    @IBOutlet private weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.attachView(self)
        setUpUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter.getMyProfile()
    }
    
    private func setUpUI() {
        avatarImageView.corner = avatarImageView.frame.size.width / 2
        organizeLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .regular, size: 14)
        nickNameLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .bold, size: 24)
        emailLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .regular, size: 12)
        versionLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .regular, size: 12)
        versionLabel.textColor = UIColor.ct.grayVersionColor
        if let version = UIApplication.appVersion, let buildVersion = UIApplication.buildVersion {
            versionLabel.text = NSLocalizedString("version_text", comment: "") + version.description + "(\(buildVersion.description))"
        } else {
            versionLabel.text = ""
        }
        
        let tapShowIcon = UITapGestureRecognizer(target: self, action: #selector(editProfile))
        avatarImageView.isUserInteractionEnabled = true
        avatarImageView.addGestureRecognizer(tapShowIcon)
        avatarImageView.image = UIImage(named: "img_avatar_user_empty")
        avatarImageView.contentMode = .scaleAspectFit
        
        tableView.tableHeaderView = UIView(frame: CGRect.zero)
        tableView.register(SettingCell.self)
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    fileprivate func setUpData() {
        if let user = User.getProfile() {
            nickNameLabel.text = user.name
            organizeLabel.text = user.provider
            organizeLabel.text = Helpers.getOrganizationsName(user: user)
            
            self.avatarImageView.contentMode = .scaleAspectFit
            avatarImageView.loadImageFromURL(user.avatarUrl, placeholder: UIImage(named: "img_avatar_user_empty"))
        }
    }
    
    private func willEditProfile() {
        if let user = presenter.user {
            if user.isAdmin {
                let vc = AdminEditProfileVC()
                vc.user = user
                vc.delegate = self
                navigationController?.pushViewController(vc, animated: true)
            } else {
                let vc = EditProfileVC()
                vc.user = user
                vc.delegate = self
                navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    // MARK: Actions
    @objc private func editProfile() {
       willEditProfile()
    }
    
    @IBAction @objc private func clickedEditProfile(sender: UIButton) {
        willEditProfile()
    }
}

extension SettingsVC: SettingView {
    func updateView() {
        setUpData()
    }
}
extension SettingsVC: AdminEditProfileVCDelagate {
    func adminUpdatedProfile(profile: User) {
        self.presenter.user = profile
        setUpData()
        let toastView = ToastView.loadViewFromNib()
        toastView.show(text: NSLocalizedString("updated_profile_text", comment: ""))
    }
}
extension SettingsVC: EditProfileDelegate {
    func staffUpdatedProfile(profile: User) {
        let toastView = ToastView.loadViewFromNib()
        toastView.show(text: NSLocalizedString("updated_profile_text", comment: ""))
    }
}
extension SettingsVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return 4
        return 2
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeue(SettingCell.self)
        cell.type = SettingType(rawValue: indexPath.row)
        cell.accessoryView?.tintColor = UIColor.ct.mainBackgroundColor
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        switch SettingType(rawValue: indexPath.row) {
        case .List?:
            if User.getProfile()?.isAdmin == false {
                let vc = MemberListVC()
                navigationController?.pushViewController(vc, animated: true)
            } else {
                // show CT-I-1
                let vc = VoiceStatementListVC()
                navigationController?.pushViewController(vc, animated: true)
            }
        case .Notification?:
            let vc = SettingNotificationVC()
            navigationController?.pushViewController(vc, animated: true)
        case .Help?:
            let vc = HelpVC()
            navigationController?.pushViewController(vc, animated: true)
        case .Logout?:
            let confirmView = ConfirmLogoutView.loadFromNib()
            var title = ""
            var message = ""
            var isAdmin = false
            if User.getProfile()?.isAdmin == true {
                isAdmin = true
                title = NSLocalizedString("Logout_label_admin_text", comment: "")
                confirmView.confirmButtonTitle = NSLocalizedString("Reset_button_admin_text", comment: "")
                confirmView.iconImageView.isHidden = true
            } else {
                isAdmin = false
                title = NSLocalizedString("Logout_label_text", comment: "")
                message = NSLocalizedString("Logout_label_message", comment: "")
                confirmView.confirmButtonTitle = NSLocalizedString("Reset_button_text", comment: "")
            }
            confirmView.show(title: title, message: message) {
                hud.show()
                API.logOut(completion: { [weak self] (result) in
                    hud.dismiss()
                    guard let `self` = self else { return }
                    switch result {
                    case .success(_):
                        // Disconnect socket
                        SocketIOManager.sharedInstance.disconnectSocket()
                        
                        // Clear user's profile & token id
                        Helpers.clearProfile()
                        AppDelegate.share()?.showLoginVC(isAdmin: isAdmin)
                    case .failure(let error):
                        self.showErrorAlertWithError(error, completion: nil)
                    }
                })
            }
        default:
            break
        }
    }
}
