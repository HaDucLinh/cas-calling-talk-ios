//
//  TalkDetailVC.swift
//  CallingTalk
//
//  Created by Vu Tran on 2/15/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit
import Kingfisher

protocol TalkDetailVCDelegate: NSObjectProtocol {
    func leaveTalkGroup(id: Int)
    func deleteTalkGroup(id: Int)
    func updatedTalkGroup(talk: TalkGroup)
    func deletedMemberFromDetail(talkID: Int, userId: Int)
    func willReloadListTalk()
}
class TalkDetailVC: BaseViewController {
    var talkDetailPresenter = TalkDetailPresenter()

    @IBOutlet weak var organizationNameLabel: UILabel!
    @IBOutlet private weak var talkImageView: UIImageView!
    
    @IBOutlet private weak var numberOfMembersLabel: UILabel!
    @IBOutlet private weak var talkNameLabel: UILabel!
    @IBOutlet private weak var collectionView: UICollectionView!
    
    @IBOutlet private weak var conversationButton: BadgeButton!
    @IBOutlet private weak var inviteButton: UIButton!
    @IBOutlet private weak var actionButton: UIButton!
    weak var delegate: TalkDetailVCDelegate?
    var shouldShowAudioLogMessages: Bool = true
    
    fileprivate var numberOfUnreadMessage: Int? {
        didSet {
            self.conversationButton.badgeString = self.numberOfUnreadMessage.toUnreadNumberBadgeString()
        }
    }
    
    // MARK: Life circle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUI()
        talkDetailPresenter.attachView(self)
        talkDetailPresenter.getMyProfile()
        talkDetailPresenter.fetch()
    }
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        collectionView.reloadData()
    }
    
    override func notifyForUnreadMessage(_ isMention: Bool, of talkGroupId: Int) {
        super.notifyForUnreadMessage(isMention, of: talkGroupId)
        if talkGroupId == self.talkDetailPresenter.talkGroupId {
            let numberOfMentionMessage = isMention ? 1 : 0
            self.numberOfUnreadMessage = (self.numberOfUnreadMessage ?? 0) + numberOfMentionMessage
        }
    }
    
    override func notifyForMarkAsReadMessage(of talkGroupId: Int) {
        super.notifyForMarkAsReadMessage(of: talkGroupId)
        if talkGroupId == self.talkDetailPresenter.talkGroupId {
            self.numberOfUnreadMessage = nil
        }
    }
    
    private func setUpUI() {
        title = NSLocalizedString("talk_detail_title", comment: "")
        let settings = CustomButtonBarSetting(imageName: "ic-close",
                                                         style: .plain,
                                                         action: #selector(close),
                                                         target: self,
                                                         possition: .left)
        _ = CustomUIItemFactory().createAndLoadButtonBar(withSetting: settings)
        
        numberOfMembersLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .bold, size: 12)
        talkNameLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .regular, size: 14)
        organizationNameLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .regular, size: 12)
        talkImageView.corner = talkImageView.frame.size.width / 2
        
        conversationButton.badgeString = ""
        conversationButton.badgeEdgeInsets = UIEdgeInsets(top: 11, left: 0, bottom: 0, right: 10)
        conversationButton.badgeBackgroundColor = UIColor.ct.badgeBackgroundColor

        collectionView.register(MemberCell.self)
        collectionView.dataSource = self
        collectionView.delegate = self
        
        //self.setHiddenFeature()
    }
    
    fileprivate func setUpEditButton() {
        guard let me = User.getProfile(), let org = talkDetailPresenter.talkGroup?.organization else {
            return
        }
        
        let myOrgs = me.organizations
        let meIsAuthor = talkDetailPresenter.talkGroup?.author?.id == me.id
        let meIsManageThatOrg = !myOrgs.filter({ $0.id == org.id }).isEmpty

        if meIsAuthor || (me.isAdmin && meIsManageThatOrg) {
            let editText = NSLocalizedString("edit_talk_detail_text", comment: "")
            let editSettingButton = CustomButtonBarSetting(tittleLabel: editText, style: .plain, action: #selector(editTalk), target: self, possition: .right)
            _ = CustomUIItemFactory().createAndLoadButtonBar(withSetting: editSettingButton)
        }
    }
    
    private func setHiddenFeature() {
        self.conversationButton.isHidden = true
        self.inviteButton.isHidden = true
        self.actionButton.isHidden = true
    }
    
    private func showToast(isEditName: Bool) {
        let text = isEditName ? NSLocalizedString("talk_updated_text", comment: "") : NSLocalizedString("invited_member_text", comment: "")
        let toastView = ToastView.loadViewFromNib()
        toastView.show(text: text)
    }
    
    // MARK: Leave & Delete Action sheet
    private func showLeaveConfirmAlert() { // CT-C-5-1-2
        let confirmAlertView = ConfirmDeleteTalkView.loadFromNib()
        confirmAlertView.talkGroup = talkDetailPresenter.talkGroup
        confirmAlertView.isDeleteMember = false
        confirmAlertView.title = NSLocalizedString("leave_alert_title", comment: "")
        confirmAlertView.message = NSLocalizedString("leave_delete_history_chat", comment: "")
        confirmAlertView.confirmButtonTitle = NSLocalizedString("leave_button_alert_text", comment: "")
        confirmAlertView.show {
            guard let id = self.talkDetailPresenter.talkGroup?.id, id != 0 else { return }
            self.talkDetailPresenter.leaveTalkGroup(id: id)
        }
    }
    
    private func showDeleteConfirmAlert() {  // CT-C-5-1-3
        let confirmAlertView = ConfirmDeleteTalkView.loadFromNib()
        confirmAlertView.talkGroup = talkDetailPresenter.talkGroup
        confirmAlertView.isDeleteMember = false
        confirmAlertView.title = NSLocalizedString("delete_alert_title", comment: "")
        confirmAlertView.message = NSLocalizedString("delete_talk_message_text", comment: "")
        confirmAlertView.confirmButtonTitle = NSLocalizedString("delete_button_alert_text", comment: "")
        confirmAlertView.show {
            guard let id = self.talkDetailPresenter.talkGroup?.id, id != 0 else { return }
            self.talkDetailPresenter.deleteTalkGroup(id: id)
        }
    }
    
    // MARK: Handle for Kickout from Channel
    fileprivate func willDeleteTalkAndBackToMainVC(channelId: Int) {
        self.delegate?.deleteTalkGroup(id: channelId)
        AppDelegate.share()?.showHomeTabbar()
    }
        
    // MARK: Actions
    @objc func close() {
        dismiss(animated: true, completion: nil)
    }
    
    @objc func editTalk() {
        let vc = EditTalkVC()
        vc.editTalkPresenter.talkGroup = talkDetailPresenter.talkGroup
        vc.delegate = self
        let navi = UINavigationController(rootViewController: vc)
        navi.navigationBar.setupNavigationBarWithColor(.appTheme)
        present(navi, animated: true, completion: nil)
    }
    
    @IBAction private func addMember(sender: UIButton) {
        let inviteMemberVC = InviteMemberVC()
        inviteMemberVC.delegate = self
        inviteMemberVC.presenter.inviteMemberForType = .Edit
        if let talkGroup = talkDetailPresenter.talkGroup {
            inviteMemberVC.presenter.talkGroup = talkGroup
        }
        present(UINavigationController(rootViewController: inviteMemberVC), animated: true, completion: nil)
    }
    
    @IBAction private func openConversation(sender: UIButton) {
        talkDetailPresenter.gotoChatVC()
    }
    
    @IBAction private func shareTalk(sender: UIButton) {
        talkDetailPresenter.showQRCode(fromVC: self)
    }
    
    @IBAction private func moreAction(sender: UIButton) {
        let leaveTitle = NSLocalizedString("leave_talk_action_text", comment: "")
        let leaveAction = UIAlertAction(title: leaveTitle, style: .default) { (action) in
            print("leaveAction")
            self.showLeaveConfirmAlert()
        }
        let deleteTitle = NSLocalizedString("delete_talk_action_text", comment: "")
        let deleteAction = UIAlertAction(title: deleteTitle, style: .default) { (action) in
            print("deleteAction")
            self.showDeleteConfirmAlert()
        }
        let cancelTitle = NSLocalizedString("cancel_text", comment: "")
        let cancelAction = UIAlertAction(title: cancelTitle, style: .cancel) { (action) in
        }
        var actions: [UIAlertAction] = [leaveAction, cancelAction]
        if talkDetailPresenter.talkGroup?.canDelete == true {
            actions.insert(deleteAction, at: 1)
        }
        Helpers.showActionSheet(title: nil, actions: actions, sourceView: sender)
    }
}
// MARK: TalkDetailView Delegate
extension TalkDetailVC: TalkDetailView {
    func gotoChatVC(withTalk talkGroup: TalkGroup) {
        let chatGroup = ChatVC()
        chatGroup.chatPresenter = ChatPresenter(view: chatGroup, talkGroup: talkGroup, messages: [])
        chatGroup.chatPresenter.shouldShowAudioLogMessages = shouldShowAudioLogMessages
        chatGroup.delegate = self
        self.present(chatGroup, animated: true, completion: nil)
    }
    
    func updateUI() {
        self.setUpEditButton()
        
        if let talkName = talkDetailPresenter.talkGroup?.name {
            self.talkNameLabel.text = talkName
        }
        if let organization = talkDetailPresenter.talkGroup?.organization {
            self.organizationNameLabel.text = organization.name
        }
        if let avatar = talkDetailPresenter.talkGroup?.avatar {
            self.talkImageView.loadImageFromURL(avatar, placeholder: UIImage(named: "img_avatar_talkgroup_empty"))
        }
        self.updateNumberOfMembers()
        
        self.collectionView.reloadData()
        print("USER:... \(String(describing: self.talkDetailPresenter.talkGroup?.allUsers))")
    }
    
    fileprivate func updateNumberOfMembers() {
        var totalMembers = 0
        if let members = talkDetailPresenter.talkGroup?.allUsers {
            totalMembers += members.count
        }
        self.numberOfMembersLabel.text = NSLocalizedString("number_of_member_text", comment: "") + totalMembers.description
    }
    
    func dismissVC() {
        delegate?.willReloadListTalk()
        dismiss(animated: true, completion: nil)
    }
    
    func leaveTalkGroup(id: Int) {
        delegate?.leaveTalkGroup(id: id)
        dismiss(animated: true, completion: nil)
    }
    
    func deleteTalkGroup(id: Int) {
        print("deleteTalkGroup")
        delegate?.deleteTalkGroup(id: id)
        dismiss(animated: true, completion: nil)
    }
    
    func updateNumberOfUnreadMessage(_ number: Int?) {
        self.numberOfUnreadMessage = number
    }
    
    func deletedMember(talkID: Int, member: User) {
        print("Deleted: ", member.name)
        
        if let index = self.talkDetailPresenter.talkGroup?.allUsers.index(where: {$0.id == member.id}) {
            let toastView = ToastView.loadViewFromNib()
            toastView.show(text: NSLocalizedString("deleteted_message", comment: ""))
            
            let indexPath = IndexPath(row: index, section: 0)
            talkDetailPresenter.talkGroup?.allUsers.remove(at: index)
            collectionView.deleteItems(at: [indexPath])
            
            delegate?.deletedMemberFromDetail(talkID: talkID, userId: member.id)
            self.updateNumberOfMembers()
        }
    }
}
// MARK: EditTalkVC Delegate
extension TalkDetailVC: EditTalkVCDelegate {
    func didUpdateTalk(isEditName: Bool, talk: TalkGroup) {
        self.talkNameLabel.text = talk.name
        self.talkImageView.loadImageFromURL(talk.avatar, placeholder: UIImage(named: "img_avatar_talkgroup_empty"))
        
        self.showToast(isEditName: isEditName)
        self.delegate?.updatedTalkGroup(talk: talk)
    }
}
// MARK: InviteMemberVC Delegate
extension TalkDetailVC: InviteMemberVCDelegate {
    func invitedMembers(_ members: [User], in inviteMemberVC: InviteMemberVC) {
        inviteMemberVC.dismiss(animated: true, completion: nil)
        self.showToast(isEditName: false)
    }
}
// MARK: ChatVCDelegate
extension TalkDetailVC: ChatVCDelegate {
    func chatVCDismissed(shouldShowAudioLogMessages: Bool) {
        self.shouldShowAudioLogMessages = shouldShowAudioLogMessages
    }
}
// MARK: UICollectionView DataSource & Delegate
extension TalkDetailVC: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.talkDetailPresenter.talkGroup?.allUsers.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeue(MemberCell.self, forIndexPath: indexPath)
        cell.user = self.talkDetailPresenter.talkGroup?.allUsers[indexPath.item]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let widthCell = (GlobalVariable.screenWidth - 30) / 4
        return CGSize(width: widthCell, height: widthCell + 10)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        talkDetailPresenter.willShowTalkMemberAlert(index: indexPath.row)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}
