//
//  AddOnCell.swift
//  CallingTalk
//
//  Created by Toof on 2/18/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit

class AddOnCell: UICollectionViewCell {
    
    // MARK: Properties
    
    @IBOutlet weak var addOnNumberLabel: UILabel!
    
    // MARK: Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        addOnNumberLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .regular, size: 11)
    }
    
    public func updateAddOnNumber(_ text: String) {
        self.addOnNumberLabel.text = text
    }

}
