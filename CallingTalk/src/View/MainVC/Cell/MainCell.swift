//
//  MainCell.swift
//  CallingTalk
//
//  Created by Toof on 2/14/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit
import Kingfisher
import ObjectMapper

enum StackMemberSectionType: Int {
    case MemberStack = 0
    case AddOn = 1
}

class MainCell: UITableViewCell {
    
    // MARK: Properties
    
    var presenter: MainCellPresenter!
    
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var isCallingImageView: UIImageView!
    @IBOutlet weak var statusImageView: UIImageView!

    @IBOutlet weak var containView: UIView!
    @IBOutlet weak var talkGroupLabel: UILabel!
    
    @IBOutlet weak var inviteNoticeView: UIView!
    @IBOutlet weak var inviteNoticeLabel: UILabel!

    @IBOutlet weak var personJoiningView: UIView!
    @IBOutlet weak var personJoiningLabel: UILabel!
    @IBOutlet weak var chatButton: BadgeButton!
    @IBOutlet weak var settingsButton: UIButton!
    
    @IBOutlet weak var inviteNoticeHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var collectionView: UICollectionView!
    
    fileprivate var numberOfUnreadMessage: Int? {
        didSet {
            self.chatButton.badgeString = self.numberOfUnreadMessage.toUnreadNumberBadgeString()
        }
    }
    
    // MARK: Lifecycle

    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupSubviews()
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleNewMessageNotification(_:)), name: NSNotification.Name(rawValue: kNewMessageEvent), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleLocalMarkAsReadNotification(_:)), name: NSNotification.Name(rawValue: kLocalMarkMessageAsReadEvent), object: nil)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.avatarImageView.cancelRequestImage()
    }
    
    fileprivate func setupSubviews() {
        self.backgroundColor = UIColor.white.withAlphaComponent(0.0)
        self.contentView.backgroundColor = UIColor.white.withAlphaComponent(0.0)
        self.containView.backgroundColor = UIColor.white
        
        self.avatarImageView.roundedCornerRadius()
        self.inviteNoticeView.corner = 10.0
        self.personJoiningView.corner = 10.0
        
        self.chatButton.badgeBackgroundColor = UIColor.ct.badgeBackgroundColor
        self.chatButton.badgeString = ""
        self.chatButton.badgeEdgeInsets = UIEdgeInsets(top: 11, left: 0, bottom: 0, right: 10)
        self.chatButton.addTarget(self, action: #selector(chatButtonPressed(_:)), for: .touchUpInside)
        
        self.settingsButton.addTarget(self, action: #selector(settingsButtonPressed(_:)), for: .touchUpInside)
        
        self.collectionView.register(MemberMeetingCell.self)
        self.collectionView.register(AddOnCell.self)
        
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        
    }
}

// MARK: - Handle Events

extension MainCell {
    @objc fileprivate func chatButtonPressed(_ sender: UIButton) {
        self.presenter.handleForShowingChatInGroup()
    }
    
    @objc fileprivate func settingsButtonPressed(_ sender: UIButton) {
        self.presenter.handleForShowingTalkGroupDetail()
    }
}

// MARK: - UICollectionViewDataSource

extension MainCell: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0 {
            return min(8, presenter.talkGroup.meetingUsers.count)
        }
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.section == 0 {
            let cell = collectionView.dequeue(MemberMeetingCell.self, forIndexPath: indexPath)
            cell.member = presenter.talkGroup.meetingUsers[indexPath.row]
            return cell
        } else {
            let cell = collectionView.dequeue(AddOnCell.self, forIndexPath: indexPath)
            var addOn = ""
            if presenter.talkGroup.meetingUsers.count > 8 {
                addOn = "+" + (presenter.talkGroup.meetingUsers.count - 8).description
            }
            cell.updateAddOnNumber(addOn)
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("didSelectItemAt: ", indexPath.row)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return -6
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 28.0, height: 28.0)
    }
}

// MARK: Implement View

extension MainCell: MainCellView {
    func updateUI(talkGroup: TalkGroup) {
        avatarImageView.image = UIImage(named: "img_avatar_talkgroup_empty")
        self.talkGroupLabel.text = talkGroup.name
        avatarImageView.loadImageFromURL(talkGroup.avatar, placeholder: UIImage(named: "img_avatar_talkgroup_empty"))
        avatarImageView.contentMode = .scaleAspectFit
        
        if talkGroup.isInvited {
            let inviterName = talkGroup.inviter?.name ?? ""
            let invitationString = NSLocalizedString("invite_join_talkGroup_text", comment: "")
            self.inviteNoticeLabel.text = String(format: invitationString, inviterName)
            self.inviteNoticeView.isHidden = false
            self.chatButton.isHidden = true
            self.settingsButton.isHidden = true
        } else {
            self.chatButton.isHidden = false
            self.settingsButton.isHidden = false
            self.inviteNoticeView.isHidden = true
            self.inviteNoticeLabel.text = ""
            self.numberOfUnreadMessage = talkGroup.isNewUnreadMessage ? talkGroup.numberOfUnreadMessageMention : nil
        }
        
        let talkGroupIsNotCalling = talkGroup.meetingUsers.isEmpty
        self.isCallingImageView.isHidden = talkGroupIsNotCalling
        self.statusImageView.isHidden = talkGroupIsNotCalling
            
        self.inviteNoticeHeightConstraint.constant = talkGroup.isInvited ? 23 : 0
        self.personJoiningLabel.updateActiveNumber(talkGroup.meetingUsers.count, inTotalNumber: talkGroup.joinUsersCount)
        self.collectionView.reloadData()
    }
}

// MARK: - Receiving Notification

extension MainCell {
    @objc
    fileprivate
    func handleNewMessageNotification(_ notification: Notification) {
        guard let dict = notification.object as? JSObject else { return }
        guard let message = Mapper<Message>().map(JSON: dict) else { return }
        guard let userId = User.getProfile()?.id else { return }

        if message.owner?.id != userId {
            if message.channelId == self.presenter.talkGroup.id {
                if Array(message.mentionIds).contains(userId) || message.isMentionAll {
                    self.numberOfUnreadMessage = (self.numberOfUnreadMessage ?? 0) + 1
                }
                else {
                    if self.numberOfUnreadMessage == nil {
                        self.numberOfUnreadMessage = 0
                    }
                }
            }
        }
    }
    
    @objc private func handleLocalMarkAsReadNotification(_ notification: Notification) {
        guard let channelId = notification.object as? Int else { return }
        if channelId == self.presenter.talkGroup.id {
            self.numberOfUnreadMessage = nil
        }
    }
}
