//
//  MemberMeetingCell.swift
//  CallingTalk
//
//  Created by Vu Tran on 3/21/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit

class MemberMeetingCell: UICollectionViewCell {
    @IBOutlet private weak var memberImageView: UIImageView!
    @IBOutlet private weak var mainView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        mainView.corner = mainView.frame.size.width / 2
        mainView.border(color: .white, width: 2)
        memberImageView.image = UIImage(named: "img_avatar_user_empty")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        mainView.corner = mainView.frame.size.width / 2
        mainView.border(color: .white, width: 2)
    }
    
    override func willMove(toSuperview newSuperview: UIView?) {
        self.layoutIfNeeded()
    }
    
    var member: User? {
        didSet {
            guard let member = self.member else { return }
            memberImageView.loadImageFromURL(member.avatarUrl, placeholder: UIImage(named: "img_avatar_user_empty"))
        }
    }
    
}
