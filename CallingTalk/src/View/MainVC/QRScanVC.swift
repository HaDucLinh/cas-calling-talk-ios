//
//  QRScanViewController.swift
//  DemoQRCodeScan
//
//  Created by Toof on 2/18/19.
//  Copyright © 2019 Toof. All rights reserved.
//

import UIKit
import AVFoundation

protocol QRScanVCDelegate: class {
    func qrScanVC(_ qrScanVC: QRScanVC, willPresentToIncomingVCWith code: String)
}

class QRScanVC: BaseViewController {
    
    // MARK: Properties
    
    fileprivate var presenter: QRScanPresenter!
    
    @IBOutlet weak var topView: UIVisualEffectView!
    @IBOutlet weak var bottomView: UIVisualEffectView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var openImageLibraryButton: UIButton!
    
    @IBOutlet weak var cameraView: UIView!
    @IBOutlet weak var focusImageView: UIImageView!
    @IBOutlet weak var qrCodeImageView: UIImageView!
    
    fileprivate var captureSession = AVCaptureSession()
    fileprivate var previewLayer: AVCaptureVideoPreviewLayer?
    
    weak var delegate: QRScanVCDelegate?
    var timer: Timer?
    
    fileprivate let supportedCodeTypes = [
        AVMetadataObject.ObjectType.upce,
        AVMetadataObject.ObjectType.code39,
        AVMetadataObject.ObjectType.code39Mod43,
        AVMetadataObject.ObjectType.code93,
        AVMetadataObject.ObjectType.code128,
        AVMetadataObject.ObjectType.ean8,
        AVMetadataObject.ObjectType.ean13,
        AVMetadataObject.ObjectType.aztec,
        AVMetadataObject.ObjectType.pdf417,
        AVMetadataObject.ObjectType.itf14,
        AVMetadataObject.ObjectType.dataMatrix,
        AVMetadataObject.ObjectType.interleaved2of5,
        AVMetadataObject.ObjectType.qr
    ]
    
    // MARK: Gide status's bar
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    // MARK: Lidecycles
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.presenter = QRScanPresenter(view: self)
        self.setupSubviews()
        self.setupCaptureSession()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector:#selector(self.didChangeCaptureInputPortFormatDescription(notification:)), name: NSNotification.Name.AVCaptureInputPortFormatDescriptionDidChange, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.releaseTimer()
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.AVCaptureInputPortFormatDescriptionDidChange, object: nil)
    }
    
    override func localizeString() {
        super.localizeString()
        self.titleLabel.text = NSLocalizedString("qr_scan_title", comment: "")
        self.cancelButton.setTitle(NSLocalizedString("qr_scan_cancel_button_title", comment: ""), for: .normal)
        self.openImageLibraryButton.setTitle(NSLocalizedString("qr_scan_link_button_title", comment: ""), for: .normal)
    }
    
    fileprivate
    func setupSubviews() {
        self.cancelButton.layer.cornerRadius = 10
        self.cancelButton.layer.borderColor = UIColor.white.cgColor
        self.cancelButton.layer.borderWidth = 1.0
        self.cancelButton.addTarget(self, action: #selector(cancelButtonPressed(_:)), for: .touchUpInside)
        self.openImageLibraryButton.addTarget(self, action: #selector(openImageLibraryButtonPressed(_:)), for: .touchUpInside)
    }
    
    fileprivate
    func setupCaptureSession() {
        guard let captureDevice = AVCaptureDevice.default(for: AVMediaType.video) else { return }
        do {
            let input = try AVCaptureDeviceInput(device: captureDevice)
            if self.captureSession.inputs.isEmpty {
                self.captureSession.addInput(input)
                self.setupPreviewLayer {
                    self.startScanQRcode()
                    self.addMetaDataCaptureOutToSession()
                }
            }
        } catch {
            self.showAlertForRetrievingError(error.localizedDescription)
        }
    }
    
    fileprivate
    func setupPreviewLayer(_ completion: @escaping () -> Void) {
        self.view.layoutIfNeeded()
        // Initialize the video preview layer and add it as a sublayer to the viewPreview view's layer.
        self.previewLayer = AVCaptureVideoPreviewLayer(session: self.captureSession)
        self.previewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
        self.cameraView.layer.addSublayer(self.previewLayer!)
        
        // Move bottom bar and top bar to the front
        self.view.bringSubviewToFront(self.topView)
        self.view.bringSubviewToFront(self.bottomView)
        
        // Initialize QR Code Frame to highlight the QR code
        let layerRect = self.cameraView.layer.frame
        self.previewLayer?.frame = layerRect
        self.view.bringSubviewToFront(self.focusImageView)
        completion()
    }
    
    fileprivate
    func addMetaDataCaptureOutToSession() {
        if self.captureSession.outputs.isEmpty {
            let captureMetadataOutput = AVCaptureMetadataOutput()
            self.captureSession.addOutput(captureMetadataOutput)
            captureMetadataOutput.metadataObjectTypes = self.supportedCodeTypes
            captureMetadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
        }
    }
    
    func startScanQRcode() {
        if !self.captureSession.isRunning {
            self.captureSession.startRunning()
        }
    }
    
    func stopScanQRcode(_ isScanSuccess: Bool = true) {
        if self.captureSession.isRunning {
            self.captureSession.stopRunning()
            if isScanSuccess {
                AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
            }
        }
    }
    
}

// MARK: - Handle Events

extension QRScanVC {
    
    @objc
    fileprivate
    func cancelButtonPressed(_ sender: UIButton) {
        self.stopScanQRcode(false)
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc
    fileprivate
    func openImageLibraryButtonPressed(_ sender: UIButton) {
        self.openPhotoLibrary()
    }
    
    @objc
    fileprivate
    func didChangeCaptureInputPortFormatDescription(notification: NSNotification) {
        guard   let metadataOutput = self.captureSession.outputs.last as? AVCaptureMetadataOutput,
                let rect = self.previewLayer?.metadataOutputRectConverted(fromLayerRect: self.view.frame)
        else    { return }
        metadataOutput.rectOfInterest = rect
    }
    
}

// MARK: - AVCaptureMetadataOutputObjectsDelegate

extension QRScanVC: AVCaptureMetadataOutputObjectsDelegate {
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        guard   let metadataObj = metadataObjects[0] as? AVMetadataMachineReadableCodeObject,
                let qrCodeString = metadataObj.stringValue
        else    { return }
        if metadataObj.type == AVMetadataObject.ObjectType.qr {
            self.stopScanQRcode()
            self.presenter.joinTalkGroupBy(qrCode: qrCodeString)
        }
    }
    
}

// MARK: - Photo Library

extension QRScanVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    fileprivate
    func openPhotoLibrary() {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self
            myPickerController.sourceType = .photoLibrary
            self.present(myPickerController, animated: true, completion: nil)
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let image = info[.originalImage] as? UIImage else { return }
        DispatchQueue.main.async { [weak self] in
            guard let `self` = self else { return }
            self.stopScanQRcode()
            if let qrCodeString = image.parseQR().first {
                self.cameraView.isHidden = true
                self.qrCodeImageView.image = image
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
                    self.presenter.joinTalkGroupBy(qrCode: qrCodeString)
                })
            }
            else {
                self.showAlertForRetrievingError(NSLocalizedString("qr_scan_fail_notice_text", comment: ""))
            }
        }
        
        picker.dismiss(animated: true, completion: nil)
    }
    
}

// MARK: - Implement View

extension QRScanVC: QRScanView {
    
    func showAlertForRetrievingError(_ message: String) {
        let toastView = ToastView.loadViewFromNib()
        toastView.show(text: message)
        
        self.releaseTimer()
        self.timer = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(restartScanQRCode(_:)), userInfo: nil, repeats: false)
    }
    
    func dismissQRScanVCWithQRCode(_ code: String) {
        self.delegate?.qrScanVC(self, willPresentToIncomingVCWith: code)
    }
    
    @objc
    fileprivate
    func restartScanQRCode(_ sender: Timer) {
        self.startScanQRcode()
    }
    
    fileprivate
    func releaseTimer() {
        self.timer?.invalidate()
        self.timer = nil
    }
    
}
