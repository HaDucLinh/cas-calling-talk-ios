//
//  MainVC.swift
//  CallingTalk
//
//  Created by Toof on 2/14/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit
import Reachability
import AMPopTip

class MainVC: BaseViewController {
    // MARK: Properties
    
    fileprivate var presenter: MainPresenter!
    fileprivate var qrPopTip: PopTip?
    fileprivate var addPopTip: PopTip?
    
    fileprivate let refreshControl = UIRefreshControl()
    fileprivate let searchBar: UISearchBar = UISearchBar()
    
    @IBOutlet weak var tipAddView: UIView!
    @IBOutlet weak var qrButton: UIButton!
    
    @IBOutlet weak var emptyTalkView: UIView!
    @IBOutlet weak var emptyTalkLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.registerReachability()
        self.presenter = MainPresenter(view: self)
        self.setupSubviews()
        self.presenter.fetch(completion: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(reloadTalkGroups), name: NSNotification.Name(rawValue: "ReloadTalkGroups"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(getMyProfile), name: NSNotification.Name(rawValue: "ReloadProfile"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reconnectSocket(_:)), name: Notification.Name.ReconnectSocket, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleReceivingAcceptOrDeclineInvitation(_:)), name: Notification.Name.AcceptOrDeclineInvitationPushNotification, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func reachabilityChanged(note: Notification) {
        if GlobalVariable.didComeToIncomingVC {
            return
        }
        guard let reachability =  note.object as? Reachability else { return }
        switch reachability.connection {
        case .none:
            hud.setDefaultMaskType(.clear)
            hud.show()
        default:
            // Waiting for reconnect socket
            break
        }
    }
    
    fileprivate
    func setupSubviews() {
        let buttonBarSetting = CustomButtonBarSetting.init(imageName: "ic_add",
                                                           style: .plain,
                                                           action: #selector(addButtonPressed(_:)),
                                                           target: self,
                                                           possition: .right)
        _ = CustomUIItemFactory().createAndLoadButtonBar(withSetting: buttonBarSetting)
        self.emptyTalkLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .bold, size: 14.0)
        self.emptyTalkLabel.textColor = UIColor.ct.headerTextSubtitle
        // Setup TableView
        self.tableView.register(MainCell.self)
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.backgroundColor = UIColor.white.withAlphaComponent(0.0)
        self.tableView.separatorColor = UIColor.white.withAlphaComponent(0.0)
        self.tableView.contentInset.bottom = 150.0
        self.tableView.tableFooterView = UIView()
        
        // Setup SearchController
        self.searchBar.frame = CGRect(x: 0, y: 1, width: GlobalVariable.screenWidth, height: 59)
        self.searchBar.delegate = self
        self.searchBar.returnKeyType = .done
        
        // Setup RefreshControl
        self.refreshControl.tintColor = UIColor.ct.mainBackgroundColor
        self.refreshControl.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)
        
        if #available(iOS 10.0, *) {
            self.tableView.refreshControl = self.refreshControl
        } else {
            self.tableView.addSubview(self.refreshControl)
        }
        
        // Add events for QR's Button
        self.qrButton.addTarget(self, action: #selector(qrButtonPressed(_:)), for: .touchUpInside)
    }
    
    override func localizeString() {
        self.navigationItem.title = NSLocalizedString("main_navigation_item_title", comment: "")
        self.searchBar.customWithPlaceholder(NSLocalizedString("search_talk_placeholder_text", comment: ""))
        self.emptyTalkLabel.text = NSLocalizedString("empty_talk_label_text", comment: "")
    }
    
    fileprivate func deleteCellForTalkGroup(id: Int) {
        if let index = self.presenter.talkGroups.index(where: {$0.id == id}) {
            let indexPath = IndexPath(row: index, section: 0)
            
            presenter.talkGroups.remove(at: index)
            tableView.beginUpdates()
            tableView.deleteRows(at: [indexPath], with: .fade)
            tableView.endUpdates()
        }
    }
    
    // MARK: Functions
    @objc private func reloadTalkGroups() {
        presenter.resetListTalkGroup()
    }
    
    @objc func getMyProfile() {
        presenter.getMyProfile()
    }
    
    @objc fileprivate func reconnectSocket(_ notification: Notification) {
        /*
         Excuting only when user inside the main screen, otherwise skip
         */
        if !GlobalVariable.didComeToIncomingVC {
            self.presenter.resetListTalkGroup {
                hud.setDefaultMaskType(.none)
            }
        }
    }
    
    @objc fileprivate func handleReceivingAcceptOrDeclineInvitation(_ notification: Notification) {
        self.presenter.resetListTalkGroup()
    }
    
    @objc fileprivate func searchTalkGroup() {
        self.presenter.searchTalkGroups(name: searchBar.text, completion: nil)
    }
}

extension MainVC: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView is UITableView {
            if scrollView.contentOffset.y < 0 {
                if self.tableView.tableHeaderView == nil && self.presenter.talkGroups.count != 0 {      // If tableHeaderView == nil, add searchbar
                    self.tableView.tableHeaderView = self.searchBar
                    self.tableView.tableHeaderView?.alpha = 0
                    UIView.animate(withDuration: 0.5) {
                        self.tableView.tableHeaderView?.alpha = 1
                    }
                }
            }
        }
    }
}

// MARK: - Handle Events

extension MainVC {
    @objc fileprivate func qrButtonPressed(_ sender: UIButton) {
        let qrScanVC = QRScanVC()
        qrScanVC.delegate = self
        self.present(qrScanVC, animated: true, completion: nil)
    }
    
    @objc fileprivate func addButtonPressed(_ sender: UIBarButtonItem) {
//<<<<<<< HEAD
//        if let organizations = User.getProfile()?.organizations, !organizations.isEmpty {
//            let vc = StartNewTalkVC()
//            vc.delegate = self
//            let newTalkNav = UINavigationController(rootViewController: vc)
//            newTalkNav.navigationBar.setupNavigationBarWithColor(.appTheme)
//            self.present(newTalkNav, animated: true, completion: nil)
//        } else {
//            let joinOrganizationText = NSLocalizedString("Can't_create_talk_this_time", comment: "")
//            self.showErrorAlertWithError(joinOrganizationText, completion: nil)
//=======
        self.presenter.getMyProfile { [weak self] in
            guard let `self` = self else { return }
            if let organizations = User.getProfile()?.organizations, !organizations.isEmpty {
                let vc = StartNewTalkVC()
                vc.delegate = self
                let newTalkNav = UINavigationController(rootViewController: vc)
                newTalkNav.navigationBar.setupNavigationBarWithColor(.appTheme)
                self.present(newTalkNav, animated: true, completion: nil)
            } else {
                let joinOrganizationText = NSLocalizedString("can't_create_talk_this_time", comment: "")
                self.showErrorAlertWithError(joinOrganizationText, completion: nil)
            }
//>>>>>>> origin/develop
        }
    }
    
    @objc fileprivate func refreshData(_ sender: UIRefreshControl) {
        self.refreshControl.beginRefreshing()
        if presenter.isSearching {
            self.presenter.searchTalkGroups(name: searchBar.text, isPullToRefresh: true) {
                self.refreshControl.endRefreshing()
            }
        } else {
            self.presenter.fetch(isPullToRefresh: true) {
                self.refreshControl.endRefreshing()
            }
        }
    }
}

// MARK: - QRScanVCDelegate

extension MainVC: QRScanVCDelegate {
    func qrScanVC(_ qrScanVC: QRScanVC, willPresentToIncomingVCWith code: String) {
        qrScanVC.dismiss(animated: true, completion: nil)
        self.presenter.verifyTalkGroupByQRCode(code)
    }
}

// MARK: - UITableView DataSource & Delegate

extension MainVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.presenter.numberOfRows(inSection: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeue(MainCell.self)
        cell.presenter = self.presenter.transferToRowPresenter(cell, at: indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let numberOfCurrentTalkGroups = presenter.isSearching ?
            presenter.searchedTalkGroups.count : presenter.talkGroups.count
        if indexPath.row == (numberOfCurrentTalkGroups - 1) {
            presenter.willLoadMoreTalkGroups(completion: {})
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.presenter.didSelectRow(at: indexPath)
    }
}

// MARK: - ConfirmAlertViewDelegate
extension MainVC: ConfirmAlertViewDelegate {
    func confirmAlertView(_ confirmAlertView: ConfirmAlertView, didSelectButtonWithType type: JoinActionType) {
        switch type {
        case .Accept:
            switch confirmAlertView.presenter.type {
            case .Invitation:
                self.showIncomingVCWithTalkGroupId(confirmAlertView.presenter.talkGroup.id)
                self.presenter.resetListTalkGroup(completion: nil)
            case .JoinByQR(code: _):
                let talkGroup = confirmAlertView.presenter.talkGroup
                if let organization = talkGroup.organization, User.getProfile()?.organizations.isEmpty == true {
                    User.getProfile()?.addOrganization(organization: organization)
                }
                self.showIncomingVCWithTalkGroupId(talkGroup.id)
                self.presenter.resetListTalkGroup(completion: nil)
            default: break
            }
        case .Decline:
            switch confirmAlertView.presenter.type {
            case .Invitation:
                self.presenter.resetListTalkGroup()
            default: break
            }
        }
    }
}

// MARK: MainView Presenter Delegate
extension MainVC: MainView {
    func showConfirmAlertViewForJoinTalkGroup(_ talkGroup: TalkGroup, code: String) {
        let confirmAlertView = ConfirmAlertView()
        confirmAlertView.presenter = ConfirmAlertPresenter(view: confirmAlertView,
                                                           talkGroup: talkGroup,
                                                           type: .JoinByQR(code: code))
        confirmAlertView.delegate = self
        confirmAlertView.show()
    }
    
    func showConfirmAlertViewForInviteTalkGroup(_ talkGroup: TalkGroup) {
        let confirmAlertView = ConfirmAlertView()
        confirmAlertView.presenter = ConfirmAlertPresenter(view: confirmAlertView,
                                                           talkGroup: talkGroup,
                                                           type: .Invitation)
        confirmAlertView.delegate = self
        confirmAlertView.show()
    }
    
    func showIncomingVCWithTalkGroupId(_ id: Int) {
        let incomingVC = IncomingVC()
        incomingVC.delegate = self
        incomingVC.presenter = IncomingPresenter(view: incomingVC, talkGroupId: id)
        self.present(incomingVC, animated: true, completion: nil)
    }
    
    func showTalkDetailVC(_ presenter: TalkDetailPresenter) {
        let talkDetailVC = TalkDetailVC()
        talkDetailVC.delegate = self
        talkDetailVC.talkDetailPresenter = presenter
        
        let talkDetailNav = UINavigationController(rootViewController: talkDetailVC)
        talkDetailNav.navigationBar.setupNavigationBarWithColor(.appTheme)
        
        self.present(talkDetailNav, animated: true, completion: nil)
    }
    
    func showChatGroup(_ talkGroup: TalkGroup) {
        let chatGroup = ChatVC()
        chatGroup.delegate = self
        chatGroup.chatPresenter = ChatPresenter(view: chatGroup, talkGroup: talkGroup, messages: [])
        chatGroup.chatPresenter.shouldShowAudioLogMessages = true
        self.present(chatGroup, animated: true, completion: nil)
    }
    
    func showTutorialPopup(_ isShow: Bool) {
        if presenter.isSearching {
            return
        }
        if isShow {
            if self.qrPopTip == nil {
                self.qrPopTip = self.createPopTipWithMessage(contentInset: UIEdgeInsets(top: 3.0, left: 21.0, bottom: 6.0, right: 21.0), margin: 15.0)
                self.qrPopTip?.show(text: NSLocalizedString("qr_tip_text", comment: ""), direction: .up, maxWidth: 200.0, in: self.view, from: self.qrButton.frame)
            }
            
            if self.addPopTip == nil {
                self.addPopTip = self.createPopTipWithMessage(contentInset: UIEdgeInsets(top: 3.0, left: 15.0, bottom: 6.0, right: 15.0), margin: 8.0)
                self.addPopTip!.show(text: NSLocalizedString("add_tip_text", comment: ""), direction: .down, maxWidth: 200.0, in: self.view, from: self.tipAddView.frame)
            }
        } else {
            self.qrPopTip?.hide()
            self.qrPopTip = nil
            self.addPopTip?.hide()
            self.addPopTip = nil
        }
    }
    
    func showEmptyTalkView(_ isShow: Bool) {
        self.emptyTalkView.isHidden = !isShow
    }
    
    fileprivate func createPopTipWithMessage(contentInset: UIEdgeInsets, margin: CGFloat) -> PopTip {
        let popTip = PopTip()
        popTip.bubbleColor = UIColor.ct.popTipBackgroundColor
        popTip.textColor = UIColor.white
        popTip.font = UIFont.ct.font(.NotoSansCJKjp, type: .bold, size: 13.0)
        popTip.textAlignment = .center
        popTip.cornerRadius = 5.0
        popTip.edgeInsets = contentInset
        popTip.edgeMargin = margin
        popTip.offset = 0.0
        popTip.arrowSize = CGSize(width: 12.0, height: 10.0)
        return popTip
    }
    
    func updateView() {
        if !presenter.isSearching {
            self.searchBar.isHidden = self.presenter.talkGroups.count == 0
        }
        showTutorialPopup(presenter.talkGroups.count == 0)
        self.tableView.reloadData()
    }
    
    func insertTalkGroups(talkGroups: [TalkGroup]) {
        var indexPaths: [IndexPath] = []
        let currentTalkGroups: [TalkGroup] = presenter.isSearching ? presenter.searchedTalkGroups : presenter.talkGroups
        for i in currentTalkGroups.count..<(currentTalkGroups.count + talkGroups.count) {
            let indexPath = IndexPath(row: i, section: 0)
            indexPaths.append(indexPath)
        }
        presenter.isSearching ? presenter.searchedTalkGroups.append(contentsOf: talkGroups) : presenter.talkGroups.append(contentsOf: talkGroups)
        
        self.tableView.performBatchUpdates({
            self.tableView.setContentOffset(self.tableView.contentOffset, animated: false)
            self.tableView.insertRows(at: indexPaths, with: .fade)
        }, completion: nil)
    }
    
    func reloadRowAt(indexPath: IndexPath) {
        self.tableView.performBatchUpdates({
            self.tableView.setContentOffset(self.tableView.contentOffset, animated: false)
            self.tableView.reloadRows(at: [indexPath], with: .fade)
        }, completion: nil)
    }
    
    func hasNewEvent() {
        // To do: when we have a new talk group
    }
    
    func clearSearchBar() {
        self.searchBar.text = nil
        self.searchBar.endEditing(true)
    }
}

// MARK: StartNewTalkVC Delegate
extension MainVC: StartNewTalkVCDelegate {
    func didCreateNewTalkGroupWithId(_ id: Int, in startNewTalkVC: StartNewTalkVC) {
        self.showIncomingVCWithTalkGroupId(id)
    }
}

// MARK: IncomingVC Delegate
extension MainVC: IncomingVCDelegate {
    func reloadListTalkGroup() {
        hud.setDefaultMaskType(.clear)
        self.presenter.resetListTalkGroup {
            hud.setDefaultMaskType(.none)
        }
    }
    
    func deletedMemberFromInComing(groupID: Int, userId: Int) {
        deleteMemberInTalkgroup(talkID: groupID, userId: userId)
    }
}

// MARK: Talk Detail Delegate
extension MainVC: TalkDetailVCDelegate {
    func leaveTalkGroup(id: Int) {
        deleteCellForTalkGroup(id: id)
    }
    
    func deleteTalkGroup(id: Int) {
        deleteCellForTalkGroup(id: id)
    }
    
    func updatedTalkGroup(talk: TalkGroup) {
        if let index = self.presenter.talkGroups.index(where: {$0.id == talk.id}) {
            let indexPath = IndexPath(row: index, section: 0)
            
            presenter.talkGroups[index] = talk
            tableView.reloadRows(at: [indexPath], with: .fade)
        }
    }
    
    func deletedMemberFromDetail(talkID: Int, userId: Int) {
        deleteMemberInTalkgroup(talkID: talkID, userId: userId)
    }
    
    func willReloadListTalk() {
        presenter.resetListTalkGroup()
    }
}
// MARK: ChatVC Delegate
extension MainVC: ChatVCDelegate {
    func chatVCDismissed(shouldShowAudioLogMessages: Bool) {
    }
}
// MARK: Handle for remove, banish Member
extension MainVC {
    func deleteMemberInTalkgroup(talkID: Int, userId: Int) {
        guard let index = self.presenter.talkGroups.index(where: {$0.id == talkID}) else {
            return
        }
        
        // Remove talk group and reload if current user is removed from it
        if userId == User.getProfile()?.id {
            self.presenter.talkGroups.remove(at: index)
            self.tableView.reloadData()
            return
        }
        
        let talkGroup = self.presenter.talkGroups[index]
        guard let memberIndex = talkGroup.allUsers.index(where: {$0.id == userId}) else {
            return
        }
        _ = talkGroup.allUsers.remove(at: memberIndex)
        talkGroup.joinUsersCount -= 1
        if let meetingIndex = talkGroup.meetingUsers.index(where: {$0.id == userId}) {
            talkGroup.meetingUserCount -= 1
            talkGroup.meetingUsers.remove(at: meetingIndex)
        }
        
        print("Talk: \(talkGroup.name) total: \(talkGroup.joinUsersCount) meeting: \(talkGroup.meetingUserCount)")
        
        let indexPath = IndexPath(row: index, section: 0)
        tableView.beginUpdates()
        tableView.reloadRows(at: [indexPath], with: .fade)
        tableView.endUpdates()
    }
}

// MARK: UISearchBar Delegate
extension MainVC: UISearchBarDelegate {
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        if searchBar.text?.isEmpty == true || searchBar.text == nil {
            self.presenter.isSearching = false
            self.updateView()
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        self.presenter.isSearching = true
        return true
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.searchTalkGroup), object: nil)
        
        if searchText.isEmpty {
            self.presenter.isSearching = false
            self.presenter.searchText = ""
            self.tableView.reloadData()
            return
        }
        self.presenter.searchHasResponed = false
        self.presenter.hasMoreSearchPage = false

        self.perform(#selector(self.searchTalkGroup), with: nil, afterDelay: 0.5)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
    }
}
