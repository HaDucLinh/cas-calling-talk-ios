//
//  BaseViewController.swift
//  CallingTalk
//
//  Created by Van Trung on 1/15/19.
//  Copyright © 2019 Van Trung. All rights reserved.
//

import UIKit
import SwiftEntryKit

protocol BaseViewProtocol: NSObjectProtocol {
    func showConfirmAlertWithMessage(_ message: String, completion: AlertCompletionHandle?)
}
typealias AlertCompletionHandle = (_ success: Bool) -> Void

class BaseViewController: UIViewController{
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.localizeString()
        // self.setupNavigationBar()
        // self.hiddenNavigationBar(isHidden: false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    // Overide this func to set text for all UI in viewcontroller
    func localizeString(){
    }
    
    // Set hide navigation bar
    
    func hiddenNavigationBar(isHidden: Bool){
        self.navigationController?.setNavigationBarHidden(isHidden, animated: false)
    }
    
    // Endeditting
    
    @objc func endEditting(){
        self.view.endEditing(true)
    }
    
    /// show alert message with message
    ///
    /// - Parameters:
    ///   - message: message to show
    ///   - handler: handle OK button clicked
    func showConfirmAlertWithMessage(_ message: String, completion: AlertCompletionHandle?) {
        let titleAttributeText = NSMutableAttributedString(string: message, attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16)])
        let alert = UIAlertController(title: "", message: "", preferredStyle: .alert)
        alert.setValue(titleAttributeText, forKey: "attributedMessage")
        
        let cancelTitle = NSLocalizedString("cancel_button_title", comment: "")
        let cancelAction = UIAlertAction(title: cancelTitle, style: .default) { ( _) -> Void in
            completion?(false)
        }
        alert.addAction(cancelAction)

        let okTitle = NSLocalizedString("ok_button_title", comment: "")
        let okAction = UIAlertAction(title: okTitle, style: .cancel) { ( _) -> Void in
            completion?(true)
        }
        alert.addAction(okAction)
        present(alert, animated: true, completion: nil)
    }
    
    /// show error alert with message
    ///
    /// - Parameters:
    ///   - message: message to show
    ///   - completion: handle OK button clicked
    func showErrorAlertWithMesseage(_ message: String, completion: @escaping () -> Void) {
        var attributes = EKAttributes.centerFloat
        
        // set type for alert
        attributes.windowLevel = .alerts
        attributes.hapticFeedbackType = .error
        
        // set interaction for alert
        attributes.screenInteraction = .absorbTouches
        attributes.entryInteraction = .absorbTouches
        attributes.scroll = .disabled
        
        // set duration for displaying
        attributes.displayDuration = .infinity
        
        // set color
        attributes.entryBackground = .color(color: UIColor.white)
        attributes.screenBackground = .color(color: UIColor.black.withAlphaComponent(0.23))
        
        // set corner & border
        attributes.roundCorners = .all(radius: 12.0)
        attributes.border = .value(color: UIColor.ct.grayBorderColor, width: 1.0)
        
        // set animation
        attributes.entranceAnimation = .init(fade: .init(from: 0, to: 1, duration: 0.3))
        attributes.exitAnimation = .init(fade: .init(from: 1, to: 0, duration: 0.2))
        attributes.positionConstraints.size.width = .ratio(value: 0.8)
        
        let title = EKProperty.LabelContent(text: "\n\(message)\n", style: .init(font: UIColor.ct.font(.NotoSansCJKjp, type: .bold, size: 15), color: .black, alignment: .center))
        let description = EKProperty.LabelContent(text: "", style: .init(font: UIColor.ct.font(.NotoSansCJKjp, type: .regular, size: 16), color: .black, alignment: .center))
        let okButtonLabelStyle = EKProperty.LabelStyle(font: UIColor.ct.font(.NotoSansCJKjp, type: .regular, size: 16), color: UIColor.ct.okAlertButtonColor)
        let okButtonLabel = EKProperty.LabelContent(text: "OK", style: okButtonLabelStyle)
        let okButton = EKProperty.ButtonContent(label: okButtonLabel, backgroundColor: .clear, highlightedBackgroundColor: UIColor.ct.grayBorderColor.withAlphaComponent(0.3)) {
            completion()
            SwiftEntryKit.dismiss()
        }
        
        let simpleMessage = EKSimpleMessage(title: title, description: description)
        let buttonsBarContent = EKProperty.ButtonBarContent(with: okButton, separatorColor: UIColor.ct.grayBorderColor, expandAnimatedly: true)
        let alertMessage = EKAlertMessage(simpleMessage: simpleMessage, buttonBarContent: buttonsBarContent)
        let contentView = EKAlertMessageView(with: alertMessage)
        
        SwiftEntryKit.display(entry: contentView, using: attributes)
    }
    
    fileprivate
    func setupNavigationBar() {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.barTintColor = .red

        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.titleTextAttributes = [
            .font: UIFont.ct.font(.NotoSansCJKjp, type: .bold, size: 16.0),
            .foregroundColor: UIColor.white
        ]
    }
    
}
