//
//  BaseViewController.swift
//  CallingTalk
//
//  Created by Van Trung on 1/15/19.
//  Copyright © 2019 Van Trung. All rights reserved.
//

import UIKit
import Reachability
import Swinject
import ObjectMapper

protocol BaseViewProtocol: NSObjectProtocol {
    func showConfirmAlertWithMessage(_ message: String, completion: @escaping (_ success: Bool) -> Void)
    func showErrorAlertWithError(_ error: Error, completion: ((_ errorCode: Int?) -> Void)?)
}

struct NumberOfUnreadMessage {
    var talkGroupId: Int
    var number: Int?
}

typealias KickedCompletion = (_ confirm: Bool) -> Void
class BaseViewController: UIViewController {
    fileprivate var reachability: Reachability?
    fileprivate var talkConnection: TalkConnectionAlert?
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        let systemTheme = Container.default.resolve(SystemThemeable.self)
        switch systemTheme!.getCurrentMode() {
        case ThemeMode.normal:
            return .lightContent
        case ThemeMode.omiseno:
            return .default
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.localizeString()
        
        // Add listener
        NotificationCenter.default.addObserver(self, selector: #selector(handleNewMessageNotification(_:)), name: NSNotification.Name(rawValue: kNewMessageEvent), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleLocalMarkAsReadNotification(_:)), name: NSNotification.Name(rawValue: kLocalMarkMessageAsReadEvent), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleDeleteOrganizationNotification(_:)), name: NSNotification.Name(rawValue: kDeleteOrganizationEvent), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleKickedFromChannel(notification:)), name: NSNotification.Name(rawValue: kKickedFromChannelEvent), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleUpdateOrganizations(notification:)), name: NSNotification.Name(rawValue: kUpdateOrganizationsEvent), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleDeleteChannel(notification:)), name: NSNotification.Name(rawValue: kDeleteChannelEvent), object: nil)
    }
    
    deinit {
        self.reachability?.stopNotifier()
        self.reachability = nil
        NotificationCenter.default.removeObserver(self, name: .reachabilityChanged, object: self.reachability)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kNewMessageEvent), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kLocalMarkMessageAsReadEvent), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kDeleteOrganizationEvent), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kKickedFromChannelEvent), object: nil)
    }
    
    public func registerReachability() {
        self.reachability = Reachability()
        NotificationCenter.default.addObserver(self, selector: #selector(reachabilityChanged(note:)), name: .reachabilityChanged, object: self.reachability)
        do {
            try self.reachability?.startNotifier()
        } catch {
            print("Could not start reachability notifier")
        }
    }
    
    @objc func reachabilityChanged(note: Notification) {
        guard let reachability =  note.object as? Reachability else { return }
        switch reachability.connection {
        case .none:
            self.talkConnection = TalkConnectionAlert.loadFromNib()
            self.talkConnection?.show()
        default:
            if let talkConnection = self.talkConnection {
                talkConnection.dismiss()
            }
        }
    }
    // Overide this func to update number of unread message
    func notifyForUnreadMessage(_ isMention: Bool, of talkGroupId: Int) { }
    func notifyForMarkAsReadMessage(of talkGroupId: Int) { }
    func notifyForNewMessage(_ message: Message) { }
    
    // Overide this func to set text for all UI in viewcontroller
    func localizeString() {
    }
    
    // Set hide navigation bar
    
    func hiddenNavigationBar(isHidden: Bool){
        self.navigationController?.setNavigationBarHidden(isHidden, animated: false)
    }
    
    // Endeditting
    
    @objc func endEditting(){
        self.view.endEditing(true)
    }
    
    /// show alert message with message
    ///
    /// - Parameters:
    ///   - message: message to show
    ///   - handler: handle OK button clicked
    func showConfirmAlertWithMessage(_ message: String, completion:  @escaping (_ success: Bool) -> Void) {
        let titleAttributeText = NSMutableAttributedString(string: message, attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16)])
        let alert = UIAlertController(title: "", message: "", preferredStyle: .alert)
        alert.setValue(titleAttributeText, forKey: "attributedMessage")
        
        let cancelTitle = NSLocalizedString("cancel_button_title", comment: "")
        let cancelAction = UIAlertAction(title: cancelTitle, style: .default) { ( _) -> Void in
            completion(false)
        }
        alert.addAction(cancelAction)
        
        let okTitle = NSLocalizedString("ok_button_title", comment: "")
        let okAction = UIAlertAction(title: okTitle, style: .cancel) { ( _) -> Void in
            completion(true)
        }
        alert.addAction(okAction)
        present(alert, animated: true, completion: nil)
    }
    
    /// show error alert with message
    ///
    /// WARNING: if error is returned from server, please update status code to check unauthorized's error
    ///
    /// - Parameters:
    ///   - message: message to show
    ///   - completion: handle OK button clicked
    public func showErrorAlertWithError(_ error: Error, completion: ((_ errorCode: Int?) -> Void)?) {
        let networkErrorAlertView = NetworkErrorAlertView(error: error)
        networkErrorAlertView.show { (errorCode) in
            if  let errorCode = errorCode,
                let statusCode = StatusCode(rawValue: errorCode),
                statusCode == .unauthorized
            {
                // Post notification to release meeting if exist
                NotificationCenter.default.post(name: Notification.Name.ReleaseChatNotification, object: nil)
                
                // Disconnect Socket
                SocketIOManager.sharedInstance.disconnectSocket()
                
                // Clear user's profile & token id
                Helpers.clearProfile()
                GlobalVariable.didComeToIncomingVC = false
                
                AppDelegate.share()?.showLoginVC()
            }
            else {
                completion?(errorCode)
            }
        }
    }
    
    public func showKickOutAlertView(title: String, completion: KickedCompletion?) {
        let titleAttributeText = NSMutableAttributedString(string: title, attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16)])
        let alert = UIAlertController(title: "", message: "", preferredStyle: .alert)
        alert.setValue(titleAttributeText, forKey: "attributedMessage")
        
        let okTitle = NSLocalizedString("ok_button_title", comment: "")
        let okAction = UIAlertAction(title: okTitle, style: .cancel) { ( _) -> Void in
            completion?(true)
        }
        alert.addAction(okAction)
        present(alert, animated: true, completion: nil)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
            completion?(true)
            alert.dismiss(animated: true, completion: nil)
        }
    }
}

// MARK: - Receiving Notification

extension BaseViewController {
    @objc
    fileprivate func handleNewMessageNotification(_ notification: Notification) {
        guard let dict = notification.object as? JSObject else { return }
        guard let message = Mapper<Message>().map(JSON: dict) else { return }
        guard let userId = User.getProfile()?.id else { return }

        // Notify for new message
        self.notifyForNewMessage(message)
        
        // Notify for new unread message
        if message.owner?.id != userId {
            let isMentionedMessage = (Array(message.mentionIds).contains(userId) || message.isMentionAll)
            self.notifyForUnreadMessage(isMentionedMessage, of: message.channelId)
        }
    }
    
    @objc
    fileprivate func handleDeleteOrganizationNotification(_ notification: Notification) {
        let error = NSError(code: StatusCode.unauthorized.rawValue, message: NSLocalizedString("delete_organization_message", comment: ""))
        self.showErrorAlertWithError(error, completion: nil)
    }
    
    @objc
    fileprivate func handleLocalMarkAsReadNotification(_ notification: Notification) {
        guard let channelId = notification.object as? Int else { return }
        self.notifyForMarkAsReadMessage(of: channelId)
    }
}
