//
//  BaseVC+NotificationHandler.swift
//  CallingTalk
//
//  Created by Vu Tran on 5/31/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import Foundation

extension BaseViewController {
    @objc func handleKickedFromChannel(notification: Notification) {
        guard let topViewController = UIApplication.topViewController() as? BaseViewController else { return }
        
        let topVCIsTalkDetailVC = topViewController is TalkDetailVC
        let topVCIsEditTalkVC = topViewController is EditTalkVC
        let topVCIsInComingVC = topViewController is IncomingVC
        let topVCIsChatVC = topViewController is ChatVC

        if topVCIsTalkDetailVC || topVCIsEditTalkVC || topVCIsInComingVC || topVCIsChatVC || GlobalVariable.didComeToIncomingVC {
            guard let object = notification.object as? JSObject,
                let channelId = object["channelId"] as? Int,
                let userId = object["userId"] as? Int,
                let currentUserId = User.getProfile()?.id else { return }
            
            if let vc = AppDelegate.share()?.window?.rootViewController?.presentedViewController as? IncomingVC {
                if vc.presenter != nil && vc.presenter.talkGroupId == channelId {
                    if userId == currentUserId {
                        vc.presenter = nil
                        BPHeadsetManager.shared.disconnectHeadset()
                        let kickOutMessage = NSLocalizedString("kicked_from_channel_message", comment: "")
                        self.showKickOutAlertView(title: kickOutMessage) { (_) in
                            AppDelegate.share()?.showHomeTabbar()
                        }
                    } else {
                        vc.deletedMemberInTalkGroup(talkID: channelId, userId: userId, shouldShowToast: false)
                    }
                }
            }
            
            if let vc = topViewController as? ChatVC {
                if (vc.chatPresenter.talkGroup?.id == channelId) && (userId == currentUserId) {
                    vc.releaseStatusBarView()
                    let kickOutMessage = NSLocalizedString("kicked_from_channel_message", comment: "")
                    self.showKickOutAlertView(title: kickOutMessage) { (_) in
                        AppDelegate.share()?.showHomeTabbar()
                    }
                }
            }
        }
    }
    
    @objc func handleUpdateOrganizations(notification: Notification) {
        guard let object = notification.object as? JSObject,
            let organizationIds = object["organizationIds"] as? [Int],
            let topViewController = UIApplication.topViewController() as? BaseViewController else { return }

        if let incomingVC = topViewController as? IncomingVC {
            if let orgId = incomingVC.presenter.talkGroup.organization?.id, organizationIds.contains(orgId) {
                incomingVC.presenter.handleTalkGroupChanged()
            }
        } else if let talkDetailsVC = topViewController as? TalkDetailVC {
            if let orgId = talkDetailsVC.talkDetailPresenter.talkGroup?.organization?.id, organizationIds.contains(orgId) {
                talkDetailsVC.talkDetailPresenter.handleTalkGroupChanged()
            }
        } else if let editTalkVC = topViewController as? EditTalkVC {
            if let orgId = editTalkVC.editTalkPresenter.talkGroup?.organization?.id, organizationIds.contains(orgId) {
                editTalkVC.editTalkPresenter.handleTalkChanged()
            }
        }
    }
    
    @objc func handleDeleteChannel(notification: Notification) {
        guard let topViewController = UIApplication.topViewController() as? BaseViewController else { return }
        guard let object = notification.object as? JSObject,
            let channelId = object["channelId"] as? Int,
            let channelName = object["channelName"] as? String else {
            return
        }

        if let vc = topViewController as? MainVC {
            let deletedChannel = NSLocalizedString("channel_deleted_message", comment: "")
            let deleteTitle = String.init(format: deletedChannel, channelName)
            self.showKickOutAlertView(title: deleteTitle) { (_) in
                vc.reloadListTalkGroup()
            }
            return
        }
        
        let topVCIsTalkDetailVC = topViewController is TalkDetailVC
        let topVCIsEditTalkVC = topViewController is EditTalkVC
        let topVCIsInComingVC = topViewController is IncomingVC
        let topVCIsChatVC = topViewController is ChatVC
        
        if topVCIsTalkDetailVC || topVCIsEditTalkVC || topVCIsInComingVC || topVCIsChatVC || GlobalVariable.didComeToIncomingVC {
            if let vc = AppDelegate.share()?.window?.rootViewController?.presentedViewController as? IncomingVC {
                if vc.presenter != nil && vc.presenter.talkGroupId == channelId {
                    vc.presenter = nil
                    BPHeadsetManager.shared.disconnectHeadset()
                    let deletedChannel = NSLocalizedString("channel_deleted_message", comment: "")
                    let deleteTitle = String.init(format: deletedChannel, channelName)
                    self.showAlertAndBackToHome(title: deleteTitle)
                }
                return
            }
            
            if let vc = topViewController as? ChatVC {
                if vc.chatPresenter.talkGroup?.id == channelId {
                    let deletedChannel = NSLocalizedString("channel_deleted_message", comment: "")
                    let deleteTitle = String.init(format: deletedChannel, channelName)
                    self.showAlertAndBackToHome(title: deleteTitle)
                }
                return
            }
            
            if let vc = topViewController as? TalkDetailVC {
                if vc.talkDetailPresenter.talkGroupId == channelId {
                    let deletedChannel = NSLocalizedString("channel_deleted_message", comment: "")
                    let deleteTitle = String.init(format: deletedChannel, channelName)
                    self.showAlertAndBackToHome(title: deleteTitle)
                }
                return
            }
            
            if let vc = topViewController as? EditTalkVC {
                if vc.editTalkPresenter.talkGroup?.id == channelId {
                    let deletedChannel = NSLocalizedString("channel_deleted_message", comment: "")
                    let deleteTitle = String.init(format: deletedChannel, channelName)
                    self.showAlertAndBackToHome(title: deleteTitle)
                }
                return
            }
        }
    }
    
    fileprivate func showAlertAndBackToHome(title: String) {
        self.showKickOutAlertView(title: title) { (_) in
            AppDelegate.share()?.showHomeTabbar()
        }
    }
}
