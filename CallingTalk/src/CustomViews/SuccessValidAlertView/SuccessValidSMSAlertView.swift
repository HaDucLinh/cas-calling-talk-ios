//
//  SuccessValidSMSAlertView.swift
//  CallingTalk
//
//  Created by Toof on 2/14/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit

class SuccessValidSMSAlertView: UIView {
    
    // MARK: Properties
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var containView: UIView!
    
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var messageLabel: UILabel!
    
    var image: String
    var message: String
    
    // MARK: Init
    
    required init?(coder aDecoder: NSCoder) {
        self.image = ""
        self.message = ""
        
        super.init(coder: aDecoder)
    }
    
    init(image: String, message: String) {
        self.image = image
        self.message = message
        
        super.init(frame: UIScreen.main.bounds)
        self.setup()
    }
    
    fileprivate func setup() {
        self.initFromNIB()
        
        self.contentView.frame = self.bounds
        self.contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        self.contentView.backgroundColor = UIColor.white.withAlphaComponent(0.0)
        
        self.containView.layer.cornerRadius = 20.0
        self.containView.layer.shadowColor = UIColor.black.withAlphaComponent(0.2).cgColor
        self.containView.layer.shadowOffset = CGSize.zero
        self.containView.layer.shadowRadius = 10.0
        self.containView.layer.shadowOpacity = 1.0
        
        self.messageLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .bold, size: 16.0)
        self.messageLabel.textColor = .black
        
        self.iconImageView.image = UIImage(named: self.image)
        self.messageLabel.text = self.message
    }
    
    fileprivate func initFromNIB() {
        Bundle.main.loadNibNamed(String(describing: SuccessValidSMSAlertView.self), owner: self, options: nil)
        self.addSubview(self.contentView)
        self.backgroundColor = UIColor.white.withAlphaComponent(0)
    }
    
    public func show(completion: @escaping () -> Void) {
        let topWindow = UIApplication.shared.windows[0]
        for view in topWindow.subviews {
            if view is SuccessValidSMSAlertView {
                view.removeFromSuperview()
            }
        }
        
        topWindow.addSubview(self)
        
        self.alpha = 0.0
        UIView.animate(withDuration: 0.3, animations: { [weak self] in
            guard let `self` = self else { return }
            self.alpha = 1.0
        }) { (isFinish) in
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: { [weak self] in
                guard let `self` = self else { return }
                self.dismiss(completion: completion)
            })
        }
    }
    
    fileprivate func dismiss(completion: @escaping () -> Void) {
        UIView.animate(withDuration: 0.3, animations: { [weak self] in
            guard let `self` = self else { return }
            self.alpha = 0.0
        }) { (completed) -> Void in
            if completed {
                self.removeFromSuperview()
                completion()
            }
        }
    }
    
}

