//
//  NetworkErrorAlertView.swift
//  CallingTalk
//
//  Created by Toof on 3/29/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit

class NetworkErrorAlertView: UIView {

    // MARK: Properties
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var containView: UIView!
    
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var okButton: UIButton!
    
    fileprivate var dismissBlock: ((_ errorCode: Int?) -> Void)?
    
    var error: Error
    
    // MARK: Init
    
    required init?(coder aDecoder: NSCoder) {
        self.error = NSError()
        
        super.init(coder: aDecoder)
    }
    
    init(error: Error) {
        self.error = error
        
        super.init(frame: UIScreen.main.bounds)
        self.setup()
    }
    
    fileprivate func setup() {
        self.initFromNIB()
        
        self.messageLabel.text = self.error.localizedDescription
        
        self.contentView.frame = self.bounds
        self.contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        self.contentView.backgroundColor = UIColor.white.withAlphaComponent(0.0)
        
        self.containView.layer.cornerRadius = 12.0
        self.containView.layer.shadowColor = UIColor.black.withAlphaComponent(0.14).cgColor
        self.containView.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        self.containView.layer.shadowRadius = 3.0
        self.containView.layer.shadowOpacity = 1.0
        
        self.messageLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .bold, size: 15.0)
        self.messageLabel.textColor = .black
        
        self.okButton.setTitle("OK", for: .normal)
        self.okButton.titleLabel?.font = UIFont.ct.font(.NotoSansCJKjp, type: .regular, size: 15.0)
        self.okButton.setTitleColor(UIColor.ct.highlightMessageTextColor, for: .normal)
        
        self.okButton.addTarget(self, action: #selector(okButtonPressed(_:)), for: .touchUpInside)
    }
    
    fileprivate func initFromNIB() {
        Bundle.main.loadNibNamed(String(describing: NetworkErrorAlertView.self), owner: self, options: nil)
        self.addSubview(self.contentView)
        self.backgroundColor = UIColor.white.withAlphaComponent(0)
    }
    
    public func show(completion: @escaping (_ errorCode: Int?) -> Void) {
        let topWindow = UIApplication.shared.windows[0]
        for view in topWindow.subviews {
            if view is NetworkErrorAlertView {
                view.removeFromSuperview()
            }
        }
        
        topWindow.addSubview(self)
        
        self.alpha = 0.0
        UIView.animate(withDuration: 0.3, animations: { [weak self] in
            guard let `self` = self else { return }
            self.alpha = 1.0
        })
        
        self.dismissBlock = completion
    }
    
    fileprivate func dismiss() {
        UIView.animate(withDuration: 0.3, animations: { [weak self] in
            guard let `self` = self else { return }
            self.alpha = 0.0
        }) { (completed) -> Void in
            self.removeFromSuperview()
        }
    }
    
}

// MARK: - Handle events

extension NetworkErrorAlertView {
    
    @objc fileprivate func okButtonPressed(_ sender: UIButton) {
        self.dismissBlock?(self.error.code)
        self.dismiss()
    }
    
}
