//
//  ConfirmLogoutView.swift
//  CallingTalk
//
//  Created by Vu Tran on 2/27/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import Foundation
import UIKit

class ConfirmLogoutView: UIView {
    
    // MARK: Properties
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet public weak var iconImageView: UIImageView!
    @IBOutlet weak var backgroundImageView: UIImageView!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    
    @IBOutlet weak var confirmButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var topConstraint: NSLayoutConstraint!

    var handleConfirmAction: (() -> Void)?
    
    open var backgroundImage: UIImage? {
        didSet {
            backgroundImageView.image = backgroundImage
        }
    }
    
    open var title: String = "" {
        didSet { titleLabel.text = title }
    }
    
    open var message: String = "" {
        didSet { messageLabel.text = message }
    }
   
    open var confirmButtonTitle: String = "" {
        didSet { confirmButton.setTitle(confirmButtonTitle, for: .normal) }
    }
    
    open var avatarImageName: String = "" {
        didSet { self.iconImageView.image = UIImage(named: self.avatarImageName) }
    }
    
    // MARK: Init
    
    fileprivate func setup() {
        self.frame = UIScreen.main.bounds
        self.backgroundColor = UIColor.black.withAlphaComponent(0.23)
        self.clipsToBounds = true
        self.contentView.corner = 20.0
        self.cancelButton.corner = 10.0
        self.confirmButton.corner = 10.0
        
        self.confirmButton.backgroundColor = .black
        self.cancelButton.border(color: .black, width: 1)
        let cancelTitle = NSLocalizedString("cancel_crop_button_text", comment: "")
        let confirmTitle = NSLocalizedString("Reset_button_text", comment: "")
        
        cancelButton.setTitle(cancelTitle, for: .normal)
        confirmButton.setTitle(confirmTitle, for: .normal)
        
        titleLabel.textColor = UIColor.ct.redKeyColor
        titleLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .bold, size: 20)
        messageLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .regular, size: 15)
        messageLabel.textColor = .black

        self.confirmButton.addTarget(self, action: #selector(confirmButtonPressed(_:)), for: .touchUpInside)
        self.cancelButton.addTarget(self, action: #selector(closeButtonPressed(_:)), for: .touchUpInside)
        self.closeButton.addTarget(self, action: #selector(closeButtonPressed(_:)), for: .touchUpInside)

        topConstraint.constant = 150 * GlobalVariable.ratioHeight
    }

    
    @objc fileprivate func confirmButtonPressed(_ sender: UIButton) {
        self.handleConfirmAction?()
        dismiss()
    }
    
    @objc fileprivate func closeButtonPressed(_ sender: UIButton) {
        dismiss()
    }
    
    class func loadFromNib() -> ConfirmLogoutView {
        let confirmView = ConfirmLogoutView.loadFromNibNamed(type: ConfirmLogoutView.self)!
        confirmView.setup()
        return confirmView
    }
    
    public func show(title: String?, message: String?, handleConfirmAction: @escaping () -> Void) {
        let topWindow = UIApplication.shared.windows[0]
        for view in topWindow.subviews {
            if view is ConfirmDeleteTalkView {
                view.removeFromSuperview()
            }
        }
        self.handleConfirmAction = handleConfirmAction
        self.titleLabel.text = title
        self.messageLabel.text = message
        
        self.alpha = 0.0
        topWindow.addSubview(self)
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.alpha = 1.0
        }) { (completed) -> Void in
        }
    }
    
    func dismiss() {
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.alpha = 0.0
        }) { (completed) -> Void in
            self.removeFromSuperview()
        }
    }
    
}

