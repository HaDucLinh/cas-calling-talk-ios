//
//  RoundCicleAnimation.swift
//  CallingTalk
//
//  Created by NeoLabx on 5/31/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit
@IBDesignable class RoundCicleAnimation: UIView {
    @IBInspectable public var innerRadius: CGFloat = 20.0
    @IBInspectable public var outerRadius: CGFloat = 97.0
    @IBInspectable public var numberOfCircle: Int = 10
    @IBInspectable public var baseColor: UIColor = UIColor.blue
    
    private var setOfViewCircle = [UIView]()
    private var maximumWidthAnimate: CGFloat = 0.0
    private var minimumWidthAnimate: CGFloat = 0.0
    private let padding:CGFloat = 0.0
    private var isLayoutSetup = false
    func setLayoutCompletedLoad() {
        isLayoutSetup = true
    }
    func resizeRoundCirleView(){
        if !isLayoutSetup { return }
        let nearItemSize = self.frame.size.width * 2/3
        self.resizeRoundCirleView(withInnerValue: nearItemSize, andOuterValue: self.frame.size.width)
    }
    func resizeRoundCirleView(withInnerValue: CGFloat, andOuterValue: CGFloat){
        self.innerRadius = withInnerValue
        self.outerRadius = andOuterValue
        self.initializeItem()
    }
    func initializeItem() {
        let numberOfViews = Int((outerRadius - innerRadius) / CGFloat(numberOfCircle))
        for i in 0...numberOfViews {
            let widthOfInitializeView = CGFloat(i) * self.frame.size.width / CGFloat(numberOfViews)
            let heightOfInitializeView = CGFloat(i) * self.frame.size.height / CGFloat(numberOfViews)
            var aView:UIView!
            if i < (setOfViewCircle.count - 1)  {
                aView = setOfViewCircle[i]
            } else {
                aView = UIView.init(frame: CGRect.zero)
                setOfViewCircle.append(aView)
                
            }
            let alphaComponent = CGFloat(i) / CGFloat(numberOfViews + 3)
            aView.backgroundColor = baseColor.withAlphaComponent(alphaComponent)
            aView.clipsToBounds = true
            self.addSubview(aView)
            aView.isHidden = true
            self.ajustToCenterOfParent(view:aView,
                                       toSizeWidth: widthOfInitializeView,
                                       toSizeHeight: heightOfInitializeView,
                                       andRadius: CGFloat(widthOfInitializeView / 2))
        }
        self.maximumWidthAnimate = self.superview!.frame.size.width
        self.minimumWidthAnimate = self.maximumWidthAnimate - 20.0
        /*
         Any time created it will be autoplay(..) -> hide(..)
         In order to showing with UI, please use showAnimation(...).
         */
//        self.hideAnimation()
        self.autoPlayAnimation()
    }
   
    private func autoPlayAnimation() {
        DispatchQueue.main.async {
            self.setOfViewCircle.enumerated().forEach { [weak self] (info) in
                guard let self = self else { return }
                let circleToAnimate = info.element
                if !(circleToAnimate.layer.animationKeys()?.isEmpty ?? true) {
                    return
                }
                circleToAnimate.layer.removeAllAnimations()
                UIView.animate(withDuration:  0.5,
                               delay: 0.1  * Double((info.offset)),
                               options: [.repeat,.beginFromCurrentState,.autoreverse],
                               animations: { [weak self] in
                                if let self = self {
                                    self.ajustToCenterOfParent(view: circleToAnimate,
                                                               toSizeWidth: self.maximumWidthAnimate,
                                                               toSizeHeight: self.maximumWidthAnimate,
                                                               andRadius: CGFloat(self.maximumWidthAnimate / 2))
                                }
                    },completion: { [weak self] finished in
                })
            }
        }
    }
    func showAnimation() {
        self.setOfViewCircle.forEach{ $0.isHidden = false }
    }
    func hideAnimation() {
        self.setOfViewCircle.forEach{ $0.isHidden = true }
    }
    private func ajustToCenterOfParent(view: UIView, toSizeWidth: CGFloat, toSizeHeight: CGFloat, andRadius: CGFloat){
        guard let parentView = view.superview else { return }

        let targetWidth = toSizeWidth - padding
        let targetHeight = toSizeHeight - padding
        let parentViewSize = parentView.frame.size
        view.frame = CGRect(x: (parentViewSize.width - targetWidth) / 2.0 ,
                            y: (parentViewSize.height - targetHeight) / 2.0,
                            width: targetWidth,
                            height: targetHeight)
        view.layer.cornerRadius = andRadius - (padding / CGFloat(2.0))
    }
}
