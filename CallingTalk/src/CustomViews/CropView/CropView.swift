//
//  CropView.swift
//  CallingTalk
//
//  Created by Vu Tran on 2/15/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.

import UIKit

class CircleView: UIView {
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        return nil
    }
}
@IBDesignable class CropView: UIView {
    fileprivate var scrollView: UIScrollView!
    fileprivate var sourceImageView: UIImageView!
    public var  sourceImage = UIImage()

    // MARK: Properties
    var limitRect: CGRect = CGRect.zero
    var croppedRect: CGRect = CGRect.zero
    let maskLayer: CAShapeLayer = CAShapeLayer()
    let boderLayer: CAShapeLayer = CAShapeLayer()
    
    var drawingRect: CGRect = CGRect.zero
    var croppedImageView: CircleView = CircleView()
    
    fileprivate let defaultContentInsetLeft: CGFloat = 10
    fileprivate let defaultContentInsetTop: CGFloat = 54.0
    fileprivate let defaultContentInsetBottom: CGFloat = 54.0
    fileprivate let defaultSizePointView: CGSize = CGSize(width: 80.0, height: 80.0)
    fileprivate let sizeResizeButtonView: CGSize = CGSize(width: 50.0, height: 50.0)
    private var currentScale: CGFloat = 1
    
    var maxSize: CGFloat {
        let max = self.bounds.width < self.bounds.height ? self.bounds.width : self.bounds.height
        return (max - 2 * defaultContentInsetLeft)
    }
    var minSize: CGFloat {
        return bounds.width * 0.3
    }
    
    private var defaultSize: CGFloat {
        let max = self.bounds.width < self.bounds.height ? self.bounds.width : self.bounds.height
        return (max - 2 * defaultContentInsetLeft - 50) / currentScale
    }

    // MARK: Int
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setupCropView() {
        // Set scrollview
        scrollView = UIScrollView(frame: self.bounds)
        scrollView.delegate = self
        
        sourceImageView = UIImageView(image: sourceImage)
        sourceImageView.frame = CGRect(x: 0, y: 0, width: sourceImage.size.width, height: sourceImage.size.height)
        scrollView.contentSize = sourceImage.size
        scrollView.addSubview(sourceImageView)
        self.addSubview(scrollView)

        // Set background for crop view
        self.backgroundColor = UIColor.white.withAlphaComponent(0.0)
        
        // 2. Calculate limit rect for original image
        self.calculateRectsForImageView()
        
        // 3. Create view to display cropped image
        self.croppedImageView.frame = self.croppedRect
        self.addSubview(self.croppedImageView)
        
        // 4. Mask layer
        self.maskLayer.fillRule = CAShapeLayerFillRule.evenOdd
        self.maskLayer.fillColor = UIColor.white.withAlphaComponent(0.5).cgColor
        self.layer.addSublayer(self.maskLayer)
        
        self.updateMask()
        
        // 5. Create resize points
        self.createResizeImages()
        self.updateResizePoints()
        
        updateMinZoomScaleForSize(self.bounds.size)
    }
    
    func createResizeImages() {
        let rect = CGRect(origin: .zero, size: self.defaultSizePointView)
        
        let resizeView = UIView(frame: rect)
        resizeView.tag = 4
        let resizeButtonLayer = CALayer()
        resizeButtonLayer.bounds = CGRect(origin: .zero, size: self.sizeResizeButtonView)
        resizeButtonLayer.position = resizeView.center
        resizeButtonLayer.contents = UIImage(named: "ic-panButton")?.cgImage
        resizeView.layer.addSublayer(resizeButtonLayer)
        self.addSubview(resizeView)
        let pan = UIPanGestureRecognizer(target: self, action: #selector(moveResizePointHandler(_:)))
        resizeView.addGestureRecognizer(pan)
    }
    
    // MARK:- Gesture handler
    
    @objc func moveResizePointHandler(_ gesture: UIPanGestureRecognizer) {
//        let inTopArea = gesture.location(in: self).x < bounds.size.width / 2
        let dx = gesture.translation(in: self).x
        var newSizeW = croppedImageView.frame.width + dx
        newSizeW = min(maxSize, newSizeW)
        newSizeW = max(minSize, newSizeW)
        currentScale = newSizeW / defaultSize
        
        let newSizeCropped = CGSize(width: newSizeW, height: newSizeW)
        self.croppedImageView.frame.size = newSizeCropped
        self.croppedImageView.center = CGPoint(x: self.center.x, y: (self.bounds.size.height - self.defaultContentInsetBottom + self.defaultContentInsetTop) / 2)
        
        self.updateResizePoints()
        self.updateMask()
        gesture.setTranslation(CGPoint.zero, in: self)
    }
    
    // MARK:- Crop Effect
    
    func calculateRectsForImageView() {
        // limit rect
        let max = self.bounds.width < self.bounds.height ? self.bounds.width : self.bounds.height
        self.limitRect = self.bounds
        
        if self.croppedRect == CGRect.zero {
            let width = max - self.defaultContentInsetLeft * 2
            self.croppedRect.origin = CGPoint(x: self.center.x - width / 2, y: (self.bounds.size.height - self.defaultContentInsetBottom + self.defaultContentInsetTop) / 2 - width / 2)
            self.croppedRect.size = CGSize(width: width, height: width)
        }
    }
    
    func updateResizePoints() {
        let rect = self.croppedImageView.frame
        let abc = 0.145 * rect.size.width
        let point: CGPoint = CGPoint(x: rect.maxX - abc, y: rect.maxY - abc)
        if let view: UIView = self.viewWithTag(4) {
            view.center = point
        }
    }
    
    func updateMask() {
        let path = UIBezierPath(rect: self.limitRect)
        let holePath = UIBezierPath.init(ovalIn: self.croppedImageView.frame)
        path.append(holePath)

        path.usesEvenOddFillRule = true
        
        self.maskLayer.path = path.cgPath
        maskLayer.strokeColor = UIColor.white.cgColor
        maskLayer.lineWidth = 3
        boderLayer.frame = maskLayer.bounds
    }
    
    func convertCroppedRect() -> CGRect {
        return self.croppedImageView.convert(self.croppedImageView.bounds, to: self.sourceImageView)
    }
    
    func convertCropViewToSupperView() -> CGRect {
        return self.croppedImageView.convert(self.croppedImageView.bounds, to: self)
    }
    
    func cropImage() -> UIImage? {
        let cropRectInSuperView = self.convertCropViewToSupperView()
        
        print("Image size: \(sourceImage.size.width), \(sourceImage.size.height)")
        print("Offset: ", scrollView.contentOffset)
    
        print("Zoom scale: ", scrollView.zoomScale)
        print("Crop rect x: \(cropRectInSuperView.origin.x) y: \(cropRectInSuperView.origin.y)")
        print("Width: \(cropRectInSuperView.size.width) Hight: \(cropRectInSuperView.size.height)")
        
        let cropRectWithScale = CGRect(
            x: cropRectInSuperView.origin.x / scrollView.zoomScale,
            y: cropRectInSuperView.origin.y / scrollView.zoomScale,
            width: cropRectInSuperView.size.width / scrollView.zoomScale,
            height: cropRectInSuperView.size.height / scrollView.zoomScale)
        
        print("Crop rect After x: \(cropRectWithScale.origin.x) y: \(cropRectWithScale.origin.y)")
        print("Width After: \(cropRectWithScale.size.width) Hight: \(cropRectWithScale.size.height)")
        
        let newOriginX = (scrollView.contentOffset.x / scrollView.zoomScale) + cropRectWithScale.origin.x
        let newOriginY = (scrollView.contentOffset.y / scrollView.zoomScale) + cropRectWithScale.origin.y
        
        let newCropRect = CGRect(
            x: newOriginX,
            y: newOriginY,
            width: cropRectWithScale.size.width,
            height: cropRectWithScale.size.height
        )
        
        print("--CropRect x: ", newCropRect.origin.x)
        print("--CropRect y: ", newCropRect.origin.y)
        print("--CropRect Width: ", newCropRect.size.width)
        print("--CropRect Hight: ", newCropRect.size.height)
    
        return sourceImage.croppedInRect(rect: newCropRect)
    }
    
    fileprivate func updateMinZoomScaleForSize(_ size: CGSize) {
        let widthScale = size.width / sourceImageView.bounds.width
        let heightScale = size.height / sourceImageView.bounds.height
        let minScale = max(widthScale, heightScale)
        
        scrollView.minimumZoomScale = minScale
        scrollView.zoomScale = minScale
    }
    
}
extension CropView: UIScrollViewDelegate {
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return sourceImageView
    }
    
    func scrollViewDidEndZooming(_ scrollView: UIScrollView, with view: UIView?, atScale scale: CGFloat) {
    }
}
