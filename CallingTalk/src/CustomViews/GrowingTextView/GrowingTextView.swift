//
//  GrowingTextView.swift
//  CallingTalk
//
//  Created by Toof on 6/5/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit

/// The type of the block which contains user defined actions that will run during the height change.
public typealias HeightChangeUserActionsBlockType = ((_ oldHeight: CGFloat, _ newHeight: CGFloat) -> Void)

/// The `GrowingTextViewDelegate` protocol extends the `UITextViewDelegate` protocol by providing a set of optional methods you can use to receive messages related to the change of the height of `GrowingTextView` objects.
@objc public protocol GrowingTextViewDelegate: UITextViewDelegate {
    ///
    /// Tells the delegate that the growing text view did change height.
    ///
    /// - Parameters:
    ///     - textView: The growing text view object that has changed the height.
    ///     - growingTextViewHeightBegin: CGFloat that identifies the start height of the growing text view.
    ///     - growingTextViewHeightEnd: CGFloat that identifies the end height of the growing text view.
    ///
    @objc optional func growingTextView(_ textView: GrowingTextView, didChangeHeightFrom growingTextViewHeightBegin: CGFloat, to growingTextViewHeightEnd: CGFloat)
    
    ///
    /// Tells the delegate that the growing text view will change height.
    ///
    /// - Parameters:
    ///     - textView: The growing text view object that will change the height.
    ///     - growingTextViewHeightBegin: CGFloat that identifies the start height of the growing text view.
    ///     - growingTextViewHeightEnd: CGFloat that identifies the end height of the growing text view.
    ///
    @objc optional func growingTextView(_ textView: GrowingTextView, willChangeHeightFrom growingTextViewHeightBegin: CGFloat, to growingTextViewHeightEnd: CGFloat)
}

/// A light-weight UITextView subclass that automatically grows and shrinks based on the size of user input and can be constrained by maximum and minimum number of lines.
@IBDesignable open class GrowingTextView: PlaceholderTextView {
    
    // MARK: - Tagging Properties
    var symbol: String = "@"
    var tagableList: [User] = []
    public var taggedList: [TaggingModel] = []
    weak var dataSource: TaggingDataSource?
    var hasMentionAll = false
    
    private var currentTaggingText: String? {
        didSet {
            guard let currentTaggingText = currentTaggingText, !tagableList.isEmpty else {
                hasMentionAll = false
                dataSource?.tagging(self, didChangedTagableList: [])
                return
            }
            if currentTaggingText == symbol {
                hasMentionAll = false
                dataSource?.tagging(self, didChangedTagableList: tagableList)
                return
            }
            let matchedTagableList = tagableList.filter {
                $0.name.lowercased().contains(currentTaggingText.lowercased())
            }
            if currentTaggingText.lowercased() == "al" || currentTaggingText.lowercased() == "all" {
                hasMentionAll = true
            } else {
                hasMentionAll = false
            }
            dataSource?.tagging(self, didChangedTagableList: matchedTagableList)
        }
    }
    private var currentTaggingRange: NSRange?
    private var tagRegex: NSRegularExpression! {return try! NSRegularExpression(pattern: "\(symbol)([^\\s\\K]+)")}
    
    open var defaultAttributes: [NSAttributedString.Key: Any] = {
        return [NSAttributedString.Key.foregroundColor: UIColor.black,
                NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 15),
                NSAttributedString.Key.underlineStyle: NSNumber(value: 0)]
    }()
    open var symbolAttributes: [NSAttributedString.Key: Any] = {
        return [NSAttributedString.Key.foregroundColor: UIColor.black,
                NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 15),
                NSAttributedString.Key.underlineStyle: NSNumber(value: 0)]
    }()
    open var taggedAttributes: [NSAttributedString.Key: Any] = {return [NSAttributedString.Key.underlineStyle: NSNumber(value: 1)]}()
    
    // MARK: - Private Properties
    private var calculatedHeight: CGFloat {
        let calculationTextStorage: NSTextStorage?
        if let attributedText = attributedText, attributedText.length > 0 {
            calculationTextStorage = NSTextStorage(attributedString: attributedText)
        } else if let attributedPlaceholder = attributedPlaceholder, attributedPlaceholder.length > 0 {
            calculationTextStorage = NSTextStorage(attributedString: attributedPlaceholder)
        } else {
            calculationTextStorage = nil
        }
        var height: CGFloat
        if let _calculationTextStorage = calculationTextStorage {
            
            _calculationTextStorage.addLayoutManager(calculationLayoutManager)
            
            calculationTextContainer.lineFragmentPadding = textContainer.lineFragmentPadding
            calculationTextContainer.size = CGSize(width: textContainer.size.width, height: 0.0)
            
            calculationLayoutManager.ensureLayout(for: calculationTextContainer)
            
            height = ceil(calculationLayoutManager.usedRect(for: calculationTextContainer).height + contentInset.top + contentInset.bottom + textContainerInset.top + textContainerInset.bottom)
            
            if height < minHeight {
                height = minHeight
            } else if height > maxHeight {
                height = maxHeight
            }
        } else {
            height = minHeight
        }
        return height
    }
    
    private let calculationLayoutManager = NSLayoutManager()
    
    private let calculationTextContainer = NSTextContainer()
    
    private weak var heightConstraint: NSLayoutConstraint?
    
    private var maxHeight: CGFloat { return heightForNumberOfLines(maximumNumberOfLines) }
    
    private var minHeight: CGFloat { return heightForNumberOfLines(minimumNumberOfLines) }
    
    // MARK: - Open Properties
    
    /// A Boolean value that determines whether the animation of the height change is enabled. Default value is `true`.
    @IBInspectable open var animateHeightChange: Bool = true
    
    /// The receiver's delegate.
    open weak var growingTextViewDelegate: GrowingTextViewDelegate? { didSet { delegate = growingTextViewDelegate } }
    
    /// The duration of the animation of the height change. The default value is `0.35`.
    @IBInspectable open var heightChangeAnimationDuration: Double = 0.35
    
    /// The block which contains user defined actions that will run during the height change.
    open var heightChangeUserActionsBlock: HeightChangeUserActionsBlockType?
    
    /// The maximum number of lines before enabling scrolling. The default value is `5`.
    @IBInspectable open var maximumNumberOfLines: Int = 5 {
        didSet {
            if maximumNumberOfLines < minimumNumberOfLines {
                maximumNumberOfLines = minimumNumberOfLines
            }
            refreshHeightIfNeededAnimated(false)
        }
    }
    
    /// The minimum number of lines. The default value is `1`.
    @IBInspectable open var minimumNumberOfLines: Int = 1 {
        didSet {
            if minimumNumberOfLines < 1 {
                minimumNumberOfLines = 1
            } else if minimumNumberOfLines > maximumNumberOfLines {
                minimumNumberOfLines = maximumNumberOfLines
            }
            refreshHeightIfNeededAnimated(false)
        }
    }
    
    /// The current displayed number of lines. This value is calculated at run time.
    open var numberOfLines: Int {
        var numberOfLines = 0
        var index = 0
        var lineRange = NSRange()
        let numberOfGlyphs = layoutManager.numberOfGlyphs
        while index < numberOfGlyphs {
            if #available(iOS 9.0, *) {
                _ = layoutManager.lineFragmentRect(forGlyphAt: index, effectiveRange: &lineRange, withoutAdditionalLayout: true)
            } else {
                _ = layoutManager.lineFragmentRect(forGlyphAt: index, effectiveRange: &lineRange)
            }
            index = NSMaxRange(lineRange)
            numberOfLines += 1
        }
        return numberOfLines
    }
    
    // MARK: - Superclass Properties
    
    override open var attributedText: NSAttributedString! {
        didSet {
            superview?.layoutIfNeeded()
        }
    }
    
    override open var contentSize: CGSize {
        didSet {
            guard !oldValue.equalTo(contentSize) else {
                return
            }
            if window != nil && isFirstResponder {
                refreshHeightIfNeededAnimated(animateHeightChange)
            } else {
                refreshHeightIfNeededAnimated(false)
            }
        }
    }
    
    // MARK: - Object Lifecycle
    
    required public init?(coder aDecoder: NSCoder) {
        calculationLayoutManager.addTextContainer(calculationTextContainer)
        super.init(coder: aDecoder)
        commonInitializer()
    }
    
    override public init(frame: CGRect, textContainer: NSTextContainer?) {
        calculationLayoutManager.addTextContainer(calculationTextContainer)
        super.init(frame: frame, textContainer: textContainer)
        commonInitializer()
    }
    
    override open var intrinsicContentSize: CGSize {
        if heightConstraint != nil {
            return CGSize(width: UIView.noIntrinsicMetric, height: UIView.noIntrinsicMetric)
        } else {
            return CGSize(width: UIView.noIntrinsicMetric, height: calculatedHeight)
        }
    }
    
    // MARK: - Private API
    
    private func commonInitializer() {
        contentInset = UIEdgeInsets(top: 1.0, left: 0.0, bottom: 1.0, right: 0.0)
        scrollsToTop = false
        showsVerticalScrollIndicator = false
        
        for constraint in constraints {
            if constraint.firstAttribute == .height && constraint.relation == .equal {
                heightConstraint = constraint
                break
            }
        }
    }
    
    private func heightForNumberOfLines(_ numberOfLines: Int) -> CGFloat {
        var height = contentInset.top + contentInset.bottom + textContainerInset.top + textContainerInset.bottom
        
        var numberOfNonEmptyLines = 0
        var index = 0
        let numberOfGlyphs = calculationLayoutManager.numberOfGlyphs
        while index < numberOfGlyphs && numberOfNonEmptyLines < numberOfLines {
            var lineRange = NSRange()
            if #available(iOS 9.0, *) {
                height += calculationLayoutManager.lineFragmentRect(forGlyphAt: index, effectiveRange: &lineRange, withoutAdditionalLayout: true).height
            } else {
                height += calculationLayoutManager.lineFragmentRect(forGlyphAt: index, effectiveRange: &lineRange).height
            }
            index = NSMaxRange(lineRange)
            numberOfNonEmptyLines += 1
        }
        
        let numberOfEmptyLines = (numberOfLines - numberOfNonEmptyLines)
        if numberOfEmptyLines > 0 {
            let font = (self.typingAttributes[.font] as? UIFont) ?? self.font ?? UIFont.systemFont(ofSize: UIFont.systemFontSize)
            var lineHeight = font.lineHeight
            if let paragraphStyle = self.typingAttributes[.paragraphStyle] as? NSParagraphStyle {
                if paragraphStyle.lineHeightMultiple > 0.0 {
                    lineHeight *= paragraphStyle.lineHeightMultiple
                }
                if paragraphStyle.minimumLineHeight > 0.0, lineHeight < paragraphStyle.minimumLineHeight {
                    lineHeight = paragraphStyle.minimumLineHeight
                } else if paragraphStyle.maximumLineHeight > 0.0, lineHeight > paragraphStyle.maximumLineHeight {
                    lineHeight = paragraphStyle.maximumLineHeight
                }
                lineHeight += paragraphStyle.lineSpacing
            }
            height += lineHeight * CGFloat(numberOfEmptyLines)
        }
        
        return ceil(height)
    }
    
    private func refreshHeightIfNeededAnimated(_ animated: Bool) {
        let oldHeight = bounds.height
        let newHeight = calculatedHeight
        
        if oldHeight != newHeight {
            typealias HeightChangeSetHeightBlockType = ((_ oldHeight: CGFloat, _ newHeight: CGFloat) -> Void)
            let heightChangeSetHeightBlock: HeightChangeSetHeightBlockType = { (oldHeight: CGFloat, newHeight: CGFloat) -> Void in
                self.setHeight(newHeight)
                self.heightChangeUserActionsBlock?(oldHeight, newHeight)
                self.superview?.layoutIfNeeded()
            }
            typealias HeightChangeCompletionBlockType = ((_ oldHeight: CGFloat, _ newHeight: CGFloat) -> Void)
            let heightChangeCompletionBlock: HeightChangeCompletionBlockType = { (oldHeight: CGFloat, newHeight: CGFloat) -> Void in
                self.layoutManager.ensureLayout(for: self.textContainer)
                self.scrollToVisibleCaretIfNeeded()
                self.growingTextViewDelegate?.growingTextView?(self, didChangeHeightFrom: oldHeight, to: newHeight)
            }
            growingTextViewDelegate?.growingTextView?(self, willChangeHeightFrom: oldHeight, to: newHeight)
            if animated {
                UIView.animate(
                    withDuration: heightChangeAnimationDuration,
                    delay: 0.0,
                    options: [.allowUserInteraction, .beginFromCurrentState],
                    animations: { () -> Void in
                        heightChangeSetHeightBlock(oldHeight, newHeight)
                },
                    completion: { (finished: Bool) -> Void in
                        heightChangeCompletionBlock(oldHeight, newHeight)
                }
                )
            } else {
                heightChangeSetHeightBlock(oldHeight, newHeight)
                heightChangeCompletionBlock(oldHeight, newHeight)
            }
        } else {
            scrollToVisibleCaretIfNeeded()
        }
    }
    
    private func scrollRectToVisibleConsideringInsets(_ rect: CGRect) {
        let insets = UIEdgeInsets(top: contentInset.top + textContainerInset.top, left: contentInset.left + textContainerInset.left + textContainer.lineFragmentPadding, bottom: contentInset.bottom + textContainerInset.bottom, right: contentInset.right + textContainerInset.right)
        let visibleRect = bounds.inset(by: insets)
        
        guard !visibleRect.contains(rect) else {
            return
        }
        
        var contentOffset = self.contentOffset
        if rect.minY < visibleRect.minY {
            contentOffset.y = rect.minY - insets.top * 2
        } else {
            contentOffset.y = rect.maxY + insets.bottom * 2 - bounds.height
        }
        setContentOffset(contentOffset, animated: false)
    }
    
    private func scrollToVisibleCaretIfNeeded() {
        guard let textPosition = selectedTextRange?.end else {
            return
        }
        
        if textStorage.editedRange.location == NSNotFound && !isDragging && !isDecelerating {
            let caretRect = self.caretRect(for: textPosition)
            let caretCenterRect = CGRect(x: caretRect.midX, y: caretRect.midY, width: 0.0, height: 0.0)
            scrollRectToVisibleConsideringInsets(caretCenterRect)
        }
    }
    
    private func setHeight(_ height: CGFloat) {
        if let heightConstraint = self.heightConstraint {
            heightConstraint.constant = height
        } else if !constraints.isEmpty {
            invalidateIntrinsicContentSize()
            setNeedsLayout()
        } else {
            frame.size.height = height
        }
    }
}

extension GrowingTextView {
    // MARK: - Public methods
    func updateTaggedList(allText: String, tagUser: User) {
        guard let range = currentTaggingRange else {return}
        
        let origin = (allText as NSString).substring(with: range)
        let tag = tagFormat(tagUser.name)
        let replace = tag.appending(" ")
        let changed = (allText as NSString).replacingCharacters(in: range, with: replace)
        let tagRange = NSMakeRange(range.location, tag.utf16.count)
        
        taggedList.append(TaggingModel(user: tagUser, range: tagRange))
        for i in 0..<taggedList.count-1 {
            var location = taggedList[i].range.location
            let length = taggedList[i].range.length
            if location > tagRange.location {
                location += replace.count - origin.count
                taggedList[i].range = NSMakeRange(location, length)
            }
        }
        
        self.text = changed
        // updateAttributeText(selectedLocation: range.location+replace.count)
        dataSource?.tagging(self, didChangedTaggedList: taggedList)
    }
    
    // MARK: - private methods
    private func tagFormat(_ text: String) -> String {
        return symbol.appending(text)
    }
    
    public func tagging(textView: UITextView) {
        let selectedLocation = textView.selectedRange.location
        let taggingText = (textView.text as NSString).substring(with: NSMakeRange(0, selectedLocation))
        let space: Character = " "
        let lineBrak: Character = "\n"
        var tagable: Bool = false
        var characters: [Character] = []
        
        for char in Array(taggingText).reversed() {
            if char == symbol.first {
                characters.append(char)
                tagable = true
                break
            } else if char == space || char == lineBrak {
                tagable = false
                break
            }
            characters.append(char)
        }
        
        guard tagable else {
            currentTaggingRange = nil
            currentTaggingText = nil
            return
        }
        
        let data = matchedData(taggingCharacters: characters, selectedLocation: selectedLocation, taggingText: taggingText)
        currentTaggingRange = data.0
        currentTaggingText = data.1
    }
    
    public func matchedData(taggingCharacters: [Character], selectedLocation: Int, taggingText: String) -> (NSRange?, String?) {
        var matchedRange: NSRange?
        var matchedString: String?
        let tag = String(taggingCharacters.reversed())
        let textRange = NSMakeRange(selectedLocation-tag.count, tag.count)
        
        guard tag == symbol else {
            let matched = tagRegex.matches(in: taggingText, options: .reportCompletion, range: textRange)
            if matched.count > 0, let range = matched.last?.range {
                matchedRange = range
                matchedString = (taggingText as NSString).substring(with: range).replacingOccurrences(of: symbol, with: "")
            }
            return (matchedRange, matchedString)
        }
        
        matchedRange = textRange
        matchedString = symbol
        return (matchedRange, matchedString)
    }
    
    public func updateAttributeText(selectedLocation: Int) {
        let attributedString = NSMutableAttributedString(string: self.text)
        attributedString.addAttributes(defaultAttributes, range: NSMakeRange(0, self.text.utf16.count))
        taggedList.forEach { (model) in
            let symbolAttributesRange = NSMakeRange(model.range.location, symbol.count)
            let taggedAttributesRange = NSMakeRange(model.range.location+1, model.range.length-1)
            
            attributedString.addAttributes(symbolAttributes, range: symbolAttributesRange)
            attributedString.addAttributes(taggedAttributes, range: taggedAttributesRange)
        }
        
        self.attributedText = attributedString
        self.selectedRange = NSMakeRange(selectedLocation, 0)
    }

    public func updateTaggedList(range: NSRange, textCount: Int) {
        taggedList = taggedList.filter({ (model) -> Bool in
            if model.range.location < range.location && range.location < model.range.location+model.range.length {
                return false
            }
            if range.length > 0 {
                if range.location <= model.range.location && model.range.location < range.location+range.length {
                    return false
                }
            }
            return true
        })
        
        for i in 0..<taggedList.count {
            var location = taggedList[i].range.location
            let length = taggedList[i].range.length
            if location >= range.location {
                if range.length > 0 {
                    if textCount > 1 {
                        location += textCount - range.length
                    } else {
                        location -= range.length
                    }
                } else {
                    location += textCount
                }
                taggedList[i].range = NSMakeRange(location, length)
            }
        }
        
        currentTaggingText = nil
        dataSource?.tagging(self, didChangedTaggedList: taggedList)
    }
}

public struct TaggingModel {
    var user: User
    public var range: NSRange
}
protocol TaggingDataSource: class {
    func tagging(_ taggingTextView: GrowingTextView, didChangedTagableList tagableList: [User])
    func tagging(_ taggingTextView: GrowingTextView, didChangedTaggedList taggedList: [TaggingModel])
}

extension TaggingDataSource {
    func tagging(_ taggingTextView: GrowingTextView, didChangedTagableList tagableList: [User]) {return}
    func tagging(_ taggingTextView: GrowingTextView, didChangedTaggedList taggedList: [TaggingModel]) {return}
}
