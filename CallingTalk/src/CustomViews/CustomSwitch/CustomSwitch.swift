//
//  CustomSwitch.swift
//  CallingTalk
//
//  Created by Toof on 2/19/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit

@IBDesignable
final class CustomSwitch: UIControl {
    
    // MARK: Public properties
    
    @IBInspectable public var isOn: Bool = false
    
    public var animationDuration: Double = 0.5
    
    @IBInspectable public var tintBarColor: UIColor = UIColor(red: 128.0 / 255.0, green: 128.0 / 255.0, blue: 128.0 / 255.0, alpha: 0.8) {
        didSet {
            self.setupUI()
        }
    }
    
    @IBInspectable public var onTintThumbColor: UIColor = UIColor(red: 85.0 / 255.0, green: 204.0 / 255.0, blue: 204.0 / 255.0, alpha: 1) {
        didSet {
            self.setupUI()
        }
    }
    
    @IBInspectable public var offTintThumbColor: UIColor = UIColor(red: 255.0 / 255.0, green: 238.0 / 255.0, blue: 17.0 / 255.0, alpha: 1) {
        didSet {
            self.setupUI()
        }
    }
    
    @IBInspectable public var thumbMargin: CGFloat = 2.0 {
        didSet {
            self.layoutSubviews()
        }
    }
    
    // MARK: Private properties
    fileprivate var thumbView = CustomThumbView(frame: CGRect.zero)
    
    fileprivate var onPoint = CGPoint.zero
    fileprivate var offPoint = CGPoint.zero
    fileprivate var isAnimating = false
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.setupUI()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.setupUI()
    }
    
    override func beginTracking(_ touch: UITouch, with event: UIEvent?) -> Bool {
        super.beginTracking(touch, with: event)
        
        self.animate()
        
        return true
    }
    
    public
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.layer.cornerRadius = self.bounds.size.width * 0.5
        self.thumbView.backgroundColor = self.isOn ? self.onTintThumbColor : self.offTintThumbColor
        
        // thumb managment
        // get thumb size, if none set, use one from bounds
        let thumbSize = CGSize(width: self.bounds.size.width - self.thumbMargin * 2, height: self.bounds.size.width - self.thumbMargin * 2)
        let xPostition = (self.bounds.size.width - thumbSize.width) * 0.5
        
        self.onPoint = CGPoint(x: xPostition, y: self.bounds.size.height - thumbSize.height - self.thumbMargin)
        self.offPoint = CGPoint(x: xPostition, y: self.thumbMargin)
        
        self.thumbView.frame = CGRect(origin: self.isOn ? self.onPoint : self.offPoint, size: thumbSize)
        self.thumbView.layer.cornerRadius = thumbSize.width * 0.5
    }
    
}

// MARK: - Private methods

extension CustomSwitch {
    
    fileprivate
    func setupUI() {
        // clear self before configuration
        self.clear()
        
        self.clipsToBounds = false
        
        // configure thumb view
        self.thumbView.backgroundColor = self.isOn ? self.onTintThumbColor : self.offTintThumbColor
        self.thumbView.isUserInteractionEnabled = false
        
        // set background color
        self.backgroundColor = self.tintBarColor
        
        self.addSubview(self.thumbView)
    }
    
    fileprivate
    func clear() {
        for view in self.subviews {
            view.removeFromSuperview()
        }
    }
    
    public
    func setOn(on:Bool, animated:Bool) {
        switch animated {
        case true:
            self.animate(on: on)
        case false:
            self.isOn = on
            self.setupViewsOnAction()
            self.completeAction()
        }
    }
    
    fileprivate
    func animate(on:Bool? = nil) {
        self.isOn = on ?? !self.isOn
        self.isAnimating = true
        
        UIView.animate(withDuration: self.animationDuration, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.5, options: [UIView.AnimationOptions.curveEaseOut, UIView.AnimationOptions.beginFromCurrentState, UIView.AnimationOptions.allowUserInteraction], animations: {
            self.setupViewsOnAction()
        }, completion: { _ in
            self.completeAction()
        })
    }
    
    fileprivate
    func setupViewsOnAction() {
        self.thumbView.frame.origin.y = self.isOn ? self.onPoint.y : self.offPoint.y
        self.thumbView.backgroundColor = self.isOn ? self.onTintThumbColor : self.offTintThumbColor
    }
    
    fileprivate func completeAction() {
        self.isAnimating = false
        self.sendActions(for: UIControl.Event.valueChanged)
    }
    
}
