//
//  ChatStatusBarView.swift
//  CallingTalk
//
//  Created by Vu Tran on 4/5/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit

typealias ChatStatusBarViewCompletion = () -> Void
class ChatStatusBarView: UIView {
    lazy var textLabel: UILabel =  { [unowned self] in
        let label = UILabel()
        label.text = NSLocalizedString("Return_to_talk_text", comment: "")
        label.textColor = .white
        label.font = UIFont.ct.font(.NotoSansCJKjp, type: .bold, size: 12)
        label.sizeToFit()
        return label
    }()
    
    var button: UIButton!
    
    var completion: ChatStatusBarViewCompletion?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.setupUI()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupUI()
    }
    
    private func setupUI() {
        var height: CGFloat = 0
        if iPhoneX || iPhoneXSMax {
           height = 70
        } else {
            height = 30
        }
        self.frame = CGRect(x: 0, y: 0, width: GlobalVariable.screenWidth, height: height)
        backgroundColor = UIColor.ct.handFreeModeColor
        let label = self.textLabel
        label.frame.origin.y = height - 25
        label.center.x = self.center.x
        label.textAlignment = .center
        self.addSubview(self.textLabel)
        
        self.button = UIButton(frame: self.bounds)
        self.button.addTarget(self, action: #selector(buttonPressed(_:)), for: .touchUpInside)
        self.addSubview(self.button)
    }
    
    @objc
    fileprivate
    func buttonPressed(_ sender: UIButton) {
        self.completion?()
        self.dismiss()
    }
    
    public func show(completion: ChatStatusBarViewCompletion?) {
        self.completion = completion
        UIApplication.shared.statusBarView?.addSubview(self)
    }
    
    @objc public func dismiss() {
        self.removeFromSuperview()
    }
}
