//
//  MentionView.swift
//  CallingTalk
//
//  Created by Vu Tran on 3/11/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit

typealias MentionCompletion = (_ user: User) -> Void
class MentionView: UIView {
    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var contentView: UIView!
    @IBOutlet private weak var closeButton: UIButton!

    var completion: MentionCompletion?
    var hasMentionAll = false
    
    fileprivate var users: [User] = []
    fileprivate var frameV: CGRect = CGRect.zero
    fileprivate var originY: CGFloat = 0
    var keyboardHeight: CGFloat = 0
    var mentionText: String?
    
    private func setUpUI() {
        closeButton.addTarget(self, action: #selector(dismiss), for: .touchUpInside)
        
        self.contentView.backgroundColor = UIColor.ct.mentionBackgroundColor.withAlphaComponent(0.9)
        tableView.register(MentionCell.self)
        tableView.register(MentionAllCell.self)
        
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        // self.frame = frameV
        layoutIfNeeded()
    }
    
    func show(text: String?, users: [User],
              isUpdateFrame: Bool = false, completion: MentionCompletion?) {
      
        // self.frameV = frame
        self.mentionText = text
        self.completion = completion
        self.users = users
        self.tableView.reloadData()
    }
    
    class func loadFromNib(frame: CGRect) -> MentionView {
        let confirmView = MentionView.loadFromNibNamed(type: MentionView.self)!
        confirmView.frame = frame
        confirmView.frameV = frame
        confirmView.setUpUI()
        
        let topWindow = UIApplication.shared.windows[0]
        for view in topWindow.subviews {
            if view is MentionView {
                view.removeFromSuperview()
            }
        }
        confirmView.alpha = 0.0
        confirmView.frame.origin.y += frame.height
        topWindow.addSubview(confirmView)
        UIView.animate(withDuration: 0.45, animations: { () -> Void in
            confirmView.alpha = 1.0
            confirmView.frame.origin.y = frame.origin.y
        }) { (completed) -> Void in
        }
        
        return confirmView
    }
    
    func updateFrameView(newFrame: CGRect) {
        self.frame = newFrame
        self.frameV = newFrame
        self.layoutIfNeeded()
    }
    
    func updateOrigin(newKBHeight: CGFloat) {
        if newKBHeight > self.keyboardHeight, self.keyboardHeight > 0 {
            self.frame.origin.y -= (newKBHeight - self.keyboardHeight)
            self.frameV.origin.y -= (newKBHeight - self.keyboardHeight)
            self.keyboardHeight = newKBHeight
            self.layoutIfNeeded()
        }
    }
    
    @objc func dismiss() {
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.alpha = 0.0
        }) { (completed) -> Void in
            self.removeFromSuperview()
        }
    }
}
extension MentionView: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if hasMentionAll {
            return users.count + 1
        }
        return users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if hasMentionAll && indexPath.row == users.count {
            let cell = tableView.dequeue(MentionAllCell.self)
            return cell
        }
        let cell = tableView.dequeue(MentionCell.self)
        let user = users[indexPath.row]
        cell.member = user
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if hasMentionAll && indexPath.row == users.count {
            completion?(User.allUser())
        } else {
            completion?(users[indexPath.row])
        }
    }
}
