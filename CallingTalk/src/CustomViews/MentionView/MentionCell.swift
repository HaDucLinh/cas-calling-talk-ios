//
//  MentionCell.swift
//  CallingTalk
//
//  Created by Vu Tran on 3/12/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit
import Kingfisher

class MentionCell: UITableViewCell {
    @IBOutlet private weak var avatarView: UIImageView!
    @IBOutlet weak var nickNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        avatarView.corner = avatarView.frame.size.width / 2
        nickNameLabel.textColor = .white
        nickNameLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .regular, size: 15)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    var member: User? {
        didSet {
            nickNameLabel.text = member?.name
            if let url = member?.avatarUrl {
                avatarView.loadImageFromURL(url, placeholder: UIImage(named: "img_avatar_user_empty"))
            } else {
                avatarView.image = UIImage(named: "img_avatar_user_empty")
            }
        }
    }
}
