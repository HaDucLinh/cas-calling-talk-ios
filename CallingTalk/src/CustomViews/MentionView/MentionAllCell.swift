//
//  MentionAllCell.swift
//  CallingTalk
//
//  Created by Vu Tran on 5/8/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit

class MentionAllCell: UITableViewCell {
    @IBOutlet private weak var allUserLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        allUserLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .regular, size: 15)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
