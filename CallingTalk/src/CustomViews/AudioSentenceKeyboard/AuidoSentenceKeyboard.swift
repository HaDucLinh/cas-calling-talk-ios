//
//  AuidoSentenceCollectionView.swift
//  CallingTalk
//
//  Created by Toof on 3/12/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit

protocol AudioSentenceKeyboardDelegate: class {
    func numberOfItems(in audioSentenceKeyboard: AudioSentenceKeyboard) -> Int
    func audioSentenceKeyboard(_ audioSentenceKeyboard: AudioSentenceKeyboard, audioSentenceForItemAt indexPath: IndexPath) -> String
    func audioSentenceKeyboard(_ audioSentenceKeyboard: AudioSentenceKeyboard, didSelectItemAt indexPath: IndexPath)
}

class AudioSentenceKeyboard: UIView {
    
    // MARK: Properties
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var delegate: AudioSentenceKeyboardDelegate?
    
    // MARK: Initial
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.initFromNIB()
        self.setupSubviews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.initFromNIB()
        self.setupSubviews()
    }
    
    fileprivate func initFromNIB() {
        Bundle.main.loadNibNamed(String(describing: AudioSentenceKeyboard.self), owner: self, options: nil)
        self.backgroundColor = UIColor.white.withAlphaComponent(0.0)
        
        self.addSubview(self.contentView)
        self.contentView.frame = self.bounds
        self.contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    }
    
    // MARK: Setup Subviews
    public func reloadData() {
        collectionView.reloadData()
    }
    
    fileprivate
    func setupSubviews() {
        self.setupCollectionView()
    }
    
    fileprivate
    func setupCollectionView() {
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        
        self.collectionView.register(AudioSentenceCell.self)
        self.collectionView.backgroundColor = UIColor.white.withAlphaComponent(0.0)
    }
    
}

// MARK: - UICollectionViewDataSource

extension AudioSentenceKeyboard: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.delegate?.numberOfItems(in: self) ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let audioSentence = self.delegate?.audioSentenceKeyboard(self, audioSentenceForItemAt: indexPath)
        let cell = collectionView.dequeue(AudioSentenceCell.self, forIndexPath: indexPath)
        cell.updateAudioText(audioSentence)
        
        return cell
    }
    
}

// MARK: - UICollectionViewDelegateFlowLayout

extension AudioSentenceKeyboard: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 4.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 4.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (UIScreen.main.bounds.size.width - 15.0*2 - 4.0)/2, height: self.collectionView.bounds.size.height/3.5)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 15.0, left: 15.0, bottom: 15.0, right: 15.0)
    }
    
}

// MARK: - UICollectionViewDelegate

extension AudioSentenceKeyboard: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.delegate?.audioSentenceKeyboard(self, didSelectItemAt: indexPath)
    }
    
}
