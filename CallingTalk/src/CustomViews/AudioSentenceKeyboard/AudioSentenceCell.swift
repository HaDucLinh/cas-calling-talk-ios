//
//  AudioSentenceCell.swift
//  CallingTalk
//
//  Created by Toof on 3/12/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit

class AudioSentenceCell: UICollectionViewCell {
    
    // MARK: Properties
    
    @IBOutlet weak var containView: UIView!
    @IBOutlet weak var audioTextLabel: UILabel!
    @IBOutlet weak var selectedView: UIView!
    
    override var isSelected: Bool {
        didSet {
            self.selectedView.isHidden = !self.isSelected
        }
    }
    
    // MARK: Lifecycles

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.containView.corner = 8.0
        self.selectedView.corner = 8.0
        
        self.audioTextLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .bold, size: 14.0)
        self.audioTextLabel.textColor = UIColor.ct.highlightMessageTextColor
        
        self.selectedView.isHidden = true
        self.selectedView.backgroundColor = UIColor.ct.selectAudioSentenceBackgroundColor.withAlphaComponent(0.97)
        self.selectedView.layer.borderColor = UIColor.ct.highlightMessageTextColor.withAlphaComponent(0.5).cgColor
        self.selectedView.layer.borderWidth = 2.0
    }
    
    public
    func updateAudioText(_ text: String?) {
        self.audioTextLabel.text = text
    }
    
}
