//
//  ConfirmAlertView.swift
//  CallingTalk
//
//  Created by Toof on 2/18/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit
import Kingfisher
import Swinject

public enum ConfirmAlertType {
    case Invitation
    case Incoming
    case JoinByQR(code: String)
}

enum JoinActionType: Int {
    case Accept = 0, Decline = 1
}

protocol ConfirmAlertViewDelegate: class {
    func confirmAlertView(_ confirmAlertView: ConfirmAlertView, didSelectButtonWithType type: JoinActionType)
}

class ConfirmAlertView: UIView {
    
    // MARK: Properties
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var blurView: UIView!
    @IBOutlet weak var containView: UIView!
    @IBOutlet weak var backgroundAlertView: ThemeUIImageView!
    @IBOutlet weak var iconImageView: UIImageView!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var confirmButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var dismissButton: UIButton!
    
    weak var delegate: ConfirmAlertViewDelegate?
    var presenter: ConfirmAlertPresenter!
    
    // MARK: Init
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupSubviews()
    }
    
    init() {
        super.init(frame: UIScreen.main.bounds)
        self.setupSubviews()
    }
    
    fileprivate func setupSubviews() {
        self.initFromNIB()
        
        self.backgroundAlertView.backgroundColor = .white
        self.contentView.frame = self.bounds
        self.contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        self.contentView.backgroundColor = UIColor.white.withAlphaComponent(0.0)
        
        let tapGesture = UITapGestureRecognizer()
        tapGesture.addTarget(self, action: #selector(handleTapGesture(_:)))
        self.blurView.addGestureRecognizer(tapGesture)
        
        self.containView.corner = 20.0
        self.confirmButton.corner = 10.0
        self.cancelButton.corner = 10.0
        
        self.titleLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .bold, size: 20)
        self.titleLabel.textColor = .black
        
        self.messageLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .regular, size: 15)
        self.messageLabel.textColor = .black
        self.messageLabel.text = NSLocalizedString("join_group_confirm_alert_message", comment: "")
        
        let confirmText = NSLocalizedString("join_group_confirm_button_title", comment: "")
        self.confirmButton.setTitle(confirmText, for: .normal)
        self.confirmButton.titleLabel?.font = UIFont.ct.font(.NotoSansCJKjp, type: .bold, size: 16)
        self.confirmButton.setTitleColor(UIColor.ct.headerText, for: .normal)
        self.confirmButton.backgroundColor = UIColor.ct.mainBackgroundColor
        
        let cancelText = NSLocalizedString("join_group_cancel_button_title", comment: "")
        self.cancelButton.titleLabel?.font = UIFont.ct.font(.NotoSansCJKjp, type: .regular, size: 15)
        self.cancelButton.setTitle(cancelText, for: .normal)
        
        self.confirmButton.addTarget(self, action: #selector(confirmButtonPressed(_:)), for: .touchUpInside)
        self.cancelButton.addTarget(self, action: #selector(cancelButtonPressed(_:)), for: .touchUpInside)
        self.dismissButton.addTarget(self, action: #selector(closeButtonPressed(_:)), for: .touchUpInside)
        
        self.setUpStyleForBuild()
    }
    
    func setUpStyleForBuild() {
        let systemTheme = Container.default.resolve(SystemThemeable.self)
        switch systemTheme!.getCurrentMode() {
        case ThemeMode.normal:
            self.cancelButton.border(color: UIColor.ct.redKeyColor, width: 1)
        case ThemeMode.omiseno:
            self.cancelButton.backgroundColor = UIColor.ct.boderTagViewColor
            self.cancelButton.setTitleColor(.white, for: .normal)
        }
    }
    
    fileprivate func initFromNIB() {
        Bundle.main.loadNibNamed(String(describing: ConfirmAlertView.self), owner: self, options: nil)
        self.addSubview(self.contentView)
        self.backgroundColor = UIColor.white.withAlphaComponent(0)
    }
    
    fileprivate func loadData() {
        self.titleLabel.text = self.presenter.talkGroup.name
        self.iconImageView.loadImageFromURL(self.presenter.talkGroup.avatar, placeholder: UIImage(named: "img_avatar_talkgroup_empty"))
    }
    
    public func show() {
        self.loadData()
        
        let topWindow = UIApplication.shared.windows[0]
        for view in topWindow.subviews {
            if view is ConfirmAlertView {
                view.removeFromSuperview()
            }
        }
        
        topWindow.addSubview(self)
        
        self.alpha = 0.0
        self.iconImageView.alpha = 0.0
        UIView.animate(withDuration: 0.3, animations: { [weak self] in
            guard let `self` = self else { return }
            self.alpha = 1.0
        }) { (completed) -> Void in
            self.iconImageView.roundedCornerRadius()
            self.iconImageView.alpha = 1.0
        }
    }
    
    class func isShowing() -> Bool {
        let topWindow = UIApplication.shared.windows[0]
        for view in topWindow.subviews {
            if view is ConfirmAlertView { return true }
        }
        return false
    }
    
    fileprivate func dismiss() {
        UIView.animate(withDuration: 0.3, animations: { [weak self] in
            guard let `self` = self else { return }
            self.alpha = 0.0
        }) { (completed) -> Void in
            self.removeFromSuperview()
        }
    }
    
    public func handleAcceptAction(error: Error? = nil) {
        if let error = error {
            self.showErrorAlertWithError(error, completion: nil)
        }
        else {
            self.delegate?.confirmAlertView(self, didSelectButtonWithType: .Accept)
        }
        self.dismiss()
    }
    
    public func handleDeclineAction(error: Error? = nil) {
        if let error = error {
            self.showErrorAlertWithError(error, completion: nil)
        }
        else {
            self.delegate?.confirmAlertView(self, didSelectButtonWithType: .Decline)
        }
        self.dismiss()
    }
    
    fileprivate func showErrorAlertWithError(_ error: Error, completion: ((_ errorCode: Int?) -> Void)?) {
        let networkErrorAlertView = NetworkErrorAlertView(error: error)
        networkErrorAlertView.show { (errorCode) in
            if  let errorCode = errorCode,
                let statusCode = StatusCode(rawValue: errorCode),
                statusCode == .unauthorized
            {
                // Post notification to release meeting if exist
                NotificationCenter.default.post(name: Notification.Name.ReleaseChatNotification, object: nil)
                
                // Disconnect Socket
                SocketIOManager.sharedInstance.disconnectSocket()
                
                // Clear user's profile & token id
                Helpers.clearProfile()
                GlobalVariable.didComeToIncomingVC = false
                
                AppDelegate.share()?.showLoginVC()
            }
            else {
                completion?(errorCode)
            }
        }
    }
}

// MARK: - Handle events

extension ConfirmAlertView {
    @objc fileprivate func handleTapGesture(_ sender: UITapGestureRecognizer) {
        self.dismiss()
    }
    
    @objc fileprivate func closeButtonPressed(_ sender: UIButton) {
        self.dismiss()
    }
    
    @objc fileprivate func confirmButtonPressed(_ sender: UIButton) {
        switch self.presenter.type {
        case .Incoming:
            self.handleAcceptAction()
        case .Invitation:
            self.presenter.joinTalkGroupWithId(self.presenter.talkGroup.id)
        case .JoinByQR(code: let code):
            self.presenter.joinTalkGroupByQRCode(code)
        }
    }
    
    @objc fileprivate func cancelButtonPressed(_ sender: UIButton) {
        switch self.presenter.type {
        case .Incoming, .JoinByQR(code: _):
            self.dismiss()
        case .Invitation:
            self.presenter.declineJoinTalkGroupWithId(self.presenter.talkGroup.id)
        }
    }
}
