//
//  ConfirmAlertViewPresenter.swift
//  CallingTalk
//
//  Created by Toof on 5/31/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import Foundation

protocol ConfirmAlertViewPresenter: class {
    init(view: ConfirmAlertView, talkGroup: TalkGroup, type: ConfirmAlertType)
    func joinTalkGroupByQRCode(_ code: String)
    func joinTalkGroupWithId(_ id: Int)
    func declineJoinTalkGroupWithId(_ id: Int)
}

class ConfirmAlertPresenter: ConfirmAlertViewPresenter {
    var view: ConfirmAlertView
    var talkGroup: TalkGroup
    var type: ConfirmAlertType
    
    required init(view: ConfirmAlertView, talkGroup: TalkGroup, type: ConfirmAlertType) {
        self.view = view
        self.talkGroup = talkGroup
        self.type = type
    }
    
    func joinTalkGroupByQRCode(_ code: String) {
        if let channelId = code.components(separatedBy: "/").last {
            hud.show()
            API.joinTalkGroupByQRCode(channelId) { (result) in
                DispatchQueue.main.async { [weak self] in
                    guard let `self` = self else { return }
                    hud.dismiss()
                    switch result {
                    case .success(let talkGroup):
                        guard let talkGroup = talkGroup as? TalkGroup else { return }
                        if let organization = talkGroup.organization, User.getProfile()?.organizations.isEmpty == true {
                            User.getProfile()?.addOrganization(organization: organization)
                        }
                        self.view.handleAcceptAction()
                    case .failure(let error):
                        self.view.handleAcceptAction(error: error)
                    }
                }
            }
        }
    }
    
    func joinTalkGroupWithId(_ id: Int) {
        hud.show()
        API.joinTalkGroupByID(id) { (result) in
            DispatchQueue.main.async  { [weak self] in
                guard let `self` = self else {return}
                hud.dismiss()
                switch result {
                case .success(_):
                    self.view.handleAcceptAction()
                case .failure(let error):
                    self.view.handleAcceptAction(error: error)
                }
            }
        }
    }
    
    func declineJoinTalkGroupWithId(_ id: Int) {
        hud.show()
        API.declineJoiningTalkGroupByID(id) { (result) in
            DispatchQueue.main.async { [weak self] in
                guard let `self` = self else { return }
                hud.dismiss()
                switch result {
                case .success(_):
                    self.view.handleDeclineAction()
                case .failure(let error):
                    self.view.handleDeclineAction(error: error)
                }
            }
        }
    }
}
