//
//  ToastView.swift
//  CallingTalk
//
//  Created by Vu Tran on 2/18/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit

class ToastView: UIView {
    @IBOutlet private weak var backgroundView: UIView!
    @IBOutlet private weak var contentLabel: UILabel!
    @IBOutlet private weak var widthConstraint: NSLayoutConstraint!
    fileprivate let tapGestureRecognizer = UITapGestureRecognizer()
    
    public var toastViewColor: UIColor = UIColor.black {
        didSet {
            self.backgroundView.backgroundColor = self.toastViewColor
        }
    }

    private func setUpUI() {
        self.backgroundColor = .clear
        backgroundView.corner = 10
        backgroundView.backgroundColor = .black
        contentLabel.textColor = .white
        contentLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .bold, size: 16)
        
        self.tapGestureRecognizer.addTarget(self, action: #selector(handleTapGestureRecognizer(_:)))
        self.backgroundView.addGestureRecognizer(self.tapGestureRecognizer)
    }
    
    public
    class func loadViewFromNib() -> ToastView {
        let toastView = ToastView.loadFromNibNamed(type: ToastView.self)!
        toastView.setUpUI()
        toastView.frame = CGRect(x: 0, y: 28, width: GlobalVariable.screenWidth, height: 98)
        return toastView
    }
    
    func show(text: String) {
        contentLabel.text = text
        let topWindow = UIApplication.shared.windows[0]
        for view in topWindow.subviews {
            if view is ToastView {
                view.removeFromSuperview()
            }
        }
        self.alpha = 0.0
        topWindow.addSubview(self)
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.8) {
            self.dismiss()
        }
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.alpha = 1.0
        }) { (completed) -> Void in
        }
    }
    
    public func dismiss() {
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.alpha = 0.0
        }) { (completed) -> Void in
            self.removeFromSuperview()
        }
    }
    
    @objc
    fileprivate
    func handleTapGestureRecognizer(_ sender: UITapGestureRecognizer) {
        self.dismiss()
    }
}
