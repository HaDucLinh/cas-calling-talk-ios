//
//  MessageToastView.swift
//  CallingTalk
//
//  Created by Toof on 5/14/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit

typealias MessageToastViewDidTapHandler = () -> Void
class MessageToastView: UIView {
    @IBOutlet private weak var backgroundView: UIView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var messageLabel: UILabel!
    @IBOutlet private weak var widthConstraint: NSLayoutConstraint!
    fileprivate let tapGestureRecognizer = UITapGestureRecognizer()
    
    var messageToastViewDidTapHandler: MessageToastViewDidTapHandler?
    
    public var toastViewColor: UIColor = UIColor.black {
        didSet {
            self.backgroundView.backgroundColor = self.toastViewColor
        }
    }
    
    private func setUpUI() {
        self.backgroundColor = .clear
        backgroundView.corner = 10
        backgroundView.backgroundColor = .black
        titleLabel.textColor = .white
        titleLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .bold, size: 16)
        messageLabel.textColor = .white
        messageLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .regular, size: 14)
        
        self.tapGestureRecognizer.addTarget(self, action: #selector(handleTapGestureRecognizer(_:)))
        self.backgroundView.addGestureRecognizer(self.tapGestureRecognizer)
    }
    
    public
    class func loadViewFromNib() -> MessageToastView {
        let toastView = MessageToastView.loadFromNibNamed(type: MessageToastView.self)!
        toastView.setUpUI()
        toastView.frame = CGRect(x: 0, y: 28, width: GlobalVariable.screenWidth, height: 98)
        return toastView
    }
    
    func show(title: String, message: String) {
        titleLabel.text = title
        messageLabel.text = message
        
        let topWindow = UIApplication.shared.windows[0]
        for view in topWindow.subviews {
            if view is ToastView {
                view.removeFromSuperview()
            }
        }
        self.alpha = 0.0
        topWindow.addSubview(self)
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.8) {
            self.dismiss()
        }
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.alpha = 1.0
        }) { (completed) -> Void in
        }
    }
    
    public func dismiss() {
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.alpha = 0.0
        }) { (completed) -> Void in
            self.removeFromSuperview()
        }
    }
    
    @objc
    fileprivate
    func handleTapGestureRecognizer(_ sender: UITapGestureRecognizer) {
        self.dismiss()
        self.messageToastViewDidTapHandler?()
    }
}

