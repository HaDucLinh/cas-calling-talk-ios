//
//  LabelSwitchState.swift
//  CallingTalk
//
//  Created by Vu Tran on 2/28/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import Foundation

struct LabelSwitchPartState {
    var backMaskFrame: CGRect = .zero
}

struct LabelSwitchUIState {
    var backgroundColor: UIColor = .clear
    var circleFrame:CGRect = .zero
    var leftPartState  = LabelSwitchPartState()
    var rightPartState = LabelSwitchPartState()
}
