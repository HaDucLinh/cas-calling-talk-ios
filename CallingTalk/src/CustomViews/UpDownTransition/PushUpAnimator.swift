//
//  PushUpAnimator.swift
//  CallingTalk
//
//  Created by Toof on 3/4/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit

open class PushUpAnimator: UpDownAnimator {
    
    open override func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        guard   let fromViewController = transitionContext.viewController(forKey: .from),
                let toViewController = transitionContext.viewController(forKey: .to)
                else { return }
        
        let containerView = transitionContext.containerView
        toViewController.view.frame = containerView.bounds.offsetBy(dx: 0.0, dy: containerView.frame.size.height)
        containerView.addSubview(toViewController.view)
        
        let animations = {
            toViewController.view.frame = containerView.bounds
            fromViewController.view.alpha = 0
        }
        
        UIView.animate(withDuration: self.transitionDuration(using: transitionContext), delay: 0, options: [.curveEaseOut], animations: animations)
        { _ in
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
            fromViewController.view.alpha = 1
        }
    }
    
}

open class FadePushAnimator: UpDownAnimator {
    open override func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        guard
            let toViewController = transitionContext.viewController(forKey: .to)
            else {
                return
        }
        toViewController.view.frame = UIScreen.main.bounds
        transitionContext.containerView.addSubview(toViewController.view)
        toViewController.view.alpha = 0
        
        let duration = self.transitionDuration(using: transitionContext)
        UIView.animate(withDuration: duration, animations: {
            toViewController.view.alpha = 1
        }, completion: { _ in
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
        })
    }
}
open class LeftEdgeInteractionController: UIPercentDrivenInteractiveTransition {
    
    open var inProgress = false
    
    private var shouldCompleteTransition = false
    private weak var viewController: UIViewController!
    
    public init(viewController: UIViewController) {
        super.init()
        
        self.viewController = viewController
        
        self.setupGestureRecognizer(in: viewController.view)
    }
    
    private func setupGestureRecognizer(in view: UIView) {
        let edge = UIScreenEdgePanGestureRecognizer(target: self,
                                                    action: #selector(self.handleEdgePan(_:)))
        edge.edges = .left
        view.addGestureRecognizer(edge)
    }
    
    @objc func handleEdgePan(_ gesture: UIScreenEdgePanGestureRecognizer) {
        let translate = gesture.translation(in: gesture.view)
        let percent = translate.x / gesture.view!.bounds.size.width
        
        switch gesture.state {
        case .began:
            self.inProgress = true
            if let navigationController = viewController.navigationController {
                navigationController.popViewController(animated: true)
                return
            }
            viewController.dismiss(animated: true, completion: nil)
        case .changed:
            self.update(percent)
        case .cancelled:
            self.inProgress = false
            self.cancel()
        case .ended:
            self.inProgress = false
            
            let velocity = gesture.velocity(in: gesture.view)
            
            if percent > 0.5 || velocity.x > 0 {
                self.finish()
            }
            else {
                self.cancel()
            }
        default:
            break
        }
    }
}
