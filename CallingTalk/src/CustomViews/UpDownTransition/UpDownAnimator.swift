//
//  FilpUpDownAnimator.swift
//  CallingTalk
//
//  Created by Toof on 3/4/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit

open class UpDownAnimator: NSObject, UIViewControllerAnimatedTransitioning {
    
    public enum TransitionType {
        case navigation
        case modal
    }
    
    let type: TransitionType
    let duration: TimeInterval
    
    public init(type: TransitionType, duration: TimeInterval = 0.35) {
        self.type = type
        self.duration = duration
        
        super.init()
    }
    
    open func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return self.duration
    }
    
    open func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        fatalError("You have to implement this method for yourself!")
    }
    
}

