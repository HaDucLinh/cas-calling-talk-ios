//
//  PopDownAnimator.swift
//  CallingTalk
//
//  Created by Toof on 3/4/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit

open class PopDownAnimator: UpDownAnimator {
    
    let interactionController: UIPercentDrivenInteractiveTransition?
    
    public init(type: TransitionType, duration: TimeInterval = 0.35, interactionController: UIPercentDrivenInteractiveTransition? = nil) {
        self.interactionController = interactionController
        
        super.init(type: type, duration: duration)
    }
    
    open override func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        guard   let fromViewController = transitionContext.viewController(forKey: .from),
                let toViewController = transitionContext.viewController(forKey: .to)
                else { return }
        
        let containerView = transitionContext.containerView
        let dismissFrame = CGRect(origin: CGPoint(x: 0, y: UIScreen.main.bounds.size.height), size: fromViewController.view.frame.size)
        
        if self.type == .navigation {
            containerView.insertSubview(toViewController.view, belowSubview: fromViewController.view)
        }
        
        let animations = {
            fromViewController.view.frame = dismissFrame
            toViewController.view.frame = containerView.bounds
            fromViewController.view.alpha = 0
        }
        
        UIView.animate(withDuration: self.transitionDuration(using: transitionContext), delay: 0, options: [.curveEaseOut], animations: animations)
        { _ in
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
        }
    }
    
}
open class FadePopAnimator: UpDownAnimator {
    
    let interactionController: UIPercentDrivenInteractiveTransition?
    
    public init(type: TransitionType,
                duration: TimeInterval = 0.25,
                interactionController: UIPercentDrivenInteractiveTransition? = nil) {
        self.interactionController = interactionController
        
        super.init(type: type, duration: duration)
    }
    
    open override func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        guard
            let fromViewController = transitionContext.viewController(forKey: .from)
            else {
                return
        }
        
        if self.type == .navigation, let toViewController = transitionContext.viewController(forKey: .to) {
            transitionContext.containerView.insertSubview(toViewController.view, belowSubview: fromViewController.view)
        }
        
        let duration = self.transitionDuration(using: transitionContext)
        UIView.animate(withDuration: duration, animations: {
            fromViewController.view.alpha = 0
        }, completion: { _ in
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
        })
    }
}
