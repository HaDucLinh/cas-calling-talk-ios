//
//  SwiftyCodeItemView.swift
//  CallingTalk
//
//  Created by Toof on 2/13/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit

open class SwiftyCodeItemView: UIView {

    @IBOutlet open weak var textField: SwiftyCodeTextField!
    
    override open func awakeFromNib() {
        super.awakeFromNib()
        
        self.textField.text = ""
        self.applyBorder()
        
        self.isUserInteractionEnabled = false
    }
    
    fileprivate func applyBorder() {
        self.layer.borderWidth = 1.0
        self.layer.borderColor = UIColor.ct.grayBorderColor.cgColor
        self.layer.cornerRadius = 4.0
    }
    
    override open func becomeFirstResponder() -> Bool {
        return self.textField.becomeFirstResponder()
    }
    
    override open func resignFirstResponder() -> Bool {
        return self.textField.resignFirstResponder()
    }
}
