//
//  SwiftyCodeTextField.swift
//  CallingTalk
//
//  Created by Toof on 2/13/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit

public protocol SwiftyCodeTextFieldDelegate: class {
    func deleteBackward(sender: SwiftyCodeTextField)
}

open class SwiftyCodeTextField: UITextField {
    
    weak open var deleteDelegate: SwiftyCodeTextFieldDelegate?
    
    override open func deleteBackward() {
        super.deleteBackward()
        
        self.deleteDelegate?.deleteBackward(sender: self)
    }
}
