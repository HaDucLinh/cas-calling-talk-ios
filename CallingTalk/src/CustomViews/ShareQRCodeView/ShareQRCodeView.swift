//
//  ShareQRCodeView.swift
//  CallingTalk
//
//  Created by Vu Tran on 2/15/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit
import Swinject
enum ActionType {
    case Cancel
    case ShareCode(_ image: UIImage)
    case ShareUrl(_ image: UIImage)
}

typealias ShareQRCodeViewHandle = (_ action: ActionType) -> Void

class ShareQRCodeView: UIView {
    @IBOutlet private weak var backgroundView: UIView!
    @IBOutlet private weak var backgroundImageView: UIImageView!
    @IBOutlet private weak var contentView: UIView!
    
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var qrCodeImageView: UIImageView!
    @IBOutlet private weak var shareButton: UIButton!
    
    @IBOutlet private weak var trailingCloseButtonConstraint: NSLayoutConstraint?
    @IBOutlet private weak var leadinglCloseButtonConstraint: NSLayoutConstraint?
    
    var completion: ShareQRCodeViewHandle?
    
    init() {
        super.init(frame: .zero)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    fileprivate func setup() {
        contentView.corner = 20
        backgroundView.backgroundColor = UIColor.black.withAlphaComponent(0.23)
        
        let shareTalkText = NSLocalizedString("share_Talk_title", comment: "")
        titleLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .bold, size: 14.0)
        titleLabel.textColor = UIColor.black
        titleLabel.text = shareTalkText
        
        let shareText = NSLocalizedString("share_Talk_button_text", comment: "")
        shareButton.setTitle(shareText, for: .normal)
        shareButton.titleLabel?.font = UIFont.ct.font(.NotoSansCJKjp, type: .regular, size: 15)
        shareButton.corner = 10
        self.setUpStyleForBuild()
    }
    func setUpStyleForBuild() {
        //TODO: this is 2 difference style of button, we can't just modify the value
        //So make hard-code right here!! we need to fix it later if has more than this place
        let systemTheme = Container.default.resolve(SystemThemeable.self)
        switch systemTheme!.getCurrentMode() {
        case ThemeMode.normal:
            shareButton.border(color: UIColor.ct.redKeyColor, width: 1)
        case ThemeMode.omiseno:
            shareButton.setTitleColor(UIColor.white, for: .normal)
            shareButton.backgroundColor = UIColor.black.withAlphaComponent(0.47)
            break
        }
    }
    @IBAction private func close(sender: UIButton) {
        self.dismiss()
    }
    
    public
    class func loadViewFromNib() -> ShareQRCodeView {
        let shareView = ShareQRCodeView.loadFromNibNamed(type: ShareQRCodeView.self)!
        shareView.setup()
        shareView.frame = UIScreen.main.bounds
        return shareView
    }
    
    func show(_ qrCodeImageUrlString: String, title: String? = nil, isBackgroundImage: Bool = false, isCloseButtonMarginLeft: Bool = true, completion: ShareQRCodeViewHandle?) {
        let topWindow = UIApplication.shared.windows[0]
        
        for view in topWindow.subviews {
            if view is ShareQRCodeView {
                view.removeFromSuperview()
            }
        }
        
        self.leadinglCloseButtonConstraint?.priority = isCloseButtonMarginLeft ? .defaultHigh : .defaultLow
        self.trailingCloseButtonConstraint?.priority = isCloseButtonMarginLeft ? .defaultLow : .defaultHigh
        self.backgroundImageView.isHidden = !isBackgroundImage
        self.titleLabel.text = title
        self.qrCodeImageView.loadImageFromURL(qrCodeImageUrlString)
        self.completion = completion
        
        topWindow.addSubview(self)
        
        self.alpha = 0.0
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.alpha = 1.0
        }) { _ in }
    }
    
    public func dismiss() {
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.alpha = 0.0
        }) { (completed) -> Void in
            self.removeFromSuperview()
        }
    }
    
    @IBAction private func shareCode(sender: UIButton) {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.popoverPresentationController?.sourceView = sender
        alert.popoverPresentationController?.sourceRect = sender.bounds
        
        let cancelTitle = NSLocalizedString("cancel_button_title", comment: "")
        let cancelAction = UIAlertAction(title: cancelTitle, style: .cancel) { ( _) -> Void in
            self.completion?(.Cancel)
        }
        alert.addAction(cancelAction)
        
        let shareQRCode = NSLocalizedString("share_QRCode_button_text", comment: "")
        let shareQRCodeAction = UIAlertAction(title: shareQRCode, style: .default) { ( _) -> Void in
            self.completion?(.ShareCode(self.qrCodeImageView.image ?? UIImage()))
        }
        alert.addAction(shareQRCodeAction)
        
        let shareURL = NSLocalizedString("share_URL_button_text", comment: "")
        let shareURLAction = UIAlertAction(title: shareURL, style: .default) { ( _) -> Void in
            self.completion?(.ShareUrl(self.qrCodeImageView.image ?? UIImage()))
        }
        // alert.addAction(shareURLAction)
        alert.show()
    }
}
