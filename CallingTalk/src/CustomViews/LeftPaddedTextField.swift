//
//  LeftPaddedTextField.swift
//  CallingTalk
//
//  Created by Van Trung on 1/17/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit

class LeftPaddedTextField: UITextField{
    let padding = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10);
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        //        return CGRect(x: bounds.origin.x + 20, y: bounds.origin.y, width: bounds.width - 10, height: bounds.height)
        return bounds.inset(by: padding)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        //        return CGRect(x: bounds.origin.x + 20, y: bounds.origin.y, width: bounds.width, height: bounds.height)
        return bounds.inset(by: padding)
    }
    
}
