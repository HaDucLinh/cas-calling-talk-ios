//
//  ReportMemberConfirmView.swift
//  CallingTalk
//
//  Created by Vu Tran on 2/21/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit

class ReportMemberConfirmView: UIView {
    
    // MARK: Properties
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var backgroundImageView: UIImageView!
    
    @IBOutlet weak var organizeLabel: UILabel!
    @IBOutlet weak var moreInforLabel: UILabel!
    @IBOutlet weak var memberNameLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    
    @IBOutlet weak var confirmButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
  
    @IBOutlet weak var notInWorkspaceLabel: UILabel!
    @IBOutlet weak var nuisanceActionLabel: UILabel!
    @IBOutlet weak var otherLabel: UILabel!
    
    @IBOutlet weak var notInWorkSpaceButton: UIButton!
    @IBOutlet weak var nuisanceActButton: UIButton!
    @IBOutlet weak var otherButton: UIButton!
    @IBOutlet weak var reportTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var confirmHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var cancelHeightConstraint: NSLayoutConstraint!

    var handleConfirmAction: (() -> Void)?
    
    open var backgroundImage: UIImage? {
        didSet {
            backgroundImageView.image = backgroundImage
        }
    }
    open var organize: String? {
        didSet {
            organizeLabel.text = organize
            let org = organize ?? ""
            let workSpaceText = String(format: NSLocalizedString("is_not_user_of_Workspace", comment: ""), org)
            notInWorkspaceLabel.text = workSpaceText
        }
    }
    
    open var memberName: String = "" {
        didSet { memberNameLabel.text = memberName }
    }
    
    open var confirmButtonTitle: String = "" {
        didSet { confirmButton.setTitle(confirmButtonTitle, for: .normal) }
    }
    
    open var avatarImageName: String = "" {
        didSet { self.iconImageView.image = UIImage(named: self.avatarImageName) }
    }
    
    // MARK: Init
    
    fileprivate func setup() {
        self.frame = UIScreen.main.bounds
        self.backgroundColor = UIColor.black.withAlphaComponent(0.23)
        self.clipsToBounds = true
        self.contentView.corner = 20.0
        self.cancelButton.corner = 10.0
        self.confirmButton.corner = 10.0
        
        self.confirmButton.backgroundColor = .black
        self.cancelButton.border(color: .black, width: 1)
        let cancelTitle = NSLocalizedString("cancel_crop_button_text", comment: "")
        let confirmTitle = NSLocalizedString("report_talk_button_title", comment: "")
        cancelButton.setTitle(cancelTitle, for: .normal)
        confirmButton.setTitle(confirmTitle, for: .normal)
        
        organizeLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .regular, size: 14)
        memberNameLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .bold, size: 24)
        messageLabel.textColor = UIColor.ct.redKeyColor
        messageLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .bold, size: 18)
        messageLabel.text = NSLocalizedString("report_member_infor_sent_to_admin", comment: "")
        moreInforLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .regular, size: 15)
        moreInforLabel.text = NSLocalizedString("report_member_title", comment: "")
        
        notInWorkspaceLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .regular, size: 14)
        nuisanceActionLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .regular, size: 14)
        otherLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .regular, size: 14)
        notInWorkspaceLabel.text = NSLocalizedString("is_not_user_of_Workspace", comment: "")
        nuisanceActionLabel.text = NSLocalizedString("is_nuisance_act", comment: "")
        otherLabel.text = NSLocalizedString("is_other_reason", comment: "")
        if kScreenSize.width == DeviceType.iPhone5.size.width {
            reportTopConstraint.constant = 0
            confirmHeightConstraint.constant = 50 * GlobalVariable.ratioHeight
            cancelHeightConstraint.constant = 60 * GlobalVariable.ratioHeight
        }
        self.confirmButton.addTarget(self, action: #selector(confirmButtonPressed(_:)), for: .touchUpInside)
        self.cancelButton.addTarget(self, action: #selector(closeButtonPressed(_:)), for: .touchUpInside)
    }
    
    class func loadFromNib() -> ReportMemberConfirmView {
        let confirmView = ReportMemberConfirmView.loadFromNibNamed(type: ReportMemberConfirmView.self)!
        confirmView.setup()
        return confirmView
    }
    
    public func show(handleConfirmAction: @escaping () -> Void) {
        let topWindow = UIApplication.shared.windows[0]
        for view in topWindow.subviews {
            if view is ReportMemberConfirmView {
                view.removeFromSuperview()
            }
        }
        self.handleConfirmAction = handleConfirmAction
        self.alpha = 0.0
        topWindow.addSubview(self)
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.alpha = 1.0
        }) { (completed) -> Void in
        }
    }
    
    func dismiss() {
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.alpha = 0.0
        }) { (completed) -> Void in
            self.removeFromSuperview()
        }
    }
    
    @objc fileprivate func confirmButtonPressed(_ sender: UIButton) {
        self.handleConfirmAction?()
        dismiss()
    }
    
    @objc fileprivate func closeButtonPressed(_ sender: UIButton) {
        dismiss()
    }
    
    @IBAction private func notInWorkSpace(sender: UIButton) {
        notInWorkSpaceButton.setImage(UIImage(named: "ic-report_selected"), for: .normal)
        nuisanceActButton.setImage(UIImage(named: "ic-report_unSelected"), for: .normal)
        otherButton.setImage(UIImage(named: "ic-report_unSelected"), for: .normal)
    }
    
    @IBAction private func nuisanceAct(sender: UIButton) {
        notInWorkSpaceButton.setImage(UIImage(named: "ic-report_unSelected"), for: .normal)
        nuisanceActButton.setImage(UIImage(named: "ic-report_selected"), for: .normal)
        otherButton.setImage(UIImage(named: "ic-report_unSelected"), for: .normal)
    }
    
    @IBAction private func Other(sender: UIButton) {
        notInWorkSpaceButton.setImage(UIImage(named: "ic-report_unSelected"), for: .normal)
        nuisanceActButton.setImage(UIImage(named: "ic-report_unSelected"), for: .normal)
        otherButton.setImage(UIImage(named: "ic-report_selected"), for: .normal)
    }
    
}

