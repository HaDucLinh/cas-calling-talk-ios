//
//  BlockMemberAlertView.swift
//  CallingTalk
//
//  Created by Vu Tran on 2/21/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit
import Swinject
typealias ActionHandle = (_ action: MemberAction) -> Void
enum MemberAction: Int {
    case Report = 0, Block, UnBlock, RemoveMember, Banish
}
class BlockMemberAlertView: UIView {
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var memberNameLabel: UILabel!
    @IBOutlet weak var organizeLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var actionButton: UIButton!

    var role = ""
    var talkGroup: TalkGroup?
    var member: User?
    
    var completion: ActionHandle?
    
    func setUpUI() {
        self.frame = UIScreen.main.bounds
        self.backgroundColor = UIColor.black.withAlphaComponent(0.23)
        self.contentView.corner = 20.0
        
        iconImageView.corner = iconImageView.bounds.width / 2
        iconImageView.contentMode = .scaleAspectFill
        
        closeButton.corner = 10
        closeButton.titleLabel?.font = UIFont.ct.font(.NotoSansCJKjp, type: .regular, size: 15)
        let closeTitle = NSLocalizedString("close_button_text", comment: "")
        closeButton.setTitle(closeTitle, for: .normal)
        
        messageLabel.textColor = UIColor.ct.grayInvitedTextColor
        memberNameLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .bold, size: 18)
        organizeLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .regular, size: 14)
        messageLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .regular, size: 14)
        
        self.closeButton.addTarget(self, action: #selector(closeButtonPressed(_:)), for: .touchUpInside)
        self.setUpStyleForBuild()
    }
    
    func setUpStyleForBuild() {
        let systemTheme = Container.default.resolve(SystemThemeable.self)
        switch systemTheme!.getCurrentMode() {
        case ThemeMode.normal:
            closeButton.backgroundColor = .white
            closeButton.border(color: UIColor.ct.redKeyColor, width: 1)
        case ThemeMode.omiseno:
            closeButton.backgroundColor = UIColor.black.withAlphaComponent(0.47)
            closeButton.setTitleColor(.white, for: .normal)
            break
        }
    }
    
    private func setUpData(member: User?, talkGroup: TalkGroup?) {
        guard let user = member else {
            return
        }
        self.organizeLabel.text = Helpers.getOrganizationsName(user: user)
        self.memberNameLabel.text = member?.name
        if let url = member?.avatarUrl {
            iconImageView.loadImageFromURL(url, placeholder: UIImage(named: "img_avatar_user_empty"))
        } else {
            iconImageView.image = UIImage(named: "img_avatar_user_empty")
        }
        
        self.messageLabel.text = ""
        // let message = NSLocalizedString("blocked_cant_invite_you", comment: "")
        
        guard let myOrgs = User.getProfile()?.organizations, let org = talkGroup?.organization else {
            actionButton.isHidden = true
            return
        }
        let me = User.getProfile()
        let memberIsAdmin = member?.isAdmin == true
        let memberIsMe  = me?.id == member?.id
        let meIsManageThatOrg = !myOrgs.filter({ $0.id == org.id }).isEmpty
        if memberIsMe || !meIsManageThatOrg || memberIsAdmin {
            actionButton.isHidden = true
        } else {
            actionButton.isHidden = false
        }

        // Hide feature for release Version 3
        let meIsMemberAndNotIsAdmin = me?.id != talkGroup?.author?.id && me?.isAdmin == false
        if meIsMemberAndNotIsAdmin {
            actionButton.isHidden = true
        }
    }
    
    class func loadFromNib() -> BlockMemberAlertView {
        let confirmView = BlockMemberAlertView.loadFromNibNamed(type: BlockMemberAlertView.self)!
        confirmView.setUpUI()
        return confirmView
    }
    
    func show(member: User?, talkGroup: TalkGroup?, completion: ActionHandle?) {
        self.member = member
        self.talkGroup = talkGroup
        self.setUpData(member: member, talkGroup: talkGroup)
        
        let topWindow = UIApplication.shared.windows[0]
        for view in topWindow.subviews {
            if view is BlockMemberAlertView {
                view.removeFromSuperview()
            }
        }
        self.completion = completion
        self.alpha = 0.0
        topWindow.addSubview(self)
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.alpha = 1.0
        }) { (completed) -> Void in
        }
    }
    
    
    // MARK: Actions
    @objc fileprivate func closeButtonPressed(_ sender: UIButton) {
        dismiss()
    }
    
    fileprivate func dismiss() {
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.alpha = 0.0
        }) { (completed) -> Void in
            self.removeFromSuperview()
        }
    }
    
    @IBAction private func moreAction(sender: UIButton) {
        guard let profile = User.getProfile() else { return }
        if profile.isAdmin {
            showActionAsAdmin(member: member, sourceView: sender)
        } else if profile.id == talkGroup?.author?.id {
            showActionAsAuthor(member: member, sourceView: sender)
        } else {
            showReportMember(member: member, sourceView: sender)
        }
    }
    
    // Alert Action
    func showReportMember(member: User?, sourceView: UIView? = nil) {
        let reportTitle = NSLocalizedString("report_talk_button_title", comment: "")
        let reportAction = UIAlertAction(title: reportTitle, style: .default) { (action) in
            self.completion?(.Report)
            self.dismiss()
        }
        let blockTitle = NSLocalizedString("block_talk_button_title", comment: "")
        let blockAction = UIAlertAction(title: blockTitle, style: .default) { (action) in
            self.completion?(.Block)
            self.dismiss()
        }
        let cancelTitle = NSLocalizedString("cancel_text", comment: "")
        let cancelAction = UIAlertAction(title: cancelTitle, style: .cancel) { (action) in
        }
        var actions: [UIAlertAction] = [reportAction, blockAction, cancelAction]
        if member?.isAdmin == true { // Member is Admin, do't have block action
            actions.remove(at: 1)
        }
        Helpers.showActionSheet(title: nil, actions: actions, sourceView: sourceView)
    }
    
    func showActionAsAuthor(member: User? ,sourceView: UIView? = nil) {
        
        let removeMemberTitle = NSLocalizedString("remove_member_button_title", comment: "")
        let removeMemberAction = UIAlertAction(title: removeMemberTitle, style: .default) { (action) in
            self.completion?(.RemoveMember)
            self.dismiss()
        }
        let reportTitle = NSLocalizedString("report_talk_button_title", comment: "")
        let _ = UIAlertAction(title: reportTitle, style: .default) { (action) in
            self.completion?(.Report)
            self.dismiss()
        }
        let blockTitle = NSLocalizedString("block_talk_button_title", comment: "")
        let _ = UIAlertAction(title: blockTitle, style: .default) { (action) in
            self.completion?(.Block)
            self.dismiss()
        }
        let cancelTitle = NSLocalizedString("cancel_text", comment: "")
        let cancelAction = UIAlertAction(title: cancelTitle, style: .cancel) { (action) in
        }
        // var actions: [UIAlertAction] = [removeMemberAction, reportAction, blockAction, cancelAction]
        //        if member?.isAdmin == true { // Member is Admin, do't have block action
        //            actions.remove(at: 2)
        //        }
        
        // Hide feature for release Version 3
        let actions: [UIAlertAction] = [removeMemberAction, cancelAction]
        
        Helpers.showActionSheet(title: nil, actions: actions, sourceView: sourceView)
    }
    
    func showActionAsAdmin(member: User?, sourceView: UIView? = nil) {
        let removeMemberTitle = NSLocalizedString("remove_member_button_title", comment: "")
        let removeMemberAction = UIAlertAction(title: removeMemberTitle, style: .default) { (action) in
            self.completion?(.RemoveMember)
            self.dismiss()
        }
        let banishTitle = NSLocalizedString("banish_member_button_title", comment: "")
        let _ = UIAlertAction(title: banishTitle, style: .default) { (action) in
            self.completion?(.Banish)
            self.dismiss()
        }
        let cancelTitle = NSLocalizedString("cancel_text", comment: "")
        let cancelAction = UIAlertAction(title: cancelTitle, style: .cancel) { (action) in
        }
        
//        let actions: [UIAlertAction] = [removeMemberAction, banishAction, cancelAction]
        
        // Hide feature for release Version 3
        let actions: [UIAlertAction] = [removeMemberAction, cancelAction]
        Helpers.showActionSheet(title: nil, actions: actions, sourceView: sourceView)
        
        /*
         Question: Admin Area can REMOVE Author of a talk?, can remove a member who is Admin Area?
         Admin Area can BANISH Author of a talk?, can BANISH a member who is Admin Area?
         */
    }
}
