//
//  ConfirmDeleteTalkView.swift
//  CallingTalk
//
//  Created by Vu Tran on 2/20/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import Foundation
import UIKit

class ConfirmDeleteTalkView: UIView {
    
    // MARK: Properties
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var backgroundImageView: UIImageView!

    @IBOutlet weak var organizeLabel: UILabel!
    @IBOutlet weak var groupNameLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var moreInforLabel: UILabel!

    @IBOutlet weak var confirmButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var deleteTopConstraint: NSLayoutConstraint!
    
    var talkGroup: TalkGroup? {
        didSet {
            setUpTalkgroupData()
        }
    }
    
    var member: User? {
        didSet {
            setUpMemberData()
        }
    }
    
    var isDeleteMember = false {
        didSet {
            organizeLabel.isHidden = !isDeleteMember
            // groupNameTopConstraint.constant = !isDeleteMember ? 75: 95
            let font = (organize?.isEmpty == true) ? UIFont.ct.font(.NotoSansCJKjp, type: .regular, size: 17) : UIFont.ct.font(.NotoSansCJKjp, type: .regular, size: 17)
            messageLabel.font = font
        }
    }
    
    var handleConfirmAction: (() -> Void)?
    
    open var backgroundImage: UIImage? {
        didSet {
            backgroundImageView.image = backgroundImage
            backgroundImageView.contentMode = .scaleAspectFit
        }
    }
    open var organize: String? {
        didSet {
            organizeLabel.text = organize
        }
    }
    
    open var groupName: String = "" {
        didSet { groupNameLabel.text = groupName }
    }
    
    open var title: String = "" {
        didSet { titleLabel.text = title }
    }
    
    open var message: String = "" {
        didSet { messageLabel.text = message }
    }
    open var moreInfor: String = "" {
        didSet {
            moreInforLabel.text = moreInfor
            deleteTopConstraint.constant = moreInfor.isEmpty ? 28 : 78
        }
    }
    open var confirmButtonTitle: String = "" {
        didSet { confirmButton.setTitle(confirmButtonTitle, for: .normal) }
    }
    
    open var avatarImageName: String = "" {
        didSet { self.iconImageView.image = UIImage(named: self.avatarImageName) }
    }
    
    // MARK: Init
    
    fileprivate func setup() {
        self.frame = UIScreen.main.bounds
        self.backgroundColor = UIColor.black.withAlphaComponent(0.23)
        self.clipsToBounds = true
        self.contentView.corner = 20.0
        self.cancelButton.corner = 10.0
        self.confirmButton.corner = 10.0
        
        self.iconImageView.corner = self.iconImageView.frame.width / 2
        self.iconImageView.contentMode = .scaleAspectFill
        
        self.confirmButton.backgroundColor = .black
        self.cancelButton.border(color: .black, width: 1)
        let cancelTitle = NSLocalizedString("cancel_crop_button_text", comment: "")
        let confirmTitle = NSLocalizedString("leave_talk_button_text", comment: "")
        let title = NSLocalizedString("leave_alert_title", comment: "")
        cancelButton.setTitle(cancelTitle, for: .normal)
        confirmButton.setTitle(confirmTitle, for: .normal)
        titleLabel.text = title
        
        organizeLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .regular, size: 14)
        messageLabel.textColor = UIColor.ct.redKeyColor
        moreInforLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .regular, size: 15)
        groupNameLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .bold, size: 18)
        titleLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .bold, size: 18)
        messageLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .regular, size: 17)
        
        self.confirmButton.addTarget(self, action: #selector(confirmButtonPressed(_:)), for: .touchUpInside)
        self.cancelButton.addTarget(self, action: #selector(closeButtonPressed(_:)), for: .touchUpInside)
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        self.iconImageView.corner = self.iconImageView.frame.width / 2
    }
    
    fileprivate func setUpTalkgroupData() {
        titleLabel.text = ""
        groupNameLabel.text = " "
        guard let talk = self.talkGroup else { return }
        groupNameLabel.text = talkGroup?.name
        iconImageView.loadImageFromURL(talk.avatar, placeholder: UIImage(named: "img_avatar_talkgroup_empty"))
    }
    
    fileprivate func setUpMemberData() {
        titleLabel.text = ""
        groupNameLabel.text = " "

        guard let member = self.member else { return }
        groupNameLabel.text = member.name
        organizeLabel.text = Helpers.getOrganizationsName(user: member)
        iconImageView.loadImageFromURL(member.avatarUrl, placeholder: UIImage(named: "img_avatar_user_empty"))
    }
    
    @objc fileprivate func confirmButtonPressed(_ sender: UIButton) {
        self.handleConfirmAction?()
        dismiss()
    }
    
    @objc fileprivate func closeButtonPressed(_ sender: UIButton) {
        dismiss()
    }
    
    class func loadFromNib() -> ConfirmDeleteTalkView {
        let confirmView = ConfirmDeleteTalkView.loadFromNibNamed(type: ConfirmDeleteTalkView.self)!
        confirmView.setup()
        return confirmView
    }
    
    public func show(handleConfirmAction: @escaping () -> Void) {
        let topWindow = UIApplication.shared.windows[0]
        for view in topWindow.subviews {
            if view is ConfirmDeleteTalkView {
                view.removeFromSuperview()
            }
        }
        self.handleConfirmAction = handleConfirmAction
        self.alpha = 0.0
        topWindow.addSubview(self)
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.alpha = 1.0
        }) { (completed) -> Void in
        }
    }
    
    func dismiss() {
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.alpha = 0.0
        }) { (completed) -> Void in
            self.removeFromSuperview()
        }
    }
    
}

