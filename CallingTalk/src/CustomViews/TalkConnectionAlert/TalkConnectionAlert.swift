//
//  TalkConnectionAlert.swift
//  CallingTalk
//
//  Created by Vu Tran on 3/14/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import Foundation

import UIKit

class TalkConnectionAlert: UIView {
    
    // MARK: Properties
    @IBOutlet weak var contentView: UIView!
    @IBOutlet public weak var iconImageView: UIImageView!
    @IBOutlet public weak var textLabel: UILabel!

    // MARK: Init
    
    fileprivate func setup() {
        self.frame = UIScreen.main.bounds
        self.backgroundColor = UIColor.black.withAlphaComponent(0.23)
        self.clipsToBounds = true
        contentView.corner = 10
        contentView.backgroundColor = UIColor.ct.talkConnectionIssusAlert.withAlphaComponent(0.94)
        textLabel.font = UIFont.ct.font(.NotoSansCJKjp, type: .bold, size: 15)
        textLabel.text = NSLocalizedString("Connection_issue_alert_text", comment: "")
        let tapLabel = UITapGestureRecognizer(target: self, action: #selector(self.dismiss))
        self.isUserInteractionEnabled = true
        self.addGestureRecognizer(tapLabel)
    }
    
    @objc fileprivate func closeButtonPressed(_ sender: UIButton) {
        dismiss()
    }
    
    class func loadFromNib() -> TalkConnectionAlert {
        let confirmView = TalkConnectionAlert.loadFromNibNamed(type: TalkConnectionAlert.self)!
        confirmView.setup()
        return confirmView
    }
    public func setupToast() {
        /*
         Turn the view from view to toast style
         */
        self.isUserInteractionEnabled = false
    }
    public func show() {
        let topWindow = UIApplication.shared.windows[0]
        for view in topWindow.subviews {
            if view is TalkConnectionAlert {
                view.removeFromSuperview()
            }
        }
    
        self.alpha = 0.0
        topWindow.addSubview(self)
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.alpha = 1.0
            self.willShowAudio()
        }) { (completed) -> Void in
        }
    }
    
    private func willShowAudio() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.75) {
            self.showAudio()
        }
    }
    
    private func showAudio() {
        AVPlayerManager.shared.playLocalAudio(name: "audio-connection-lost", type: "mp3")
    }
    
    @objc func dismiss() {
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.alpha = 0.0
        }) { (completed) -> Void in
            self.removeFromSuperview()
        }
    }
    
}

