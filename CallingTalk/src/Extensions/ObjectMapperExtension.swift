//
//  ObjectMapperExtension.swift
//  CallingTalk
//
//  Created by Toof on 5/10/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import RealmSwift
import ObjectMapper

/// Transforms Swift Arrays to Realm Arrays. E.g. [String] to List<String>.
public class RealmTypeCastTransform<T: RealmCollectionValue>: TransformType {
    public typealias Object = List<T>
    public typealias JSON = [Any]
    
    public init() {}
    
    public func transformFromJSON(_ value: Any?) -> Object? {
        guard let array = value as? [T] else { return nil }
        let list = List<T>()
        list.append(objectsIn: array)
        return list
    }
    
    public func transformToJSON(_ value: Object?) -> JSON? {
        guard let value = value else { return nil }
        return Array(value)
    }
}
