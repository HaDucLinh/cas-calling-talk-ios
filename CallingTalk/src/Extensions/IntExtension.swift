//
//  IntExtension.swift
//  CallingTalk
//
//  Created by Toof on 5/29/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import Foundation

extension Optional where Wrapped == Int {
    public func toUnreadNumberBadgeString() -> String {
        if let number = self {
            if number == 0 { return "!" }
            if number > kMaxBadgeNumberOfNotifications { return "\(kMaxBadgeNumberOfNotifications)+" }
            return number.description
        } else {
            return ""
        }
    }
}
