//
//  UIImageViewExtension.swift
//  CallingTalk
//
//  Created by Van Trung on 1/17/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit
import Kingfisher

extension UIImageView {
    func roundedCornerRadius() {
        self.layer.masksToBounds = true
        let cornerRadius = self.frame.height / 2
        self.layer.cornerRadius = cornerRadius
    }
    
    func loadImageFromURL(_ urlString: String, placeholder: UIImage? = nil, completion: CompletionHandler? = nil) {
        guard let url = URL(string: urlString) else {
            self.image = placeholder
            return
        }
        self.kf.setImage(with: url,
                         placeholder: placeholder,
                         options: [.transition(.fade(0.5)),
                                   .keepCurrentImageWhileLoading],
                         progressBlock: nil,
                         completionHandler: completion)
    }
    
    func cancelRequestImage() {
        self.kf.cancelDownloadTask()
    }
}
