//
//  StringExtension.swift
//  CallingTalk
//
//  Created by Van Trung on 1/17/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import Foundation
import UIKit

extension String {
    var host: String? { return (try? asURL())?.host }
    // check a string is valid email?
    ///
    /// - Returns: true if the string is valid email
    func isValidEmail() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
    
    public
    func isValidPhone() -> Bool {
        let phoneRegex = "^[0-9]{6,14}$"
        return NSPredicate(format: "SELF MATCHES %@", phoneRegex).evaluate(with: self)
    }
    
    func trim() -> String {
        return self.trimmingCharacters(in: NSCharacterSet.whitespaces)
    }
    public
    func isValidQRCodeDomain() -> Bool {
        guard let host = self.host else { return false }
        return host.contains(kCallingDomain)
    }
    
    var length: Int {
        return count
    }
    
    subscript (i: Int) -> String {
        return self[i ..< i + 1]
    }
    
    func substring(fromIndex: Int) -> String {
        return self[min(fromIndex, length) ..< length]
    }
    
    func substring(toIndex: Int) -> String {
        return self[0 ..< max(0, toIndex)]
    }
    
    subscript (r: Range<Int>) -> String {
        let range = Range(uncheckedBounds: (lower: max(0, min(length, r.lowerBound)),
                                            upper: min(length, max(0, r.upperBound))))
        let start = index(startIndex, offsetBy: range.lowerBound)
        let end = index(start, offsetBy: range.upperBound - range.lowerBound)
        return String(self[start ..< end])
    }
    
    func lastIndexOf(text: String, location: Int) -> Int? {
        var indexs: [Int] = []
        for i in 0..<location {
            if self[i] == text {
                indexs.append(i)
            }
        }
        return indexs.last
    }
    
    public func insert(_ index: Int, _ string: String) -> String {
        if index > length {
            return self + string
        } else if index < 0 {
            return string + self
        }
        return self[0 ..< index] + string + self[index ..< length]
    }
    
    mutating func deleteCharactersInRange(range: NSRange) -> String {
        let mutableSelf = NSMutableString(string: self)
        if range.location <= self.utf16.count && range.location < defaultMessageLengh {
            mutableSelf.deleteCharacters(in: range)
        }
        return String(mutableSelf)
    }
    
    func ranges(of substring: String, options: CompareOptions = [], locale: Locale? = nil) -> [Range<Index>] {
        var ranges: [Range<Index>] = []
        while ranges.last.map({ $0.upperBound < self.endIndex }) ?? true,
            let range = self.range(of: substring, options: options, range: (ranges.last?.upperBound ?? self.startIndex)..<self.endIndex, locale: locale)
        {
            ranges.append(range)
        }
        return ranges
    }
}

extension String {
    func attributesString(color: UIColor, font: UIFont) -> NSMutableAttributedString {
        let attributedString = NSMutableAttributedString(string: self, attributes: [
            .font: font,
            .foregroundColor: color
            ])
        return attributedString
    }
}
