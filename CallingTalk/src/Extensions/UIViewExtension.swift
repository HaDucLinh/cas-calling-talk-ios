//
//  UIViewExtension.swift
//  CallingTalk
//
//  Created by Vu Tran on 2/12/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit

extension UIView {
    class func loadFromNibNamed<T: UIView>(type: T.Type) -> T? {
        return UINib(nibName: String(describing: T.self), bundle: nil).instantiate(withOwner: nil,
                                                                                   options: nil)[0] as? T
    }
    
    public func border(color: UIColor, width: CGFloat) {
        layer.borderColor = color.cgColor
        layer.borderWidth = width
        layer.cornerRadius = corner
        layer.masksToBounds = true
    }
    
    public var corner: CGFloat {
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = true
        }
        get {
            return layer.cornerRadius
        }
    }
}

public extension UIAlertController {
    func show() {
        let win = UIWindow(frame: UIScreen.main.bounds)
        let vc = UIViewController()
        vc.view.backgroundColor = .clear
        win.rootViewController = vc
        win.windowLevel = UIWindow.Level.alert + 1
        win.makeKeyAndVisible()
        vc.present(self, animated: true, completion: nil)
    }
}
extension NSLayoutConstraint {
    func constraintWithMultiplier(_ multiplier: CGFloat) -> NSLayoutConstraint {
        return NSLayoutConstraint(item: self.firstItem!, attribute: self.firstAttribute, relatedBy: self.relation, toItem: self.secondItem, attribute: self.secondAttribute, multiplier: multiplier, constant: self.constant)
    }
}

extension UITextView {
    func dynamicHeight(heightConstraint: NSLayoutConstraint, defaultHeight: CGFloat? = nil, minHeight: CGFloat? = nil) {
        let fixedWidth = self.frame.size.width
        self.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
        let newSize = self.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
        
        var newHeight: CGFloat = 0.0
        if let height = defaultHeight {
            newHeight = height
        } else {
            newHeight = newSize.height < 40 ? 40 : newSize.height
        }
        if kScreenSize.width == DeviceType.iPhone5.size.width && newHeight > 50 {
            newHeight = 60
        }
        if let minH = minHeight, newHeight < minH {
            print("H: ", minH)
            heightConstraint.constant = minH
        } else {
            print("H: ", newHeight)
            heightConstraint.constant = newHeight
        }
    }
    
    func centerVertically() {
        let fittingSize = CGSize(width: bounds.width, height: CGFloat.greatestFiniteMagnitude)
        let size = sizeThatFits(fittingSize)
        let topOffset = (bounds.size.height - size.height * zoomScale) / 2
        let positiveTopOffset = max(0, topOffset)
        contentOffset.y = -positiveTopOffset
    }
}
