//
//  DataExtension.swift
//  CallingTalk
//
//  Created by Toof on 2/26/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import Foundation

extension Data {
    
    var isValidNetworkSize: Bool {
        get {
            return Double(self.count) / 1024.0 / 1024.0 <= 5.0
        }
    }
    
    func toJSON() -> Any? {
        do {
            return try JSONSerialization.jsonObject(
                with: self,
                options: JSONSerialization.ReadingOptions.allowFragments
            )
        } catch {
            return nil
        }
    }
    
    func toString() -> String? {
        return String(data: self, encoding: .utf8)
    }
}
