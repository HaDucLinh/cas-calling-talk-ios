//
//  UILabelExtension.swift
//  CallingTalk
//
//  Created by Vu Tran on 2/12/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit

extension UILabel {
    func lineSpacing(text: String, space: CGFloat, font: UIFont) {
        
        let attributedString = NSMutableAttributedString(string: text)
        
        let styple = NSMutableParagraphStyle()
        styple.lineSpacing = space
        
        attributedString.addAttribute(NSAttributedString.Key.font, value: font, range: NSRange(location: 0, length: attributedString.length - 1))
        self.attributedText = attributedString
    }
    
    func updateActiveNumber(_ activeNumber: Int, inTotalNumber total: Int) {
        let text = "\(activeNumber) /\(total)"
        let attributedString = NSMutableAttributedString(string: text, attributes: [
            .font: UIFont.ct.font(.NotoSansCJKjp, type: .regular, size: 14.0),
            .foregroundColor: UIColor.ct.totalPersonTextColor
            ])
        if activeNumber > 0 {
            attributedString.addAttributes([
                .font: UIFont.ct.font(.NotoSansCJKjp, type: .bold, size: 16.0),
                .foregroundColor: UIColor.ct.numberOfPersonJoiningTextColor
                ], range: NSRange(location: 0, length: "\(activeNumber)".count))
        }
        self.attributedText = attributedString
    }
}
