//
//  UIButtonExtension.swift
//  CallingTalk
//
//  Created by Van Trung on 1/17/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import Foundation
import UIKit

extension UIButton {
    
    func setUnderlineTitle(_ title: String, font: UIFont, color: UIColor) {
        let underlineAttributes = NSAttributedString(string: title,
                                                     attributes: [.font: font,
                                                                  .foregroundColor: color,
                                                                  .underlineStyle: 1])
        self.setAttributedTitle(underlineAttributes, for: .normal)
    }
    
    func alignmentCenter(_ alignment: CGFloat, title: String? = nil, image: UIImage? = nil) {
        if let title = title {
            self.setTitle(title, for: .normal)
        }
        
        if let image = image {
            self.setImage(image.withRenderingMode(.alwaysOriginal), for: .normal)
        }
        
        self.titleLabel?.lineBreakMode = .byWordWrapping
        self.titleLabel?.textAlignment = .center
        self.adjustsImageWhenHighlighted = false
        
        // set content inset
        guard let titleLabel = self.titleLabel else { return }
        guard let buttonImage = self.imageView?.image else { return }
        guard let titleSize = titleLabel.text?.size(withAttributes: [.font: titleLabel.font]) else { return }
        
        self.imageEdgeInsets = UIEdgeInsets(top: alignment, left: 0.0, bottom: self.frame.size.height/2, right: -titleSize.width)
        self.titleEdgeInsets = UIEdgeInsets(top: self.frame.size.height/2, left: -buttonImage.size.width, bottom: alignment, right: 0.0)
    }
    
}
