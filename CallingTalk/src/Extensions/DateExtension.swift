//
//  DateExtension.swift
//  CallingTalk
//
//  Created by Toof on 3/4/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import Foundation

extension Date {
    /// convert date string to system date object
    var stringLocale: String {
        
        var outputdateFormat = "yyyy/MM/dd"
        if let code = Locale.current.languageCode {
            switch code {
            case "en":
                outputdateFormat = "yyyy/MM/dd HH:mm"
            case "ja":
                outputdateFormat = "yyyy年MM月dd日 HH:mm"
            default:
                outputdateFormat = "yyyy年MM月dd日 HH:mm"
            }
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = outputdateFormat
        dateFormatter.locale = Locale.current
        
        return dateFormatter.string(from: self)
    }
}

// MARK: - Enums
public extension Date {
    
    /// SwifterSwift: Day name format.
    ///
    /// - threeLetters: 3 letter day abbreviation of day name.
    /// - oneLetter: 1 letter day abbreviation of day name.
    /// - full: Full day name.
    enum DayNameStyle {
        
        /// 3 letter day abbreviation of day name.
        case threeLetters
        
        /// 1 letter day abbreviation of day name.
        case oneLetter
        
        /// Full day name.
        case full
        
    }
    
    /// SwifterSwift: Month name format.
    ///
    /// - threeLetters: 3 letter month abbreviation of month name.
    /// - oneLetter: 1 letter month abbreviation of month name.
    /// - full: Full month name.
    enum MonthNameStyle {
        
        /// 3 letter month abbreviation of month name.
        case threeLetters
        
        /// 1 letter month abbreviation of month name.
        case oneLetter
        
        /// Full month name.
        case full
        
    }
    
}

// MARK: - Properties
public extension Date {
    /// SwifterSwift: User’s current calendar.
    var calendar: Calendar {
        return Calendar.current
    }
    
    var day: Int {
        get {
            return Calendar.current.component(.day, from: self)
        }
        set {
            let allowedRange = Calendar.current.range(of: .day, in: .month, for: self)!
            guard allowedRange.contains(newValue) else { return }
            
            let currentDay = Calendar.current.component(.day, from: self)
            let daysToAdd = newValue - currentDay
            if let date = Calendar.current.date(byAdding: .day, value: daysToAdd, to: self) {
                self = date
            }
        }
    }
    
    func string(withFormat format: String = "dd/MM/yyyy HH:mm:ss.s") -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: self)
    }
    
    func stringToJapanTime(withFormat format: String = kDateGetListChannel) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.calendar = Calendar.init(identifier: .gregorian)
        dateFormatter.timeZone = TimeZone(identifier: "JST")
        let dateString = dateFormatter.string(from: self)
        
        return dateString
    }
    
    func dateString(ofStyle style: DateFormatter.Style = .medium) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = .none
        dateFormatter.dateStyle = style
        return dateFormatter.string(from: self)
    }
    
    func dateTimeString(ofStyle style: DateFormatter.Style = .medium) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = style
        dateFormatter.dateStyle = style
        return dateFormatter.string(from: self)
    }
 
    func timeString(ofStyle style: DateFormatter.Style = .medium) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = style
        dateFormatter.dateStyle = .none
        return dateFormatter.string(from: self)
    }

    func adding(_ component: Calendar.Component, value: Int) -> Date {
        return Calendar.current.date(byAdding: component, value: value, to: self)!
    }

}
