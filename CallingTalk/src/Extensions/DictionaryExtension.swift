//
//  DictionaryExtension.swift
//  CallingTalk
//
//  Created by Toof on 2/26/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import Foundation

extension Dictionary {
    
    mutating func updateValues(_ info: [Key: Value]?) {
        guard let info = info else { return }
        for (key, value) in info {
            self[key] = value
        }
    }
    
}
