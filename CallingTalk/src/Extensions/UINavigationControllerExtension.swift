//
//  UINavigationControllerExtension.swift
//  CallingTalk
//
//  Created by Toof on 2/13/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit

extension UINavigationController {
    open override var preferredStatusBarStyle: UIStatusBarStyle {
        return self.topViewController?.preferredStatusBarStyle ?? .lightContent
    }
}

public enum NavigationBarStyle {
    case appTheme
    case white
    case none
}

extension UINavigationBar {
    open func setupNavigationBarWithColor(_ styleColor: NavigationBarStyle = .none) {
        switch styleColor {
        case .appTheme:
            self.barTintColor = UIColor.ct.mainBackgroundColor
            self.isTranslucent = false
            
            self.tintColor = UIColor.white
            self.titleTextAttributes = [
                .font: UIFont.ct.font(.NotoSansCJKjp, type: .bold, size: 16.0),
                .foregroundColor: UIColor.ct.headerText
            ]
        case .white:
            self.barTintColor = UIColor.white
            self.isTranslucent = false
            
            self.tintColor = UIColor.ct.mainBackgroundColor
            self.titleTextAttributes = [
                .font: UIFont.ct.font(.NotoSansCJKjp, type: .bold, size: 16.0),
                .foregroundColor: UIColor.ct.mainBackgroundColor
            ]
        case .none:
            self.setBackgroundImage(UIImage(), for: .default)
            self.isTranslucent = true
            
            self.tintColor = UIColor.white
            self.titleTextAttributes = [
                .font: UIFont.ct.font(.NotoSansCJKjp, type: .bold, size: 16.0),
                .foregroundColor: UIColor.white
            ]
        }
        
        self.shadowImage = UIImage()
    }
}

