//
//  UIApplicationExtension.swift
//  CallingTalk
//
//  Created by Van Trung on 3/28/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import Foundation

extension UIApplication {
    class func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topViewController(controller: selected)
            }
        }
        if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)
        }
        return controller
    }
    
    class func mainViewController() -> MainVC? {
        guard let tabBarController = UIApplication.shared.keyWindow?.rootViewController as? UITabBarController,
            let talkNavigationController = tabBarController.viewControllers!.first as? UINavigationController else {
            return nil
        }
        
        return talkNavigationController.viewControllers.first as? MainVC
    }
    
    var statusBarView: UIView? {
        return value(forKey: "statusBar") as? UIView
    }
    
    static var appName: String {
        return (Bundle.main.object(forInfoDictionaryKey: "CFBundleName") as? String) ?? "Calling Talk"
    }
    
    static var appVersion: String? {
        return Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String
    }
    
    static var buildVersion: String? {
        return Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as? String
    }
}
