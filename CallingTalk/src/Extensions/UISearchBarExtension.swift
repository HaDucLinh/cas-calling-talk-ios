//
//  UISearchControllerExtension.swift
//  CallingTalk
//
//  Created by Toof on 2/14/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

extension UISearchBar {
    
    public
    func customWithPlaceholder(_ placeholderString: String) {
        // Set styles for search
        self.searchBarStyle = .minimal
        self.isTranslucent = false
        self.backgroundColor = UIColor.ct.searchBarBackgroundColor
        self.barTintColor = UIColor.ct.searchBarBackgroundColor
        
        // Increase spacing between icon and text
        self.setImage(UIImage(named: "ic_search")?.withRenderingMode(.alwaysOriginal), for: .search, state: .normal)
        self.setPositionAdjustment(UIOffset(horizontal: 5.0, vertical: 0.0), for: .search)
        self.searchTextPositionAdjustment = UIOffset(horizontal: 7.0, vertical: 0.0)
        
        // Find the index of the search field in the search bar subviews.
        if  let index = self.indexOfSearchFieldInSubviews() {
            
            let searchBarView = self.subviews.first
            guard let searchField: UITextField = searchBarView?.subviews[index] as? UITextField else { return }
            
            searchField.clearButtonMode = .whileEditing
            searchField.tintColor = UIColor.black
            searchField.textColor = UIColor.black
            searchField.backgroundColor = UIColor.ct.searchFieldBackgroundColor
            
            // Custom font search field
            searchField.font = UIFont.ct.font(.NotoSansCJKjp, type: .regular, size: 15.0)
            searchField.attributedPlaceholder = NSAttributedString(string: placeholderString,
                                                                   attributes: [
                                                                    .font: UIFont.ct.font(.NotoSansCJKjp, type: .regular, size: 15.0),
                                                                    .foregroundColor: UIColor.ct.placeholderSearchTextColor])
            // Custom corner radius search field
            searchField.borderStyle = .none
            searchField.layer.cornerRadius = 4.0
            searchField.layer.borderWidth = 1.0
            searchField.layer.borderColor = UIColor.ct.borderSearchTextFieldColor.cgColor
            searchField.clipsToBounds = true
        }
    }
    
    fileprivate
    func indexOfSearchFieldInSubviews() -> Int? {
        guard let searchBarView = self.subviews.first else { return nil }
        for index in 0..<searchBarView.subviews.count {
            if searchBarView.subviews[index] is UITextField {
                return index
            }
        }
        return nil
    }
    
}

