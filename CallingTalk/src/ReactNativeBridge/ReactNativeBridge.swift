//
//  ReactNativeBridge.swift
//  CallingTalk
//
//  Created by NeoLabx on 3/26/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit
/**
 A controller class that handle all the request from ios Client to React native library.
 All the action will be move to the front functions. Please use it and do not create command it self.
 variable defaultConnector is some kind of anti-patterns, but before we add some context implementation
 I will use this for easy handle now, but I want to make some refactoring right here!!!!
 */
class ReactNativeBridge: NSObject, ReactNativeBridging {
    var bridgeDidReceivedInformation: (([AnyHashable : Any]?) -> Void)?
    var reactNativeConnector:ReactNativeConnectable!
    private static var defaultConnector = NotificationConnector()
    init(with connector:ReactNativeConnectable = NotificationConnector()) {
        super.init()
        self.reactNativeConnector = connector
        self.setupConnector()
    }
    
    func muteMicroPhone() {
        let muteAction = MuteCommand.init(newMuteStatus: true, withConnector: self.reactNativeConnector)
        muteAction.excuteReactNative()
    }
    
    func unMuteMicroPhone() {
        let unMuteAction = MuteCommand.init(newMuteStatus: false, withConnector: self.reactNativeConnector)
        unMuteAction.excuteReactNative()
    }
    
    func updateConferenceName(withNewName name:String) {
        let updateConferenCommand = UpdateConferenceNameCommand.init(withConferenceName:name, withConnector: self.reactNativeConnector )
        updateConferenCommand.excuteReactNative()
    }
    
    func updateCallingSetting(withNewHost host:String, andToken token:String, andLocalConfig localConfig: String) {
        let updateHostSettingCommand = UpdateConferenceSettingCommand.init(withHost: host,
                                                                           andUserToken: token,
                                                                           andLocalConfig: localConfig)
        updateHostSettingCommand.excuteReactNative()
    }
    static func getDefaultBrigde() -> ReactNativeBridge {
        return ReactNativeBridge.init(with: ReactNativeBridge.defaultConnector)
    }
}
extension ReactNativeBridge {
    // private setup connector
    private func setupConnector() {
        self.reactNativeConnector.receivedInformationFromReact = { [weak self] receivedInfo in
            guard let self = self else {
                return
            }
            if let concreteDelegate = self.bridgeDidReceivedInformation {
                concreteDelegate(receivedInfo)
            }
        }
    }
}
