//
//  UpdateConferenceNameCommand.swift
//  CallingTalk
//
//  Created by NeoLabx on 4/1/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit

class UpdateConferenceNameCommand: ReactNativeCommandable {
    private var conferenceName = ""
    private var connector:ReactNativeConnectable
    
    init(withConferenceName updateConferenceName:String = "",
         withConnector:ReactNativeConnectable = NotificationConnector()) {
        self.conferenceName = updateConferenceName
        self.connector = withConnector
    }
    func excuteReactNative() {
        let newConferenceName: [AnyHashable : Any] = ["value": self.conferenceName];
        self.connector.sendMessageToReactWith(command: ReactCommand.updateConferenceName.rawValue, messageValue: newConferenceName)
    }
}
