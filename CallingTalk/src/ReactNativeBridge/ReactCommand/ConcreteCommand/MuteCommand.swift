//
//  MuteCommand.swift
//  CallingTalk
//
//  Created by NeoLabx on 3/26/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit
class MuteCommand: ReactNativeCommandable {
    private var isMute = false
    private var connector:ReactNativeConnectable
    
    init(newMuteStatus requestMute: Bool, withConnector:ReactNativeConnectable = NotificationConnector() ) {
        self.isMute = requestMute
        self.connector = withConnector
    }
    func excuteReactNative() {
        let messageCommand = self.isMute ? ReactCommand.mute.rawValue : ReactCommand.unmute.rawValue
        connector.sendMessageToReactWith(command: messageCommand, messageValue: nil)
    }
}
