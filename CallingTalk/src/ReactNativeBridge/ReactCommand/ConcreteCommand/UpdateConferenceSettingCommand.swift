//
//  UpdateConferenceSettingCommand.swift
//  CallingTalk
//
//  Created by NeoLabx on 4/3/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit

class UpdateConferenceSettingCommand: ReactNativeCommandable {
    private var hostSetting = ""
    private var tokenSetting = ""
    private var localConfig = ""
    private var connector:ReactNativeConnectable
    
    init(withHost host:String = "",
         andUserToken token:String = "",
         andLocalConfig localConfig:String = "",
         withConnector:ReactNativeConnectable = NotificationConnector()) {
        self.hostSetting = host
        self.tokenSetting = token
        self.connector = withConnector
    }
    func excuteReactNative() {
        let newSetting: [AnyHashable : Any] = ["host": self.hostSetting,"token":self.tokenSetting,"localconfig":localConfig];
        self.connector.sendMessageToReactWith(command: ReactCommand.updateHostSetting.rawValue, messageValue: newSetting)
    }
}
