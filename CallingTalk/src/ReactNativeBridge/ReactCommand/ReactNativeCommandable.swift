//
//  Command.swift
//  CallingTalk
//
//  Created by NeoLabx on 3/26/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit

protocol ReactNativeCommandable {
    func excuteReactNative()
}
