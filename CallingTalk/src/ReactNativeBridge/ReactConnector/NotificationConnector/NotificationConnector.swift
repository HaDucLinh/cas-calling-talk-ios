//
//  NotificationConnector.swift
//  CallingTalk
//
//  Created by NeoLabx on 3/26/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit

class NotificationConnector: ReactNativeConnectable {
    var receivedInformationFromReact: (([AnyHashable: Any]?) -> Void)?
    
    private let notification_sending_key = "NATIVE_EVENT"
    private let notification_receiving_key = "JITSI_NOTIFY_STATE_INTEVAL_NAME"
    
    init(){
        NotificationCenter.default.addObserver(self, selector: #selector(handle(withNotification:)), name: NSNotification.Name(rawValue: notification_receiving_key), object: nil)
    }
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: notification_receiving_key), object: nil)
    }
    @objc func handle(withNotification notification : NSNotification) {
        if let concreteDelegate = self.receivedInformationFromReact {
            concreteDelegate(notification.userInfo)
        }
    }
    func sendMessageToReactWith(command:String, messageValue: Any?) {
        var message: [AnyHashable : Any] = ["command":command]
        if let messageUpdate = messageValue as? [AnyHashable : Any] {
            message = message.merging(messageUpdate) { $1 }
        }
        NotificationCenter.default.post(name: Notification.Name(notification_sending_key), object: self, userInfo: message)
    }
}
