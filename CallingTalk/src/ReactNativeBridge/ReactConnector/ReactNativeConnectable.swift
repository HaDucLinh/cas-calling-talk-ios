//
//  ReactNativeConnectable.swift
//  CallingTalk
//
//  Created by NeoLabx on 3/26/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit
enum ReactCommand: String {
    case mute = "MUTE"
    case unmute = "UNMUTE"
    case updateConferenceName = "UPDATE_CONFERENCENAME"
    case updateHostSetting = "UPDATE_CONFERENCE_SETTING"
}
protocol ReactNativeConnectable {
    func sendMessageToReactWith(command:String, messageValue:Any?)
    var receivedInformationFromReact: (([AnyHashable: Any]?) -> Void)? { get set }
}
