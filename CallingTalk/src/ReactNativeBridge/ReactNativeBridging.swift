//
//  ReactNativeConnectable.swift
//  CallingTalk
//
//  Created by NeoLabx on 3/26/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit

protocol ReactNativeBridging {
    func muteMicroPhone()
    func unMuteMicroPhone()
    func updateConferenceName(withNewName name:String)
    func updateCallingSetting(withNewHost host:String, andToken token:String, andLocalConfig localConfig: String)
    var bridgeDidReceivedInformation: (([AnyHashable: Any]?) -> Void)? { get set }
}
