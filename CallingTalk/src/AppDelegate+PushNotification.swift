//
//  AppDelegate+PushNotification.swift
//  CallingTalk
//
//  Created by Toof on 5/31/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit
import UserNotifications
import ObjectMapper
import PushKit

extension Notification.Name {
    static let AcceptOrDeclineInvitationPushNotification = Notification.Name("AcceptOrDeclineInvitationPushNotification")
}

// MARK: - PKPushRegistryDelegate

extension AppDelegate: PKPushRegistryDelegate {
    func pushRegistry(_ registry: PKPushRegistry, didUpdate pushCredentials: PKPushCredentials, for type: PKPushType) {
        if type == .voIP && pushCredentials.token.count > 0 {
            UserDefaults.setPushKitToken(pushCredentials.token.map { String(format: "%02.2hhx", $0) }.joined())
        }
    }
    
    func pushRegistry(_ registry: PKPushRegistry, didReceiveIncomingPushWith payload: PKPushPayload, for type: PKPushType, completion: @escaping () -> Void) {
        guard type == .voIP else { return }
        guard UserDefaults.getToken() != nil else { return }
        
        if  let message = payload.dictionaryPayload["message"] as? JSObject,
            let type = message["type"] as? String
        {
            DispatchQueue.main.async { [weak self] in
                guard let `self` = self else { return }
                switch PushMessageType.init(rawValue: type)! {
                case .incoming: break
                    // Warning: Handle for Incoming is hiddend in this version
                    // self.handlePushNotificationWithMessage(message, type: .incoming)
                case .invite_message:
                    self.handlePushNotificationWithMessage(message, type: .invite_message)
                    UITabBarControllerManager.shared.handleNewInvitation()
                case .mention:
                    if  let messageJson = message["data"] as? JSObject,
                        let channelJson = messageJson["channel"] as? JSObject,
                        let message = Mapper<Message>().map(JSON: messageJson),
                        let talkGroup = Mapper<TalkGroup>().map(JSON: channelJson),
                        let userId = User.getProfile()?.id
                    {
                        if message.owner?.id != userId {
                            if Array(message.mentionIds).contains(userId) || message.isMentionAll {
                                if UIApplication.shared.applicationState == UIApplication.State.background || UIApplication.shared.applicationState == UIApplication.State.inactive {
                                    let title = UIApplication.appName
                                    let message = String(format: NSLocalizedString("new_message_mention_alert_message", comment: ""), talkGroup.name)
                                    var userInfo: JSObject = [:]
                                    userInfo["channel"] = channelJson
                                    userInfo["type"] = type
                                    
                                    let notification = LocalNotification(id: UUID().uuidString,
                                                                         title: title,
                                                                         message: message,
                                                                         userInfo: userInfo)
                                    NotificationManager.shared.addLocalNotification(notification)
                                } else {
                                    // Don't show toast in chat screen of the same talkGroup or incoming screen
                                    if let topViewController = UIApplication.topViewController() {
                                        if let _ = topViewController as? IncomingVC { return }
                                        if let chatVC = topViewController as? ChatVC,
                                        chatVC.chatPresenter.talkGroup?.id == talkGroup.id { return }
                                    }
                                    // Show toast
                                    let toastView = MessageToastView.loadViewFromNib()
                                    toastView.toastViewColor = UIColor.ct.incomingToastViewColor.withAlphaComponent(0.94)
                                    toastView.messageToastViewDidTapHandler = { [weak self] in
                                        guard let self = self else {
                                            return
                                        }
                                        
                                        self.enterChat(talkGroupId: talkGroup.id)
                                    }
                                    
                                    var mentionUsers: [User] = []
                                    message.mention_users.forEach({ (user) in
                                        mentionUsers.append(user)
                                    })
                                    if message.isMentionAll {
                                        mentionUsers.append(User.allUser())
                                    }
                                    
                                    let contentTuple = Helpers.replaceUserIdByName(textMessage: message.content, mentionUsers: mentionUsers, isMentionAll: message.isMentionAll)
                                    let messageContent = contentTuple.0
                                                                        
                                    toastView.show(title: talkGroup.name, message: messageContent)
                                }
                            }
                        }
                    }
                case .join_organization:
                    // JOIN_ORGANIZATION
                    // PUT code here
                    break;
                }
            }
        }
    }
    
    fileprivate func handlePushNotificationWithMessage(_ message: JSObject, type: PushMessageType) {
        if  let data = message["data"] as? JSObject,
            let talkGroupId = data["id"] as? Int,
            let talkGroupName = data["name"] as? String,
            let avatarUrl = data["avatar_url"] as? String
        {
            if UIApplication.shared.applicationState == UIApplication.State.background || UIApplication.shared.applicationState == UIApplication.State.inactive {
                let title = NSLocalizedString(type == PushMessageType.incoming ? "incoming_alert_title" : "invitation_alert_title", comment: "")
                let message = String(format: NSLocalizedString("invitation_alert_message", comment: ""), talkGroupName)

                var channelJson: JSObject = [:]
                channelJson["id"] = talkGroupId
                channelJson["name"] = talkGroupName
                channelJson["avatar_url"] = avatarUrl
                
                var userInfo: JSObject = [:]
                userInfo["type"] = type.rawValue
                userInfo["channel"] = channelJson
                let notification = LocalNotification(id: UUID().uuidString,
                                                     title: title,
                                                     message: message,
                                                     userInfo: userInfo)
                
                NotificationManager.shared.addLocalNotification(notification)
            } else {
                let talkGroup = TalkGroup()
                talkGroup.id = talkGroupId
                talkGroup.name = talkGroupName
                talkGroup.avatar = avatarUrl
                
                self.showConfirmAlertView(talkGroup: talkGroup, type: type)
            }
        }
    }
    
    fileprivate func showConfirmAlertView(talkGroup: TalkGroup, type: PushMessageType) {
        let confirmAlertView = ConfirmAlertView()
        confirmAlertView.presenter = ConfirmAlertPresenter(view: confirmAlertView,
                                                           talkGroup: talkGroup,
                                                           type: type == PushMessageType.incoming ? .Incoming : .Invitation)
        confirmAlertView.delegate = self
        if !GlobalVariable.didComeToIncomingVC && !ConfirmAlertView.isShowing() {
            confirmAlertView.show()
        }
    }
}

// MARK: - UNUserNotificationCenterDelegate

extension AppDelegate: UNUserNotificationCenterDelegate {
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        // Remove all local notification that is pending or delivered
        NotificationManager.shared.removeNotifications()
        
        if  let userInfo = response.notification.request.content.userInfo as? JSObject,
            let channelJson = userInfo["channel"] as? JSObject,
            let typeString = userInfo["type"] as? String,
            let type = PushMessageType.init(rawValue: typeString),
            let talkGroup = Mapper<TalkGroup>().map(JSON: channelJson) {
            
            switch type {
            case .mention:
                // If User is calling in another talk, just go to Incoming
                // Otherwise go to Chat
                if let topViewController = UIApplication.topViewController() {
                    if let incomingVC = topViewController as? IncomingVC, incomingVC.presenter.talkGroup.id != talkGroup.id { return }
                }
                enterChat(talkGroupId: talkGroup.id)
            case .invite_message:
                self.showConfirmAlertView(talkGroup: talkGroup, type: type)
            case .incoming: break
                // Warning: Handle for Incoming is hiddend in this version
                //self.showConfirmAlertView(talkGroup: talkGroup, type: type)
            default: break
            }
        }
    }
    
    fileprivate func enterChat(talkGroupId: Int) {
        API.detailTalkGroup(talkGroupId) { (result) in
            DispatchQueue.main.async {
                switch result {
                case .success(let talkGroup):
                    guard let talkGroup = talkGroup as? TalkGroup else { return }
                    if let topViewController = UIApplication.topViewController() {
                        if let chatVC = topViewController as? ChatVC {
                            // If talkgroup's id that is not the same => reload chat's screen
                            guard chatVC.chatPresenter.talkGroup?.id != talkGroup.id else { return }
                            let newChatPresenter = ChatPresenter(view: chatVC, talkGroup: talkGroup, messages: [])
                            chatVC.chatPresenter = newChatPresenter
                            chatVC.chatPresenter.reloadData()
                        }
                        else {
                            let chatVC: ChatVC = ChatVC()
                            chatVC.chatPresenter = ChatPresenter(view: chatVC, talkGroup: talkGroup, messages: [])
                            topViewController.present(chatVC, animated: true, completion: nil)
                        }
                    }
                case .failure(_): break
                }
            }
        }
    }
}

// MARK: - ConfirmAlertViewDelegate

extension AppDelegate: ConfirmAlertViewDelegate {
    func confirmAlertView(_ confirmAlertView: ConfirmAlertView, didSelectButtonWithType type: JoinActionType) {
        switch confirmAlertView.presenter.type {
        case .Invitation:
            NotificationCenter.default.post(name: Notification.Name.AcceptOrDeclineInvitationPushNotification, object: nil)
            UITabBarControllerManager.shared.getNotifications()
            if type == .Accept {
                self.showIncomingVCWithTalkGroupID(confirmAlertView.presenter.talkGroup.id)
            }
        case .Incoming:
            if type == .Accept {
                self.showIncomingVCWithTalkGroupID(confirmAlertView.presenter.talkGroup.id)
            }
        case .JoinByQR(_): break
        }
    }
    
    fileprivate func showIncomingVCWithTalkGroupID(_ id: Int) {
        if let topVC = UIApplication.topViewController() {
            let incomingVC = IncomingVC()
            incomingVC.presenter = IncomingPresenter(view: incomingVC, talkGroupId: id)
            topVC.present(incomingVC, animated: true, completion: nil)
        }
    }
}
