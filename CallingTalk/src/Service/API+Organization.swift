//
//  API+Organization.swift
//  CallingTalk
//
//  Created by Vu Tran on 5/17/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//


import RealmS
import ObjectMapper

extension API {
    static func getAllOrganization(completion: Completion?) {
        API.router.request(MultiEndPoint(OrganizationAPI.getListOrganization)) { (response) in
            DispatchQueue.main.async {
                switch response {
                case .success(let response):
                    guard   let object = response as? JSObject,
                            let data = object["data"] as? [JSObject] else {
                                completion?(.failure(API.Error.json))
                                return
                        }
                    let orgs = Mapper<Organization>().mapArray(JSONArray: data)
                    completion?(.success(orgs))
                case .failure(let error):
                    completion?(.failure(error))
                }
            }
        }
    }
}
