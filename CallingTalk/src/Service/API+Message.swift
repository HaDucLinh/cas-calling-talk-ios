//
//  API+Message.swift
//  CallingTalk
//
//  Created by Vu Tran on 4/3/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import Foundation
import RealmS
import ObjectMapper

extension API {
    static func getListMessage(channelID: Int, shouldHideAudioLogMessages: Bool, completion: @escaping MessageCompletion) {
        API.router.request(MultiEndPoint(MessageAPI.getListMessage(channelID: channelID, shouldHideAudioLogMessages: shouldHideAudioLogMessages))) { (result) in
            DispatchQueue.main.async {
                switch result {
                case .success(let response):
                    guard let object = response as? JSObject else {
                        completion(.failure(API.Error.json), false)
                        return
                    }
                    guard let items = object["data"] as? [JSObject],
                        let messages = Mapper<Message>().mapArray(JSONObject: items) else {
                            completion(.failure(API.Error.json), false)
                            return
                    }
                    let sortedMessages = Array(messages.reversed())
                    completion(.success(sortedMessages), false)
                case .failure(let error):
                    completion(.failure(error), false)
                }
            }
        }
    }
    
    static func sendMessage(channelId: Int,
                            type: MessageContentType = .text,
                            content: String? = nil,
                            fileData: Data? = nil,
                            voiceId: Int? = nil,
                            completion: @escaping Completion)
    {
        API.router.request(MultiEndPoint(MessageAPI.sendMessage(channelId: channelId, type: type, content: content, file: fileData, voiceId: voiceId))) { (result) in
            DispatchQueue.main.async {
                switch result {
                case .success(let response):
                    guard let object = response as? JSObject else {
                        completion(.failure(API.Error.json))
                        return
                    }
                    guard let item = object["data"] as? JSObject,
                        let messages = Mapper<Message>().map(JSON: item) else {
                            completion(.failure(API.Error.json))
                            return
                    }
                    completion(.success(messages))
                case .failure(let error):
                    completion(.failure(error))
                }
            }
        }
    }
    
    static func uploadAudioLogFileWith(path: String, toChannelId: Int ,fromUser: String, completion: @escaping ((Result<Any>?) -> Void)) {
        API.router.request(MultiEndPoint(MessageAPI.uploadAudioLogFileWith(path: path, toChannelId: toChannelId, fromUser: fromUser))) { (result) in
            DispatchQueue.main.async {
                //audio/mp4
                switch result {
                case .success:
                    //TODO: should add handle the message in here
                    completion(result)
                case .failure(let errorFailure):
                    logService.logError(error: errorFailure)// should log the error
                }
            }
        }
    }
    
    static func loadMoreMessages(channelID: Int, lastMessageID: Int, shouldHideAudioLogMessages: Bool, completion: @escaping MessageCompletion) {
        API.router.request(MultiEndPoint(MessageAPI.loadMoreMessage(channelId: channelID, lastMessageId: lastMessageID, shouldHideAudioLogMessages: shouldHideAudioLogMessages))) { (result) in
            DispatchQueue.main.async {
                switch result {
                case .success(let response):
                    guard let object = response as? JSObject else {
                        completion(.failure(API.Error.json), false)
                        return
                    }
                    guard let items = object["data"] as? [JSObject],
                        let messages = Mapper<Message>().mapArray(JSONObject: items) else {
                            completion(.failure(API.Error.json), false)
                            return
                    }
                    let sortedMessages = Array(messages.reversed())
                    completion(.success(sortedMessages), false)
                case .failure(let error):
                    completion(.failure(error), false)
                }
            }
        }
    }
    
    static func maskAsRead(messageID: Int, completion: @escaping Completion) {
        API.router.request(MultiEndPoint(MessageAPI.markAsRead(messageID: messageID))) { (result) in
            DispatchQueue.main.async {
                switch result {
                case .success:
                    completion(.success(true))
                case .failure(let error):
                    completion(.failure(error))
                }
            }
        }
    }
}
