//
//  ThemeStyleable.swift
//  CallingTalk
//
//  Created by NeoLabx on 4/4/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit
struct AppStyle {
    var mainBackgroundColor = UIColor.rgb(red: 217, green: 9, blue: 9)
    var iconHightLight = UIColor.rgb(red: 217, green: 9, blue: 9)
    var headerText = UIColor.white
    var headerTextSubtitle = UIColor.white
    var bottomTabbarColor = UIColor.white
    var redDisableButton = UIColor.rgb(red: 240, green: 157, blue: 157)
    var roleAuthorViewColor = UIColor.rgb(red: 85.0, green: 204.0, blue: 204.0)
    var roleAdminViewColor = UIColor.rgb(red: 217, green: 9, blue: 9)
    
    // AdminSMSVC's screen
    var underlineTitleButtonColor = UIColor.rgb(red: 153.0, green: 153.0, blue: 153.0)
    var grayBorderColor = UIColor.rgb(red: 204.0, green: 204.0, blue: 204.0)
    var okAlertButtonColor = UIColor.rgb(red: 85.0, green: 204.0, blue: 204.0)
    
    // TabBar Color
    var selectItemColor = UIColor.rgb(red: 217, green: 9, blue: 9)
    var unselectedItemColor = UIColor.rgb(red: 153.0, green: 153.0, blue: 153.0)
    
    // Main's screen
    var borderImageColor = UIColor.rgb(red: 85.0, green: 204.0, blue: 204.0)
    var searchBarBackgroundColor = UIColor.rgb(red: 240.0, green: 240.0, blue: 240.0)
    var searchFieldBackgroundColor = UIColor.rgb(red: 251.0, green: 252.0, blue: 253.0)
    var placeholderSearchTextColor = UIColor.rgb(red: 197.0, green: 197.0, blue: 197.0)
    var borderSearchTextFieldColor = UIColor.rgb(red: 204.0, green: 204.0, blue: 204.0)
    var popTipBackgroundColor = UIColor.rgb(red: 128.0, green: 128.0, blue: 128.0)
    var totalPersonTextColor = UIColor.rgb(red: 153.0, green: 153.0, blue: 153.0)
    var numberOfPersonJoiningTextColor = UIColor.rgb(red: 85.0, green: 204.0, blue: 204.0)
    
    var redKeyColor = UIColor.rgb(red: 217, green: 9, blue: 9)
    var grayPlaceHolderTextColor = UIColor.rgb(red: 204.0, green: 204.0, blue: 204.0)
    
    // Member's screen
    var textNoticeReadColor = UIColor.rgb(red: 197.0, green: 197.0, blue: 197.0)
    
    // NewTalk
    var grayInvitedTextColor = UIColor.rgb(red: 128, green: 128, blue: 128)
    var aboutMemberTextColor = UIColor.rgb(red: 153, green: 153, blue: 153)
    
    // Incomming
    var tipToTalkTextColor = UIColor.rgb(red: 153.0, green: 153.0, blue: 153.0)
    var pushModeColor = UIColor.rgb(red: 255.0, green: 238.0, blue: 17.0)
    var handFreeModeColor = UIColor.rgb(red: 85.0, green: 204.0, blue: 204.0)
    var pushModeBlurColor = UIColor.rgb(red: 65.0, green: 55.0, blue: 1.0)
    var handFreeModeBlurColor = UIColor.rgb(red: 18.0, green: 39.0, blue: 60.0)
    var recordOnTitleButtonColor = UIColor.rgb(red: 85.0, green: 204.0, blue: 204.0)
    var recordOffTitleButtonColor = UIColor.rgb(red: 255.0, green: 68.0, blue: 68.0)
    var incomingToastViewColor = UIColor.rgb(red: 85.0, green: 85.0, blue: 85.0)
    
    // Setting
    var grayVersionColor = UIColor.rgb(red: 204.0, green: 204.0, blue: 204.0)
    
    // Chat
    var placeHolderChatTextColor = UIColor.rgb(red: 153.0, green: 153.0, blue: 153.0)
    var highlightMessageTextColor = UIColor.rgb(red: 85.0, green: 204.0, blue: 204.0)
    var grayMessageTextColor = UIColor.rgb(red: 197.0, green: 197.0, blue: 197.0)
    var mentionBackgroundColor = UIColor.rgb(red: 68.0, green: 68.0, blue: 68.0)
    var tagViewBackgroundColor = UIColor.rgb(red: 248, green: 250, blue: 251)
    var boderTagViewColor = UIColor.rgb(red: 153, green: 153, blue: 153)
    var selectAudioSentenceBackgroundColor = UIColor.rgb(red: 222.0, green: 255.0, blue: 255.0)
    var talkConnectionIssusAlert = UIColor.rgb(red: 85.0, green: 85.0, blue: 85.0)
    var badgeBackgroundColor = UIColor.rgb(red: 217, green: 9, blue: 9)

    // Incoming's background
    var defaultIncomingBackgroundColor = UIColor.rgb(red: 79.0, green: 86.0, blue: 1.0)
}
class ReplaceAbleImages {
    private var postfix = ""
    //TODO:should read it from config
    let imagesCustom = ["img_background",
                        "ic_qr_code",
                        "ic-talk-begin",
                        "ic_local_camera",
                        "ic-edit-profile",
                        "ic-talk-help",
                        "bg_alert",
                        ]
    init(withPostFix postfix: String) {
        self.postfix = postfix
    }
    func getReplaceableImage(fromImageNamed name: String) -> String{
        if let _ = imagesCustom.first(where: { (item) -> Bool in
            item == name
        }){
            return "\(name)_\(self.postfix)"
        }
        return name
    }
}
class ReplaceAbleTexts {
    private var postfix = ""
    //TODO:should read it from config
    let textsCustom = ["notice_start_label_text"
                        ]
    init(withPostFix postfix: String) {
        self.postfix = postfix
    }
    func getReplaceableText(fromTextNamed name: String) -> String{
        if let _ = textsCustom.first(where: { (item) -> Bool in
            item == name
        }){
            return "\(name)_\(self.postfix)"
        }
        return name
    }
}
protocol ThemeStyleable {
    func getAppStyle() -> AppStyle
    func getImageReplace(withName name:String ) -> String
    func getTextReplace(withName name:String ) -> String
}
