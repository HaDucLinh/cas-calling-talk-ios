//
//  ThemeOmiseno.swift
//  CallingTalk
//
//  Created by NeoLabx on 4/4/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit

class ThemeOmiseno: ThemeStyleable {
    let mainColor = UIColor.rgb(red: 250, green: 238, blue: 1)
    let grayBarColorColor = UIColor.rgb(red: 66, green: 66, blue: 66)
    let replaceAbleImage = ReplaceAbleImages.init(withPostFix: "osemino")
    let replaceAbleText = ReplaceAbleTexts.init(withPostFix: "osemino")
    let badgeBackgroundColor = UIColor.rgb(red: 217, green: 9, blue: 9)

    init() {
        UIApplication.shared.statusBarStyle = .default
    }
    func getAppStyle() -> AppStyle {
        var appStyleOsemino = AppStyle()
        appStyleOsemino.mainBackgroundColor = self.mainColor
        appStyleOsemino.selectItemColor = self.mainColor
        appStyleOsemino.iconHightLight = appStyleOsemino.grayInvitedTextColor
        appStyleOsemino.headerText = UIColor.black
        appStyleOsemino.bottomTabbarColor = grayBarColorColor
        appStyleOsemino.headerTextSubtitle = UIColor.black.withAlphaComponent(0.47)
        appStyleOsemino.badgeBackgroundColor = self.badgeBackgroundColor
        return appStyleOsemino
    }
    func getImageReplace(withName name:String ) -> String {
        return self.replaceAbleImage.getReplaceableImage(fromImageNamed: name)
    }
    
    func getTextReplace(withName name: String) -> String {
        return self.replaceAbleText.getReplaceableText(fromTextNamed: name)
    }
}
