//
//  ThemeNormal.swift
//  CallingTalk
//
//  Created by NeoLabx on 4/4/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit

class ThemeNormal: ThemeStyleable {
    
    init() {
        UIApplication.shared.statusBarStyle = .lightContent
    }
    func getAppStyle() -> AppStyle {
        var normalStyle = AppStyle()
        normalStyle.headerTextSubtitle = UIColor.ct.placeholderSearchTextColor
        return normalStyle
    }
    
    func getImageReplace(withName name:String ) -> String {
        return name
    }
    
    func getTextReplace(withName name: String) -> String {
        return name
    }
}
