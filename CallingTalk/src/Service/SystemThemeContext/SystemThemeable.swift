//
//  SystemThemeable.swift
//  CallingTalk
//
//  Created by NeoLabx on 4/4/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit

enum ThemeMode: String,CaseIterable {
    case normal = "NORMAL"
    case omiseno = "OMISENO"
}
protocol SystemThemeable {
    init(withThemeMode mode:ThemeMode)
    func appStyleTheme() -> AppStyle
    func getCurrentMode() -> ThemeMode
    func convertImageToTheme(withName name: String) -> String
    func convertTextToTheme(withName name: String) -> String
}
