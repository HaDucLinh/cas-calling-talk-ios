//
//  CustomUIFactoryable.swift
//  CallingTalk
//
//  Created by NeoLabx on 4/5/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit
enum Possistion {
    case left
    case right
}
struct CustomButtonBarSetting {
    var imageName = ""
    var tittleLabel:String = ""
    var tintColor: UIColor?
    var style: UIBarButtonItem.Style
    var action: Selector
    var target: Any
    var possition: Possistion
    init(imageName: String = "",
        tittleLabel: String = "",
        tintColor: UIColor? = nil,
        style: UIBarButtonItem.Style,
        action: Selector,
        target: Any,
        possition: Possistion) {
        self.imageName = imageName
        self.tittleLabel = tittleLabel
        self.tintColor = tintColor
        self.style = style
        self.action = action
        self.target = target
        self.possition = possition
    }
}
protocol CustomUIFactoryable {
    func createAndLoadButtonBar(withSetting settings: CustomButtonBarSetting) -> Bool 
}
