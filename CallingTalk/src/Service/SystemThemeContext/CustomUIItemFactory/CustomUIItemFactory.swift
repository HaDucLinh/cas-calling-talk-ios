//
//  CustomUIItemFactory.swift
//  CallingTalk
//
//  Created by NeoLabx on 4/5/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit

class CustomUIItemFactory: CustomUIFactoryable {
    func createAndLoadButtonBar(withSetting settings: CustomButtonBarSetting) -> Bool {
        let buttonBarItemGenerated = self.detectAndGegeratedButtonItemByImageOrText(withSetting: settings)
        guard let viewController = settings.target as? UIViewController else {
            print("[THEME ]Please check the [target] value, it must be a view controller")
            return false
        }
        (settings.possition == .left) ?
            (viewController.navigationItem.leftBarButtonItem = buttonBarItemGenerated) :
            (viewController.navigationItem.rightBarButtonItem = buttonBarItemGenerated)
        return true
    }
    
}
extension CustomUIItemFactory {
    private func detectAndGegeratedButtonItemByImageOrText(withSetting settings: CustomButtonBarSetting) -> UIBarButtonItem {
        if settings.imageName.isEmpty {
            return self.createButtonBarItemWithTitle(withSetting:settings)
        } else {
            return self.createButtonBarItemWithImage(withSetting:settings)
        }
    }
    private func createButtonBarItemWithImage(withSetting settings: CustomButtonBarSetting) -> UIBarButtonItem{
        let buttonBarItemGenerated = UIBarButtonItem(
            image: UIImage(named: settings.imageName)?.withRenderingMode(.alwaysTemplate),
            style: settings.style,
            target: settings.target,
            action: settings.action)
        buttonBarItemGenerated.tintColor = settings.tintColor ?? UIColor.ct.headerText
        return buttonBarItemGenerated
    }
    private func createButtonBarItemWithTitle(withSetting settings: CustomButtonBarSetting)  -> UIBarButtonItem{
        let buttonBarItemGenerated = UIBarButtonItem(
            title: settings.tittleLabel,
            style: settings.style,
            target: settings.target,
            action: settings.action)
        buttonBarItemGenerated.tintColor = settings.tintColor ?? UIColor.ct.headerText
        return buttonBarItemGenerated
    }
}
