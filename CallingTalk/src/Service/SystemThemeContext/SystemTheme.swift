//
//  SystemTheme.swift
//  CallingTalk
//
//  Created by NeoLabx on 4/4/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit

class SystemTheme: SystemThemeable {
    private var currentTheme: ThemeStyleable
    private var currentThemeMode: ThemeMode
    required init(withThemeMode mode:ThemeMode = SystemTheme.readPropertyList()) {
        switch mode {
        case .normal:
            self.currentTheme = ThemeNormal()
        case .omiseno:
            self.currentTheme = ThemeOmiseno()
        }
        self.currentThemeMode = mode
    }
    func getCurrentMode() -> ThemeMode {
        return self.currentThemeMode
    }
    func appStyleTheme() -> AppStyle {
        return currentTheme.getAppStyle()
    }
    func convertImageToTheme(withName name: String) -> String {
        return currentTheme.getImageReplace(withName: name)
    }
    func convertTextToTheme(withName name: String) -> String {
        return currentTheme.getTextReplace(withName: name)
    }
    
    static func readPropertyList() -> ThemeMode {
        guard let targetName = Bundle.main.infoDictionary?["CLIENT_ID"] as? String else {
            return ThemeMode.normal
        }
        return ThemeMode.allCases.first { (themeMode) -> Bool in
            return themeMode.rawValue == targetName
        } ?? ThemeMode.normal
    }
}
