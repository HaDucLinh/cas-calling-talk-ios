//
//  ThemeUIImage.swift
//  CallingTalk
//
//  Created by NeoLabx on 4/4/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//
import Swinject
import UIKit
class ThemeUIImageView: UIImageView {
    @IBInspectable public var imageReferenceTheme: String = "" {
        didSet {
            guard let systemSetting = Container.default.resolve(SystemThemeable.self) else {
                return
            }
            let currentImage = systemSetting.convertImageToTheme(withName: imageReferenceTheme)
            self.image = UIImage.init(named: currentImage)
            self.backgroundColor = UIColor.ct.mainBackgroundColor
        }
    }
}
class ThemeUIButtonView: UIButton {
    @IBInspectable public var imageReferenceTheme: String = "" {
        didSet {
            guard let systemSetting = Container.default.resolve(SystemThemeable.self) else {
                return
            }
            let currentImage = systemSetting.convertImageToTheme(withName: imageReferenceTheme)
            let buildedImage = UIImage.init(named: currentImage)
            self.setImage(buildedImage, for: .normal)
        }
    }
}
