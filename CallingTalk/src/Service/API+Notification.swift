//
//  API+Notification.swift
//  CallingTalk
//
//  Created by Chinh Le on 5/30/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import Foundation

extension API {
    static func getNotifications(completion: @escaping Completion) {
        API.router.request(MultiEndPoint(NotificationAPI.getNotifications)) { (result) in
            DispatchQueue.main.async {
                switch result {
                case .success(let response):
                    guard let object = response as? JSObject, let data = object["data"] as? JSObject else {
                        completion(.failure(API.Error.json))
                        return
                    }
                    completion(.success(data))
                case .failure(let error):
                    completion(.failure(error))
                }
            }
        }
    }
}
