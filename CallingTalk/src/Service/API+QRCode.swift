//
//  API+QRCode.swift
//  CallingTalk
//
//  Created by Toof on 3/18/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import ObjectMapper
import RealmS

extension API {
    
    static func showQRCode(_ talkGroupId: Int, completion: @escaping Completion) {
        API.router.request(MultiEndPoint(QRCodeAPI.show(talkGroupId))) { (result) in
            switch result {
            case .success(let response):
                guard   let object = response as? JSObject,
                        let data = object["data"] as? JSObject,
                        let qrcodeImageUrl = data["qrcode_url"] as? String
                else {
                    completion(.failure(API.Error.json))
                    return
                }
                
                completion(.success(qrcodeImageUrl))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    static func verifyQRCode(_ id: String, completion: @escaping Completion) {
        API.router.request(MultiEndPoint(QRCodeAPI.verify(id))) { (result) in
            switch result {
            case .success(let response):
                guard   let object = response as? JSObject,
                    let data = object["data"] as? JSObject,
                    let talkGroup = Mapper<TalkGroup>().map(JSON: data)
                    else {
                        completion(.failure(API.Error.json))
                        return
                }
                
                completion(.success(talkGroup))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    static func joinTalkGroupByQRCode(_ id: String, completion: @escaping Completion) {
        API.router.request(MultiEndPoint(QRCodeAPI.join(id))) { (result) in
            switch result {
            case .success(let response):
                guard   let object = response as? JSObject,
                        let data = object["data"] as? JSObject,
                        let talkGroup = Mapper<TalkGroup>().map(JSON: data)
                else {
                    completion(.failure(API.Error.json))
                    return
                }
                
                // Subcribe organization if new organization for connect socket
                if let organization = talkGroup.organization {
                    SocketIOManager.sharedInstance.subscribeOrganizations([organization])
                }
                
                completion(.success(talkGroup))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
}

