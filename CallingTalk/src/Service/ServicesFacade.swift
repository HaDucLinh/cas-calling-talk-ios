//
//  ServicesFacade.swift
//  CallingTalk
//
//  Created by NeoLabx on 4/16/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit
import Swinject
class ServicesFacade {
    let audioRecorderServices = AudioServicesRecorder()
    let logService = CallingLogService(withLogStatus: .writeToServer) //default with logging
    init() { // initialize all services, can be depencies inject if needed
        Container.default.register(AudioServicesRecordable.self) {(audioRecorder) -> AudioServicesRecordable in
            return self.audioRecorderServices
        }
        Container.default.register(Logable.self) {(_) -> Logable in
            return self.logService
        }
       
    }
    static func registerBroadcastServices(broadcast: ClientBroadcastable) {
//        if let containerEsisted = Container.default.resolve(ClientBroadcastable.self) {
//            return
//        }
        Container.default.register(ClientBroadcastable.self) {(_) -> ClientBroadcastable in
            return broadcast
        }
    }
}
