//
//  API+Voice.swift
//  CallingTalk
//
//  Created by Vu Tran on 5/10/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import RealmS
import ObjectMapper

extension API {
    static func getListVoiceOfChannel(channelID: Int, completion: MessageCompletion?) {
        API.router.request(MultiEndPoint(VoiceAPI.getListVoices(channelID: channelID))) { (result) in
            DispatchQueue.main.async {
                switch result {
                case .success(let response):
                    guard let object = response as? JSObject, let data = object["data"] as? JSObject else {
                        completion?(.failure(API.Error.json), false)
                        return
                    }
                    var hasMorePages = false
                    if let nextPageUrl = data["next_page_url"] as? String, !nextPageUrl.isEmpty {
                        hasMorePages = true
                    }
                    guard   let items = data["items"] as? [JSObject],
                        let voices = Mapper<Voice>().mapArray(JSONObject: items) else {
                            completion?(.failure(API.Error.json), hasMorePages)
                            return
                    }
                    
                    completion?(.success(voices), hasMorePages)
                case .failure(let error):
                    completion?(.failure(error), false)
                }
            }
        }
    }
    
    static func getListVoiceStatements(completion: MessageCompletion?) {
        API.router.request(MultiEndPoint(VoiceAPI.getListVoiceStatements)) { (result) in
            DispatchQueue.main.async {
                switch result {
                case .success(let response):
                    guard let object = response as? JSObject, let data = object["data"] as? JSObject else {
                        completion?(.failure(API.Error.json), false)
                        return
                    }
                    var hasMorePages = false
                    if let nextPageUrl = data["next_page_url"] as? String, !nextPageUrl.isEmpty {
                        hasMorePages = true
                    }
                    guard   let items = data["items"] as? [JSObject],
                        let voices = Mapper<Voice>().mapArray(JSONObject: items) else {
                            completion?(.failure(API.Error.json), hasMorePages)
                            return
                    }
                    
                    completion?(.success(voices), hasMorePages)
                case .failure(let error):
                    completion?(.failure(error), false)
                }
            }
        }
    }
    
    static func createVoiceStatement(name: String, orgIDs: [Int], completion: Completion?) {
        API.router.request(MultiEndPoint(VoiceAPI.createVoiceStatement(name: name, ids: orgIDs))) { (result) in
            DispatchQueue.main.async {
                switch result {
                case .success(let response):
                    guard let object = response as? JSObject,
                        let data = object["data"] as? JSObject,
                        let voice = Mapper<Voice>().map(JSON: data)
                        else {
                            completion?(.failure(API.Error.json))
                            return
                    }
                    completion?(.success(voice))
                case .failure(let error):
                    completion?(.failure(error))
                }
            }
        }
    }
    
    static func updateVoiceStatement(id: Int, name: String, ids: [Int], completion: Completion?) {
        API.router.request(MultiEndPoint(VoiceAPI.updateVoiceStatement(id: id, name: name, ids: ids))) { (result) in
            DispatchQueue.main.async {
                switch result {
                case .success(let response):
                    guard let object = response as? JSObject,
                        let data = object["data"] as? JSObject,
                        let voice = Mapper<Voice>().map(JSON: data)
                        else {
                            completion?(.failure(API.Error.json))
                            return
                    }
                    completion?(.success(voice))
                case .failure(let error):
                    completion?(.failure(error))
                }
            }
        }
    }
    
    static func deleteVoiceStatement(id: Int, completion: Completion?) {
        API.router.request(MultiEndPoint(VoiceAPI.deleteVoiceStatement(id: id))) { (result) in
            DispatchQueue.main.async {
                switch result {
                case .success( _):
                    completion?(.success(true))
                case .failure(let error):
                    completion?(.failure(error))
                }
            }
        }
    }
}
