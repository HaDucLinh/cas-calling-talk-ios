//
//  ResponseSerializer.swift
//  CallingTalk
//
//  Created by Toof on 2/26/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import Alamofire

extension Request {
    
    static func responseJSONSerializer(log: Bool = true,
                                       response: HTTPURLResponse?,
                                       data: Data?,
                                       error: Error?) -> Result<Any> {
        guard let response = response else {
            return .failure(NSError(status: .noResponse))
        }
        
        if let error = error {
            return .failure(error)
        }
        
        let statusCode = response.statusCode
        
        if 204...205 ~= statusCode { // empty data status code
            return .success([:])
        }
        
        guard 200...299 ~= statusCode else {
            var err: NSError!
            if  let json = data?.toJSON() as? JSObject,
                let messageResponse = json["message"] as? String,
                let status = StatusCode(rawValue: statusCode)
            {
                if  status == .unprocessable,
                    let errorsResponse = json["errors"] as? JSObject,
                    let errors = errorsResponse.first?.value as? [String]
                {
                    err = NSError(domain: response.url?.host, status: status, message: errors.first)
                }
                else {
                    err = NSError(domain: response.url?.host, status: status, message: messageResponse)
                }
            } else {
                err = NSError(domain: response.url?.host, status: StatusCode(rawValue: statusCode) ?? StatusCode.unknown)
            }
            
            return .failure(err)
        }
        
        guard let data = data, let json = data.toJSON() else {
            return Result.failure(API.Error.json)
        }
        
        return .success(json)
    }
    
}

extension DataRequest {
    
    static func responseSerializer() -> DataResponseSerializer<Any> {
        return DataResponseSerializer { _, response, data, error in
            return Request.responseJSONSerializer(log: true,
                                                  response: response,
                                                  data: data,
                                                  error: error)
        }
    }
    
    @discardableResult
    func responseJSON(queue: DispatchQueue = .global(qos: .background),
                      completion: @escaping (DataResponse<Any>) -> Void) -> Self {
        return response(queue: queue,
                        responseSerializer: DataRequest.responseSerializer(),
                        completionHandler: completion)
    }
    
}

