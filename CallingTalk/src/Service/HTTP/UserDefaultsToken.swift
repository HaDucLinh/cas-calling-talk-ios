//
//  UserDefaultsToken.swift
//  CallingTalk
//
//  Created by Toof on 3/4/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import Foundation

extension UserDefaults {
    
    public enum UserDefaultsKey: String {
        case Token
        case PushKitToken
        case FirstLoaded
    }
    
    class func setToken(_ token: String) {
        UserDefaults.standard.set(token, forKey: UserDefaultsKey.Token.rawValue)
        UserDefaults.standard.synchronize()
    }
    
    class func getToken() -> String? {
        return UserDefaults.standard.string(forKey: UserDefaultsKey.Token.rawValue)
    }
    
    class func clearToken() {
        UserDefaults.standard.set(nil, forKey: UserDefaultsKey.Token.rawValue)
        UserDefaults.standard.synchronize()
    }
    
    // Storing PushKit Device's Token
    class func setPushKitToken(_ token: String) {
        UserDefaults.standard.set(token, forKey: UserDefaultsKey.PushKitToken.rawValue)
        UserDefaults.standard.synchronize()
    }
    
    class func getPushKitToken() -> String? {
        return UserDefaults.standard.string(forKey: UserDefaultsKey.PushKitToken.rawValue)
    }
    
    class func clearPushKitToken() {
        UserDefaults.standard.set(nil, forKey: UserDefaultsKey.PushKitToken.rawValue)
        UserDefaults.standard.synchronize()
    }
    
    class func setFirstLoaded() {
        UserDefaults.standard.set(true, forKey: UserDefaultsKey.FirstLoaded.rawValue)
        UserDefaults.standard.synchronize()
    }
    
    class func clearFirstLoad() {
        UserDefaults.standard.set(false, forKey: UserDefaultsKey.FirstLoaded.rawValue)
        UserDefaults.standard.synchronize()
    }
}
