//
//  EndPointType.swift
//  CallingTalk
//
//  Created by Toof on 2/12/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import Foundation

// MARK: - Define properties for EndPointType

public protocol EndPointType {
    
    var baseURL: String { get }
    var path: String { get }
    var method: HTTPMethod { get }
    var headers: HTTPHeaders? { get }
    var task: HTTPTask { get }
    
}
