//
//  Error.swift
//  CallingTalk
//
//  Created by Toof on 2/12/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import Foundation
import Alamofire

typealias Network = NetworkReachabilityManager

public enum StatusCode: Int {
    case success = 200
    case created = 201
    case accepted = 202
    case badRequest = 400
    case unauthorized = 401
    case forbidden = 403
    case notFound = 404
    case methodNotAllowed = 405
    case requestTimeOut = 408
    case unprocessable = 422
    case noResponse = 444
    case internalServerError = 500
    case notImplemented = 501
    case unknown = 999
}

extension StatusCode: CustomStringConvertible {
    public var description: String {
        switch self {
        case .success: // 200
            return  NSLocalizedString("success_status_description_text", comment: "")
        case .created: // 201
            return NSLocalizedString("created_status_description_text", comment: "")
        case .accepted: // 202
            return NSLocalizedString("accepted_status_description_text", comment: "")
        case .badRequest: // 400
            return NSLocalizedString("badRequest_status_description_text", comment: "")
        case .unauthorized: // 401
            return NSLocalizedString("unauthorized_status_description_text", comment: "")
        case .forbidden: // 403
            return NSLocalizedString("forbidden_status_description_text", comment: "")
        case .notFound: // 404
            return NSLocalizedString("notFound_status_description_text", comment: "")
        case .methodNotAllowed: // 405
            return NSLocalizedString("methodNotAllowed_status_description_text", comment: "")
        case .requestTimeOut: // 408
            return NSLocalizedString("requestTimeOut_status_description_text", comment: "")
        case .unprocessable: // 422
            return NSLocalizedString("unprocessable_status_description_text", comment: "")
        case .noResponse: // 444
            return NSLocalizedString("noResponse_status_description_text", comment: "")
        case .internalServerError: // 500
            return NSLocalizedString("internalServerError_status_description_text", comment: "")
        case .notImplemented: // 501
            return NSLocalizedString("notImplemented_status_description_text", comment: "")
        case .unknown: // 999
            return NSLocalizedString("unknown_status_description_text", comment: "")
        }
    }
}

// MARK: - Network

extension Network {
    
    static let shared: Network = {
        guard let manager = Network() else {
            fatalError("Cannot alloc network reachability manager!")
        }
        return manager
    }()
    
}

// MARK: - Define network error

extension API {
    
    // Define errors in mobile's side
    struct Error {
        static let network = NSError(domain: NSCocoaErrorDomain,
                                     message: NSLocalizedString("internet_offline_status_description_text", comment: ""))
        static let json = NSError(domain: NSCocoaErrorDomain,
                                  code: 3_840,
                                  message: NSLocalizedString("parse_json_fail_status_description_text", comment: ""))
    }
    
}

extension Error {
    
    func show() {
        let `self` = self as NSError
        self.show()
    }
    
    public var code: Int {
        let `self` = self as NSError
        return self.code
    }
    
}

extension NSError {
    
    public convenience init(domain: String? = nil, status: StatusCode, message: String? = nil) {
        let domain = domain ?? Bundle.main.bundleIdentifier ?? ""
        let userInfo: [String: String] = [NSLocalizedDescriptionKey: message ?? status.description]
        self.init(domain: domain, code: status.rawValue, userInfo: userInfo)
    }
    
    public convenience init(domain: String? = nil, code: Int = -999, message: String) {
        let domain = domain ?? Bundle.main.bundleIdentifier ?? ""
        let userInfo: [String: String] = [NSLocalizedDescriptionKey: message]
        self.init(domain: domain, code: code, userInfo: userInfo)
    }
    
    func show() { }
    
}

extension String: LocalizedError {
    public var errorDescription: String? {
        return self
    }
}
