//
//  Router.swift
//  CallingTalk
//
//  Created by Toof on 2/12/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import Foundation
import Alamofire

public typealias Result = Alamofire.Result
public typealias HTTPMethod = Alamofire.HTTPMethod
public typealias HTTPHeaders = Alamofire.HTTPHeaders
public typealias Parameters = Alamofire.Parameters
public typealias ParameterEncoding = Alamofire.ParameterEncoding
public typealias URLEncoding = Alamofire.URLEncoding
public typealias JSONEncoding = Alamofire.JSONEncoding

public typealias JSObject = [String: Any]
public typealias JSArray = [JSObject]

/// Closure to be executed when a request has completed.
public typealias Completion = (Result<Any>) -> Void
public typealias TalksCompletion = (Result<Any>, _ hasNewEvents: Bool, _ hasNextPage: Bool) -> Void
public typealias UsersCompletion = (Result<Any>, _ hasNextPage: Bool) -> Void
public typealias MessageCompletion = (Result<Any>, _ hasNextPage: Bool) -> Void

/// Represents an HTTP task.
public enum HTTPTask {
    /// A request with no additional data.
    case requestPlain
    
    /// A requests body set with encoded parameters.
    case requestParameters(Parameters)
    
    /// A "multipart/form-data" upload task.
    case uploadMultipart(Parameters)
    
    case delete
    
    case put(Parameters)
    // case download, upload...etc
}

// MARK: - Define NetworkRouter

protocol NetworkRouter: class {
    associatedtype EndPoint: EndPointType
    func request(_ route: EndPoint, completion: @escaping Completion)
    func cancel()
}
enum FileUpload {
    case image
    case sound
}
// MARK: - Implement

class Router<EndPoint: EndPointType>: NetworkRouter {
    fileprivate var request: DataRequest?

    func request(_ route: EndPoint, completion: @escaping Completion) {
        guard Network.shared.isReachable else {
            completion(.failure(API.Error.network))
            return
        }
        
        let encoding: ParameterEncoding
        if route.method == .post {
            encoding = JSONEncoding.default
        } else {
            encoding = URLEncoding.default
        }
        
        var _headers = API.defaultHTTPHeaders
        _headers.updateValues(route.headers)
        
        var _parameters = API.defaultParameters
        
        switch route.task {
        case .requestPlain:
            self.request = Alamofire.request(route.baseURL + route.path,
                                             method: route.method,
                                             parameters: _parameters,
                                             encoding: encoding,
                                             headers: _headers
                ).responseJSON(completion: { (response) in
                    completion(response.result)
                })
        case .requestParameters(let parameters):
            _parameters.updateValues(parameters)
            self.request = Alamofire.request(route.baseURL + route.path,
                                             method: route.method,
                                             parameters: _parameters,
                                             encoding: encoding,
                                             headers: _headers
                ).responseJSON(completion: { (response) in
                    completion(response.result)
                })
        case .uploadMultipart(let parameters):
            _parameters.updateValues(parameters)
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                for (key, value) in _parameters {
                    if let data = value as? Data {
                        switch self.getDataFileFormat(fromData: data) {
                        case .image:
                            multipartFormData.append(data, withName: key, fileName: "image.jpg", mimeType: "image/jpeg")
                        case .sound:
                            multipartFormData.append(data, withName: key, fileName: "sound.wav", mimeType: "audio/wav")
                        }
//                        if let imageable = UIImage(data: data){
//                            // check if it's image or not!
//                            multipartFormData.append(data, withName: key, fileName: "image.jpg", mimeType: "image/jpeg")
//                        } else {
//                            multipartFormData.append(data, withName: key, fileName: "sound.wav", mimeType: "audio/wav")
//                        }
                    }
                    else {
                        multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key)
                    }
                }
            }, to: route.baseURL + route.path, method: route.method,
               headers: _headers, encodingCompletion: { [weak self] (result) in
                guard let `self` = self else { return }
                switch result{
                case .success(let upload, _, _):
                    self.request = upload.responseJSON { response in
                        completion(response.result)
                    }
                case .failure(let error):
                    completion(.failure(error))
                }
            })
        case .delete:
            self.request = Alamofire.request(route.baseURL + route.path,
                                             method: route.method,
                                             parameters: _parameters,
                                             encoding: encoding,
                                             headers: _headers
                ).responseJSON(completion: { (response) in
                    completion(response.result)
                })
        case .put(let parameters):
            _parameters.updateValues(parameters)
            
            self.request = Alamofire.request(route.baseURL + route.path,
                                             method: route.method,
                                             parameters: _parameters,
                                             encoding: encoding,
                                             headers: _headers
                ).responseJSON(completion: { (response) in
                    completion(response.result)
                })
        }
        // print("REQUEST:... \(String(describing: self.request?.request?.url))")
        // print("HEADER:... \(String(describing: self.request?.request?.allHTTPHeaderFields))")
    }
    
    func cancel() {
        Alamofire.SessionManager.default.session.getAllTasks { (tasks) in
            tasks.forEach{ $0.cancel() }
        }
    }
    private func getDataFileFormat(fromData data: Data) -> FileUpload {
        if UIImage(data: data) != nil {
            return FileUpload.image
        }
        return FileUpload.sound
    }
}
