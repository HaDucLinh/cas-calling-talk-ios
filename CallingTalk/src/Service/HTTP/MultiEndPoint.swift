//
//  MutilEndPoint.swift
//  CallingTalk
//
//  Created by Toof on 2/27/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import Foundation

/// A `EndPointType` used to enable `Router` to process multiple `EndPointType`s.
public enum MultiEndPoint: EndPointType {
    
    case endPoint(EndPointType)
    
    public init(_ endPoint: EndPointType) {
        self = MultiEndPoint.endPoint(endPoint)
    }
    
    public var baseURL: String {
        return endPoint.baseURL
    }
    
    public var path: String {
        return endPoint.path
    }
    
    public var method: HTTPMethod {
        return endPoint.method
    }
    
    public var task: HTTPTask {
        return endPoint.task
    }
    
    public var headers: HTTPHeaders? {
        return endPoint.headers
    }
    
    var endPoint: EndPointType {
        switch self {
        case .endPoint(let endPoint): return endPoint
        }
    }
}
