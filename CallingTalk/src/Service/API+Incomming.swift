//
//  API+Incomming.swift
//  CallingTalk
//
//  Created by Van Trung on 3/20/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import ObjectMapper
import RealmS

extension API {
    static func getChannelSid(channelId: Int, completion: @escaping Completion){
        API.router.request(MultiEndPoint(IncommingAPI.getChannelSid(channelId: channelId))) { (result) in
            switch result {
            case .success(let response):
                guard   let object = response as? JSObject,
                    let data = object["data"] as? JSObject,
                    let channelSid = data["sfu_sid"] as? String
                    else {
                        completion(.failure(API.Error.json))
                        return
                }
                
                completion(.success(channelSid))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
}
