//
//  MessageAPI.swift
//  CallingTalk
//
//  Created by Vu Tran on 4/3/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import Foundation

enum MessageContentType: Int {
    case image = 1
    case text = 2
    case tts = 3
    case sound = 4
    case voiceText = 5
}
enum MessageAPI {
    case getListMessage(channelID: Int, shouldHideAudioLogMessages: Bool)
    case sendMessage(channelId: Int, type: MessageContentType, content: String?, file: Data?, voiceId: Int?)
    case markAsRead(messageID: Int)
    case uploadAudioLogFileWith(path: String, toChannelId: Int,fromUser: String)
    case loadMoreMessage(channelId: Int, lastMessageId: Int, shouldHideAudioLogMessages: Bool)
}

extension MessageAPI: EndPointType {
    
    var baseURL: String {
        return API.defaultBaseURL
    }
    
    var path: String {
        switch self {
        case .getListMessage(channelID: let id, shouldHideAudioLogMessages: let shouldHideAudioLogMessages):
            let hidden = shouldHideAudioLogMessages ? 1 : 0
            return "/channels/\(id)/messages?is_hidden_sound=\(hidden)&per_page=20"
            
        case .sendMessage(channelId: let id, type: _, content: _, file: _, voiceId: _):
            return "/channels/\(id)/messages"
            
        case .markAsRead(messageID: let id):
            return "/messages/\(id)/mark-as-read"

        case .loadMoreMessage(channelId: let channelId, lastMessageId: let messageID, shouldHideAudioLogMessages: let shouldHideAudioLogMessages):
            let hidden = shouldHideAudioLogMessages ? 1 : 0
             return "/channels/\(channelId)/messages?message_id=\(messageID)&is_hidden_sound=\(hidden)&per_page=20"
            
        case .uploadAudioLogFileWith(path: _, toChannelId: let channelID, fromUser: _):
            return "/channels/\(channelID)/messages"
        }   
    }
    
    var method: HTTPMethod {
        switch self {
        case .getListMessage, .loadMoreMessage:
            return .get
        case .markAsRead:
            return .put
        case .uploadAudioLogFileWith, .sendMessage:
            return .post
        }
    }
    
    var headers: HTTPHeaders? {
        return nil
    }
    
    var task: HTTPTask {
        switch self {
        case .getListMessage, .loadMoreMessage:
            return .requestPlain
        case .markAsRead:
            let params: Parameters = [:]
            return .put(params)
        case .sendMessage(channelId: _, type: let type, content: let content, file: let fileData, voiceId: let voiceId):
            var params: Parameters = [ "type": type.rawValue ]
            if let content = content {
                params["content"] = content
            }
            if let fileData = fileData {
                params["file"] = fileData
            }
            if let voiceId = voiceId {
                params["voice_text_id"] = voiceId
            }
            return .uploadMultipart(params)
        case .uploadAudioLogFileWith(let filePath,_ ,let userName):
            let dataSound:Data?
            if let URLofFile = URL.init(string: filePath) {
                do {
                    dataSound = try Data(contentsOf: URLofFile)
                } catch {
                    dataSound = Data.init()
                }
            } else {
                dataSound = Data.init()
            }
    
            let params: Parameters = [
                "content": userName,
                "type": MessageContentType.sound.rawValue,
                "file": dataSound!,
            ]
            
            return .uploadMultipart(params)
        }
    }
}

