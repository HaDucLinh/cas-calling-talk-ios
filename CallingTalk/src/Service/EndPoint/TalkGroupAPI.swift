//
//  TalkGroupAPI.swift
//  CallingTalk
//
//  Created by Vu Tran on 3/15/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import Foundation

enum TalkGroupAPI {
    case List(maxDate: String?, minDate: String?, searchText: String?)
    case Detail(id: Int)
    case DetailUserInTalk(userId: Int, talkId: Int)
    case ListUserToInvite(query: String?, maxDate: String?, minDate: String?)
    case ListUserToInviteTalkGroup(id: Int, query: String?, maxDate: String?, minDate: String?)

    case CreateTalkGroup(name: String, memberIDs: [Int], avatar: Data?, avatarIndex: Int, background: Data?, organizationId: Int?)
    case EditTalkGroup(id: Int, name: String, avatar: Data?, avatarIndex: Int, background: Data?)
    
    case InviteMembersToTalkGroup(groupID: Int, memberIDs: [Int])
    case JoinTalkGroup(id: Int)
    case DeclineJoiningTalkGroup(id: Int)
    case LeaveTalkGroup(groupID: Int)
    case DeleteTalkGroup(groupID: Int)
    case FindUsersIDFromJitsiID(jitsiIDs: [String])
    case RemoveUser(groupID: Int, userID: Int)
}
extension TalkGroupAPI: EndPointType {
    var baseURL: String {
        return API.defaultBaseURL
    }
    
    var path: String {
        switch self {
        case .List:
            return "/channels"
        case .Detail(id: let id):
            return "/channels/\(id)"
        case .DetailUserInTalk(userId: let userId, talkId: let talkId):
            return "/channels/\(talkId)/users/\(userId)"
        case .ListUserToInvite:
            return "/channel-invitation/users"
        case .ListUserToInviteTalkGroup(id: let id, query: _, maxDate: _, minDate: _):
            return "/channel-invitation/users/" + id.description
        case .CreateTalkGroup:
            return "/channels"
        case .InviteMembersToTalkGroup(groupID: let id, memberIDs: _):
            return "/channel-invitation/\(id)/invite"
        case .JoinTalkGroup(id: let id):
            return "/channel-invitation/\(id)/active"
        case .DeclineJoiningTalkGroup(id: let id):
            return "/channel-invitation/\(id)/cancel"
        case .LeaveTalkGroup(groupID: let id):
            return "/channels/\(id)/exit"
        case .DeleteTalkGroup(groupID: let id), .EditTalkGroup(id: let id, name: _, avatar: _, avatarIndex: _, background: _):
            return "/channels/\(id)"
        case .FindUsersIDFromJitsiID:
            return "/jitsi-users/find-user-id"
        case .RemoveUser(groupID: let id, userID: let userID):
            return "/channels/\(id)/users/\(userID)"
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .List, .Detail, .DetailUserInTalk, .ListUserToInvite, .ListUserToInviteTalkGroup:
            return .get
        case .CreateTalkGroup, .EditTalkGroup, .InviteMembersToTalkGroup:
            return .post
        case .JoinTalkGroup, .DeclineJoiningTalkGroup:
            return .post
        case .LeaveTalkGroup:
            return .put
        case .DeleteTalkGroup, .RemoveUser:
            return .delete
        case .FindUsersIDFromJitsiID:
            return .post
        }
    }
    
    var headers: HTTPHeaders? {
        return nil
    }
    
    var task: HTTPTask {
        switch self {
        case .ListUserToInvite(query: let query, maxDate: let maxDate, minDate: let minDate),
             .ListUserToInviteTalkGroup(id: _, query: let query, maxDate: let maxDate, minDate: let minDate):
            var params: Parameters = [:]
            if let maxDate = maxDate {
                params["max_date"] = maxDate
            }
            if let minDate = minDate {
                params["min_date"] = minDate
            }
            if let query = query {
                params["name"] = query
            }
            return .requestParameters(params)
        case .List(maxDate: let maxDate, minDate: let minDate, searchText: let text):
            var params: Parameters = [:]
            if let maxDate = maxDate {
                params["max_event_date"] = maxDate
            }
            if let minDate = minDate {
                params["min_event_date"] = minDate
            }
            if let text = text, !text.isEmpty {
                params["free_word"] = text
            }
            return .requestParameters(params)
        case .Detail, .DetailUserInTalk:
            return .requestPlain
        case .CreateTalkGroup(name: let name, memberIDs: let ids, avatar: let data, avatarIndex: let avatarIndex, background: let backgroundData, organizationId: let organizationId):
            var params: Parameters = [
                "name": name
            ]
            if let organizationId = organizationId {
                params["organization_id"] = organizationId
            }
            for (index, value) in ids.enumerated() {
                params["user_ids[\(index)]"] = value
            }
            if let data = data {
                params["avatar"] = data
                params["avatar_index"] = avatarIndex
            }
            if let backgroundData = backgroundData {
                params["background"] = backgroundData
            }
            return .uploadMultipart(params)

        case .EditTalkGroup(id: _, name: let name, avatar: let avatar, avatarIndex: let avatarIndex, background: let background):
            var params: Parameters = [
                "name": name
            ]
            if let data = avatar {
                params["avatar"] = data
                params["avatar_index"] = avatarIndex
            }
            if let backgroundData = background {
                params["background"] = backgroundData
            }
            return .uploadMultipart(params)
        case .InviteMembersToTalkGroup(groupID: _, memberIDs: let memberIDs):
            var params: Parameters = [:]
            for (index, value) in memberIDs.enumerated() {
                params["user_ids[\(index)]"] = value
            }
            return .uploadMultipart(params)
        case .JoinTalkGroup:
            let params: Parameters = [:]
            return .uploadMultipart(params)
        case .DeclineJoiningTalkGroup:
            let params: Parameters = [:]
            return .uploadMultipart(params)
        case .LeaveTalkGroup:
            let params: Parameters = [:]
            return .put(params)
        case .DeleteTalkGroup, .RemoveUser:
            return .delete
        case .FindUsersIDFromJitsiID(jitsiIDs: let jitsiIDs):
            var params: Parameters = [:]
            let buildedInfo = jitsiIDs.enumerated().forEach { (index, value) in
                params["jitsi_ids[\(index)]"] = value
            }
            return .uploadMultipart(params)
        }
    }
}

