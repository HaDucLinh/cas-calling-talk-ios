//
//  IncommingAPI.swift
//  CallingTalk
//
//  Created by Van Trung on 3/20/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import Foundation

enum IncommingAPI {
    case getChannelSid(channelId: Int)
}

extension IncommingAPI: EndPointType {
    var path: String {
        switch self {
        case .getChannelSid(channelId: let channelId):
            return "/channels/\(channelId)/call"
        }
    }
    
    var method: HTTPMethod {
        return .post
    }
    
    var headers: HTTPHeaders? {
        return nil
    }
    
    var task: HTTPTask {
        return .requestPlain
    }
    
    
    var baseURL: String {
        return API.defaultBaseURL
    }
    
}
