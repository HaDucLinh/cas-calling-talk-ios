//
//  TextToSpeechAPI.swift
//  CallingTalk
//
//  Created by Vu Tran on 3/14/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import AVFoundation
import Alamofire

enum TextToSpeechApi {
    case textToSpeech(_ text: String, languageCode: String, voiceType: VoiceSpeechType)
}

enum VoiceSpeechType: String {
    case EnglishStandardFemale = "en-US-Standard-E"
    case EnglishWaveNetFemale = "en-US-Wavenet-C"
    case JapaneseStandardFemale = "ja-JP-Standard-A"
    case JapaneseWaveNetFemale = "ja-JP-Wavenet-A"
}

var textToSpeechKey = "AIzaSyDo8oawOBJjuSPM8204hKVJU2z8Mu98obk"
extension TextToSpeechApi: EndPointType {
    var baseURL: String {
        return "https://texttospeech.googleapis.com/v1"
    }
    
    var path: String {
        return "text:synthesize"
    }
    
    var method: HTTPMethod {
        return .post
    }
    
    var headers: HTTPHeaders? {
        return [ "X-Goog-Api-Key": textToSpeechKey,
                 "Content-type": "application/json; charset=utf-8"
        ]
    }
    
    var defaultConfig: JSObject {
        var config = JSObject()
        config["audioEncoding"] = "LINEAR16"
        config["pitch"] = "0.00"
        config["speakingRate"] = "1.38"
        return config
    }
    
    var task: HTTPTask {
        switch self {
        case .textToSpeech(let text, languageCode: let code, voiceType: let type):
            var params = JSObject()
            params["audioConfig"] = defaultConfig
            
            var voiceJson = JSObject()
            voiceJson["languageCode"] = code
            voiceJson["name"] = type.rawValue
            params["voice"] = voiceJson
            
            var inputJson = JSObject()
            inputJson["text"] = text
            params["input"] = inputJson
            return .requestParameters(params)
        }
    }
}
