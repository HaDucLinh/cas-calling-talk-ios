//
//  NotificationAPI.swift
//  CallingTalk
//
//  Created by Chinh Le on 5/30/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import Foundation

enum NotificationAPI {
    case getNotifications
}

extension NotificationAPI: EndPointType {
    var baseURL: String {
        return API.defaultBaseURL
    }
    
    var path: String {
        switch self {
        case .getNotifications:
            return "/notifications"
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .getNotifications:
            return .get
        }
    }
    
    var headers: HTTPHeaders? {
        return nil
    }
    
    var task: HTTPTask {
        switch self {
        case .getNotifications:
            return .requestPlain
        }
    }
}
