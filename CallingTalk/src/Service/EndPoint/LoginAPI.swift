//
//  CallingTalkApi.swift
//  CallingTalk
//
//  Created by Toof on 2/26/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import Foundation

enum LoginAPI {
    case staffLogin(_ name: String, avatar: Data, avatarIndex: Int, countryCode: String)
    case adminLogin(_ phoneNumber: String)
    case adminVerify(_ phoneNumber: String, code: String)
    case updateProfile(_ name: String, avatar: Data, avatarIndex: Int, email: String?)
    case listOrganizations
    case getMyProfile
    case logOut
}

extension LoginAPI: EndPointType {
    
    var baseURL: String {
        return API.defaultBaseURL
    }
    
    var path: String {
        switch self {
        case .staffLogin:
            return "/staff/login"
        case .adminLogin:
            return "/area-admin-activation/verify-phone-number"
        case .adminVerify:
            return "/area-admin-activation/submit-verification-code"
        case .updateProfile:
            return "/me/profile"
        case .listOrganizations:
            return "/me/organizations/all"
        case .getMyProfile:
            return "/me/profile"
        case .logOut:
            return "/me/profile/device-token"
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .getMyProfile, .listOrganizations:
            return .get
        case .logOut:
            return .delete
        default:
            return .post
        }
    }
    
    var headers: HTTPHeaders? {
        return nil
    }
    
    var task: HTTPTask {
        switch self {
        case .staffLogin(let name, avatar: let avatar, avatarIndex: let avatarIndex, countryCode: let countryCode):
            var params: Parameters = [
                "name": name,
                "avatar": avatar,
                "avatar_index": avatarIndex,
                "talk_app_token": UUID().uuidString,
                "country_code": countryCode
            ]
            
            if let pushKitToken = UserDefaults.getPushKitToken() {
                params["device_token"] = pushKitToken
            }
            
            return .uploadMultipart(params)
        case .adminLogin(let phoneNumber):
            let params: Parameters = [
                "phone_number": phoneNumber
            ]
            return .uploadMultipart(params)
        case .adminVerify(let phoneNumber, code: let code):
            var params: Parameters = [
                "phone_number": phoneNumber,
                "code": code,
                "talk_app_token": UUID().uuidString
            ]
            
            if let pushKitToken = UserDefaults.getPushKitToken() {
                params["device_token"] = pushKitToken
            }
            
            return .uploadMultipart(params)
        case .updateProfile(let name, avatar: let avatar, avatarIndex: let avatarIndex, email: let email):
            var params: Parameters = [
                "name": name,
                "avatar": avatar,
                "avatar_index": avatarIndex
            ]
            if let email = email, !email.isEmpty {
                params["email"] = email
            }
            print("Update params: ", params)
            return .uploadMultipart(params)
        case .getMyProfile, .listOrganizations:
            return .requestPlain
        case .logOut:
            return .delete
        }
    }
    
}
