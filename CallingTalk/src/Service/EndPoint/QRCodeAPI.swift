//
//  QRCodeAPI.swift
//  CallingTalk
//
//  Created by Toof on 3/18/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import Foundation

enum QRCodeAPI {
    case show(_ id: Int)
    case verify(_ id: String)
    case join(_ id: String)
}

extension QRCodeAPI: EndPointType {
    
    var baseURL: String {
        return API.defaultBaseURL
    }
    
    var path: String {
        switch self {
        case .show(let id):
            return "/channels/\(id)/qr-code"
        case .verify(let id):
            return "/channels/\(id)/verify-qr-code"
        case .join(let id):
            return "/channels/\(id)/join-by-qr-code"
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .show, .verify:
            return .get
        case .join:
            return .post
        }
    }
    
    var headers: HTTPHeaders? {
        return nil
    }
    
    var task: HTTPTask {
         return .requestPlain
    }
}


