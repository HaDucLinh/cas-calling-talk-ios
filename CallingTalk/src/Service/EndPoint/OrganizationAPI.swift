//
//  OrganizationAPI.swift
//  CallingTalk
//
//  Created by Vu Tran on 5/17/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import Foundation

enum OrganizationAPI {
    case getListOrganization
}

extension OrganizationAPI: EndPointType {
  
    var baseURL: String {
        return API.defaultBaseURL
    }
    
    var path: String {
        switch self {
        case .getListOrganization:
            return "/me/organizations/all"
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .getListOrganization:
            return .get
        }
    }
    
    var headers: HTTPHeaders? {
        return nil
    }
    
    var task: HTTPTask {
        switch self {
        case .getListOrganization:
            return .requestPlain
        }
    }
}
