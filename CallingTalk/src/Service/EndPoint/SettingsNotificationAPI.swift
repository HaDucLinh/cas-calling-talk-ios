//
//  SettingsNotificationAPI.swift
//  CallingTalk
//
//  Created by Toof on 5/17/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import Foundation

enum SettingsNotificationAPI {
    case updateState(_ isOn: Bool)
}

extension SettingsNotificationAPI: EndPointType {
    var baseURL: String {
        return API.defaultBaseURL
    }
    
    var path: String {
        switch self {
        case .updateState:
            return "/me/setting-notification"
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .updateState:
            return .post
        }
    }
    
    var headers: HTTPHeaders? {
        return nil
    }
    
    var task: HTTPTask {
        switch self {
        case .updateState(let isOn):
            let params: Parameters = ["status": isOn]
            return .requestParameters(params)
        }
    }
}
