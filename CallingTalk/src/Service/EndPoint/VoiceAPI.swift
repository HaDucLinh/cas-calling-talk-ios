//
//  VoiceAPI.swift
//  CallingTalk
//
//  Created by Vu Tran on 5/10/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import Foundation

enum VoiceAPI {
    case getListVoices(channelID: Int)
    case getListVoiceStatements
    case createVoiceStatement(name: String, ids: [Int])
    case updateVoiceStatement(id: Int, name: String, ids: [Int])
    case deleteVoiceStatement(id :Int)
}

extension VoiceAPI: EndPointType {
    
    var baseURL: String {
        return API.defaultBaseURL
    }
    
    var path: String {
        switch self {
        case .getListVoices(channelID: let id):
            return "/channels/\(id)/voice-texts"
        case .getListVoiceStatements:
            return "/voice-texts"
        case .createVoiceStatement:
            return "/voice-texts"
        case .updateVoiceStatement(id: let id, name: let _, ids: let _):
            return "/voice-texts/\(id)"
        case .deleteVoiceStatement(id: let id):
            return "/voice-texts/\(id)"
        }
    }
        
    var method: HTTPMethod {
        switch self {
        case .getListVoices, .getListVoiceStatements:
            return .get
        case .createVoiceStatement:
            return .post
        case .updateVoiceStatement:
            return .put
        case .deleteVoiceStatement:
            return .delete
        }
    }
    
    var headers: HTTPHeaders? {
        return nil
    }
        
    var task: HTTPTask {
        switch self {
        case .getListVoices, .getListVoiceStatements:
            return .requestPlain
        case .createVoiceStatement(name: let name, ids: let ids):
            var params: Parameters = [
                "content": name
            ]
            for (index, value) in ids.enumerated() {
                params["organization_ids[\(index)]"] = value
            }
            return .uploadMultipart(params)
        case .updateVoiceStatement(id: _, name: let name, ids: let ids):
            var params: Parameters = [
                "content": name
            ]
            for (index, value) in ids.enumerated() {
                params["organization_ids[\(index)]"] = value
            }
            return .put(params)
        case .deleteVoiceStatement:
            return .delete
        }
    }
}
