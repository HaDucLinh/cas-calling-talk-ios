//
//  ChatServicesProtocol.swift
//  CallingTalk
//
//  Created by NeoLabx on 3/27/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit
extension Notification.Name {
    public static let ReleaseChatNotification = Notification.Name("ReleaseChatNotification")
}
enum ChatServicesState {
    case soundOn
    case soundOff
}
protocol MeetingAudioServiceable {
    var isMute: Bool { get }
    func connectChat()
    func muteChat()
    func unMuteChat()
    func releaseChat()
    var servicesReady: (() -> Void)? { get set }
    var servicesDidLeft: (() -> Void)? { get set }
    var servicesDidChangeState: ((ChatServicesState) -> Void)? { get set }
}

