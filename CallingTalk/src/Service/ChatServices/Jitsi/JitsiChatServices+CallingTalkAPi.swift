//
//  JitsiChatServices+CallingTalkAPi.swift
//  CallingTalk
//
//  Created by NeoLabx on 4/1/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//
import Alamofire
/*
 We join to the jitsi conference and also login to our services.
 So we need to create a hook to our system.
 */
extension JitsiChatServices {
    /*NOTICE :(na.ngopo)This is hard-code logic: our application will continous send login section to the ReactNative(RN) app
     becase no way to change the original code in RN without touching to library.
     I continous send this info to RN: this is strongly cheat right here because the RN side encapsulate the process(init -> setting-> join) and we need to inject code to setting.
     I strongly suggest we should remove this logic and fix in server-side.
     */
    func createUserJitsi() {
        guard let userToken = UserDefaults.getToken() else {
            return
        }
        let apiHost = API.defaultBaseURL + "/jitsi-users"
        self.getCurrentBridgeReactNative().updateCallingSetting(withNewHost: apiHost, andToken: userToken, andLocalConfig: jitsiDomain)
        /*
         This code below is ugly,forgive me, but their is noway to satisfy the logical of server.
         For better control, please request server change their logic.
         */
        if(!isJitsiJoined()) {
            Timer.scheduledTimer(withTimeInterval: 0.3, repeats: false) { [weak self] _ in
                guard let self = self else {
                    return
                }
                 self.createUserJitsi()
            }
        }
    }
}
