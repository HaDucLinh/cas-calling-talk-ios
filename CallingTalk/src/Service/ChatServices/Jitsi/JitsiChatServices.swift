//
//  JitsiChatServices.swift
//  CallingTalk
//
//  Created by NeoLabx on 3/27/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//
import JitsiMeet
import UIKit
import SVProgressHUD
import Swinject
import RxSwift
import RxCocoa

enum JitsiUserIDState: String {
    case notConnected = "NO_ID"
}

class JitsiChatServices: NSObject, MeetingAudioServiceable {
    //Delegate as variables
    var servicesDidLeft: (() -> Void)?
    var servicesDidChangeState: ((ChatServicesState) -> Void)?
    var servicesReady: (() -> Void)?
    var servicesMembersDidChanged: ((ServicesMemberState) -> Void)?
    var servicesDidReceivedAnError: ((AudioChatError) -> Void)?
    var userDidChangeTalkingstatus: ((TalkStatus) -> Void)?
    var intevalTimer:Timer?
    //Private variables
    private var jitsiMeetView: JitsiMeetView?
    private var uniqueChatRoomId = ""
    private var uniqueCallingTalkChatRoomId = 0
    private var uniqueChatRoomHost = ""
    private var uniqueChatRoomName = ""
    private var userName = ""
    private var isJoinedJitsi = false
    private var reactBridge:ReactNativeBridge!
    private var subjectJitsiPingPong = Observable<Bool>.just(true)
    private var userJoinedChatService:Set<String> = Set<String>() {
        didSet {
            if userJoinedChatService != oldValue {
                let changedInfo = userJoinedChatService.subtracting(oldValue)
                    .union(oldValue.subtracting(userJoinedChatService))
                let modifiedState: ActionState = userJoinedChatService.count > oldValue.count ? .join :.left
              
                let notifyChangedInfo = ServicesMemberState.init(action: modifiedState,
                                                           allCurrentMembers: userJoinedChatService,
                                                           modifiedMembers: changedInfo)
                if let delegateChanged = servicesMembersDidChanged {
                    delegateChanged(notifyChangedInfo)
                }
            }
        }
    }
    var audioRecorderService:AudioServicesRecordable {
        get {
            return Container.default.resolve(AudioServicesRecordable.self)!
        }
        set {}
    }
    
    //Observer variables
    var isMute: Bool {
        return self.isJitsiAudioMute
    }
    var isConnected: Bool {
        return self.isJitsiCanChat
    }
    private var isJitsiAudioMute = false {
        didSet {
            if isJitsiAudioMute == oldValue { return }
            if let concreteObserver = servicesDidChangeState {
                let result = isJitsiAudioMute ? ChatServicesState.soundOff : ChatServicesState.soundOn
                concreteObserver(result)
            }
        }
    }
    private var isJitsiCanChat = false {
        didSet {
            guard isJitsiCanChat != oldValue else { return }
            if let concreteObserver = servicesDidChangeState {
                let result = isJitsiCanChat ? ChatServicesState.roomConnectOK : ChatServicesState.roomConnectFall
                concreteObserver(result)
            }
        }
    }
    private var jitsiUserID = JitsiUserIDState.notConnected.rawValue {
        didSet {
            if jitsiUserID != JitsiUserIDState.notConnected.rawValue
                && jitsiUserID != oldValue {
                print("changed from \(oldValue) to \(jitsiUserID)")
            }
        }
    }
    
    required init(withChatRoomId chatRoomID: String,
                  andHost host: String ,
                  andDisplayName roomName:String,
                  andCurrentUserName userName:String,
                  andGroupChatID groupChatID:Int,
                  insideView view: UIView,
                  andReactBridge newBridge: ReactNativeBridge = ReactNativeBridge.getDefaultBrigde()) {
        super.init()
        self.uniqueChatRoomId = chatRoomID
        self.uniqueChatRoomHost = host
        self.uniqueCallingTalkChatRoomId = groupChatID
        self.uniqueChatRoomName = roomName
        self.jitsiMeetView = JitsiMeetView()
        self.reactBridge = newBridge
        self.isJoinedJitsi = false
        self.userName = userName
        if let concreteJitsi = self.jitsiMeetView {
            concreteJitsi.frame = CGRect(x: 0, y: -300, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            view.addSubview(concreteJitsi)
            view.sendSubviewToBack(concreteJitsi)
        }
        self.createUserJitsi()
        //MARK:IMPORTANT: observing the state changing from brigde, and relect to our code
        self.reactBridge.bridgeDidReceivedInformation = { [weak self] data in
            guard let self = self,
                let validateData = data as? [String: AnyObject] else {
                    return
            }
            if let isMuteState = validateData["isMute"] as? Int{
                self.isJitsiAudioMute = (isMuteState == 1)
            }
            if let jitsiIsConnectingStatus = validateData["isConnecting"] as? Int{
                self.isJitsiCanChat = (jitsiIsConnectingStatus == 1)
            }
            if let localID = validateData["localID"] as? String{
                self.jitsiUserID = localID
            }
            if let allParticipants = validateData["allParticipants"] as? [String] {
                /*
                    - This is delegation for members has join, the member named "local" is from jitsi default, so it must be removed from my code
                    - The first time value changed from local to id is the first time jitsi completed their load
                    - Observing only in case the user has been joined
                 */
                if !self.isJoinedJitsi { return }
                self.userJoinedChatService = Set(allParticipants.filter({ (participantID) -> Bool in
                    return participantID != "local"
                }).map{ $0 })
            }
        }
    }
    deinit {
        print("deinit")
    }
    func shouldSetupJitsi(isSoundMute isMute: Bool) {
        guard let jitsiMeetView = self.jitsiMeetView else {
            return
        }
        let options = JitsiMeetConferenceOptions.fromBuilder { (options) in
            if let url = URL(string: "\(self.uniqueChatRoomHost)") {
                options?.serverURL = url
                options?.room = self.uniqueChatRoomId;
                options?.welcomePageEnabled = true
                options?.audioOnly = true
                options?.audioMuted = isMute
            }
        }
        jitsiMeetView.join(options)
        self.isJitsiAudioMute = isMute
    }
    func getJitsiView() -> JitsiMeetView? {
        return self.jitsiMeetView
    }
    func isJitsiJoined() -> Bool {
        return self.isJoinedJitsi
    }
    func getCurrentBridgeReactNative() -> ReactNativeBridge{
        return self.reactBridge
    }
}

/*
 Observing the delegate from Jitsi framework:
 - conferenceJoined(..) : We need to update the name of chat inside Callkit implementation, so we trigger updateConferenceName(),
 for more detail information please refer to react bridge
 - conferenceTerminated(..) : We delegate the method services left
 */
extension JitsiChatServices: JitsiMeetViewDelegate {
    func conferenceJoined(_ data: [AnyHashable : Any]!) {
        if let concreteChatAudioServices = servicesReady {
            concreteChatAudioServices()
        }
        self.isJoinedJitsi = true// jisi has been joined
        self.reactBridge.updateConferenceName(withNewName: self.uniqueChatRoomName)
        self.audioRecorderService.startSmartRecord(withID: self.uniqueCallingTalkChatRoomId,
                                                   andUserName: self.userName)
        //Inteval sending value right here
        intevalTimer = Timer.scheduledTimer(withTimeInterval: 0.5, repeats: true) {[weak self] (timer) in
            guard let self = self else { return }
            let statusBroadcast = self.audioRecorderService.getTalkingStatus() ? TalkStatus.talking : TalkStatus.idle
            guard let concreteDelegate = self.userDidChangeTalkingstatus else { return}
            concreteDelegate(statusBroadcast)
        }

        self.audioRecorderService.audioServicesDidStartSentence = {
            guard let concreteDelegate = self.userDidChangeTalkingstatus else { return}
            concreteDelegate(.talking)
        }
        self.audioRecorderService.audioServicesDidStopSentence = {
            guard let concreteDelegate = self.userDidChangeTalkingstatus else { return}
            concreteDelegate(.idle)
        }
    }
    func conferenceStartVoice(_ data: [AnyHashable : Any]!) {
        
    }
    func conferenceTerminated(_ data: [AnyHashable : Any]!) {
        if data["error"] != nil {
            guard let delegate = self.servicesDidReceivedAnError else { return }
            self.audioRecorderService.stopSmartRecord()
            delegate(.internetDown)
        } else {
            guard let delegate = self.servicesDidReceivedAnError else { return }
            delegate(.interupt)
        }
        
    }
}
