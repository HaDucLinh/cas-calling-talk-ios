//
//  JitsiChatServices+SupportCommand.swift
//  CallingTalk
//
//  Created by NeoLabx on 4/1/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit
/*
 Audio chat services support some avaible command:
 - connectChat() : Connect to the host of chat.
 - muteChat() : mute the audio of chat.
 - unMuteChat() : mute the audio of chat.
 - releaseChat() : release the chat, got your memory back.
 */
extension JitsiChatServices {
    func connectChat() {
        if let concreteJitsi = self.getJitsiView() {
            concreteJitsi.delegate = self
        }
        self.shouldSetupJitsi(isSoundMute: false)
    }
    
    func muteChat() {
        self.getCurrentBridgeReactNative().muteMicroPhone()
    }
    
    func unMuteChat() {
        self.getCurrentBridgeReactNative().unMuteMicroPhone()
    }
    func releaseChat() {
        if let timer = intevalTimer {
            timer.invalidate()
        }
        self.audioRecorderService.releaseSmartRecord()
        let jitsiView = self.getJitsiView()
        if let _ = jitsiView?.superview {
            jitsiView?.leave()
            jitsiView?.removeFromSuperview()
        }
    }
}
