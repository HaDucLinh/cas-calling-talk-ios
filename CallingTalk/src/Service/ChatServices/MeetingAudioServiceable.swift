//
//  ChatServicesProtocol.swift
//  CallingTalk
//
//  Created by NeoLabx on 3/27/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
extension Notification.Name {
    public static let ReleaseChatNotification = Notification.Name("ReleaseChatNotification")
}
enum ChatServicesState {
    case soundOn
    case soundOff
    case roomConnectFall
    case roomConnectOK
}
enum AudioChatError {
    case internetDown
    case interupt
}
enum ActionState {
    case join
    case left
    case loadCompleted
}
enum TalkStatus{
    case talking
    case idle
}
struct ServicesMemberState {
    let action: ActionState
    let allCurrentMembers: Set<String>
    let modifiedMembers: Set<String>
}
protocol MeetingAudioServiceable {
    var isMute: Bool { get }
    var isConnected: Bool { get }
    func connectChat()
    func muteChat()
    func unMuteChat()
    func releaseChat()
    var servicesReady: (() -> Void)? { get set }
    var servicesMembersDidChanged: ((ServicesMemberState) -> Void)? { get set }
    var servicesDidChangeState: ((ChatServicesState) -> Void)? { get set }
    var servicesDidReceivedAnError: ((AudioChatError) -> Void)? { get set }
    var userDidChangeTalkingstatus:((TalkStatus) -> Void)? { get set }
}

