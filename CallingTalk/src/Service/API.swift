//
//  API.swift
//  CallingTalk
//
//  Created by Toof on 2/12/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import Foundation
import Swinject
// MARK: - Define default's value for API

enum AppEnvironment {
    case production
    case staging
    case develop
}

class API {
    
    static let enviroment: AppEnvironment = .production
    
    static var defaultBaseURL: String {
        let defaultWorkspace: String? = nil
        var baseURL: String = ""
            
        switch enviroment {
        case .production:
            baseURL = "https://talk-api.calling.fun/api/v1"
        case .staging:
            baseURL = "https://talk-api.stg.calling.fun/api/v1"
        case .develop:
            baseURL = "https://talk-api.dev.calling.fun/api/v1"
        }
        
        guard let workspace = defaultWorkspace, let host = baseURL.host
        else { return baseURL }
        
        return "https://" + workspace + "." + host
    }
    
    static var defaultParameters: Parameters {
        return [:]
    }
    
    static var defaultHTTPHeaders: HTTPHeaders {
        var header: HTTPHeaders = [ "language": Locale.current.languageCode ?? "ja" ]
        
        if let uuid = UserDefaults.getToken() {
            print("Access-Token: ", uuid)
            header["Access-Token"] = uuid
        }
        
        return header
    }
    
    static let router = Router<MultiEndPoint>()
    static var logService: Logable {
        get {
            return Container.default.resolve(Logable.self)!
        }
        set {}
    }
}
