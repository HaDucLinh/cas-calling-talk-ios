//
//  MessageBroadcastable.swift
//  CallingTalk
//
//  Created by NeoLabx on 5/30/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit
/*
 The flow of processing a packet is:
 processPacket(...) --> canProcess(...) --> if return value is true --> excuteAction(...)
                                        --> if return value is false --> They will find the next handler in chain
 Any packet implement must be create their own excuteAction(...) function.
 */
protocol MessageBroadcastable {
    init(withPacketName:String,nextNode: MessageBroadcastable?)
    var next: MessageBroadcastable? { get set }
    var packetName: String { get }
    func processPacket(messageName: String, messageData: Any)
    func canProcess(messageName: String) -> Bool
    func excuteAction(messageData: Any)
    var messagePacketDidCompletedProcess: ((Int) -> ())? { get set }
}
/*
 Some functions will be keep the same implementation across child object.
 - canProcess(...): if the packet name same with the name object can resolve, it will return true, otherwise return false.
 - processPacket(...): implement the main flow at describle in top of this file.
 */
extension MessageBroadcastable {
    func canProcess(messageName: String) -> Bool {
        if messageName == self.packetName {
            return true
        }
        return false
    }
    func processPacket(messageName: String, messageData: Any) {
        if canProcess(messageName: messageName){
            self.excuteAction(messageData: messageData)
            return
        }
        if let concreteNext = self.next{
            concreteNext.processPacket(messageName: messageName, messageData: messageData)
        }
    }
}
