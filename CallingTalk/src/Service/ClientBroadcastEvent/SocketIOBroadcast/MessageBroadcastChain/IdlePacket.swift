//
//  IdlePacket.swift
//  CallingTalk
//
//  Created by NeoLabx on 5/30/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit

class IdlePacket: MessageBroadcastable {
    var messagePacketDidCompletedProcess: ((Int) -> ())?
    var next: MessageBroadcastable?
    var packetName: String
    required init(withPacketName: String, nextNode: MessageBroadcastable?) {
        self.packetName = withPacketName
        if let concreteNode = nextNode {
            self.next = concreteNode
        } else {
            self.next = nil
        }
    }
    func excuteAction(messageData: Any) {
        guard let arrayData = messageData as? Array<Any> else { return }
        guard arrayData.count > 0,
            let idOfUser = arrayData[0] as? Int,
            let concreteDelegate = messagePacketDidCompletedProcess else { return }
//        print("[SocketIOBroadcastPacket] Received IdlePacket from userid : \(idOfUser)")
        concreteDelegate(idOfUser)
    }
}
