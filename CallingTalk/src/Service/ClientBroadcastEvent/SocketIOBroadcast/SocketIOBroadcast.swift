//
//  SocketIOBroadcast.swift
//  CallingTalk
//
//  Created by NeoLabx on 5/30/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit
import SocketIO
enum PacketStructure: Int {
    case name = 0
    case data = 1
}
class SocketIOBroadcast: ClientBroadcastable {
    var didReceivedMessageInformation: ((ClientBroadcastEvent, [Any]) -> Void)?
    private var echoLaravel: Echo!
    private var channelID: Int!
    private var subcribeSection:IChannel?
    private var packetChain = [MessageBroadcastable]()
    private var channelName:String {
        get{
            return ClientBroadCastConst.buildChannel(withIndex: self.channelID)
        }
    }
    required init(withChannel channelId: Int = ClientBroadcastChannel.typingChannel.rawValue,
                  andEcho: Echo) {
        self.echoLaravel = andEcho
        unsubcrible()
        self.channelID = channelId
        self.packetChain = self.buildPacketChain()
        self.subcrible()
    }
    
    func sendBroadcastEvent(event: ClientBroadcastEvent, withData: [AnyObject]) {
        guard let privateChannel = self.getPrivateChannel() else { return }
        _ = privateChannel.whisper(eventName: event.rawValue, data: withData)
    }
    func subcrible() {
        guard let privateChannel = self.getPrivateChannel() else { return }
        /*
         Registering all packet the system can handle
         */
        ClientBroadcastEvent.allCases.forEach{ item in
            let packetName = self.getPacketName(packet: item.rawValue)
            print("[SocketIOBroadcast] registering to listen packet named: \(packetName)")
            _ = privateChannel.listen(event: packetName, callback: { [weak self] data, ack in
                guard let self = self else { return }
//                print("[SocketIOBroadcast]received event \(packetName)")
                self.processChain(packetName: packetName,
                                  packetData: data)
            })
            privateChannel.listenForWhisper(event: packetName, callback: { [weak self] data, ack in
                guard let self = self else { return }
//                print("[SocketIOBroadcast]received event \(packetName)")
                self.processChain(packetName: packetName,
                packetData: data)

            })
        }
    }
    deinit {
        print("removed")
    }
    func unsubcrible() {
        guard let concreteEcho = self.echoLaravel else { return }
        concreteEcho.leave(channel: "channel.1")
    }
}
extension SocketIOBroadcast{
    private func processChain(packetName: String, packetData: [Any]) {
        guard let firstChainNode = self.packetChain.first,
            packetData.count > 0 else { return }
        let dataReceived = packetData[PacketStructure.data.rawValue] as Any
        /*
         Process all the packet and send the information via delegate setting
         Please check the buildPacketChain(...) for delegate setting
         */
        firstChainNode.processPacket(messageName: packetName, messageData: dataReceived)
    }
    private func getPrivateChannel() -> IPrivateChannel? {
        let channel = self.echoLaravel.privateChannel(channel: self.channelName)
        return channel as? IPrivateChannel
    }
    private func getPacketName(packet: String) -> String{
        let serverDefinedPrefix = "client-"
        return  "\(serverDefinedPrefix)\(packet)"
    }
    /*
     This function use to build the packet chain.
     We may optimize it by using factory right here! But for simple now i using loop event
     */
    private func buildPacketChain() -> [MessageBroadcastable]{
        var result = [MessageBroadcastable]();
        let packetNameTyping = self.getPacketName(packet: ClientBroadcastEvent.talking.rawValue)
        let packetNameIdle = self.getPacketName(packet: ClientBroadcastEvent.idle.rawValue)
        let packetIdle = IdlePacket(withPacketName: packetNameIdle,
                   nextNode: nil)
        packetIdle.messagePacketDidCompletedProcess = { [weak self] idOfUser in
            guard let self = self else { return }
//            print("[SocketIOBroadcast] did received info messagePacketDidCompletedProcess from packetIdle")
            guard let concreteDelegate = self.didReceivedMessageInformation else { return }
            concreteDelegate(.idle,[idOfUser])
        }
        let packetTyping = TypingPacket(withPacketName: packetNameTyping,
                                        nextNode: packetIdle)
        packetTyping.messagePacketDidCompletedProcess = {  [weak self] idOfUser in
            guard let self = self else { return }
//            print("[SocketIOBroadcast] did received info messagePacketDidCompletedProcess from packetTyping")
            guard let concreteDelegate = self.didReceivedMessageInformation else { return }
            concreteDelegate(.talking,[idOfUser])
        }
        result.append(packetTyping)
        result.append(packetIdle)
        return result
    }
}
