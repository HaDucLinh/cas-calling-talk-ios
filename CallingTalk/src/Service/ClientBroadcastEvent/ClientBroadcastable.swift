//
//  ClientBroadcastable.swift
//  CallingTalk
//
//  Created by NeoLabx on 5/30/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit
enum ClientBroadcastChannel: Int {
    case typingChannel = 1
}
enum ClientBroadcastEvent: String,CaseIterable {
    case talking
    case idle
}

class ClientBroadCastConst{
    /*
     The channel id is force to format [channel.<int value>] that define by server.
     Any message broadcast without this format channel name are dropped by server.
     This function in converted to correct format. Please use it any time you connect to a channel.
     */
    static func buildChannel(withIndex indexOfchannel:Int ) -> String {
        return "channel.\(indexOfchannel)"
    }
}
protocol ClientBroadcastable {
    init(withChannel channelId: Int, andEcho: Echo)
    func sendBroadcastEvent(event: ClientBroadcastEvent,withData: [AnyObject])
    func subcrible()
    func unsubcrible()
    var didReceivedMessageInformation: ((ClientBroadcastEvent, [Any]) -> Void)? { get set }
}
