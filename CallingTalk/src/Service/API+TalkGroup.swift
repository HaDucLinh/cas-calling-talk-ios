//
//  API+TalkGroup.swift
//  CallingTalk
//
//  Created by Vu Tran on 3/15/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import ObjectMapper
import RealmS

extension API {
    static func listTalkGroups(searchText: String?, maxDate: String?, minDate: String?, completion: @escaping TalksCompletion) {
        API.router.request(MultiEndPoint(TalkGroupAPI.List(maxDate: maxDate, minDate: minDate, searchText: searchText))) { (result) in
            switch result {
            case .success(let response):
                guard let object = response as? JSObject, let data = object["data"] as? JSObject else {
                    completion(.failure(API.Error.json), false, false)
                    return
                }
                var hasNewEvent = false
                var hasMorePages = false
                if let newEvents = data["has_new_events"] as? Bool {
                    hasNewEvent = newEvents
                }
                if let morePages = data["has_more_pages"] as? Bool {
                    hasMorePages = morePages
                }
                guard   let items = data["items"] as? [JSObject],
                        let talkGroups = Mapper<TalkGroup>().mapArray(JSONObject: items) else {
                            completion(.failure(API.Error.json), false, false)
                            return
                    }
                
                completion(.success(talkGroups), hasNewEvent, hasMorePages)
                return
            case .failure(let error):
                completion(.failure(error), false, false)
            }
        }
    }
    
    static func detailTalkGroup(_ id: Int, completion: @escaping Completion) {
        API.router.request(MultiEndPoint(TalkGroupAPI.Detail(id: id))) { (result) in
            switch result {
            case .success(let response):
                guard   let object = response as? JSObject,
                        let data = object["data"] as? JSObject,
                        let talkGroup = Mapper<TalkGroup>().map(JSON: data)
                else {
                    completion(.failure(API.Error.json))
                    return
                }
                completion(.success(talkGroup))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    static func detailUserInTalkGroup(_ userId: Int, talkId: Int, completion: @escaping Completion) {
        API.router.request(MultiEndPoint(TalkGroupAPI.DetailUserInTalk(userId: userId, talkId: talkId))) { (result) in
            switch result {
            case .success(let response):
                guard   let object = response as? JSObject,
                    let data = object["data"] as? JSObject,
                    let user = Mapper<User>().map(JSON: data)
                    else {
                        completion(.failure(API.Error.json))
                        return
                }
                completion(.success(user))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    static func createTalkGroup(_ name: String, memberIds: [Int], avatar: Data?, avatarIndex: Int, background: Data?, organizationId: Int?, completion: @escaping Completion) {
        API.router.request(MultiEndPoint(TalkGroupAPI.CreateTalkGroup(name: name, memberIDs: memberIds, avatar: avatar, avatarIndex: avatarIndex, background: background, organizationId: organizationId))) { (result) in
            switch result {
            case .success(let response):
                guard let object = response as? JSObject,
                    let data = object["data"] as? JSObject,
                    let talkId = data["id"] as? Int
                    else {
                        completion(.failure(API.Error.json))
                        return
                }
                completion(.success(talkId))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    static func listUserToInvite(_ query: String? = nil, maxDate: String? = nil, minDate: String?, completion: @escaping UsersCompletion) {
        API.router.request(MultiEndPoint(TalkGroupAPI.ListUserToInvite(query: query, maxDate: maxDate, minDate: minDate))) { (result) in
            switch result {
            case .success(let response):
                guard   let object = response as? JSObject,
                        let data = object["data"] as? JSObject,
                        let items = data["items"] as? JSArray,
                        let morePages = data["has_more_pages"] as? Bool
                else {
                    completion(.failure(API.Error.json), false)
                    return
                }
                completion(.success(Mapper<User>().mapArray(JSONArray: items)), morePages)
            case .failure(let error):
                completion(.failure(error), false)
            }
        }
    }
    
    static func listUserToInviteTalkGroup(_ id: Int, query: String? = nil, maxDate: String? = nil, minDate: String?, completion: @escaping UsersCompletion) {
        API.router.request(MultiEndPoint(TalkGroupAPI.ListUserToInviteTalkGroup(id: id, query: query, maxDate: maxDate, minDate: minDate))) { (result) in
            switch result {
            case .success(let response):
                guard   let object = response as? JSObject,
                        let data = object["data"] as? JSObject,
                        let items = data["items"] as? JSArray,
                        let morePages = data["has_more_pages"] as? Bool
                else {
                    completion(.failure(API.Error.json), false)
                    return
                }
                completion(.success(Mapper<User>().mapArray(JSONArray: items)), morePages)
            case .failure(let error):
                completion(.failure(error), false)
            }
        }
    }
    
    static func editTalkGroup(id: Int, _ name: String, avatar: Data? = nil, avatarIndex: Int, background: Data? = nil, completion: @escaping Completion) {
        API.router.request(MultiEndPoint(TalkGroupAPI.EditTalkGroup(id: id, name: name, avatar: avatar, avatarIndex: avatarIndex, background: background))) { (result) in
            switch result {
            case .success(let response):
                guard let object = response as? JSObject,
                    let data = object["data"] as? JSObject,
                    let talkGroup = Mapper<TalkGroup>().map(JSON: data)
                    else {
                        completion(.failure(API.Error.json))
                        return
                }
                completion(.success(talkGroup))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    static func inviteMembersToTalkGroup(groupId: Int, memberIDs: [Int], completion: @escaping Completion) {
        API.router.request(MultiEndPoint(TalkGroupAPI.InviteMembersToTalkGroup(groupID: groupId, memberIDs: memberIDs))) { (result) in
            switch result {
            case .success( _):
                completion(.success(true))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    static func joinTalkGroupByID(_ id: Int, completion: @escaping Completion) {
        API.router.request(MultiEndPoint(TalkGroupAPI.JoinTalkGroup(id: id))) { (result) in
            switch result {
            case .success(let response):
                guard   let object = response as? JSObject,
                        let data = object["data"] as? JSObject,
                        let talkGroup = Mapper<TalkGroup>().map(JSON: data)
                else {
                    completion(.failure(API.Error.json))
                    return
                }
                
                // Subcribe organization if new organization for connect socket
                if let organization = talkGroup.organization {
                    SocketIOManager.sharedInstance.subscribeOrganizations([organization])
                }
                
                completion(.success(true))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    static func declineJoiningTalkGroupByID(_ id: Int, completion: @escaping Completion) {
        API.router.request(MultiEndPoint(TalkGroupAPI.DeclineJoiningTalkGroup(id: id))) { (result) in
            switch result {
            case .success( _):
                completion(.success(true))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    static func leaveTalkGroup(talkGroupId: Int, completion: @escaping Completion) {
        API.router.request(MultiEndPoint(TalkGroupAPI.LeaveTalkGroup(groupID: talkGroupId))) { (result) in
            switch result {
            case .success( _):
                completion(.success(true))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    static func deleteTalkGroup(talkGroupId: Int, completion: @escaping Completion) {
        API.router.request(MultiEndPoint(TalkGroupAPI.DeleteTalkGroup(groupID: talkGroupId))) { (result) in
            switch result {
            case .success( _):
                completion(.success(true))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    static func findAllUserIDFromJitsiID(jitsiID: [String], completion: @escaping Completion) {
        API.router.request(MultiEndPoint(TalkGroupAPI.FindUsersIDFromJitsiID(jitsiIDs: jitsiID))) { (result) in
            switch result {
            case .success(let response):
                guard let object = response as? JSObject else {
                    return
                }
                guard let items = object["data"] as? [JSObject],
                    let messages = Mapper<AudioServicesInfo>().mapArray(JSONObject: items) else {
                        return
                }
                completion(.success(messages))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    static func removeUserFromTalkGroup(talkID: Int, userID: Int, completion: @escaping Completion) {
        API.router.request(MultiEndPoint(TalkGroupAPI.RemoveUser(groupID: talkID, userID: userID))) { (result) in
            switch result {
            case .success(_):
                completion(.success(true))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
}
