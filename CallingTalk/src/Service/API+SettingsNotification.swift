//
//  API+SettingsNotification.swift
//  CallingTalk
//
//  Created by Toof on 5/17/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import Foundation

extension API {
    static func updateNotificationState(_ isOn: Bool, completion: @escaping Completion) {
        API.router.request(MultiEndPoint(SettingsNotificationAPI.updateState(isOn))) { (result) in
            DispatchQueue.main.async {
                switch result {
                case .success(let response):
                    guard   let object = response as? JSObject,
                            let statusCode = object["status_code"] as? Int,
                            let status = StatusCode(rawValue: statusCode)
                    else {
                        completion(.failure(API.Error.json))
                        return
                    }
                    completion(.success(status))
                case .failure(let error):
                    completion(.failure(error))
                }
            }
        }
    }
}
