//
//  FabricOnlineLogService.swift
//  CallingTalk
//
//  Created by NeoLabx on 5/2/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit
import Crashlytics
class CallingLogService: Logable {
    private var currentLogStatus: LogStatus = .disable
    init(withLogStatus logStatus: LogStatus) {
        currentLogStatus = logStatus
    }
    
    func logError(error: Error) {
        switch currentLogStatus {
        case .writeToServer:
            Crashlytics.sharedInstance().recordError(error)
        case .writeToClient:
            print(error)
        default:
            break
        }
    }
}
