//
//  Logable.swift
//  CallingTalk
//
//  Created by NeoLabx on 5/2/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit
enum LogStatus {
    case writeToServer
    case writeToClient
    case disable
}
protocol Logable {
    func logError(error: Error)
}
