//
//  UserTalkingDetector.swift
//  CallingTalk
//
//  Created by NeoLabx on 4/10/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit

class UserTalkingDetector {
    var userDidStartOneSentenceTalking: (() -> Void)?
    var userDidCompleteOneSentenceTalking: (() -> Void)?
    private var decibelThresold = 0.0
    private var expiredThresold = 0.0
    private var silenceTimer:Timer?
    private var userIsTalking = false {
        didSet {
            if userIsTalking && !oldValue {
                guard let delegate = userDidStartOneSentenceTalking else {
                    return
                }
                delegate()
            }
        }
    }
    private var lastCheckTalking:TimeInterval // the last time check inteval
    init(withDecibel decibel: Double, andTimeExpireDetect expire: Double) {
        self.decibelThresold = decibel
        self.expiredThresold = expire
        lastCheckTalking = Date.timeIntervalBetween1970AndReferenceDate
    }
    func startDetector() {
        // starting the detector....
    }
    func updateUserTalking(withCurrentDecibel decibel: Double) {
        if self.decibelThresold < decibel { // user is still talking
            self.userIsTalking = true
            if let timerExisted = self.silenceTimer {
                timerExisted.invalidate()
                self.silenceTimer = nil
            }
//            print("User currently talking \(decibel)")
        } else {
//            print("User end talking")
            if self.silenceTimer != nil {
                return// should not starting the timer if the the timer is calculated
            }
            self.startCounterWhenUserSilence()
        }
    }
}
extension UserTalkingDetector {
    func startCounterWhenUserSilence() {
        self.silenceTimer = Timer.scheduledTimer(withTimeInterval: self.expiredThresold, repeats: false, block: { [weak self] (_) in
            guard let self = self else {
                return
            }
            
            if let delegate = self.userDidCompleteOneSentenceTalking, self.userIsTalking {
                self.userIsTalking = false
                delegate()
            }
        })
    }
}
