//
//  AudioServicesRecordable.swift
//  CallingTalk
//
//  Created by NeoLabx on 4/9/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit
enum VoiceLogStatus {
    case off
    case on
}
struct AudioServicesData {
    var urlOfRecordFile:String
}
protocol AudioServicesRecordable {
    func startSmartRecord(withID: Int, andUserName userName: String)
    func stopSmartRecord()
    func releaseSmartRecord()
    func startUserRecord(withID: Int, andUserName userName: String)
    func stopUserRecord()
    func getAllCurrentRecord() -> [URL]
    func playSound(withFileUrl: String)
    var audioServicesDidCompletedRecordASentence: ((AudioServicesData) -> Void)? { get set }
    var audioServicesDidCompletedUpload: ((Result<Any>) -> Void)? { get set }
    var audioServicesDidStartSentence: (()->Void)? { get set }
    var audioServicesDidStopSentence: (()->Void)? { get set }
    func getTalkingStatus() -> Bool
}
