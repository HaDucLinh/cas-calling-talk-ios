//
//  ChatServicesProtocol.swift
//  CallingTalk
//
//  Created by NeoLabx on 3/27/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//
import AVFoundation
import AudioToolbox
class AudioServicesRecorder: NSObject, AudioServicesRecordable {
    var audioServicesDidStartSentence: (() -> Void)?
    var audioServicesDidStopSentence: (() -> Void)?
    var audioServicesDidCompletedUpload: ((Result<Any>) -> Void)?
    var audioServicesDidCompletedRecordASentence: ((AudioServicesData) -> Void)?
    private var recordingSession: AVAudioSession!
    //MARK: IMPORTANT NOTE!!!
    /*
     Their are 2 recorder that running right now:
     - audioRecorderNoiseChecker: always running in the background that help us detect user is talking or not.
     This thread do nothing with recorder.
     - audioRecorderUserSaved: start once audioRecorderNoiseChecker detected a sentence is talking. This thread
     will record the voice of user.
     */
    private var audioRecorderNoiseChecker: AVAudioRecorder?
    private var audioRecorderUserSaved: AVAudioRecorder?
    private var player: AVAudioPlayer?
    private var userDetectionTimer:Timer?
    private var savedSoundsPath: [URL] = []
    private var channelID = 0
    private var userName = ""
    private var audioFilter = AudioRecorderFilter(withMinSoundDuration: 2.0)
    private var userIsTalking = false {
        didSet {
            guard let startTalkDelegate = self.audioServicesDidStartSentence,
            let endTalkDelegate = self.audioServicesDidStopSentence else { return }
            if userIsTalking {
                startTalkDelegate()
            } else {
                endTalkDelegate()
            }
            
        }
    }
    static let decibelThreshold = -30.0// threshold to check user talking or not
    static let exprireInSecsThreshoud = 1// sec delay defore we close the file
    private var userTalkingDetector: UserTalkingDetector
    override init() {
        self.userTalkingDetector = UserTalkingDetector.init(withDecibel: AudioServicesRecorder.decibelThreshold,
                                                            andTimeExpireDetect: Double(AudioServicesRecorder.exprireInSecsThreshoud))
        super.init()
        
        self.requestUserPermission()
    }
    func getAllCurrentRecord() -> [URL] {
        return self.savedSoundsPath
    }
    func getTalkingStatus() -> Bool {
        return self.userIsTalking
    }
    func playSound(withFileUrl url: String) {
        let url = URL.init(string: url)//getDocumentsDirectory().appendingPathComponent(withFileName)
        do {
            player = try AVAudioPlayer(contentsOf: url!)
            guard let player = player else { return }
            
            player.prepareToPlay()
            player.play()
            
        } catch let error as NSError {
            print(error.description)
        }
    }
    func startSmartRecord(withID: Int, andUserName userName: String) {
        self.channelID = withID
        self.userName = userName
        self.createNewTrackRecordObserver()
        userDetectionTimer?.invalidate()
        userDetectionTimer = nil
        userDetectionTimer = Timer.scheduledTimer(withTimeInterval: 0.2, repeats: true, block: { [weak self] (_) in
            guard let self = self,
                let audioRecord = self.audioRecorderNoiseChecker else {
                    return
            }
            if !audioRecord.isRecording {
                print("[AudioLog] record did not start, try to starting...")
                audioRecord.record() // call the recording...
                return
            }
            audioRecord.updateMeters()

            let currentDecibel = audioRecord.averagePower(forChannel: 0)
            self.userTalkingDetector.updateUserTalking(withCurrentDecibel: Double(currentDecibel))
        })
        self.userTalkingDetector.userDidCompleteOneSentenceTalking = { [weak self] in
            guard let self = self else { return }
            print("[AudioLog]complete a sentence")
            self.stopARecording()
        }
        self.userTalkingDetector.userDidStartOneSentenceTalking = {[weak self] in
            guard let self = self else { return }
            print("[AudioLog] starting a sentence")
            self.startNewRecording()
        }
    }
    func stopSmartRecord() {
        //        guard let concreteAudioRecord = audioRecorderNoiseChecker else {
        //            return
        //        }
        //        concreteAudioRecord.stop()
    }
    func stopARecording() {
        self.userIsTalking = false
        guard  let concreteAudioRecord = audioRecorderUserSaved else {
            return
        }
        concreteAudioRecord.stop()
    }
    func releaseSmartRecord() {
        if audioRecorderUserSaved != nil { // audio recorder existed, i will remove it
            stopARecording()
        }
        if let concreteUserDetection = userDetectionTimer {
            concreteUserDetection.invalidate()
            userDetectionTimer = nil
        }
        if let concreteAudioRecorderNoiseChecker = audioRecorderNoiseChecker {
            concreteAudioRecorderNoiseChecker.deleteRecording()
            audioRecorderNoiseChecker = nil
        }
        
        let fileManager = FileManager.default
        self.savedSoundsPath.forEach({ (fileURL) in
            print("[AudioLog]Clearing temp file...")
            do {
                try fileManager.removeItem(at: fileURL)
            } catch {
                print("[AudioLog]Could not clear temp file: \(error)")
            }
        })
    }
    func startUserRecord(withID: Int, andUserName userName: String) {
        print("[AudioLog][User Record] Starting a sentence")
        self.startNewRecording()
    }
    
    func stopUserRecord() {
        print("[AudioLog][User Record] Complete a sentence")
        self.stopARecording()
    }
}
extension AudioServicesRecorder: AVAudioRecorderDelegate {
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        print("[AudioLog] completed save sound")
        guard let urlOfSound = self.getAllCurrentRecord().last else { return }
        if audioFilter.isSatisfiable(soundURL: urlOfSound) {
            self.uploadSoundFileToServer(withFilePath: urlOfSound.absoluteString ?? "")
        }
    }
    func audioRecorderEncodeErrorDidOccur(_ recorder: AVAudioRecorder, error: Error?) {
        print("[AudioLog] error occurred \(error.debugDescription)")
    }
    func audioRecorderBeginInterruption(_ recorder: AVAudioRecorder) {
        print("[AudioLog] Begin interuption!")
    }
    func audioRecorderEndInterruption(_ recorder: AVAudioRecorder, withOptions flags: Int){
        print("[AudioLog] End interuption!")
    }
}
extension AudioServicesRecorder {
    private func uploadSoundFileToServer(withFilePath path: String) {
        API.uploadAudioLogFileWith(path: path, toChannelId: self.channelID, fromUser: self.userName) { result in
            if let concreteResult = result {
                if let concreteDelegate = self.audioServicesDidCompletedUpload {
                    concreteDelegate(concreteResult)
                }
                print("[AudioLog] upload file completed")
            } else {
                print("[AudioLog] upload file failed")
            }
        }
    }
    private func createNewTrackRecordObserver() {
        if audioRecorderNoiseChecker != nil { // audio recorder existed, i will remove it
            stopSmartRecord()
        }
        let settings = [
            AVFormatIDKey: Int(kAudioFormatLinearPCM),
            AVSampleRateKey: 44100,
            AVNumberOfChannelsKey: 1,
            AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue
        ]
        
        let audioFilenameTemp = getDocumentsDirectory().appendingPathComponent("noSaved.wav")
        do {
            audioRecorderNoiseChecker = try AVAudioRecorder(url: audioFilenameTemp, settings: settings)
            if let concreteAudioRecorderNoiseChecker = audioRecorderNoiseChecker {
                concreteAudioRecorderNoiseChecker.isMeteringEnabled = true
                concreteAudioRecorderNoiseChecker.delegate = self
            }
        } catch let errorFounded{
            print("[AudioLog] error1: \(errorFounded)")
            stopSmartRecord()
        }
    }
    private func startNewRecording() {
        if audioRecorderUserSaved != nil { // audio recorder existed, i will remove it
            stopARecording()
        }
        self.userIsTalking = true
        let currentTimeIntevalAsString = String(Date.timeIntervalSinceReferenceDate)
        let settings = [
            AVFormatIDKey: Int(kAudioFormatLinearPCM),
            AVSampleRateKey: 44100,
            AVNumberOfChannelsKey: 1,
            AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue
        ]
        let audioFilename = getDocumentsDirectory().appendingPathComponent("\(currentTimeIntevalAsString).wav")
        savedSoundsPath.append(audioFilename)
        do {
            audioRecorderUserSaved = try AVAudioRecorder(url: audioFilename, settings: settings)
            if let concreteAudioRecorderUserSaved = audioRecorderUserSaved {
                concreteAudioRecorderUserSaved.isMeteringEnabled = true
                concreteAudioRecorderUserSaved.delegate = self
                concreteAudioRecorderUserSaved.record()
            }
        } catch let errorFounded {
            print("[AudioLog] error2: \(errorFounded)")
            stopARecording()
        }
        guard let concreteDelegate = self.audioServicesDidStartSentence else { return }
        concreteDelegate()
    }
    private func requestUserPermission() {
        recordingSession = AVAudioSession.sharedInstance()
        do {
            try recordingSession.setCategory(.playAndRecord, mode: .default)
            try recordingSession.setActive(true)
            recordingSession.requestRecordPermission() { allowed in //[unowned self]
                DispatchQueue.main.async {
                    if allowed {
                        print("permission approved")
                    } else {
                        print("permission rejected")
                    }
                }
            }
        } catch {
        }
    }
    private func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
}
