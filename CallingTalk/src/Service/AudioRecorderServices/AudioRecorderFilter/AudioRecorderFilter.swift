//
//  AudioRecorderFilter.swift
//  CallingTalk
//
//  Created by NeoLabx on 4/23/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import UIKit
import AVFoundation
protocol AudioRecorderFilterable {
    func isSatisfiable(soundURL: URL) -> Bool
}
class AudioRecorderFilter: AudioRecorderFilterable {
    private var minSoundDuration = 0.0
    init(withMinSoundDuration duration: Double = 2.0) {
        self.minSoundDuration = duration
    }
    func isSatisfiable(soundURL url: URL) -> Bool {
        do {
            let player = try AVAudioPlayer(contentsOf: url)
            print("[AudioLog] sound duration: \(player.duration)")
            if player.duration > self.minSoundDuration {
                return true
            }
        } catch let error as NSError {
            print("[AudioLog] error while verify sound \(error.description)")
            return false
        }
       return false
    }
}
