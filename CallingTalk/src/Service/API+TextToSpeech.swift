//
//  API+TextToSpeech.swift
//  CallingTalk
//
//  Created by Toof on 3/18/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import Foundation

extension API {
    
    static func textToSpeech(text: String, languageCode: String, voiceType: VoiceSpeechType, completion: @escaping Completion) {
        API.router.request(MultiEndPoint(TextToSpeechApi.textToSpeech(text, languageCode: languageCode, voiceType: voiceType))) { (result) in
            switch result {
            case .success(let response):
                guard let object = response as? JSObject, let audioContent = object["audioContent"] as? String else {
                    completion(.failure(API.Error.json))
                    return
                }
                
                // Decode the base64 string into a Data object
                guard let audioData = Data(base64Encoded: audioContent) else {
                    completion(.failure(API.Error.json))
                    return
                }
                completion(.success(audioData))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
}
