//
//  API+Login.swift
//  CallingTalk
//
//  Created by Toof on 3/18/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import ObjectMapper
import RealmS

extension API {
    
    static func staffLogin(_ name: String, avatar: Data, avatarIndex: Int, countryCode: String, completion: @escaping Completion) {
        API.router.request(MultiEndPoint(LoginAPI.staffLogin(name, avatar: avatar, avatarIndex: avatarIndex, countryCode: countryCode))) { (result) in
            DispatchQueue.main.async {
                switch result {
                case .success(let response):
                    guard   let object = response as? JSObject,
                        let data = object["data"] as? JSObject,
                        let userInfo = data["user_info"] as? JSObject,
                        let profile = Mapper<User>().map(JSON: userInfo)
                        else {
                            completion(.failure(API.Error.json))
                            return
                    }
                    
                    completion(.success(profile))
                case .failure(let error):
                    completion(.failure(error))
                }
            }
        }
    }
    
    static func adminLogin(_ phoneNumber: String, completion: @escaping Completion) {
        API.router.request(MultiEndPoint(LoginAPI.adminLogin(phoneNumber))) { (result) in
            DispatchQueue.main.async {
                switch result {
                case .success(let response):
                    guard   let object = response as? JSObject,
                        let statusCode = object["status_code"] as? Int,
                        let status = StatusCode(rawValue: statusCode)
                        else {
                            completion(.failure(API.Error.json))
                            return
                    }
                    
                    completion(.success(status))
                case .failure(let error):
                    completion(.failure(error))
                }
            }
        }
    }
    
    static func adminVerify(_ phoneNumber: String, code: String, completion: @escaping Completion) {
        API.router.request(MultiEndPoint(LoginAPI.adminVerify(phoneNumber, code: code))) { (result) in
            DispatchQueue.main.async {
                switch result {
                case .success(let response):
                    guard   let object = response as? JSObject,
                        let message = object["message"] as? String,
                        let data = object["data"] as? JSObject,
                        let userInfo = data["user_info"] as? JSObject,
                        let profile = Mapper<User>().map(JSON: userInfo)
                        else {
                            completion(.failure(API.Error.json))
                            return
                    }
                    
                    let dataGroup: JSObject = [
                        "message": message,
                        "profile": profile
                    ]
                    
                    completion(.success(dataGroup))
                case .failure(let error):
                    completion(.failure(error))
                }
            }
        }
    }
    
    static func updateProfile(_ name: String, avatar: Data, avatarIndex: Int, email: String?, completion: @escaping Completion) {
        API.router.request(MultiEndPoint(LoginAPI.updateProfile(name, avatar: avatar, avatarIndex: avatarIndex, email: email))) { (result) in
            switch result {
            case .success(let response):
                guard let object = response as? JSObject,
                    let data = object["data"] as? JSObject,
                    let profile = Mapper<User>().map(JSON: data)
                    else {
                        completion(.failure(API.Error.json))
                        return
                }
                
                completion(.success(profile))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    static func getMyOrganizations(completion: @escaping Completion) {
        API.router.request(MultiEndPoint(LoginAPI.listOrganizations)) { (result) in
            switch result {
            case .success(let response):
                guard   let object = response as? JSObject,
                        let data = object["data"] as? JSArray
                else {
                    completion(.failure(API.Error.json))
                    return
                }
                
                completion(.success(Mapper<Organization>().mapArray(JSONArray: data)))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    static func getMyProfile(completion: @escaping Completion) {
        API.router.request(MultiEndPoint(LoginAPI.getMyProfile)) { (result) in
            switch result {
            case .success(let response):
                guard   let object = response as? JSObject,
                    let data = object["data"] as? JSObject,
                    let profile = Mapper<User>().map(JSON: data)
                    else {
                        completion(.failure(API.Error.json))
                        return
                }
                
                completion(.success(profile))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    static func logOut(completion: @escaping Completion) {
        API.router.request(MultiEndPoint(LoginAPI.logOut)) { (result) in
            DispatchQueue.main.async {
                switch result {
                case .success( _):
                    completion(.success(true))
                case .failure(let error):
                    completion(.failure(error))
                }
            }
        }
    }
}
