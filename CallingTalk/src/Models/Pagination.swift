//
//  Pagination.swift
//  CallingTalk
//
//  Created by Toof on 4/10/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import RealmSwift
import ObjectMapper
import RealmS

class Pagination: Object, Mappable {
    @objc dynamic var currentPage: Int  = 0
    @objc dynamic var perPage: Int      = 0
    @objc dynamic var from: Int         = 0
    @objc dynamic var to: Int           = 0
    @objc dynamic var total: Int        = 0
    
    convenience required init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        self.currentPage    <- map["current_page"]
        self.perPage        <- map["per_page"]
        self.from           <- map["from"]
        self.to             <- map["to"]
        self.total          <- map["total"]
    }
}

