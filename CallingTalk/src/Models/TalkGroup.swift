//
//  TalkGroup.swift
//  CallingTalk
//
//  Created by Toof on 2/18/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import RealmSwift
import ObjectMapper
import RealmS

class TalkGroup: Object, Mappable {
    @objc dynamic var id: Int                   = 0
    @objc dynamic var organizationId: Int       = 0
    @objc dynamic var name: String              = ""
    @objc dynamic var avatar: String            = ""
    @objc dynamic var avatarIndex: Int          = 1
    @objc dynamic var background: String        = ""
    
    @objc dynamic var isInvited: Bool           = false
    @objc dynamic var isCalling: Bool           = false
    @objc dynamic var canDelete: Bool           = false

    @objc dynamic var meetingUserCount: Int     = 0
    @objc dynamic var joinUsersCount: Int       = 0
    @objc dynamic var eventTime: Date?
    
    @objc dynamic var numberOfUnreadMessageMention: Int = 0
    @objc dynamic var isNewUnreadMessage: Bool = false
    
    var author: User?
    var inviter: User?
    var allUsers: List<User> = List<User>()
    var meetingUsers: List<User> = List<User>()
    var organization: Organization?

    convenience required init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        self.id                 <- map["id"]
        self.organizationId     <- map["organization_id"]
        self.name               <- map["name"]
        self.avatar             <- map["avatar_url"]
        self.avatarIndex        <- map["avatar_index"]
        self.background         <- map["background_url"]
        
        self.author             <- map["owner"]
        self.isInvited          <- map["is_invited"]
        self.isCalling          <- map["is_calling"]
        self.canDelete          <- map["can_delete"]
        
        self.meetingUserCount   <- map["meeting_users_count"]
        self.joinUsersCount     <- map["join_users_count"]
        
        self.organization       <- map["organization"]
        self.inviter            <- map["inviter"]
        self.allUsers           <- map["join_users"]
        self.meetingUsers       <- map["meeting_users"]
        
        self.numberOfUnreadMessageMention   <- map["count_message_mentions"]
        self.isNewUnreadMessage             <- map["has_unread_message"]
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = kDateGetListChannel
        dateFormatter.timeZone = TimeZone.current
        
        self.eventTime <- (map["event_at"], DateFormatterTransform(dateFormatter: dateFormatter))
    }
}
