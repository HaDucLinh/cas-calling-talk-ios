//
//  Organization.swift
//  CallingTalk
//
//  Created by Vu Tran on 3/5/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import RealmSwift
import ObjectMapper
import RealmS

class Organization: Object, Mappable {
    @objc dynamic var id: Int = 0
    @objc dynamic var name: String = ""
    
    convenience required init?(map: Map) {
        self.init()
        id <- map["id"]
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    struct APIPath {
        static let list = ""
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
    }
}
