//
//  Message.swift
//  CallingTalk
//
//  Created by Toof on 3/7/19.
//  Created by Vu Tran on 3/7/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import RealmSwift
import ObjectMapper
import RealmS

public enum MessageType: Int {
    case Image = 1
    case Text = 2
    case SystemAudio = 3
    case LogAudio = 4
    case VoiceText = 5
}

class Message: Object, Mappable {
 
    @objc dynamic var id: Int = 0
    @objc dynamic var channelId: Int = 0
    @objc dynamic var ownerId: Int = 0
    @objc dynamic var content: String = ""
    @objc dynamic var messageType: Int = 0
    @objc dynamic var fileURL: String = ""
    @objc dynamic var duration: Double = 0
    
    var owner: User?
    var seenUsers: List<User> = List<User>()
    var mention_users: List<User> = List<User>()
    var mentionIds: List<Int> = List<Int>()
    @objc dynamic var isMentionAll: Bool = false

    @objc dynamic var createTime: Date?
    @objc dynamic var updateTime: Date?
    
    static let shareObject = Message()
    var fakeId: Int = 0
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    convenience required init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        self.id             <- map["id"]
        self.channelId      <- map["channel_id"]
        self.owner          <- map["user"]
        self.ownerId        <- map["user_id"]
        
        self.content        <- map["content"]
        self.messageType    <- map["type"]
        if messageType == MessageType.Image.rawValue {
            self.fileURL    <- map["file_url.thumbnail"]
        } else {
            self.fileURL    <- map["file_url"]
        }
        self.duration       <- map["duration"]
        self.seenUsers      <- map["user_readers"]
        self.mention_users  <- map["mention_users"]
        self.mentionIds     <- (map["mentions"], RealmTypeCastTransform<Int>())
        
        for id in self.mentionIds {
            if id == -1 {
                self.isMentionAll = true
                break
            }
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone.current
        
        self.createTime <- (map["created_at"], DateFormatterTransform(dateFormatter: dateFormatter))
        self.updateTime <- (map["updated_at"], DateFormatterTransform(dateFormatter: dateFormatter))
    }
    
    class func fakeIdForSendingMessage() -> Int {
        shareObject.fakeId -= 1
        print("Fake ID: ", shareObject.fakeId)
        return shareObject.fakeId
    }
}
