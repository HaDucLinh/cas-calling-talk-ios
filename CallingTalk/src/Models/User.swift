//
//  User.swift
//  CallingTalk
//
//  Created by Vu Tran on 2/12/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import RealmSwift
import ObjectMapper
import RealmS

class User: Object, Mappable {
    
    @objc dynamic var id: Int               = 0
    @objc dynamic var name: String          = ""
    @objc dynamic var email: String          = ""

    @objc dynamic var avatar: String        = ""
    @objc dynamic var avatarIndex: Int = 1
    @objc dynamic var provider: String      = ""
    @objc dynamic var avatarUrl: String     = ""
    @objc dynamic var talkAppToken: String  = ""
    @objc dynamic var isCalling: Bool       = false
    @objc dynamic var isOwner: Bool         = false
    @objc dynamic var isAdmin: Bool         = false
    @objc dynamic var enableNotification: Bool = true
    @objc dynamic var createAt: Date?
        
    var organizations: List<Organization> = List<Organization>()
    
    @objc dynamic var talkRole: String      = ""
    
    override static func primaryKey() -> String? {
        return "talkAppToken"
    }
    
    convenience required init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        self.id             <- map["id"]
        self.name           <- map["name"]
        self.email          <- map["email"]
        self.avatar         <- map["avatar"]
        self.avatarIndex    <- map["avatar_index"]
        
        self.provider       <- map["provider"]
        self.avatarUrl      <- map["avatar_url"]
        self.talkAppToken   <- map["talk_app_token"]
        self.isCalling      <- map["is_calling"]
        self.isOwner        <- map["is_owner"]
        self.isAdmin        <- map["is_admin"]
        self.enableNotification <- map["enable_notification"]
        self.organizations  <- map["organizations"]
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = kDateGetListChannel
        dateFormatter.timeZone = TimeZone.current
        self.createAt <- (map["created_at"], DateFormatterTransform(dateFormatter: dateFormatter))
        
        if self.organizations.isEmpty {
            var organization: Organization?
            organization    <- map["organizations"]
            if let profileOrganization = organization {
                self.organizations.append(profileOrganization)
            }
        }
    }
}

// MARK: - GET USER'S PROFILE

extension User {
    
    class func getProfile() -> User? {
        guard let uuid = UserDefaults.getToken() else { return nil }
        return RealmS().objects(User.self).filter("talkAppToken == %@", uuid).first
    }
    
    class func removeCurrentUser() {
        let realm = RealmS()
        if let currentUser = User.getProfile() {
            realm.write {
                realm.delete(currentUser)
            }
        }
    }
    
    func update(userName: String) {
        let realm = RealmS()
        realm.write {
            self.name = userName
            realm.add(self)
        }
    }
    
    func addOrganization(organization: Organization) {
        let realm = RealmS()
        realm.write {
            if let org = realm.objects(Organization.self).filter({ $0.id == organization.id }).first {
                self.organizations.append(org)
            } else {
                self.organizations.append(organization)
            }
        }
    }
    
    class func allUser() -> User {
        let user = User()
        user.id = -1
        user.name = "all"
        return user
    }
    
    class func clearAll() {
        let realm = RealmS()
        let users = realm.objects(User.self)
        realm.write {
            realm.delete(users)
        }
    }

}
