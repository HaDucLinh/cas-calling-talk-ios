//
//  Voice.swift
//  CallingTalk
//
//  Created by Vu Tran on 3/5/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import RealmSwift
import ObjectMapper
import RealmS

enum VoiceType: Int {
    case Default = 1, Normal = 2
}
class Voice: Object, Mappable {
    @objc dynamic var id: Int = 0
    @objc dynamic var content: String = ""
    @objc dynamic var voicePathUrl = ""
    
    var organizations: List<Organization> = List<Organization>()
    var organizationNames = List<String>()
    
    @objc dynamic var type: Int = 1
    
    convenience required init?(map: Map) {
        self.init()
        id <- map["id"]
    }
    
    struct APIPath {
        static let list = ""
    }
    
    func mapping(map: Map) {
        id                  <- map["id"]
        content             <- map["content"]
        voicePathUrl        <- map["voice_path_url"]
        organizations       <- map["organizations"]
        
        var is_default: Bool = false
        is_default   <- map["is_default"]
        type = is_default == true ? 1 : 2
    }
}
