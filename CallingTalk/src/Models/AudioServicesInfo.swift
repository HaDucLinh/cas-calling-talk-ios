//
//  AudioServicesInfo.swift
//  CallingTalk
//
//  Created by NeoLabx on 5/8/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import RealmSwift
import ObjectMapper
import RealmS

class AudioServicesInfo: Object, Mappable {
    @objc dynamic var user_id: Int = 0
    @objc dynamic var jitsi_id: String = ""
    
    convenience required init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        self.user_id <- map["user_id"]
        self.jitsi_id <- map["jitsi_id"]
    }
}
