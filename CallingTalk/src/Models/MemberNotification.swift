//
//  ReportHistory.swift
//  CallingTalk
//
//  Created by Toof on 3/1/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import RealmSwift
import ObjectMapper
import RealmS

class MemberNotification: Object, Mappable {
    
    @objc dynamic var id: Int   = 0
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    convenience required init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        self.id <- map["id"]
    }
    
}
