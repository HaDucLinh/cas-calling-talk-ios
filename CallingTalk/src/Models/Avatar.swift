//
//  Avatar.swift
//  CallingTalk
//
//  Created by Vu Tran on 2/14/19.
//  Copyright © 2019 NeoLabVN. All rights reserved.
//

import Foundation
import ObjectMapper

class Avatar: Mappable {
    @objc dynamic var id: Int = 0
    @objc dynamic var name: String = ""
    @objc dynamic var urlString: String = ""
    
    convenience required init?(map: Map) {
        self.init()
        id <- map["id"]
    }
    
    struct APIPath {
        static let list = ""
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        urlString <- map["url"]
    }
}
