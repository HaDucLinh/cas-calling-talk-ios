/* 
  Localizable.strings
  CallingTalk

  Created by Van Trung on 1/15/19.
  Copyright © 2019 NeoLabVN. All rights reserved.
*/

"change_color_button_title" = "Change Color";

// GuestJoinVC
"talk_label_text" = "Welcome to Talk!";
"notice_start_label_text" = "Please set avatar and username \n Let's start!";
"notice_start_label_text_osemino" = "Please set avatar and username \n Let's start!";

"username_label_text" = "Username";
"username_placeholder_text" = "Please enter a username";
"member_label_text" = "Member ID";
"memberid_placeholder_text" = "Please enter member ID";
"login_button_title" = "Start Talking";
"meeting_button_title" = "Start talking";
"calling_info_label_text" = "You can add members in the space to your friends by logging in with your Calling account";
"administrator_label_text" = "If you are an administrator";
"selectIcon_label_text" = "Please select an icon";
"select_avatar_from_Album" = "Select from album";
"take_photo" = "Take a photo";
"cancel_text" = "Cancel";
"photo_library_permission" = "You do not have permission to access Photo, please change privacy settings";
"camera_permission" = "You do not have permission to access Camera, please change privacy settings";

// SpaceLoginVC
"calling_title_label_text" = "Login to Calling";
"calling_notice_label_text" = "When you sign in with your Calling account,\n Automatically add members to friends! ";
"spaceid_label_text" = "Space ID";
"spaceid_placeholder_text" = "Please enter space ID";
"login_workspace_button_title" = "Login to workspace";
"back_button_title" = "Back";
"image_size_larger_than_5MB" = "Images larger than 5MB in size can not be used";

//UserEmailLoginVC
"email_label_text" = "Email";
"email_placeholder_text" = "Please enter your email address";
"password_label_text" = "Password";
"password_placeholder_text" = "Please enter your password";
"cancel_button_title" = "Cancel";
"ok_button_title" = "Ok";

// AdminLoginVC
"admin_login_label_text" = "Administrator Login";
"phone_number_notice_label_text" = "Please Enter Phone Number";
"phone_number_label_text" = "Phone Number";
"error_validation_label_text" = "Phone number is invalid.";
"phone_number_placeholder_text" = "Please enter phone number";
"sms_button_title" = "Send SMS Authentication Code";
"staff_label_text" = "Staff Here";

"confirm_sms_label_text" = "SMS Authentication";
"confirm_sms_notice_label_text" = "Please enter\nthe authentication code we received via SMS.";
"resend_sms_label_text" = "When SMS does not arrive";
"resending_button_title" = "Resend SMS";
"calling_button_title" = "Authenticate by Call";

"error_avatar_nil_alert_title" = "Image file is invalid";
"error_avatar_validation_alert_title" = "Images with a size larger than 5 MB can not be used";

// MainTabBarController
"main_item_tab_bar_title" = "Talk";
"member_item_tab_bar_title" = "Member";
"settings_item_tab_bar_title" = "Settings";

// MainVC
"main_navigation_item_title" = "Talk";
"empty_talk_label_text" = "Let's start talking!";
"search_talk_placeholder_text" = "Search by group name, participating member name";
"qr_tip_text" = "Join the talk\nwith QR code";
"add_tip_text" = "Create a new talk";

"join_group_confirm_alert_message" = "Would you like to join this talk?";
"join_group_confirm_button_title" = "Participate";
"join_group_cancel_button_title" = "Decline participation";
"invite_join_talkGroup_text" = "Invitation from %@";

// QRScanVC
"qr_scan_title" = "Please read the QR code";
"qr_scan_cancel_button_title" = "Back";
"qr_scan_link_button_title" = "Read from library";
"qr_scan_fail_notice_text" = "QR code that can not be used with this application";

// MemberVC
"member_navigation_item_title" = "Member";
"empty_member_label_text" = "I have no members yet";
"read_notice_label_text" = "Report";

// MemberDetailVC
"delete_member_button_title" = "Delete from organization";
"report_title_label_text" = "Report history";
"empty_report_label_text" = "There is no report history";
"caller_label_text" = "Caller";

// SettingsVC
"settings_navigation_item_title" = "Settings";
"member_list_text" = "Member list";
"notification_text" = "Notification settings";
"help_text" = "Help";
"Logout_text" = "Log out";
"List_voice_statements" = "List of voice statements";
"Reset_button_text" = "Reset";
"Reset_label_text" = "Reset";
"Reset_button_admin_text" = "Log out";
"Logout_label_text" = "Do you really want to reset it?";
"Logout_label_message" = "※ When resetting, all will be initialized";
"Logout_label_admin_text" = "Do you want to log out?";
"version_text" = "version ";
"push_notification_text" = "Push Notification";

// Start New Talk
"start_NewTalk_title_text" = "Start talking";
"enter_name_of_the_talk_text" = "Please enter the name of the talk";
"start_talk_button_title" = "Start";
"number_of_member_text" = "Member ";
"invite_member_text" = "Invite members";
"select_member_text" = "Select member";
"talk_detail_title" = "Talk detail";
"edit_talk_detail_text" = "Edit";
"can't_create_talk_this_time" = "At first let's participate in talk from QR code";
"author_text" = "Author";
"admin_role_text" = "Admin";
"select_the_organization_label_text" = "Select the organization";
"select_the_organization_button_title" = "Choose an organization";

// Talk Detail
"share_Talk_title" = "Calling member's QR code";
"share_Talk_button_text" = "Share";
"share_QRCode_button_text" = "Share QR code";
"share_URL_button_text" = "Share invitation URL";
"close_button_text" = "Close";

"leave_talk_action_text" = "Leave this talk";
"leave_talk_button_text" = "Exit";
"leave_alert_title" = "Do you want to leave?";
"leave_button_alert_text" = "Exit";
"leave_delete_history_chat" = "All chat history disappears";

"delete_alert_title" = "Do you want to delete?";
"delete_button_alert_text" = "Delete";
"delete_talk_action_text" = "Delete this talk";
"delete_member_confirm_text" = "Are you sure you want to delete this user?";
"delete_talk_message_text" = "When deleting other members also can not join the talk";
"deleteted_message" = "Deleted";

"block_talk_button_title" = "Block";
"blocked_cant_invite_you" = "Blocked\nThe other party can not invite you to talk";
"remove_member_button_title" = "Remove from this talk";
"banish_member_button_title" = "Banish from organization";
"band_infor_text" = "It is deleted from %@\nAll information can not be viewed.";
"band_member_message" = "Do you really want to exile this user?";
"band_button_title" = "Exile";
"banded_message" = "Banished";

"report_talk_button_title" = "Report";
"report_member_title" = "Would you like to report this user?";
"report_member_infor_sent_to_admin" = "Report information is sent to the administrator";
"reported_message" = "Reported";
"blocked_message" = "I blocked it";
"un_block_message" = "Unblocked";

"is_not_user_of_Workspace" = "It is not a user of %@";
"is_nuisance_act" = "It is a nuisance act";
"is_other_reason" = "Other";

"member_joined_talk" = "%@ joined the talk";
"member_left_talk" = "%@ left the talk";

// Invite Member
"invite_search_placeholder_text" = "Search by member name";
"invited_member_button_text" = "Invite";
"invited_member_text" = "Invited Members";
"member_to_invite_text" = "Member to invite";
"member_for_organizations_label_text" = "株式会社ネオラボ ";

// Edit Talk
"talk_updated_text" = "Talk updated";
"edit_talk_title" = "Edit talk";
"done_button_text" = "Done";


// ImcomingVC
"notice_hold_to_speak_label_text" = "Please press and hold to speak";
"notice_talk_to_micro_label_text" = "Talk to the microphone";
"talk_button_title" = "Press and Hold\nto Speak";
"chat_button_title" = "Chat";
"push_mode_label_text" = "Push";
"hand_free_mode_label_text" = "Hand Free";
"add_member_label_text" = "Invite";
"record_on_button_title" = "Microphone ON";
"record_off_button_title" = "Microphone OFF";
"trailing_leaving_talk_group_notice_text" = " has left";
"trailing_joining_talk_group_notice_text" = " has joined";
"trailing_talk_group_share_qr_code_text" = "'s QR code";
"adding_user_talk_group_label_text" = "Invite members";
"invite_member_button_title" = "Invite";
"push_mode_speech_text" = "push mode";
"hand_free_mode_speech_text" = "hands free mode";
"mike_on_text" = "microphone on";
"mike_off_text" = "microphone off";
"kicked_from_channel_message" = "You left the talk because of administrator's operation.";
"channel_deleted_message" = "%@ has been deleted by the administrator";

// Edit Photo
"confirm_crop_button_text" = "Determine";
"cancel_crop_button_text" = "Cancel";

// Edit Profile
"name_label_text" = "Name";
"update_button_text" = "Update your profile";
"updated_profile_text" = "Profile updated";

// Member List
"Member_list_title" = "Member List";

// Statement List
"Statement_list_title" = "List of voice statements";
"Register_Speech_Sentence_title" = "Register speech sentence";
"Voice_text" = "Voice text";
"Organization_be_registered_text" = "Organization (s) to be registered";
"Voice_text_placeHolder" = "Please input voice text";
"Please_select_organize" = "Please select an organization";
"Register_button_text" = "Sign up";
"Registered_voice_text" = "We have registered voice text";

// Edit voice text
"Edit_voice_text_title" = "Edit voice text";
"Change_voice_statement" = "Change";
"Updated_voice_text" = "Voice text updated";
"Deleted_voice_text" = "Voice text deleted";
"Confirm_delete_voice_text" = "Are you sure you want to delete this voice statement?";

// Chat
"send_audio_message_button_title" = "Send";
"chat_text_message_placeholder_text" = "Please enter";
"chat_audio_message_placeholder_text" = "Please select a voice sentence";
"show_log_audio_button_title" = "Display";
"hide_log_audio_button_title" = "Hide";
"people_text" = "";
"Read_member_text" = "Read member";
"Connection_issue_alert_text" = "The connection is unstable.\nReconnecting ...";
"system_text" = "system";
"Return_to_talk_text" = "Tap to return to talk";
"Play" = "PLAY";
"Pause" = "PAUSE";

// Network Status Description
"success_status_description_text" = "The request is OK.";
"created_status_description_text" = "The request has been fulfilled, and a new resource is created.";
"accepted_status_description_text" = "The request has been accepted for processing, but the processing has not been completed.";
"badRequest_status_description_text" = "The request cannot be fulfilled due to bad syntax.";
"unauthorized_status_description_text" = "The request was a legal request, but the server is refusing to respond to it. For use when authentication is possible but has failed or not yet been provided.";
"forbidden_status_description_text" = "The request was a legal request, but the server is refusing to respond to it.";
"notFound_status_description_text" = "The requested page could not be found but may be available again in the future.";
"methodNotAllowed_status_description_text" = "A request was made of a page using a request method not supported by that page.";
"requestTimeOut_status_description_text" = "The server timed out waiting for the request.";
"unprocessable_status_description_text" = "Request unable to be followed due to semantic errors.";
"noResponse_status_description_text" = "Server returns no information and closes the connection.";
"internalServerError_status_description_text" = "A generic error message, given when no more specific message is suitable.";
"notImplemented_status_description_text" = "The server either does not recognize the request method, or it lacks the ability to fulfill the request.";
"unknown_status_description_text" = "Something is wrong.";
"internet_offline_status_description_text" = "The internet connection appears to be offline.";
"parse_json_fail_status_description_text" = "The operation couldn’t be completed.";

"jitsi_disconnect_without_reason_message_text" = "You have left Talk room, because it was disconnected.";

// Push Message
"incoming_alert_title" = "Incoming";
"invitation_alert_title" = "Invitation";
"invitation_alert_message" = "You're invited to %@";
"new_message_mention_alert_message" = "%@：There is a message for you";
"image_alert_message" = "Image";

// Delete Organization
"delete_organization_message" = "The app can not be used because the organization has been deleted";
